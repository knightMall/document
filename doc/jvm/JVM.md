## JVM命令行命令

### jps

重要有如下两个

```shell
jps
jps -mlv
```

区别如下图：

![jps命令.png](../../images/21/1230/jps命令.png)

### jstat

jstat命令的相关选项

```
jstat -options
-class 类加载(Class loader)信息统计.
-compiler JIT 即时编译器相关的统计信息。
-gc GC 相关的堆内存信息. 用法: jstat -gc -h 10 -t 864 1s 20
-gccapacity 各个内存池分代空间的容量。
-gccause 看上次 GC, 本次 GC（如果正在 GC中）的原因, 其他
输出和 -gcutil 选项一致。
-gcnew 年轻代的统计信息. （New = Young = Eden + S0 + S1）
-gcnewcapacity 年轻代空间大小统计.
-gcold 老年代和元数据区的行为统计。
-gcoldcapacity old 空间大小统计.
-gcmetacapacity meta 区大小统计.
-gcutil GC 相关区域的使用率（utilization）统计。
-printcompilation 打印 JVM 编译统计信息。
```



#### 演示

```sh
jstat -gcutil pid 1000 1000

   S0     S1     E      O      M     CCS    YGC     YGCT    FGC    FGCT     GCT
  0.00   0.00  49.49  42.06  96.41  91.46     10    0.151     2    0.162    0.313
  0.00   0.00  49.49  42.06  96.41  91.46     10    0.151     2    0.162    0.313
  0.00   0.00  49.49  42.06  96.41  91.46     10    0.151     2    0.162    0.313
```

![image-20211231145124958](../../images/21/1231/jstat-gcutil.png)



```sh
jstat -gc pid 1000 1000

 S0C    S1C    S0U    S1U      EC       EU        OC         OU       MC     MU    CCSC   CCSU   YGC     YGCT    FGC    FGCT     GCT
36864.0 38912.0  0.0    0.0   194560.0 96291.1   133632.0   56205.2   29824.0 28754.5 3712.0 3395.1     10    0.151   2      0.162    0.313
36864.0 38912.0  0.0    0.0   194560.0 96291.1   133632.0   56205.2   29824.0 28754.5 3712.0 3395.1     10    0.151   2      0.162    0.313
36864.0 38912.0  0.0    0.0   194560.0 96291.1   133632.0   56205.2   29824.0 28754.5 3712.0 3395.1     10    0.151   2      0.162    0.313
36864.0 38912.0  0.0    0.0   194560.0 96291.1   133632.0   56205.2   29824.0 28754.5 3712.0 3395.1     10    0.151   2      0.162    0.313
```

![image-20211231145357789](../../images/21/1231/jstat-gc.png)





### jmap

```
常用选项就 3 个：
-heap 打印堆内存（/内存池）的配置和使用信息。
-histo 看哪些类占用的空间最多, 直方图. jmap -histo:live pid 获取活着的对象，会触发GC
-dump:format=b,file=xxxx.hprof
Dump 堆内存
```

#### 演示


```sh
jmap -heap pid
```

结果：

![image-20211231150331919](../../images/21/1231/jmap-heap.png)

```
Parallel GC with 8 thread(s) 使用垃圾收集器的名称
Heap Configuration:  堆的配置
MinHeapFreeRatio         = 0  最小堆的空闲比例
MaxHeapFreeRatio         = 100 最大堆的空闲比例
MaxHeapSize              = 2107637760 (2010.0MB) 最大的堆大小，这里使用的是不指定的大小，默认机器的1/4
NewSize                  = 44040192 (42.0MB)  新生代大小
MaxNewSize               = 702545920 (670.0MB) 最大新生代大小，默认是最大堆的1/3
OldSize                  = 88080384 (84.0MB) 老年代大小
NewRatio                 = 2  新生代与老年代的比例，表示 新生代1，老年代2
SurvivorRatio            = 8  Eden去和survivor区的比例，默认8：1：1
MetaspaceSize            = 21807104 (20.796875MB) 元素数据空间大小
CompressedClassSpaceSize = 1073741824 (1024.0MB) 压缩的类的空间大小
MaxMetaspaceSize         = 17592186044415 MB   最大的元素区大小，这个是理论上的值，实际的机器内存也就8g
G1HeapRegionSize         = 0 (0.0MB)  G1垃圾收集器Region的大小，这里不是G1收集器


Heap Usage:                                  堆的使用情况
PS Young Generation                          年轻代
Eden Space:                                  Eden区
   capacity = 199229440 (190.0MB)            总容量 
   used     = 98602104 (94.03429412841797MB) 使用了
   free     = 100627336 (95.96570587158203MB) 空闲的
   49.49173375179893% used                    百分比
From Space:                                   from区
   capacity = 37748736 (36.0MB)               from区容量
   used     = 0 (0.0MB)                       使用了
   free     = 37748736 (36.0MB)               空闲
   0.0% used                                  百分比
To Space:                                     to区空间
   capacity = 39845888 (38.0MB)               to区容量
   used     = 0 (0.0MB)                       使用了
   free     = 39845888 (38.0MB)               空闲
   0.0% used                                  百分比
PS Old Generation                             老年代
   capacity = 136839168 (130.5MB)             总容量
   used     = 57554088 (54.887855529785156MB) 使用了
   free     = 79285080 (75.61214447021484MB)  空闲
   42.05965940979706% used                    百分比
```



```sh
 jmap -histo 25336
```

![image-20211231152001415](../../images/21/1231/jmap-histo.png)



```sh
jmap -dump:format=b,file=3826.hprof 3826  #dump进程3826
```





### jstack

-F 强制执行 thread dump. 可在 Java 进程卡死
（hung 住）时使用, 此选项可能需要系统权限。
-m 混合模式(mixed mode),将 Java 帧和 native
帧一起输出, 此选项可能需要系统权限。
-l 长列表模式. 将线程相关的 locks 信息一起输出，
比如持有的锁，等待的锁。

```sh
jstack -l 13604
```

结果

![image-20211231155225688](../../images/21/1231/jstack-l.png)



## JVM图形化工具

### jconsole

![image-20211231155945882](../../images/21/1231/jconsole.png)

共有 6 个面板
第一个为概览，四项指标具体为：
堆内存使用量：此处展示的就是前面 Java 内存模型课程中提到的堆
内存使用情况，从图上可以看到，堆内存使用了 94MB 左右，并且一
直在增长。
线程：展示了 JVM 中活动线程的数量，当前时刻共有 17 个活动线程。
类：JVM 一共加载了 5563 个类，没有卸载类。
CPU 占用率：目前 CPU 使用率为 0.2%，这个数值非常低，且最高的
时候也不到 3%，初步判断系统当前并没有什么负载和压力。
有如下几个时间维度可供选择：
1分钟、5分钟、10分钟、30分钟、1小时、2小时、3小时、6小时、
12小时、1天、7天、1个月、3个月、6个月、1年、全部，一共是16
档。
当我们想关注最近1小时或者1分钟的数据，就可以选择对应的档。旁
边的3个标签页(内存、线程、类)，也都支持选择时间范围。



### jvisualvm

![image-20211231160101403](../../images/21/1231/jvisualvm.png)

VM 概要的数据有五个部分：
第一部分是虚拟机的信息；
第二部分是线程数量，以及类加载的
汇总信息；
第三部分是堆内存和 GC 统计。
第四部分是操作系统和宿主机的设备
信息，比如 CPU 数量、物理内存、虚
拟内存等等。
第五部分是 JVM 启动参数和几个关键
路径，这些信息其实跟 jinfo 命令看到
的差不多。



### jmc

![image-20211231160556258](../../images/21/1231/jmc.png)



## JVM垃圾收集算法



### 标记-清除法



### 复制算法









## GC日志分析







## JVM对象的内存占用

![image-20220107221847320](D:\gitProject\document\images\22\01\07\对象结构图.png)

在64位 JVM 中，对象头占据的空间是 12-
byte(=96bit=64+32)，但是以8字节对齐，所以一
个空类的实例至少占用16字节。如果是数组的话还有一个长度是4个字节。



### 包装类型

Integer对象占用16字节，int占用4个字节

Long对象一般占用16个字节，原生类型long占用8个字节。



### 例子

计算下列对象的大小

```java
public class MyOrder{
  private long orderId;
  private long userId;
  private byte state;
  private long createMillis;
}
```

对象头12+long是8*3+byte是1=37，对齐必须是8的倍数所以是：40。























