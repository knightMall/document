

- [Spring Cloud组件](#spring-cloud组件)
- [Spring Cloud和Dubbo的对比](#spring-cloud和dubbo的对比)



## Spring Cloud组件

| 组件名称 |                           组件作用                           |
| :------: | :----------------------------------------------------------: |
|  Eureka  | 各个服务启动时，Eureka Client都会将服务注册到Eureka Server，并且Eureka Client还可以反过来从Eureka Server拉取注册表，从而知道其他服务在哪里 |
|  Ribbon  | 服务间发起请求的时候，基于Ribbon做负载均衡，从一个服务的多台机器中选择一台 |
|  Feign   | 基于Feign的动态代理机制，根据注解和选择的机器，拼接请求URL地址，发起请求 |
| Hystrix  | 发起请求是通过Hystrix的线程池来走的，不同的服务走不同的线程池，实现了不同服务调用的隔离，避免了服务雪崩的问题 |
|   Zuul   | 如果前端、移动端要调用后端系统，统一从Zuul网关进入，由Zuul网关转发请求给对应的服务 |

​	

## Spring Cloud和Dubbo的对比

 

| content      | Dubbo     | Sprinng Cloud                |
| ------------ | --------- | ---------------------------- |
| 服务注册中心 | zookeeper | Spring Cloud Netflix Eureka  |
| 服务调用方式 | RPC       | REST API                     |
| 服务网关     | 无        | Spring Cloud Netflix Zuul    |
| 断路器       | 不完善    | Spring Cloud Netflix Hystrix |
| 分布式配置   | 无        | Spring Cloud Config          |
| 服务跟踪     | 无        | Spring Cloud Sleuth          |
| 消息总线     | 无        | Spring Cloud Bus             |
| 数据流       | 无        | Spring Cloud Stream          |
| 批量任务     | 无        | Spring Cloud Task            |