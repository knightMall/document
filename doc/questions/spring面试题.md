- [SpringMVC请求原理和流程](#springmvc请求原理和流程)
  - [SpringMVC工作流程描述](#springmvc工作流程描述)
- [springboot自动配置的原理](#springboot自动配置的原理)
- [Spring的事务传播机制](#spring的事务传播机制)
- [Spring-bean的生命周期](#spring-bean的生命周期)
- [Spring中使用了那些设计模式](#spring中使用了那些设计模式)




## SpringMVC请求原理和流程

流程图：

![springMVC流程图](../../pic/question/springMVC流程图.png)

### SpringMVC工作流程描述

1、用户向服务器发送请求，请求被Spring 前端控制Servelt DispatcherServlet捕获；

2、DispatcherServlet对请求URL进行解析，得到请求资源标识符（URI）。然后根据该URI，调用HandlerMapping获得该Handler配置的所有相关的对象（包括Handler对象以及Handler对象对应的拦截器），最后以HandlerExecutionChain对象的形式返回；

3、DispatcherServlet 根据获得的Handler，选择一个合适的HandlerAdapter。（附注：如果成功获得HandlerAdapter后，此时将开始执行拦截器的preHandler(…)方法）。

4、 提取Request中的模型数据，填充Handler入参，开始执行Handler（Controller)。 在填充Handler的入参过程中，根据你的配置，Spring将帮你做一些额外的工作：

HttpMessageConveter： 将请求消息（如Json、xml等数据）转换成一个对象，将对象转换为指定的响应信息；

数据转换：对请求消息进行数据转换。如String转换成Integer、Double等;

数据根式化：对请求消息进行数据格式化。 如将字符串转换成格式化数字或格式化日期等;

数据验证： 验证数据的有效性（长度、格式等），验证结果存储到BindingResult或Error中。

5、Handler执行完成后，向DispatcherServlet 返回一个ModelAndView对象然后调用拦截器的postHandle方法。

6、 根据返回的ModelAndView，选择一个适合的ViewResolver（必须是已经注册到Spring容器中的ViewResolver)返回给DispatcherServlet ；

7、ViewResolver 结合Model和View，来渲染视图。

8、将渲染结果返回给客户端。

9、执行拦截器的afterCompletion方法。






## springboot自动配置的原理

在spring程序main方法中 添加@SpringBootApplication或者@EnableAutoConfiguration会自动去maven中读取每个starter中的spring.factories文件  该文件里配置了所有需要被创建spring容器中的bean




## Spring Boot 有哪几种读取配置的方式
Spring Boot 可以通过 @PropertySource,@Value,@Environment, @ConfigurationProperties 来绑定变量。





## Spring的事务传播机制

- required：如果当前方法没有事务，则开启事务，如果已经开启，则加入该事务

- supports：如果当前方法存在事务，则按事务执行，如果没有事务，则按非事务执行

- mandatory：如果当前存在事务，则加入该事务，如果当前不存在事务，则抛出异常

- requires_new：无论当前是否有事务，都会新建一个新的事务

- NOT_SUPPORT：以非事务方式执行，如果当前存在事务，则挂起当前事务

- never：不支持事务执行，如果有事务则抛出异常

- nested：多个事务嵌套，外层事务如果回滚，会导致内层的事务也回滚；但是内层事务回滚，仅仅是回滚自己的代码。





## Spring-bean的生命周期

- 实例化Bean，对于BeanFactory容器，当请求一个尚未初始化的bean时或初始化bean的时候需要注入另一个尚未初始化的依赖时，容器就会调用createBean进行实例化。对于ApplicationContext容器，当容器启动完后，通过获取BeanDefinition对象中的信息，实例化所有的Bean。
- 设置对象属性（依赖注入），实例化后的对象被封装到BeanWrapper对象中，接下来Spring通过BeanDeinition中的信息以及通过BeanWrapper提供的设置属性的接口完成依赖注入。
- 处理Aware接口
  - 如果这个Bean实现了BeanNameAware接口，会调用它实现的setBeanName(String beanId)方法，此处传递的就是Spring配置文件中的Bean的id
  - 如果这个Bean实现了BeanFactoryAware接口，会调用它实现setBeanFactory()方法，传递的就是Spring工厂本身
  - 如果这个Bean实现了ApplicationContextAware接口，会调用setApplicationContext(ApplicationContext)方法，传入Spring上下文。
- BeanPostProcesser，如果我们想在bean实例构建好之后，想要对Bean进行一些自定义的处理，那么可以让Bean实现BeanPostProcesser接口，将会调用postProcessBeforeInitialization方法
- init-method和InitializingBean，如果Bean在Spring配置文件中配置了init-method属性，则会自动调用其配置的初始化方法。
- 初始化之后，如果该Bean实现了BeanPostProcesser接口，这个时候会执行postProcessAfterInitialization方法。
- DisposableBean，会进入清理阶段，如果Bean实现了DisposableBean接口，则会执行destroy()方法。
- destroy-method，如在配置该bean的时候配置了destroy-method属性，则会自动调用其配置的销毁方法。



## Spring中使用了那些设计模式

- 工厂模式

  典型的就是BeanFactory的使用，将Bean的创建交给BeanFactory创建。

  ```java
  public class BeanFactory{
      public static Bean getBean(){
          return new Bean();
      }
  }
  ```

  

- 单例模式

  bean默认是单例，确保在容器中只能存在一个实例。

  ```java
  public class MyService{
      private static volatitle MyService myService;
      
      public static MyService getInstance(){
          if(myService==null){
              synchronized(MyService.class){
                  if(myService==null){
                      myService = new MyService();
                  }
              }
          }
      }
  }
  ```

  

- 代理模式

  在spring对某些类的方法会切入一些增强代码，会创建一些代理对象，让访问这些方法时，先访问动态代理对象的增强方法，然后再执行目标的方法。例如事务

  

  

##  什么是 Spring Framework

- 它是轻量级、松散耦合的。
- Spring 是一个开源应用框架，旨在降低应用程序开发的复杂度。
- 它可以集成其他框架，如 Structs、Hibernate、EJB 等，所以又称为框架的框架。
- 它具有分层体系结构，允许用户选择组件，同时还为 J2EE 应用程序开发提供了一个有凝聚力的框架。



## 列举 Spring Framework 的优点

- 首先是开源免费的。
- 由于依赖注入和控制反转，JDBC 得以简化。
- 由于 Spring Frameworks 的分层架构，用户可以自由选择自己需要的组件。
- Spring Framework 支持 POJO(Plain Old Java Object) 编程，从而具备持续集成和可测试性。



## Spring Framework 有哪些不同的功能？

- IOC - 控制反转
- 轻量级 - Spring 在代码量和透明度方面都很轻便。
- 容器 - Spring 负责创建和管理对象（Bean）的生命周期和配置。
- MVC - 对 web 应用提供了高度可配置性，其他框架的集成也十分方便。
- AOP - 面向切面编程可以将应用业务逻辑和系统服务分离，以实现高内聚。
- JDBC 异常 - Spring 的 JDBC 抽象层提供了一个异常层次结构，简化了错误处理策略。
- 事务管理 - 提供了用于事务管理的通用抽象层。Spring 的事务支持也可用于容器较少的环境。



## Spring Framework 中有多少个模块，它们分别是什么？

Spring 核心容器，该层基本上是Spring的核心。它包含以下模块：

- Spring Core
- Spring Bean
- Spring Context
- SpEL(Spring Expression Language)

数据访问/集成 – 该层提供与数据库交互的支持。它包含以下模块：

- Transaction
- OXM(Object XML Mappers)
- JMS(Java Message Service)
- ORM(Object Relation Mapping)
- JDBC(Java DataBase Connectivity)

Web – 该层提供了创建 Web 应用程序的支持。它包含以下模块：

- Web Servlet
- Web Socket
- Portlet

其他的一些杂项

- AOP
- Aspect
- Messaging 
- Test



## 什么是 Spring IOC 容器

Spring 框架的核心是 Spring 容器。容器创建对象，将它们装配在一起，配置它们并管理它们的完整生命周期。Spring 容器使用依赖注入来管理组成应用程序的组件。容器通过读取提供的配置元数据来接收对象进行实例化，配置和组装的指令。该元数据可以通过 XML，Java 注解或 Java 代码提供。



## Spring 中有多少种 IOC 容器

BeanFactory - BeanFactory 就像一个包含 bean 集合的工厂类。它会在客户端要求时实例化 bean。

ApplicationContext - ApplicationContext 接口扩展了 BeanFactory 接口。它在 BeanFactory 基础上提供了一些额外的功能。

- 提供了支持国际化的文本消息

- 统一的资源文件读取方式
- 已在监听器中注册的 bean 的事件

以下是三种较常见的 ApplicationContext 实现方式:

1、ClassPathXmlApplicationContext：从 classpath 的 XML 配置文件中读取上下文，并生成上下文
定义。应用程序上下文从程序环境变量中取得。
ApplicationContext context = new ClassPathXmlApplicationContext(“application.xml”);

2、FileSystemXmlApplicationContext ：由文件系统中的 XML 配置文件读取上下文。
ApplicationContext context = new FileSystemXmlApplicationContext(“application.xml”);
3、XmlWebApplicationContext：由 Web 应用的 XML 文件读取上下文。



## Spring 提供了哪些配置方式

- 基于 xml 配置

  bean 所需的依赖项和服务在 XML 格式的配置文件中指定。这些配置文件通常包含许多 bean 定义和特定于应用程序的配置选项。它们通常以 bean 标签开头。例如：

  ```xml
  <bean id="studentbean" class="org.edureka.firstSpring.StudentBean">
          <property name="name" value="Edureka"></property>
  </bean>
  ```

  

- 基于注解配置

  您可以通过在相关的类，方法或字段声明上使用注解，将 bean 配置为组件类本身，而不是使用 XML 来描述 bean 装配。默认情况下，Spring 容器中未打开注解装配。因此，您需要在使用它之前在 Spring 配置文件中启用它。例如：

  ```xml
  <beans>
        <context:annotation-config/>
        <!-- bean definitions go here -->
   </beans>
  ```

- 基于 Java API 配置

  - Spring 的 Java 配置是通过使用@Bean 和 @Configuration 来实现。
  - @Bean 注解扮演与 < bean /> 元素相同的角色。
  - @Configuration 类允许通过简单地调用同一个类中的其他@Bean 方法来定义 bean 间依赖关系

  ```java
  public class StudentConfig {   
      @Bean//加入Java开发交流君样：756584822一起吹水聊天
      public StudentBean myStudent() {
          return new StudentBean();
      }
  }
  ```




## 列举IOC的一些好处

- 它将最小化应用程序中的代码量
- 它支持即时的实例化和延迟加载服务
- 它以最小的影响和最少的侵入机制促进松耦合
- 它将使您的应用程序易于测试，因为它不需要单元测试用例中的任何单例或 JNDI 查找机制



##  Spring IOC 的实现机制

Spring中的IOC 的实现原理就是工厂模式加反射机制。





## Spring 支持集中 bean scope

  Spring支持5种scope：

  - Singleton - 每个 Spring IoC 容器仅有一个单实例。
  - Prototype - 每次请求都会产生一个新的实例。
  - Request - 每一次 HTTP 请求都会产生一个新的实例，并且该 bean 仅在当前 HTTP 请求内有效。
  - Session - 每一次 HTTP 请求都会产生一个新的 bean，同时该 bean 仅在当前 HTTP session 内有效。
  - Global-session - 类似于标准的 HTTP Session 作用域，不过它仅仅在基于 portlet 的 web 应用中才有意义。Portlet 规范定义了全局 Session 的概念，它被所有构成某个 portlet web 应用的各种不同的 portlet 所共享。在 global session 作用域中定义的 bean 被限定于全局 portlet Session 的生命周期范围内。如果你在 web 中使用 global session 作用域来标识 bean，那么 web 会自动当成 session 类型来使用。

  仅当用户使用支持 Web 的 ApplicationContext 时，最后三个才可用。



## Spring bean 容器的生命周期是什么样的？

Spring bean 容器的生命周期流程如下：

1. Spring 容器根据配置中的 bean 定义实例化 bean。
2. Spring 使用依赖注入填充所有属性，如 bean 中所定义的配置。
3. 如果 bean 实现 `BeanNameAware` 接口，则工厂通过传递 bean 的 name 来调用 `setBeanName()`。
4. 如果bean实现 `BeanClassLoaderAware` 接口，则传递bean的classLoader来调用`setBeanClassLoader()` 方法。
5. 如果 bean 实现 `BeanFactoryAware` 接口，工厂通过传递自身的实例来调用 `setBeanFactory()`。
6. 如果存在与 bean 关联的任何 `BeanPostProcessors`，则调用 `postProcessBeforeInitialization()` 方法。
7. 如果bean实现了`InitializingBean`接口则调用接口的`afterPropertiesSet`方法, 如果为 bean 指定了 init 方法`（ < bean> 的 init-method 属性）`，那么将调用它。
8. 最后，如果存在与 bean 关联的任何` BeanPostProcessors`，则将调用 `postProcessAfterInitialization()` 方法。
9. 如果 bean 实现 `DisposableBean` 接口，当 Spring 容器关闭时，会调用 `destory()`。
10. 如果为 bean 指定了 `destroy` 方法（` < bean> 的 destroy-method 属性）`，那么将调用它。

## 什么是 Spring 的内部 bean

只有将 bean 用作另一个 bean 的属性时，才能将 bean 声明为内部 bean。
为了定义 bean，Spring 的基于 XML 的配置元数据在 `< property>` 或 `< constructor-arg> `中提供了 `< bean> `元素的使用。内部 bean 总是匿名的，它们的scope总是property。

例如，假设我们有一个 Student 类，其中引用了 Person 类。这里我们将只创建一个 Person 类实例并在 Student 中使用它。



## 自动装配有哪些方式

Spring 容器能够自动装配 bean。也就是说，可以通过检查 BeanFactory 的内容让 Spring 自动解析 bean 的协作者。

自动装配的不同模式：

- no - 这是默认设置，表示没有自动装配。应使用显式 bean 引用进行装配。
- byName - 它根据 bean 的名称注入对象依赖项。它匹配并装配其属性与 XML 文件中由相同名称定义的 bean。
- byType - 它根据类型注入对象依赖项。如果属性的类型与 XML 文件中的一个 bean 名称匹配，则匹配并装配属性。
- 构造函数 - 它通过调用类的构造函数来注入依赖项。它有大量的参数。
- autodetect - 首先容器尝试通过构造函数使用 autowire 装配，如果不能，则尝试通过 byType 自动装配。



## 自动装配有什么局限

- 覆盖的可能性
- 基本元数据类型  简单属性（如原数据类型，字符串和类）无法自动装配



## 你用过哪些重要的 Spring 注解

- @Controller - 用于 Spring MVC 项目中的控制器类。
- @Service - 用于服务类。
- @RequestMapping - 用于在控制器处理程序方法中配置 URI 映射。
- @ResponseBody - 用于发送 Object 作为响应，通常用于发送 XML 或 JSON 数据作为响应。
- @PathVariable - 用于将动态值从 URI 映射到处理程序方法参数。
- @Autowired - 用于在 spring bean 中自动装配依赖项。
- @Qualifier - 使用 @Autowired 注解，以避免在存在多个 bean 类型实例时出现混淆。
- @Scope - 用于配置 spring bean 的范围。
- @Configuration，@ComponentScan 和 @Bean - 用于基于 java 的配置。
- @Aspect，@Before，@After，@Around，@Pointcut - 用于切面编程（AOP）。



## Spring JDBC API 中存在哪些类

- JdbcTemplate
- SimpleJdbcTemplate
- NamedParameterJdbcTemplate
- SimpleJdbcInsert
- SimpleJdbcCall



## 什么是 AOP

AOP(Aspect-Oriented Programming), 即 面向切面编程, 它与 OOP( Object-Oriented Programming, 面向对象编程) 相辅相成, 提供了与 OOP 不同的抽象软件结构的视角. 在 OOP 中, 我们以类(class)作为我们的基本单元, 而 AOP 中的基本单元是 Aspect(切面)



### AOP的基本概念

#### 切面(Aspect)

具有相同规则的方法的集合体。“切面”在ApplicationContext 中`<aop:aspect>`来配置

#### 连接点

能够插入切面的一个点，例如调用方法时、抛出异常时、修改字段时。

#### 通知(Advice)

切面对于某个连接点产生的动作。其中一个切面可以包含多个通知

#### 切入点

匹配连接点的断言，在 AOP 中通知和一个切入点表达式关联。切面中的所有通知所关注的连接点，都由切入点表达式来决定。需要代理的具体方法

#### 切面

切面是切点和通知的结合。

#### 织入 Weaving

织入就是将切面应用到目标对象来创建新的代理对象的过程。在对象的生命周期可以在多个阶段进行织入：

- 编译期
- 类加载期
- 运行期，Spring使用的这种方式，基于子类或实现接口的方式创建一个代理对象



https://zhuanlan.zhihu.com/p/372621321







