- [为什么要使用索引](#为什么要使用索引)
- [什么样的信息能成为索引](#什么样的信息能成为索引)
- [索引的数据结构](#索引的数据结构)
- [主键索引和其他索引的区别](#主键索引和其他索引的区别)
- [如何定位并优化慢查询](#如何定位并优化慢查询)
- [Explain查询结果关键字段](#explain查询结果关键字段)
- [一个字段需要同时满足多个条件](#一个字段需要同时满足多个条件)









## 为什么要使用索引

主要是为了快速查询数据，避免全表扫描



## 什么样的信息能成为索引

主键、唯一键



## 索引的数据结构

B+tree的数据结构





## 主键索引和其他索引的区别

其他索引存储了索引和主键的对应关系。所以，其他索引查询会进行两步查询首先第一步查询到主键，然后再通过主键查询数据。



## 如何定位并优化慢查询

- 根据慢日志定位慢查询语句
- 使用explain等工具分析sql
- 修改sql或者尽量让sql走索引



## Explain查询结果关键字段

- type

  如果出现`index` 和 `all` 说明走的是全表扫描

- extra

  ![explain查询的extra](../../pic/question/explain查询的extra.jpg)

  



## 一个字段需要同时满足多个条件

表中数据如下：

![一个字段同时满足多个条件](../../pic/question/一个字段同时满足多个条件.jpg)

查询逻辑是：查询出tag_id同时等于1和2的member_id

```sql
SELECT  
    t.member_id  
FROM  
    member_tag t  
WHERE  
    t.tag_id = 1 
or t.tag_id = 2 
GROUP BY t.member_id  
HAVING count(t.member_id)=2;
```

或

```sql
SELECT  
     DISTINCT a.member_id  
FROM  
    member_tag a  
INNER JOIN member_tag b ON a.member_id = b.member_id   
WHERE  
    a.tag_id =1 
AND b.tag_id =2 
```

查询逻辑是：查询出tag_id同时等于1、2和3的member_id

```sql
SELECT  
    t.member_id 
FROM  
    member_tag t  
WHERE  
    t.tag_id = 1 
or t.tag_id = 2 
or t.tag_id = 3 
GROUP BY t.member_id  
HAVING count(t.member_id)=3;
```



查询逻辑是：查询出tag_id同时等于1和4的member_id

```sql
SELECT  
     DISTINCT a.member_id  
FROM  
    member_tag a  
INNER JOIN member_tag b ON a.member_id = b.member_id  
WHERE  
    a.tag_id =1 
AND b.tag_id =4 
```



⼀般问的最多的就是：讲讲你在线上做过哪些调优？这⾥要从发现问题开始讲，如何定位，如何
判断，如何解决都讲⼀下。
也会顺便问⼀下索引结构以及常⻅的概念，⽐如回表，聚簇索引，mvcc，让你讲下更新⼀⾏数据
的流程，索引失效的原因和解决⽅案。锁类型，以及为什么⽤b+树，然后也会问点分库分表之类
的，⽐如分表维度，数据倾斜，不停机分库分表⽅案，如何跨⻚查询等等

