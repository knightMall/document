- [HashMap结构和原理](#hashmap结构和原理)
  - [hash算法优化](#hash算法优化)
  - [寻址优化](#寻址优化)
  - [hash碰撞优化](#hash碰撞优化)
  - [HashMap扩容](#hashmap扩容)
- [synchronized关键字的底层实现](#synchronized关键字的底层实现)







## HashMap结构和原理

HashMap底层是通过数组存储，对Map的key进行hash然后对数组长度进行取模运算获取位置。



### hash算法优化

JDK8以后的HashMap的hash计算方法优化如下：

```java
  static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }
```

代码的意思就是：取key的hash进行右移16位然后在和key的hash值进行与或运算。这样做是为在hash值中包含高低16位的信息。尽量避免hash冲突的问题。

不然在和数组的长度取模的时候高16位是没有用的。

### 寻址优化

通过`(n-1) & hash` (n表示数组长度)取代hash取模，当然取代后的值是一样的，前提n必须是2的倍数。性能更好。



### hash碰撞优化

在jdk8前，解决hash碰撞的方法是通过链表的方式。在jdk8后，如果链表的长度到达一定后，会将链表转换成红黑树。如下有一些常量控制什么时候进行红黑树化，什么时候进行链表化：

```java
/**
     * The bin count threshold for using a tree rather than list for a
     * bin.  Bins are converted to trees when adding an element to a
     * bin with at least this many nodes. The value must be greater
     * than 2 and should be at least 8 to mesh with assumptions in
     * tree removal about conversion back to plain bins upon
     * shrinkage.
     * 桶的树化阈值：即 链表转成红黑树的阈值，在存储数据时，当链表长度 > 该值时，则将链表转换成红黑树
     */
    static final int TREEIFY_THRESHOLD = 8;

    /**
     * The bin count threshold for untreeifying a (split) bin during a
     * resize operation. Should be less than TREEIFY_THRESHOLD, and at
     * most 6 to mesh with shrinkage detection under removal.
     * 桶的链表还原阈值：即 红黑树转为链表的阈值，当在扩容（resize（））时（此时HashMap的数据存储位置会重新计算），在重新计算存储位置后，当原有的红黑树内数量 < 6时，则将 红黑树转换成链表
     */
    static final int UNTREEIFY_THRESHOLD = 6;

    /**
     * The smallest table capacity for which bins may be treeified.
     * (Otherwise the table is resized if too many nodes in a bin.)
     * Should be at least 4 * TREEIFY_THRESHOLD to avoid conflicts
     * between resizing and treeification thresholds.
     * 最小树形化容量阈值：即 当哈希表中的容量 > 该值时，才允许树形化链表 （即 将链表 转换成红黑树）

// 否则，若桶内元素太多时，则直接扩容，而不是树形化

// 为了避免进行扩容、树形化选择的冲突，这个值不能小于 4 * TREEIFY_THRESHOLD
     */
    static final int MIN_TREEIFY_CAPACITY = 64;
```



### HashMap扩容

2倍扩容，在扩容的时候需要重新计算存放位置。所以在使用HashMap的时候最好在创建时就指定好大小，防止扩容



## synchronized关键字的底层实现

作用：给代码加锁同步代码

通过JVM指令和monitor有关，如果使用了synchronized关键字，JVM编译指令中，会有monitor和monitorexit两个指令。

monitor其实内部有一个计数器，当有线程进入到synchronized代码块时，则加1。

如果有线程执行到加了synchroinzed关键字的代码时，会判断monitor的计数器是不是0，如果是0表示没有其他线程执行，可以加锁

然后计数器加1，如果重入锁则会多次加1。当执行完synchronized代码块时，会有一个monitorexit指令，获取锁的线程将会对加锁对象的monitor的计数器减1，如果重入锁会多次减1。





## CAS的实现原理

CAS全称compare and set。Java中的Atomic相关类就是通过CAS实现的。

假设现在有如下的一段代码，对**i**的值进行累加

```java
AutomicInteger i = new AutomicInteer(0);
i.incrementAndGet();
```

具体流程就是：

先获取i的当前值，然后执行CAS操作，先比较i的当前值是不是0，如果是0的话则设为1。否在需要再次进行读取i的值在进行CAS操作。

CAS操作是先比较再进行设置，这系列操作都是原子的。





## ConcurrentHashMap实现原理

ConcurrentHashMap可以实现多线程下的读写操作。

实现原理：

**在JDK1.7及以前：**

是通过创建多个数组的方式实现分段加锁，例如将原来的一个大数组分成多个小数组，然后在对每一个小数组进行加锁。

**在jdk1.8及以后：**

则是通过在put方法时使用CAS实现的，先获取值然后put的时候进行比较再设置值。如果这个时候出现hash冲突的话，则会将

这个数组加上synchronized，这个时候会变成链表或红黑树，然后对链表或红黑树操作的时候也是线程安全的。



## JDK的AQS实现原理

AQS全称：abstract queue synchronizer，抽象队列同步器。具体流程：

AQS的具体实现：

```java
ReentrantLock reentrantLock = new ReentrantLock();
reentrantLock.lock();
//业务代码
reentrantLock.unlock();
```



![AQS流程图](../../pic/question/AQS-流程图.png)

当线程1和线程2同时执行`reentrantLock.lock();` 代码时，会通过CAS修改state变的值，修改为1。谁修改成功则获取到锁。

没有获取到的线程添加的等待队列挂起。当线程1执行完后在唤醒等待对象的线程2。默认情况下`ReentrantLock` lock是不公平的，

假设线程1执行完，还未唤醒线程2，这个时候线程3过来执行`reentrantLock.lock();`，又可能直接获取成功了，这导致队列中的等待线程无法执行。可以通过构造方法`new ReentrantLock(true);` 实现公平锁。



## 线程池原理

线程池的作用就是为了避免程序中反复创建线程，达到可以重复使用的效果。使用方式如下：

```java
ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
fixedThreadPool.submit(()->{
    System.out.println("执行任务");
});
```



### 线程池的执行流程

一个任务提交到线程池中，如果线程池中的线程数小于corePoolSize，则创建一个线程处理该任务，执行完该任务后该线程阻塞到线程池的task队列，等待任务添加到队列。也就是只要线程池中的线程数据量小于corePoolSize，则当有新任务提交时线程池都会创建新线程来处理该任务。如果线程池中的线程数大于等于corePoolSIze，当提交任务时则将该任务添加到任务队列中。

![线程流程图](../../pic/question/线程池执行流程01.png)



在创建线程池的时候会指定几个参数，每个参数的意义，如下是`FixedThreadPool` 的参数

```java
public static ExecutorService newFixedThreadPool(int nThreads) {
    return new ThreadPoolExecutor(nThreads, nThreads,
                                  0L, TimeUnit.MILLISECONDS,
                                  new LinkedBlockingQueue<Runnable>());
}
```

**corePoolSize：**  核心线程数

**maximumPoolSize：** 线程池最大容纳的线程数

**keepAliveTime：** 不是核心线程数的线程空闲时间

**unit：** 空闲时间的单位

**BlockingQueue：** 存放task的队列

**RejectedExecutionHandler：** 当队列数满时且线程池中的线程数达到了 **maximumPoolSize** 参数值时则执行指定的reject策略

**reject策略有如下：**

`AbortPolicy`: 抛出RejectedExecutionException异常

`CallerRunsPolicy`: 直接执行线程的run方法执行任务，如果线程关了则抛弃任务

`DiscardOldestPolicy`: 丢弃最旧的任务

`DiscardPolicy`：直接丢弃任务

自定义策略



### 线程池的完整执行流程

![线程流程图](../../pic/question/线程池完整流程.png)



一个任务提交到线程池中，如果线程池中的线程数小于corePoolSize，则创建一个线程处理该任务，执行完该任务后该线程阻塞到线程池的task队列，等待任务添加到队列。也就是只要线程池中的线程数据量小于corePoolSize，则当有新任务提交时线程池都会创建新线程来处理该任务。如果线程池中的线程数大于等于corePoolSIze，当提交任务时则将该任务添加到任务队列中。如果线程池的任务队列也满了且线程池中的线程数小于maximumPoolSize参数的值则创建一个新线程来处理该任务。如果不巧线程池的任务队列满了且线程池中的线程数也不小于maximumPoolSize参数的的值，则只能使用拒绝策略了。

常用的线程池有：

```java
ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
```

和

```java
ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(3, Integer.MAX_VALUE, 
                60, TimeUnit.SECONDS, new ArrayBlockingQueue<R>(200));
```





## Happen before原则

- 程序次序规则：一个线程内，按照代码顺序，书写在前面的操作先行发生于写在后面的操作。
- 锁定规则：一个unlock操作先行发生于后面对同一个锁的lock操作。
- volatile变量规则：对一个volatile变量的写操作先于后面的读操作。
- 传递规则：如果操作A先于操作B，而操作B又先于操作C，则可以推到出操作A先于操作C
- 线程启动规则：Thread对象的start()方法先于此线程的每一个操作



## volatile是如何基于内存屏障保证可见性和有序性

volatile不能保证原子性。

当某个加了volatile关键字的变量执行写操作后，JVM会发送一条lock前缀指令给CPU，CPU在计算完成之后会立即将这个值刷回到主内存中，同时因为又MESI缓存一致性协议，所以各个CPU将自己的本地内存缓存数据过期，然后这个CPU上执行的线程读取变量的时候，就会从主内存中重新刷会最新的数据。







  





































































































