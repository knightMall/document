

## resultType 和  resultMap 的区别

resultType 是`<select>`标签的一个属性，适合简单对象（POJO、JDK 自带类型：
Integer、String、Map 等），只能自动映射，适合单表简单查询。



## Mybatis解决了什么问题

- 资源管理（底层对象封装和支持数据源）
- 结果集自动映射
- SQL 与代码分离，集中管理
- 参数映射和动态 SQL
- 其他：缓存、插件等



## MyBatis 编程式开发中的核心对象及其作用

- SqlSessionFactoryBuilder 创建工厂类
- SqlSessionFactory 创建会话
- SqlSession 提供操作接口
- MapperProxy 代理 Mapper 接口后，用于找到 SQL 执行



## Java 类型和数据库类型怎么实现相互映射？

通过 TypeHandler，例如 Java 类型中的 String 要保存成 varchar，就会自动调
用相应的 Handler。如果没有系统自带的 TypeHandler，也可以自定义。



## SIMPLE/REUSE/BATCH 三种执行器的区别？

- SimpleExecutor 使用后直接关闭 Statement
- ReuseExecutor 放在缓存中，可复用
- BatchExecutor 支持复用且可以批量执行 update()



## MyBatis 一级缓存与二级缓存的区别

- 一级缓存：在同一个会话（SqlSession）中共享，默认开启，维护在 BaseExecutor
  中。
- 二级缓存：在同一个 namespace 共享，需要在 Mapper.xml 中开启，维护在
  CachingExecutor 中。

## MyBaits 支持哪些数据源类型

- UNPOOLED：不带连接池的数据源。
- POOLED ： 带 连 接 池 的 数 据 源 ， 在 PooledDataSource 中 维 护
  PooledConnection。
- JNDI：使用容器的数据源，比如 Tomcat 配置了 C3P0
- 自定义数据源：实现 DataSourceFactory 接口，返回一个 DataSource。
  当 MyBatis 集成到 Spring 中的时候，使用 Spring 的数据源。















