- [并发编程中三个重要的概念](#并发编程中三个重要的概念)
  - [原子性](#原子性)
  - [可见性](#可见性)
  - [有序性](#有序性)
- [Volatile关键字](#volatile关键字)
  - [Volatile关键字保证可见性示例](#volatile关键字保证可见性示例)



## 并发编程中三个重要的概念

### 原子性

一个操作或者多个操作，要么都成功，要么都失败；中间不能不能由于任何原因中断。

Java中保证原子性：

对基本数据类型的变量的读取和赋值是保证了原子性的。下面有一些例子：

```java
int a = 10; //原子性
int b = a; //不满足，分为 1.读取a，2.将a赋值给b
a++; //不满足，分为 1.读取a，2.将a加1，3.赋值给a
a=a+1; //不满足，分为 1.读取a，2.将a加1，3.赋值给a
```



### 可见性

多线程下共享变量内存中，在不同的线程间是不可见的。

Java中通过关键字`volatile` 保证可见性，但是不能保证原子性

### 有序性

代码的执行顺序是不一定的，但是保证最终一致性。

Java中针对有序性有如下规则：

- 代码的执行顺序，编写在前面的在编写后面的执行
- unlock必须发生在lock之后
- volatile修饰的变量，对于一个变量的写操作先于该变量的读操作
- 传递规则：操作A先于B，B先于C，那么A肯定先于C
- 线程启动规则：start方法肯定先于run方法
- 线程中断规则：interrupt这个动作，必须发生在捕捉该动作之前
- 对象销毁规则：对象的初始化必须发生在finalize之前
- 线程终结规则：所有操作都发生在线程死亡前
- join规则：在线程调用join方法后的代码都能访问线程中修改的数据











## Volatile关键字

- Volatile可以保证可见性，如果是写操作会导致CPU的缓存失效
- 保证重排序，不会将后面的指令放到屏障前面，也不会将前面的放在后面

Volatile通过happens-before 原则来，保证可见性。happens-before是通过内存屏障来实现的。



### Volatile关键字保证可见性示例

```java
/**
 * volitale关键字的使用场景
 */
public class VolatileTest {

    private volatile static int INIT_VALUE = 0;
    private final static int MAX_LIMIT = 5;

    public static void main(String[] args) {


        new Thread(() -> {
            int localValue = INIT_VALUE;
            while (localValue < MAX_LIMIT) {
                if (localValue != INIT_VALUE) {
                    System.out.printf("The value updated to [%d]\n", INIT_VALUE);
                    localValue = INIT_VALUE;
                }
            }
        }, "READER").start();


        new Thread(() -> {
            int localValue = INIT_VALUE;
            while (localValue < MAX_LIMIT) {
                System.out.printf("Update the value  to [%d]\n", ++localValue);
                INIT_VALUE = localValue;
                try {
                    Thread.sleep(500L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "UPDATER").start();
    }
}
```

