## synchronized 的 基本语法

synchronized 有三种方式来加锁，分别是:

- 修饰实例方法，作用于当前实例加锁，进入同步代码前
  要获得当前实例的锁
- 静态方法，作用于当前类对象加锁，进入同步代码前要
  获得当前类对象的锁
- 修饰代码块，指定加锁对象，对给定对象加锁，进入同
  步代码库前要获得给定对象的锁。



## synchronized的锁的升级

synchronized 在JDK1.6 之后做了一些优化，为了减少获得锁和释放锁带来
的性能开销，引入了偏向锁、轻量级锁的概念。因此大家
会发现在 synchronized 中，锁存在四种状态
分别是：无锁、偏向锁、轻量级锁、重量级锁； 锁的状态
根据竞争激烈的程度从低到高不断升级。



## 偏向锁的基本原理

前面说过，大部分情况下，锁不仅仅不存在多线程竞争，
而是总是由同一个线程多次获得，为了让线程获取锁的代
价更低就引入了偏向锁的概念。怎么理解偏向锁呢？
当一个线程访问加了同步锁的代码块时，会在对象头中存
储当前线程的 ID，后续这个线程进入和退出这段加了同步
锁的代码块时，不需要再次加锁和释放锁。而是直接比较
对象头里面是否存储了指向当前线程的偏向锁。如果相等
表示偏向锁是偏向于当前线程的，就不需要再尝试获得锁
了。



### 偏向锁的获取和撤销逻辑

1. 首先获取锁 对象的 Markword，判断是否处于可偏向状
   态。（biased_lock=1、且 ThreadId 为空）
2. 如果是可偏向状态，则通过 CAS 操作，把当前线程的 ID
   写入到 MarkWord
   1. 如果 cas 成功，那么 markword 就会变成这样。
      表示已经获得了锁对象的偏向锁，接着执行同步代码
      块。
   2. 如果 cas 失败，说明有其他线程已经获得了偏向锁，
      这种情况说明当前锁存在竞争，需要撤销已获得偏向
      锁的线程，并且把它持有的锁升级为轻量级锁（这个
      操作需要等到全局安全点，也就是没有线程在执行字
      节码）才能执行
3. 如果是已偏向状态，需要检查 markword 中存储的
   ThreadID 是否等于当前线程的 ThreadID
   1. 如果相等，不需要再次获得锁，可直接执行同步代码
      块
   2. 如果不相等，说明当前锁偏向于其他线程，需要撤销
      偏向锁并升级到轻量级锁



## 轻量级锁的基本原理

#### 轻量级锁的加锁和解锁逻辑

锁升级为轻量级锁之后，对象的 Markword 也会进行相应
的的变化。升级为轻量级锁的过程：

1. 线程在自己的栈桢中创建锁记录 LockRecord。
2. 将锁对象的对象头中的MarkWord复制到线程的刚刚创
建的锁记录中。
3. 将锁记录中的 Owner 指针指向锁对象。
4. 将锁对象的对象头的MarkWord替换为指向锁记录的指
针。



### 自旋锁

轻量级锁在加锁过程中，用到了自旋锁
所谓自旋，就是指当有另外一个线程来竞争锁时，这个线
程会在原地循环等待，而不是把该线程给阻塞，直到那个
获得锁的线程释放锁之后，这个线程就可以马上获得锁的。
注意，锁在原地循环的时候，是会消耗 cpu 的，就相当于
在执行一个啥也没有的 for 循环。
所以，轻量级锁适用于那些同步代码块执行的很快的场景，
这样，线程原地等待很短的时间就能够获得锁了。
自旋锁的使用，其实也是有一定的概率背景，在大部分同
步代码块执行的时间都是很短的。所以通过看似无异议的
循环反而能提升锁的性能。
但是自旋必须要有一定的条件控制，否则如果一个线程执
行同步代码块的时间很长，那么这个线程不断的循环反而
会消耗 CPU 资源。默认情况下自旋的次数是 10 次，
可以通过 preBlockSpin 来修改。

在 JDK1.6 之后，引入了自适应自旋锁，自适应意味着自旋
的次数不是固定不变的，而是根据前一次在同一个锁上自
旋的时间以及锁的拥有者的状态来决定。
如果在同一个锁对象上，自旋等待刚刚成功获得过锁，并
且持有锁的线程正在运行中，那么虚拟机就会认为这次自
旋也是很有可能再次成功，进而它将允许自旋等待持续相
对更长的时间。如果对于某个锁，自旋很少成功获得过，
那在以后尝试获取这个锁时将可能省略掉自旋过程，直接
阻塞线程，避免浪费处理器资源.







## 重量级锁

当轻量级锁膨胀到重量级锁之后，意味着线程只能被挂起
阻塞来等待被唤醒了。

```java
public class APP {
    public static void main(String[] args) {
        synchronized (APP.class){

        }
    }
}
```

通过命令：`javap -v App.class`

![重量级锁](../../images/21/1030/重量级锁.png)

加 了 同 步 代 码 块 以 后 ， 在 字 节 码 中 会 看 到 一 个
monitorenter 和 monitorexit。
每一个 JAVA 对象都会与一个监视器 monitor 关联，我们
可以把它理解成为一把锁，当一个线程想要执行一段被
synchronized 修饰的同步方法或者代码块时，该线程得先
获取到 synchronized 修饰的对象对应的 monitor。
monitorenter 表示去获得一个对象监视器。monitorexit 表
示释放 monitor 监视器的所有权，使得其他被阻塞的线程
可以尝试去获得这个监视器
monitor 依赖操作系统的 MutexLock(互斥锁)来实现的, 线
程被阻塞后便进入内核（Linux）调度状态，这个会导致系
统在用户态与内核态之间来回切换，严重影响锁的性能。





## CAS介绍

CAS全称Compare And Swap，一种乐观锁。现代的CPU提供了特殊的指令，可以自动更新共享数据，而且能够检测到其他线程的干扰，而 compareAndSet() 就用这些代替了锁定。

但是同样也会有问题，会出现ABA问题。比如说一个线程one从内存位置V中取出A，这时候另一个线程two也从内存中取出A，并且two进行了一些操作变成了B，然后two又将V位置的数据变成A，这时候线程one进行CAS操作发现内存中仍然是A，然后one操作成功。尽管线程one的CAS操作成功，但是不代表这个过程就是没有问题的。

各种乐观锁的实现中通常都会用版本戳version来对记录或对象标记，避免并发操作带来的问题，在Java中，AtomicStampedReference<E>也实现了这个作用，它通过包装[E,Integer]的元组来对对象标记版本戳stamp，从而避免ABA问题，
