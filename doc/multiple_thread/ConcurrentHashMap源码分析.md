

## ConcurrentHashMap在JDK 1.8中是基于什么机制来保证线程安全性的

**在添加的时候**，如果key计算的数组位置为空，则通过CAS添加。如果有数据则将该节点加锁，然后基于链表方式添加。

**统计Map大小时**：ConcurrentHashMap 是采用 CounterCell 数组来记录元素个数的，像一般的集合
记录集合大小，直接定义一个 size 的成员变量即可，当出现改变时只需要更新该变量即可。因为
CHM是支持并发的集合，如果使用一个成员变量来统计元素个数，势必会需要通过加锁或者自旋来
实现，如果竞争比较激烈的话，将会很影响性能。所以在ConcurrentHashMap 采用了分片的方法来
记录大小。下面是CounterCell类的申明

```java
@sun.misc.Contended static final class CounterCell {
    volatile long value;
    CounterCell(long x) { value = x; }
}
```

通过统计元素个数的方法更加能清楚的反应CounterCell类是如何操作的：

```java
final long sumCount() {
    CounterCell[] as = counterCells; CounterCell a;
    long sum = baseCount;
    if (as != null) {
        for (int i = 0; i < as.length; ++i) {
            if ((a = as[i]) != null)
                sum += a.value;
        }
    }
    return sum;
}
```

通过遍历CounterCell数组来统计元素个数。所以在使用的时候可以是不同的线程配置数组不同的位置来实现无锁化。

**扩容时**：支持并发扩容，通过将Node数组拆分，让每个线程处理自己的区域。链表迁移时，会
用高低位来实现，因为通过扩容后原来在链表中的数据，再次进行hash运算值肯定会变，落到的相
应位置有可能不一样。因为CHM的元素位置的算法性，通过在遍历链表的时候就可以确定该元素是
否需要移位，对于低位的无需移动位置，对于高位则直接加上扩容的长度。

**链表查找时**：当出现hash碰撞时，使用链表。当链表数量达到8个且Node长度大于64时，则会将链表转换成红黑树，转换成红黑树在查询的时候速度更快。如果还采用单向列表方式，那么查询某个节点的时间复杂度就变为 O(n); 因此对于
队列长度超过 8 的列表，JDK1.8 采用了红黑树的结构，那么查询的时间复杂度就会降低到
O(logN),可以提升查找的性能



## ConcurrentHashMap通过get方法获取数据的时候，是否需要通过加锁来保证数据的可见性？为什么？

不需要，因为存储数据的数据已经添加了 `volatile` 关键字。通过CAS来获取。





## ConcurrentHashMap1.7和ConcurrentHashMap1.8有哪些区别？

1. 取消了 segment 分段设计，直接使用 Node 数组来保存数据，并且采用 Node 数组元素作
   为锁来实现每一行数据进行加锁来进一步减少并发冲突的概率。
2. 将原本数组+单向链表的数据结构变更为了数组+单向链表+红黑树的结构。为什么要引入
   红黑树呢？在正常情况下，key hash 之后如果能够很均匀的分散在数组中，那么 table 数
   组中的每个队列的长度主要为 0 或者 1.但是实际情况下，还是会存在一些队列长度过长的
   情况。如果还采用单向列表方式，那么查询某个节点的时间复杂度就变为 O(n); 因此对于
   队列长度超过 8 的列表，JDK1.8 采用了红黑树的结构，那么查询的时间复杂度就会降低到
   O(logN),可以提升查找的性能；





## ConcurrentHashMap1.8为什么要引入红黑树？

如果还采用单向列表方式，那么查询某个节点的时间复杂度就变为 O(n); 因此对于
队列长度超过 8 的列表，JDK1.8 采用了红黑树的结构，那么查询的时间复杂度就会降低到
O(logN),可以提升查找的性能









