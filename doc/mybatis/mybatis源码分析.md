## ResultMap和ResultType的区别

resultMap是定义在 `mapper` 标签下的，resultMap需要先在Mapper中申明才能使用。

resultType标签定义在 `select` 的属性，resultType可以自动映射到DTO。




## Statement和PreparedStatement的区别

PreparedStatement会对SQL进行预编译的，并且可以进行缓存，可以预防SQL注入。

Statement每次执行SQL，数据库都要执行SQL语句的编译，并且存在SQL注入风险。



## Association和Collection的区别

在一对一或多对一的情况下使用Association。

在一对多的情况下使用Collection，例如一个班级有多个学生，在班级类中的学生实例就可以使用Collection来







## 三种执行器的区别是什么？Simple、Reuse、Batch

Executor分成两大类，一类是CacheExecutor，另一类是普通Executor。

普通类又分为： 

ExecutorType.SIMPLE: 这个执行器类型不做特殊的事情。它为每个语句的执行创建一个新的预处理语句。（默认）
ExecutorType.REUSE: 这个执行器类型会复用预处理语句。
ExecutorType.BATCH: 这个执行器会批量执行所有更新语句,如果 SELECT 在它们中间执行还会标定它们是 必须的,来保证一个简单并易于理解的行为。

分别对应SimpleExecutor，ReuseExecutor，BatchExecutor，他们都继承于BaseExecutor，BatchExecutor专门用于执行批量sql操作，ReuseExecutor会重用statement执行sql操作，SimpleExecutor只是简单执行sql没有什么特别的。

作用范围：Executor的这些特点，都严格限制在SqlSession生命周期范围内。

`CacheExecutor`有一个重要属性delegate，它保存的是某类普通的Executor，值在构照时传入。执行数据库update操作时，它直接调用delegate的update方法，执行query方法时先尝试从cache中取值，取不到再调用delegate的查询方法，并将查询结果存入cache中。二级缓存是基于该类实现的。

　`SimpleExecutor`通过类名可以看出，它是一个简单的执行类，并不会做一些处理就执行sql。（每执行一次update或select，就开启一个Statement对象，用完立刻关闭Statement对象）源码及分析如下：

　`BatchExecutor` : 通过批量操作来提高性能。（执行update（没有select，JDBC批处理不支持select），将所有sql都添加到批处理中（addBatch()），等待统一执行（executeBatch()），它缓存了多个Statement对象，每个Statement对象都是addBatch()完毕后，等待逐一执行executeBatch()批处理。与JDBC批处理相同。）

`ReuseExecutor`: 重复使用执行，其定义了一个Map<String, Statement>，将执行的sql作为key，将执行的Statement作为value保存，这样执行相同的sql时就可以使用已经存在的Statement，就不需要新创建了。（执行update或select，以sql作为key查找Statement对象，存在就使用，不存在就创建，用完后，不关闭Statement对象，而是放置于Map内，供下一次使用。简言之，就是重复使用Statement对象。）源码及分析如下







