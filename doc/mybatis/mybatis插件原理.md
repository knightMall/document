## Mybatis插件的使用

### 插件可以拦截的方法

Mybatis可以拦截的方法如下：

- Executor (update, query, flushStatements, commit, rollback, getTransaction, close, isClosed)
- ParameterHandler (getParameterObject, setParameters)
- ResultSetHandler (handleResultSets, handleOutputParameters)
- StatementHandler (prepare, parameterize, batch, update, query)



| 对象             | 描述                                                         | 可拦截的方法           | 方法作用                                                     |
| ---------------- | ------------------------------------------------------------ | ---------------------- | ------------------------------------------------------------ |
| Executor         | 上层的对象，SQL 执行<br/>全过程，包括组装参数，组装结果集返回和<br/>执行 SQL 过程 | update                 | 执行 update、insert、delete 操作                             |
|                  |                                                              | query                  | 执行 query 操作                                              |
|                  |                                                              | flushStatements        | 在 commit 的时候自动调用，SimpleExecutor、<br/>ReuseExecutor、BatchExecutor 处理不同 |
|                  |                                                              | commit                 | 提交事务                                                     |
|                  |                                                              | rollback               | 事务回滚                                                     |
|                  |                                                              | getTransaction         | 获取事务                                                     |
|                  |                                                              | close                  | 结束（关闭）事务                                             |
|                  |                                                              | isClosed               | 判断事务是否关闭                                             |
| StatementHandler | 执行 SQL 的过程，最常<br/>用的拦截对象                       | prepare                | （BaseSatementHandler）SQL 预编译                            |
|                  |                                                              | parameterize           | 设置参数                                                     |
|                  |                                                              | batch                  | 批处理                                                       |
|                  |                                                              | update                 | 增删改操作                                                   |
|                  |                                                              | query                  | 查询操作                                                     |
| ParameterHandler | SQL 参数组装的过程                                           | getParameterObject     | 获取参数                                                     |
|                  |                                                              | setParameters          | 设置参数                                                     |
| ResultSetHandler | 结果的组装                                                   | handleResultSets       | 处理结果集                                                   |
|                  |                                                              | handleOutputParameters | 处理存储过程出参                                             |



### 使用例子

```java
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.util.Properties;

@Intercepts({@Signature(
        type= Executor.class,
        method = "query",
        args = {MappedStatement.class,Object.class, RowBounds.class, ResultHandler.class})})
public class ExamplePlugin implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        System.out.println("---ExamplePlugin intercept--");
        Object result = invocation.proceed();
        return result;
    }

    @Override
    public Object plugin(Object target) {
        System.out.println("---ExamplePlugin plugin---");
        return Plugin.wrap(target,this);
    }

    @Override
    public void setProperties(Properties properties) {

    }
}
```

通过注解`@Signature()` 定义拦截的对象、方法名、方法参数和顺序。 拦截的签名和参数的顺序有关系。

在mybatis-config.xml中注册：

```xml
<plugins>
    <plugin interceptor="com.knight.mybatis.example.plugins.ExamplePlugin"></plugin>
</plugins>
```

MyBatis 启 动 时 扫 描 `<plugins> `标 签 ， 注 册 到 Configuration 对 象 的
InterceptorChain 中。property 里面的参数，会调用 setProperties()方法处理



## 四大对象在什么什么被创建的

Executor 是 openSession() 的 时 候 创 建 的 ； StatementHandler 是
SimpleExecutor.doQuery()创建的；里面包含了处理参数的 ParameterHandler 和处理
结果集的 ResultSetHandler 的创建，创建之后即调用 InterceptorChain.pluginAll()，
返回层层代理后的对象。

## 多个插件的情况下，代理顺序如果调用

![image-20211025070600199](../../images/21/1025/mybatis多个插件调用顺序.png)



## 谁来创建代理对象

Plugin 类 。 在 我 们 重 写 的 plugin() 方 法 里 面 可 以 直 接 调 用 return
Plugin.wrap(target, this);返回代理对象。



## 被代理后，调用的是什么方法？怎么调用到原被代理对象的方法？

因为代理类是 Plugin，所以最后调用的是 Plugin 的 invoke()方法。它先调用了定义
的拦截器的 intercept()方法。可以通过 invocation.proceed()调用到被代理对象被拦截
的方法。



## 流程总结

![image-20211025070816803](../../images/21/1025/mybatis插件调用流程.png)



## 插件实现类

| 对象           | 作用                                                        |
| -------------- | ----------------------------------------------------------- |
| Interceptor    | 自定义插件需要实现接口，实现 3 个方法                       |
| InterceptChain | 配置的插件解析后会保存在 Configuration 的 InterceptChain 中 |
| Plugin         | 用来创建代理对象，包装四大对象                              |
| Invocation     | 对被代理类进行包装，可以调用 proceed()调用到被拦截的方法    |

