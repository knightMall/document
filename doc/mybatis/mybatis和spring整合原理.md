这里我们以传统的 Spring 为例，因为配置更直观，在 Spring 中使用配置类注解是
一样的。
在前面的课程里面，我们基于编程式的工程已经弄清楚了 MyBatis 的工作流程、核
心模块和底层原理。编程式的工程，也就是 MyBatis 的原生 API 里面有三个核心对象：
SqlSessionFactory、SqlSession、MapperProxy。
大部分时候我们不会在项目中单独使用 MyBatis 的工程，而是集成到 Spring 里面使
用，但是却没有看到这三个对象在代码里面的出现。我们直接注入了一个 Mapper 接口，
调用它的方法。



所以有几个关键的问题，我们要弄清楚：
1、 SqlSessionFactory 是什么时候创建的？
2、 SqlSession 去哪里了？为什么不用它来 getMapper？
3、 为什么@Autowired 注入一个接口，在使用的时候却变成了代理对象？在 IOC
的容器里面我们注入的是什么？ 注入的时候发生了什么事情？



## 关键配置

我们先看一下把 MyBatis 集成到 Spring 中要做的几件事情。
为了让大家看起来更直观，这里我们依旧用传统的 xml 配置给大家来做讲解，当然

使用配置类@Configuration 效果也是一样的，对于 Spring 来说只是解析方式的差异。
除了 MyBatis 的依赖之外，我们还需要在 pom 文件中引入 MyBatis 和 Spring 整合
的 jar 包。

```xml
< dependency>
< groupId>org.mybatis</ groupId>
< artifactId>mybatis-spring</ artifactId>
< version>2.0.0</ version>
</ dependency>
```

然后在 Spring 的 applicationContext.xml 里面配置 SqlSessionFactoryBean，它
是用来帮助我们创建会话的，其中还要指定全局配置文件和 mapper 映射器文件的路径。

```xml
<bean id" ="sqlSessionFactory" class ="org.mybatis.spring.SqlSessionFactoryBean">
  <property name" ="configLocation" value ="classpath:mybatis-config.xml"></ property>
  <property name" ="mapperLocations" value ="classpath:mapper/*.xml"></ property>
  <property name" ="dataSource" ref ="dataSource"/>
</
```

然后在 applicationContext.xml 配置需要扫描 Mapper 接口的路径。
在 Mybatis 里面有几种方式，第一种是配置一个 MapperScannerConfigurer。

```xml
<bean id" ="mapperScanner" class ="org.mybatis.spring.mapper.MapperScannerConfigurer">
  <property name" ="basePackage" value ="com.gupaoedu.crud.dao"/>
</bea>
```

第二种是配置一个`<scan>`标签：

```xml
< mybatis-springn :scan base-package />
```

还有一种就是直接用@MapperScan 注解，比如我们在 Spring Boot 的启动类上加
上一个注解：

```java
@SpringBootApplication
@MapperScan( "com.gupaoedu. . crud.dao")
  public class MybaitsApp {
    public static void main(String[] args) {
    SpringApplication. run (MybaitsApp. class, args);
    }
}
```

这三种方式实现的效果是一样的。



## 创建会话工厂

Spring 对 MyBatis 的对象进行了管理，但是并不会替换 MyBatis 的核心对象。也就
意味着：MyBatis jar 包中的 SqlSessionFactory、SqlSession、MapperProxy 这些都
会用到。而 mybatis-spring.jar 里面的类只是做了一些包装或者桥梁的工作。
所以第一步，我们看一下在 Spring 里面，工厂类是怎么创建的。
我们在 Spring 的配置文件中配置了一个 SqlSessionFactoryBean，我们来看一下这
个类。

![image-20211026135156648](../../images/21/1026/SqlSessionFactoryBean的类图.png)

它实现了 InitializingBean 接口，所以要实现 afterPropertiesSet()方法，这个方法
会在 bean 的属性值设置完的时候被调用。
另外它实现了 FactoryBean 接口，所以它初始化的时候，实际上是调用 getObject()
方法，它里面调用的也是 afterPropertiesSet()方法。
在 afterPropertiesSet()方法里面：
第一步是一些标签属性的检查，接下来调用了 buildSqlSessionFactory()方法。
然后定义了一个 Configuration，叫做 targetConfiguration。

426 行，判断 Configuration 对象是否已经存在，也就是是否已经解析过。如果已
经有对象，就覆盖一下属性。
433 行，如果 Configuration 不存在，但是配置了 configLocation 属性，就根据
mybatis-config.xml 的文件路径，构建一个 xmlConfigBuilder 对象。
436 行，否则，Configuration 对象不存在，configLocation 路径也没有，只能使
用默认属性去构建去给 configurationProperties 赋值。
后面就是基于当前 factory 对象里面已有的属性，对 targetConfiguration 对象里面
属性的赋值。
`Optional. ofNullable ( this. objectFactory).ifPresent(targetConfiguration::setObjectFactory);`
这个方法是 Java8 里面的一个判空的方法。如果不为空的话，就会调用括号里面的
对象的方法，做赋值的处理。
在第 498 行，如果 xmlConfigBuilder 不为空，也就是上面的第二种情况，调用了
xmlConfigBuilder.parse()去解析配置文件，最终会返回解析好的 Configuration 对象。
在 第 507 行 ， 如 果 没 有 明 确 指 定 事 务 工 厂 ， 默 认 使 用
SpringManagedTransactionFactory。它创建的 SpringManagedTransaction 也有
getConnection()和 close()方法。
`<property name" ="transactionFactory" value" ="" />`
在 520 行，调用 xmlMapperBuilder.parse()，这个步骤我们之前了解过了，它的作
用是把接口和对应的 MapperProxyFactory 注册到 MapperRegistry 中。
最 后 调 用 sqlSessionFactoryBuilder.build() 返 回 了 一 个
DefaultSqlSessionFactory。

OK，在这里我们完成了编程式的案例里面的第一步，根据配置文件获得一个工厂类，
它是单例的，会在后面用来创建 SqlSession。

用到的 Spring 扩展点总结:

| 接口                                | 方法                                | 作用                                |
| ----------------------------------- | ----------------------------------- | ----------------------------------- |
| FactoryBean                         | getObject()                         | 返回由 FactoryBean 创建的 Bean 实例 |
| InitializingBean                    | afterPropertiesSet()                | bean 属性初始化完成后添加操作       |
| BeanDefinitionRegistryPostProcessor | postProcessBeanDefinitionRegistry() | 注入 BeanDefination 时添加操作      |



## 创建SqlSession



### 是否可以直接使用 DefaultSqlSession吗

我们现在已经有一个 DefaultSqlSessionFactory，按照编程式的开发过程，我们接
下来就会创建一个 SqlSession 的实现类，但是在 Spring 里面，我们不是直接使用
DefaultSqlSession 的，而是对它进行了一个封装，这个 SqlSession 的实现类就是
SqlSessionTemplate。这个跟 Spring 封装其他的组件是一样的，比如 JdbcTemplate，
RedisTemplate 等等，也是 Spring 跟 MyBatis 整合的最关键的一个类。
为什么不用 DefaultSqlSession？它是线程不安全的，注意看类上的注解：

```java
Note that this class is not Thread-Safe.
```

而 SqlSessionTemplate 是线程安全的。

```java
Thread safe, Spring managed,
```

回顾一下 SqlSession 的生命周期：

| 对象                      | 生命周期   |
| ------------------------- | ---------- |
| SqlSessionFactoryBuilder  | 方法局部   |
| SqlSessionFactory（单例） | 应用级别   |
| SqlSession                | 请求和操作 |
| Mapper                    | 方法       |

在编程式的开发中，SqlSession 我们会在每次请求的时候创建一个，但是 Spring
里面只有一个 SqlSessionTemplate（默认是单例的），多个线程同时调用的时候怎么保
证线程安全？

### 为什么 SqlSessionTemplate 是线程安全的？

SqlSessionTemplate 里面有 DefaultSqlSession 的所有的方法：selectOne()、
selectList()、insert()、update()、delete()，不过它都是通过一个代理对象实现的。这
个代理对象在构造方法里面通过一个代理类创建：

```java
public SqlSessionTemplate(SqlSessionFactory sqlSessionFactory, ExecutorType executorType,
    PersistenceExceptionTranslator exceptionTranslator) {

  notNull(sqlSessionFactory, "Property 'sqlSessionFactory' is required");
  notNull(executorType, "Property 'executorType' is required");

  this.sqlSessionFactory = sqlSessionFactory;
  this.executorType = executorType;
  this.exceptionTranslator = exceptionTranslator;
  this.sqlSessionProxy = (SqlSession) newProxyInstance(
      SqlSessionFactory.class.getClassLoader(),
      new Class[] { SqlSession.class },
      new SqlSessionInterceptor());
}
```

```java
SqlSession sqlSession = getSqlSession(
          SqlSessionTemplate.this.sqlSessionFactory,
          SqlSessionTemplate.this.executorType,
          SqlSessionTemplate.this.exceptionTranslator);
```

```java
  /**
   * Gets an SqlSession from Spring Transaction Manager or creates a new one if needed.
   * Tries to get a SqlSession out of current transaction. If there is not any, it creates a new one.
   * Then, it synchronizes the SqlSession with the transaction if Spring TX is active and
   * <code>SpringManagedTransactionFactory</code> is configured as a transaction manager.
   *
   * @param sessionFactory a MyBatis {@code SqlSessionFactory} to create new sessions
   * @param executorType The executor type of the SqlSession to create
   * @param exceptionTranslator Optional. Translates SqlSession.commit() exceptions to Spring exceptions.
   * @return an SqlSession managed by Spring Transaction Manager
   * @throws TransientDataAccessResourceException if a transaction is active and the
   *             {@code SqlSessionFactory} is not using a {@code SpringManagedTransactionFactory}
   * @see SpringManagedTransactionFactory
   */
  public static SqlSession getSqlSession(SqlSessionFactory sessionFactory, ExecutorType executorType, PersistenceExceptionTranslator exceptionTranslator) {

    notNull(sessionFactory, NO_SQL_SESSION_FACTORY_SPECIFIED);
    notNull(executorType, NO_EXECUTOR_TYPE_SPECIFIED);

    SqlSessionHolder holder = (SqlSessionHolder) TransactionSynchronizationManager.getResource(sessionFactory);

    SqlSession session = sessionHolder(executorType, holder);
    if (session != null) {
      return session;
    }

    LOGGER.debug(() -> "Creating a new SqlSession");
    session = sessionFactory.openSession(executorType);

    registerSessionHolder(sessionFactory, executorType, exceptionTranslator, session);

    return session;
  }
```

```java
private static void registerSessionHolder(SqlSessionFactory sessionFactory, ExecutorType executorType,
      PersistenceExceptionTranslator exceptionTranslator, SqlSession session) {
    SqlSessionHolder holder;
    if (TransactionSynchronizationManager.isSynchronizationActive()) {
      Environment environment = sessionFactory.getConfiguration().getEnvironment();

      if (environment.getTransactionFactory() instanceof SpringManagedTransactionFactory) {
        LOGGER.debug(() -> "Registering transaction synchronization for SqlSession [" + session + "]");

        holder = new SqlSessionHolder(session, executorType, exceptionTranslator);
        TransactionSynchronizationManager.bindResource(sessionFactory, holder);
        TransactionSynchronizationManager.registerSynchronization(new SqlSessionSynchronization(holder, sessionFactory));
        holder.setSynchronizedWithTransaction(true);
        holder.requested();
      } else {
        if (TransactionSynchronizationManager.getResource(environment.getDataSource()) == null) {
          LOGGER.debug(() -> "SqlSession [" + session + "] was not registered for synchronization because DataSource is not transactional");
        } else {
          throw new TransientDataAccessResourceException(
              "SqlSessionFactory must be using a SpringManagedTransactionFactory in order to use Spring transaction synchronization");
        }
      }
    } else {
      LOGGER.debug(() -> "SqlSession [" + session + "] was not registered for synchronization because synchronization is not active");
    }

}
```

在上面**registerSessionHolder**方法中有下面两行

```java
TransactionSynchronizationManager.bindResource(sessionFactory, holder);
TransactionSynchronizationManager.registerSynchronization(new SqlSessionSynchronization(holder, sessionFactory));
```

bindResource这个方法里面有一个`resources`属性，我们看一下这个属性的定义

```java
private static final ThreadLocal<Map<Object, Object>> resources =new NamedThreadLocal<>("Transactional resources");
```

它是使用 ThreadLocal 包装的，绑定到当前线程，所以他可以实现线程安全的。

```java
/**
 * Bind the given resource for the given key to the current thread.
 * @param key the key to bind the value to (usually the resource factory)
 * @param value the value to bind (usually the active resource object)
 * @throws IllegalStateException if there is already a value bound to the thread
 * @see ResourceTransactionManager#getResourceFactory()
 */
public static void bindResource(Object key, Object value) throws IllegalStateException {
	Object actualKey = TransactionSynchronizationUtils.unwrapResourceIfNecessary(key);
	Assert.notNull(value, "Value must not be null");
	Map<Object, Object> map = resources.get();
	// set ThreadLocal Map if none found
	if (map == null) {
		map = new HashMap<>();
		resources.set(map);
	}
	Object oldValue = map.put(actualKey, value);
	// Transparently suppress a ResourceHolder that was marked as void...
	if (oldValue instanceof ResourceHolder && ((ResourceHolder) oldValue).isVoid()) {
		oldValue = null;
	}
	if (oldValue != null) {
		throw new IllegalStateException("Already value [" + oldValue + "] for key [" +
				actualKey + "] bound to thread [" + Thread.currentThread().getName() + "]");
	}
	if (logger.isTraceEnabled()) {
		logger.trace("Bound value [" + value + "] for key [" + actualKey + "] to thread [" +
				Thread.currentThread().getName() + "]");
	}
}
```



### 怎么拿到一个 SqlSessionTemplate

我们知道在 Spring 里面会用 SqlSessionTemplate 替换 DefaultSqlSession，那么
接下来看一下怎么在 DAO 层拿到一个 SqlSessionTemplate。
不知道用过 Hibernate 的同学还记不记得，如果不用注入的方式，我们在 DAO 层
注入一个 HibernateTemplate 的一种方法是什么？
——让我们 DAO 层的实现类去继承 HibernateDaoSupport。
MyBatis 里面也是一样的，它提供了一个 SqlSessionDaoSupport，里面持有一个
SqlSessionTemplate 对象，并且提供了一个 getSqlSession()方法，让我们获得一个
SqlSessionTemplate。

```java
public abstract class SqlSessionDaoSupport extends DaoSupport {

  private SqlSessionTemplate sqlSessionTemplate;

  public SqlSession getSqlSession() {
    return this.sqlSessionTemplate;
  }
```

也就是说我们让 DAO 层的实现类继承 SqlSessionDaoSupport，就可以获得
SqlSessionTemplate，然后在里面封装 SqlSessionTemplate 的方法。
当然，为了减少重复的代码，我们通常不会让我们的实现类直接去继承
SqlSessionDaoSupport，而是先创建一个 BaseDao 继承 SqlSessionDaoSupport。在
BaseDao 里面封装对数据库的操作，包括 selectOne()、selectList()、insert()、delete()
这些方法，子类就可以直接调用。



## 接口的扫描注册

在 Service 层可以使用`@Autowired `自动注入的 Mapper 接口，需要保存在
BeanFactory（比如 XmlWebApplicationContext）中。也就是说接口肯定是在 Spring
启动的时候被扫描了，注册过的。

1、 什么时候扫描的？
2、 注册的时候，注册的是什么？这个决定了我们拿到的是什么实际对象。

回 顾 一 下 ， 我 们 在 applicationContext.xml 里 面 配 置 了 一 个
MapperScannerConfigurer。
MapperScannerConfigurer 实现了 BeanDefinitionRegistryPostProcessor 接口，
BeanDefinitionRegistryPostProcessor 是 BeanFactoryPostProcessor 的子类，可以
通过编码的方式修改、新增或者删除某些 Bean 的定义。

![image-20211026141600245](../../images/21/1026/MapperScannerConfigurer类图.png)

我们只需要重写 postProcessBeanDefinitionRegistry()方法，在这里面操作 Bean
就可以了。
在这个方法里面：
scanner.scan() 方 法 是 ClassPathBeanDefinitionScanner 中 的 ， 而 它 的 子 类
ClassPathMapperScanner 覆 盖 了 doScan() 方 法 ， 在 doScan() 中 调 用 了
processBeanDefinitions：

```java
/**
 * Calls the parent search that will search and register all the candidates.
 * Then the registered objects are post processed to set them as
 * MapperFactoryBeans
 */
@Override
public Set<BeanDefinitionHolder> doScan(String... basePackages) {
  Set<BeanDefinitionHolder> beanDefinitions = super.doScan(basePackages);

  if (beanDefinitions.isEmpty()) {
    LOGGER.warn(() -> "No MyBatis mapper was found in '" + Arrays.toString(basePackages) + "' package. Please check your configuration.");
  } else {
    processBeanDefinitions(beanDefinitions);
  }

  return beanDefinitions;
}
```

它先调用父类的 doScan()扫描所有的接口。
processBeanDefinitions 方法里面，在注册 beanDefinitions 的时候，BeanClass
被改为 MapperFactoryBean（注意灰色的注释）。

```java

private void processBeanDefinitions(Set<BeanDefinitionHolder> beanDefinitions) {
  GenericBeanDefinition definition;
  for (BeanDefinitionHolder holder : beanDefinitions) {
    definition = (GenericBeanDefinition) holder.getBeanDefinition();
    String beanClassName = definition.getBeanClassName();
    LOGGER.debug(() -> "Creating MapperFactoryBean with name '" + holder.getBeanName()
        + "' and '" + beanClassName + "' mapperInterface");

    // the mapper interface is the original class of the bean
    // but, the actual class of the bean is MapperFactoryBean
    definition.getConstructorArgumentValues().addGenericArgumentValue(beanClassName); // issue #59
    definition.setBeanClass(this.mapperFactoryBean.getClass());

    definition.getPropertyValues().add("addToConfig", this.addToConfig);

    boolean explicitFactoryUsed = false;
    if (StringUtils.hasText(this.sqlSessionFactoryBeanName)) {
      definition.getPropertyValues().add("sqlSessionFactory", new RuntimeBeanReference(this.sqlSessionFactoryBeanName));
      explicitFactoryUsed = true;
    } else if (this.sqlSessionFactory != null) {
      definition.getPropertyValues().add("sqlSessionFactory", this.sqlSessionFactory);
      explicitFactoryUsed = true;
    }

    if (StringUtils.hasText(this.sqlSessionTemplateBeanName)) {
      if (explicitFactoryUsed) {
        LOGGER.warn(() -> "Cannot use both: sqlSessionTemplate and sqlSessionFactory together. sqlSessionFactory is ignored.");
      }
      definition.getPropertyValues().add("sqlSessionTemplate", new RuntimeBeanReference(this.sqlSessionTemplateBeanName));
      explicitFactoryUsed = true;
    } else if (this.sqlSessionTemplate != null) {
      if (explicitFactoryUsed) {
        LOGGER.warn(() -> "Cannot use both: sqlSessionTemplate and sqlSessionFactory together. sqlSessionFactory is ignored.");
      }
      definition.getPropertyValues().add("sqlSessionTemplate", this.sqlSessionTemplate);
      explicitFactoryUsed = true;
    }

    if (!explicitFactoryUsed) {
      LOGGER.debug(() -> "Enabling autowire by type for MapperFactoryBean with name '" + holder.getBeanName() + "'.");
      definition.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
    }
  }
}
```



### 为什么要把 BeanClass 修改成 MapperFactoryBean，这个类有什么作用？
MapperFactoryBean 继 承 了 SqlSessionDaoSupport ， 可 以 拿 到
SqlSessionTemplate。通过getObject方法就可以创建出对应的Mapper，下面就是MapperFactoryBean的getObject方法：

```java
 public T getObject() throws Exception {
   return getSqlSession().getMapper(this.mapperInterface);
 }
```

最后调用的方法是：

```java
public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
  return mapperRegistry.getMapper(type, sqlSession);
}
```

这一步我们很熟悉，跟编程式使用里面的 getMapper 一样，通过工厂类
MapperProxyFactory 获得一个 MapperProxy 代理对象。
也就是说，我们注入到 Service 层的接口，实际上还是一个 MapperProxy 代理对象。
所以最后调用 Mapper 接口的方法，也是执行 MapperProxy 的 invoke()方法，后面的
流程就跟编程式的工程里面一模一样了。

| 对象                             | 生命周期                                                     |
| -------------------------------- | ------------------------------------------------------------ |
| SqlSessionTemplate               | Spring 中 SqlSession 的替代品，是线程安全的，通过代理的方式调用<br/>DefaultSqlSession 的方法 |
| SqlSessionInterceptor（ 内部类） | 代理对象，用来代理 DefaultSqlSession，在 SqlSessionTemplate 中使用 |
| SqlSessionDaoSupport             | 用于获取 SqlSessionTemplate，只要继承它即可                  |
| MapperFactoryBean                | 注册到 IOC 容器中替换接口类，继承了 SqlSessionDaoSupport 用来获取<br/>SqlSessionTemplate，因为注入接口的时候，就会调用它的 getObject()方法 |
| SqlSessionHolder                 | 控制 SqlSession 和事务                                       |



## mybatis使用的设计模式

| 设计模式   | 类                                                           |
| ---------- | ------------------------------------------------------------ |
| 工厂       | SqlSessionFactory、ObjectFactory、MapperProxyFactory         |
| 建造者     | XMLConfigBuilder、XMLMapperBuilder、XMLStatementBuidler、SqlSessionFactoryBuilder |
| 单例模式   | SqlSessionFactory、Configuration、ErrorContext               |
| 代理模式   | 绑定：MapperProxy<br/>延迟加载：ProxyFactory（CGLIB、JAVASSIT）<br/>插件：Plugin<br/>Spring 集成 MyBaits：SqlSessionTemplate 的内部类 SqlSessionInterceptor<br/>MyBatis 自带连接池：PooledDataSource 管理的 PooledConnection<br/>日志打印：ConnectionLogger、StatementLogger |
| 适配器模式 | logging 模块，对于 Log4j、JDK logging 这些没有直接实现 slf4j 接口的日志组件，需要适配器 |
| 模板方法   | BaseExecutor 与子类 SimpleExecutor、BatchExecutor、ReuseExecutor |
| 装饰器模式 | LoggingCache、LruCache 等对 PerpectualCache 的装饰<br/>CachingExecutor 对其他 Executor 的装饰 |
| 责任链模式 | InterceptorChain                                             |















