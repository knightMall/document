buffet pool
free
flush
lru

CRUD的时候加载数据到buffer pool.

## 数据更新流程：

1. 加载数据到buffer pool
2. 写undo log
3. 修改数据
4. 写redo log
5. 准备提交事务，然后redo log落盘
6. 准备提交事务，然后binlog落盘
7. redo log落盘和binlog落盘
8. 刷入数据到磁盘

## Buffer Pool大小：
- 默认128M
- 数据库核心数据模型：表、字段、行
- 数据页：多行组成一个数据页，默认大小16k。每个缓存页有一个描述信息包括：
    - 数据页所属表空间
    - 数据页编号
    - 缓存页在buffer pool的地址


## free链表
将buffer pool划分成多块数据页(16K)、描述数据块(800字节)
free（双向链表（指向头尾节点），描述数据块放入链表中）记录空闲的数据块。

## 读取数据到buffer pool.
在free链表中找到空闲的描述数据数据块，将要从磁盘读取的数据块的相关描述信息写入到数据描述块中。最后从free链表中移除这个描述数据块。

## 确认数据页是否被缓存
MySQL中有一个哈希表数据结构，用表空间+数据页编号作为key，缓存页的地址作为value.

## flush链表
组成与free链表类似，让被修改过的描述数据块组成一个链表


## buffer pool基于LRU算法淘汰缓存

### 缓存命中率
引入LRU(Least Recently Use)链表，从磁盘加载数据页到buffer pool都会进入LRU链表的头部。
如果在尾部的数据块，只要修改和查询数据块则该对应的数据块挪动到LRU链表的头部。
如果buffer pool中的free链表为空，则表示buffer pool没有缓存空间。则需要从LRU尾部移除不经常访问的数据


## 基于LRU算法会带来那些问题

### 预读机制
预读机制，从磁盘文件加载一个数据页的时，可能会连着把这个数据页相邻的其他数据页也加载到buffer pool中。


### 预读机制在LRU算法中带来的问题
因为预读机制导致读数据页的时候会将相邻的数据页也加载到buffer pool中，所以读取的相邻数据页存入到buffer pool的头部。
而这些相邻数据并不是经常访问的数据且这个时候buffer pool没有空闲空间，需要清除LRU链表的一些数据吗，而这个时候的尾部正好是经常访问的数据页。所以这种机制会导致经常访问的数据可能会被刷回磁盘


### 哪些情况会触发预读机制
 - innodb_read_ahead_threshold，默认值56。意思是如果顺序访问一个区里的多个数据页，访问的数据页超过这个阈值，此时会触发预读机制，把相邻的数据页加载到缓存中

- 如果buffer pool里缓存了一个区里的13个连续的数据页，而这些数据页都是比较频繁访问的，此时直接触发预读机制。这个规则默认是关闭的


### 全表扫描
全表扫描也会导致频繁被访问的数据页被淘汰。


## 基于冷热数据优化LRU算法

### LRU链接拆分
LRU链接拆分成两段，热数据(7)和冷数据(3)。
第一次把数据加到缓存页放到冷数据段链表的头部。
加载到数据缓存页后，如果在1秒后访问该缓存页才会挪动到热数据区域的链表。

### 基于冷热数据的LRU优化点
由于热数据区域的频繁访问导致数据页的频繁移动，这也是不小的开销。所以只有在热区域后的3/4的缓存页被访问才会移动到链表的头部。

### LRU链接冷数据刷入到磁盘时机
 - 通过定时任务定时刷入
 - 将flush链表中的缓存定时刷入磁盘


### 多线程操作Buffer Pool优化
 - 设置多个buffer Pool，每个buffer pool负责管理一部分的缓存页和描述数据块。
 - buffer pool由多个chunk组成，可以通过这个特性实现动态调整


### 如何合理设置buffer pool的大小
给buffer pool设置机器内存大小的50%-60%
buffer pool的总大小=(chunk大小 * buffer pool数量)*倍数

### 查看innoDB status
执行命令：show engine innodb status


## MySQL物理数据模型


## MySQL存储在磁盘的数据形式

### 变长类型存储
假设一行数据有VARCHAR(5) VARCHAR(5) VARCHAR(20) CHAR(1) CHAR(1), 一共5个字段。里面有三个变长字段
假如这一行数据是:`hello hi hao a a`
此时磁盘中存储的，必须在开头的变长字段列表中存储几个变长字段的长度，这里是逆序存储的。也就是先存放VARCHAR(20)字段的长度，再VARCHAR(5)字段长度，
然后VARCHAR(5)字段长度，所以一行数据实际存储可能如下面：
`0x03 0x02 0x05 null值列表 头字段 hello hi hao a a`


### NULL值存储
加入NULL值列表，以二进制bit位进行存储。
```sql
create Table customer(
  name varchar(10) not null,
  address varchar(20),
  gender char(1),
  job varchar(30),
  school varchar(50)
)Row_format = compact;
```

假设存储了下面一行数据：
`zhang null m null a school`

这一行数据是如何存储的，存储的格式如下：
变长字段长度列表(逆序) NULL值列表（逆序） 头部信息 column1=value1 column2=value2...

变长字段长度列表，如果字段为空则不存储； null值列表使用bit来表示 1说明为空 0说明有值
存储在磁盘的格式如下
0x08 0x05 000000101 头部信息 zhangaschool



### 头部信息介绍
占用40bit
前两位为预留位置
接下来一个bit是delete_mask，标记该行是否被删除
接下来一个bit是min_rec_mask，B+树中每一层的非叶子节点里的最小值都有这个标记
接下来4bit是n_owned，一个记录数
接下来是13bit位是heap_no，表示当前这行数据在记录堆中的位置
接下来3bit位是record_type，表示这行的数据类型，0表示普通类型，1表示的是B+树非叶子节点，2表示的是最小值数据，3表示的是最大值数据
最后16bit位是next_record，表示指向下一条数据的指针



### 行溢出
每一行都是放在数据页中，数据页的默认的大小是16k。如果一行的数据大小超过页的大小怎么办？
例如一个字段的VARCHAR(65532)，这个字段最大可以包含65532字节，远大于16k。
这个时候在当前页存放这行数据，然后这个字段只包含一部分数据，同时包含一个20字节的指针，指向其他的一些数据页，那些数据页串联起来存放这个字段。

### 数据页的结构
包含：
- 文件头（38字节）
- 数据页头（56字节）
- 最小记录（26字节）
- 最大记录（26字节）
- 多个数据行（不固定）
- 空闲空间（不固定）
- 数据页目录（不固定）
- 文件尾部（8字节）


### 表空间
创建的表，其实有一个表空间的概念。在磁盘上都会对应着“表名.idb”，这样一个磁盘数据文件。具体到物理层面，
表空间就是对应一些磁盘上的数据文件。有的表空间对应多个磁盘文件，比如系统表的。用户自己创建的表对应的表空间可能就是对应了一个“表.idb”数据文件。
所以一个表空间的磁盘文件有很多的数据页。



### 数据区
一个表空间包含的数据页实在太多，为方便管理引入了数据区的概念。一个数据区对应着连续的64个数据页，每个数据页是16k，所以一个数据区大小是1MB。 256个数据区划分成一组
每一组的数据区的第一个数据区的前3个数据页，都是存放一些特殊的信息的。



### MySQL数据存储模型及数据读写机制
通过上面对表空间、数据区组、数据区、数据页的概念的介绍，大概可以推断出MySQL的数据存储模型。
一个表对应一个表空间（用户创建的表），表空间找到对应的磁盘文件，磁盘由多个数据区组，一个数据区组包含256个数据区，
一个数据区里面包含64个数据页，一个数据页里面包含若干行数据。
通过语句插入一条数据到磁盘：
 - 先找表空间，就找到了具体的磁盘文件
 - 再找数据区组，就能找到数据区，然后数据页。这个数据页可能是空的也可能有数据，完整的加载这个数据页到buffer pool
 - 添加数据，等待刷盘



### redo log文件
前面介绍过，更新buffer pool的时候，必须写一条redo log记录，记录对数据块的操作。
redo log保证了事务提交了，数据不丢失。

redo log本质上记录的就是对某个空间的某个数据页的某个偏移量的地方修改了几个字节的值，具体修改的值是什么。具体就是：
表空间+数据页号+偏移量+修改几个字节的值+具体的值。

根据修改了数据页里的几个字节的值，redo log划分了不同的类型，MLOG_1BYTE类型就是修改1个字节，MLOG_2BYTE就是修改2个字节。以此类推。
如果是修改的是一大串字符串的值，类型就是MLOG_WRITE_STRING.
redo log看起来大致结构如下：
日志类型, 表空间ID, 数据页号，数据页中的偏移量，修改数据的长度(MLOG_WRITE_STRING类型)，具体修改的数据

### redo log存入磁盘的过程
redo log内容的具体的格式已经了解，那是直接就一条一条写入到磁盘吗，肯定不是的。
MySQL内部还有一种数据结构，redo log block。 redo log block存放多个单行的日志。
一个redo log block是512字节，分成3部分，一个是12字节的header，一个是496字节的body，一个是4字节的trailer。

12字节的header头又分成4个部分：
 - 4个字节的block no，块编号
 - 2个字节的data length,，就是block写入了多少数据
 - 2个字节的first record group。 一个事务可能对应多个redo log，是一个redo log group，即一组redo log。那么这就是第一组在redo log block的偏移量
 - 4个字节的checkpoint on


MySQL执行增删改以后，要写入磁盘的redo log先进入redo log block数据结构，然后再进入到磁盘文件。
其实在中间还有一个组件，redo log buffer是用来缓存buffer log写入的。
redo log buffer其实就是MySQL在启动的时候，跟操作系统申请的一块连续内存空间，然后划分成若干个空的redo log block。
所以当有redo log的时候先写第一个redo log buffer中的redo log block，写满再写下一个redo log block。



### redo log写入磁盘的时机

- 写入redo log buffer的日志大小已经占据了redo log buffer的一半也就是8M，此时会把他们刷入到磁盘文件
- 一个事务提交的时候，必须把这个事务的redo log所属的redo log block都刷入到磁盘文件
- 后台线程定时刷新，有一个线程每1秒就会把redo log buffer里的redo log block刷到磁盘
- MySQL关闭的时候，redo log block都会刷入磁盘

redo log文件大小数量问题：
随着不停的增删改，MySQL产生大量的redo log写入文件。redo log默认的大小是48MB，日志文件数量默认是2个



### undo log回滚日志
对应的就是事务回滚的场景。 记录insert、update、delete操作回滚的操作。


### insert语句undo log日志

insert语句的undo log日志类型是TRX_UNDO_INSERT_REC，这个undo log包含如下内容：
 - 这条日志的开始位置
 - 主键的各列长度和值（删除时指定的主键可能这个主键就一个列，也可能时多列组合的一个主键）
 - 表id
 - undo log日志编号
 - undo log日志类型
 - 这条日志的结束位置

回滚insert语句的具体操作：
通过insert语句在buffer pool的添加了一行数据，执行insert语句，然后写了一条类似上面结构的undo log日志文件。
现在事务需要回滚，直接读取这条undo log日志。通过undo log可以知道在哪个表插入的数据，主键是什么，通过表id和主键可以直接定位到对应的缓存页，从里面删除之前insert进去的数据。这样实现回滚的效果。



### 多事务并发更新及查询
如果多个事务对缓存中的同一条数据同时进行更新或查询，将会出现如下问题：
- 脏写，一个事务修改了某个数据，但过段时间突然就没了。事务B修改了事务A未提交的值，所以事务A随时会回滚，导致事务B修改的值也没了，这就是脏写。
- 脏读，一个事务读取了另一个事务未提交的数据。
- 不可重复读，在同一个事务内，两次读取同一个字段的值不同
- 幻读，在同一个事务内，第二次查询到了第一次没查询到的数据或第二次查询的结果缺少了相对于第一次查询

### SQL标准的事务隔离级别

- 读未提交的（read uncommitted)，不允许脏写
- 读已提交的（read committed)，不允许脏写、脏读
- 可重复读(repeatable read)，不允许脏写、脏读、不可重复读
- 串行化（serializable)，不允许脏写、脏读、不可重复读、幻读


### MySQL的事务级别
MySQL的事务隔离级别和SQL标准事务隔离级别类似，只是在可重复读级别不仅可以实现可重复读还实现了不可幻读。基于MVCC机制实现的



### undo log版本链
每条数据都有两个隐藏字段，一个是trx_id，一个是roll_pointer。这个trx_id就是最近一次更新这条数据的事务id，roll_pointer就是指向你更新这个事务之前生成的undo log.
假如有一个事务A(id=50)，插入一条数据，那么此时trx_id和roll_pointer的指向如下图。trx_id为50，roll_pointer指向一个空的undo log.
![version-chain1](image/undo-log/version-chain1.PNG)

接下来事务B修改了该记录，将值改为B，事务B的id是58，此时会更新当前值并生成一个undo log记录之前的值，
然后trx_id改为58，roll_pointer指向这个回滚的undo log日志，具体如下图:
![version-chain2](image/undo-log/version-chain2.PNG)

接下来事务C修改了该记录，将值改为C，事务C的id是69，此时trx_id改为69，
然后生成一个undo log日志且roll_pointer指向这条log。如下图：
![version-chain3](image/undo-log/version-chain3.PNG)


### 基于undo log实现的ReadView机制
ReadView就是执行一个事务的时候，就会生成一个ReadView，里面比较关键的有4个：
- m_ids，此时有哪些事务在MySQL执行还没提交
- min_trx_id，就是m_ids最小的值
- max_trx_id，就是MySQL下一个要生成的事务id，也就是最大的事务id
- creator_trx_id，就是当前事务的id

#### ReadView运行
假设现在数据库中的某个表中有一条记录，具体如下：

|  原始值   | trx_id=32  |  roll_pointer  |
|  ----  | ----  | ----  |

此时事务A(id=45)，事务B(id=59)并发过来执行。事务A要去读取该数据，事务B要去修改该数据。
现在事务A会开启一个ReadView，这个ReadView里的m_ids就包含了事务A和事务B的两个id。

- m_ids={45,59}
- min_trx_id=45
- max-trx_id=60
- creator_trx_id=45

这时事务A第一次查询该记录时，会进行一个判断，当前这行数据的trx_id是否小于ReadView中的min_trx_id。
此时该行记录的trx_id=32，是小于ReadView里的min_trx_id的。说明事务开启前修改或添加这行的事务早就提交了。
此时可以查询该行记录。
![ReadView1](image/undo-log/ReadView1.PNG)

这时事务B update完成将该行的trx_id修改为59。roll_pointer指向了之前记录的undo log。

![ReadView2](image/undo-log/ReadView2.PNG)

这个时候事务A再次查询，再进行判断，这行数据的trx_id是否小于ReadView中的min_trx_id。
此时该行记录的trx_id=59，是大于ReadView里的min_trx_id的，同时小于max_trx_id。说明更新这条记录的
事务是和当前事务差不多时候开启的，于是查59是不是在m_ids中，比较一下果然在里面。所以这行数据不能查询出来。
那只能顺着roll_pointer找到undo log日志往下找，就会找到最近的一条undo log，trx_id是32。小于min_trx_id
说明undo log在当前事务前就已经提交了。那就可以查询最近的那个undo log里的值，这就是undo log多版本链条的作用，
可以保存一个快照。

![ReadView3](image/undo-log/ReadView3.PNG)

![ReadView4](image/undo-log/ReadView4.PNG)

总结：
ReadView机制就是当某个事务开启时，它只能读取小于或等于该trx_id的记录或undo log镜像的记录，
还有trx_id不在m_ids内说明，生成ReadView的时候该事务已经提交了。


### Read Committed隔离级别是如何基于ReadView机制实现的
RC隔离级别是读已提交的，说明不支持不可重复读、幻读。ReadView机制已经清楚。
核心就在于每次查询的时候重新进行ReadView。就能实现可重复读、幻读。
假如现在事务(trx_id=50)已经将数据提交到数据块并提交了。
现在并发同时执行A(trx_id=60)、B(trx_id=70)两个事务。现在B update了记录，将数值修改为B
trx_id修改为70，roll_pointer指向生成的undo log文件。
这个时候事务A执行了查询，生成的ReadView如下：
- m_ids={60,70}
- min_trx_id=60
- max_trx_id=71
- creator_trx_id=60

进行判断，查询记录的trx_id=70，大于min_trx_id且在m_ids范围内。此时基于ReadView机制此时事务A
无法查询事务B的值。然后通过roll_pointer查询undo log记录的镜像，再找到镜像的trx_id=50，小于min_trx_id
所以可以查询。具体如下图

![read-committed1](image/undo-log/read-committed1.PNG)


这个时候事务B提交了，让事务A再次发起查询，重新生成ReadView，具体如下：
- m_ids={60}
- min_trx_id=60
- max_trx_id=71
- creator_trx_id=60

查询到记录的trx_id=70，虽然在min_trx_id和max_trx_id之间，但并不在m_ids中。所以说明该事务已经在生成ReadView前已经提交
可以进行查询。
![read-committed2](image/undo-log/read-committed2.PNG)


### RR隔离级别是如何基于ReadView实现的

RR可重复读，在MySQL的RR隔离级别下，在同一事务下，无论查询多少次，结果都是相同的。可以避免不可重复读和幻读。

假设现在有一条记录是由trx_id=50添加到数据库的。同时此时由事务A(trx_id=60)和事务B(trx_id=70)同时运行。
![RR1](image/undo-log/RR1.PNG)

此时事务A发起查询，生成ReadView，值如下：
- m_ids={60,70}
- min_trx_id=60
- max_trx_id=71
- creator_trx_id=60

事务A基于上面的ReadView去查询上面的记录，发现记录的trx_id=50，小于min_trx_id。所以说明该记录当前的数据的事务早于ReadView事务的开启时间，可以查询。

![RR2](image/undo-log/RR2.PNG)


接着事务B更新该记录将值修改为B，记录的trx_id=70，roll_pointer指向前一个版本的undo log file。并提交事务。
事务A再次发起查询，事务A的ReadView相关值不会改变。
查询记录的trx_id改变为70，70在事务的ReadView的max_trx_id和min_trx_id之间，并且在m_ids中。说明该事务开启的时间和事务A相同。
所以不能查询，只能通过roll_pointer的undo log查找历史版本。查找到前一个版本的记录的trx_id=50，小于min_trx_id，说明该记录的事务提交早于事务A，
所以可以查询。
![RR3](image/undo-log/RR3.PNG)


如何实现防止幻读的
这个时候事务C添加了一条记录，如下图：
![RR4](image/undo-log/RR4.PNG)

接着事务A继续查询发现返回两条记录，一条是原来的，一条是事务C添加的记录。
但是事务C添加的那条记录的trx_id=80。80是大于事务A的ReadView的max_trx_id，说明事务C是在事务A开启后发起的。
不能查询。所以只能查询原来的记录。规避的幻读的发生。
![RR5](image/undo-log/RR5.PNG)



### 数据库多事务并行运行的锁机制
锁机制，解决就是多个事务同时更新一行数据，此时必须需要一个加锁的的机制。

### MySQL更新一行数据的加锁过程
假设现在事务A要更新一行数据，先判断此时该行数据有没有其他事务加锁。正巧该行数据没有加锁，
此时事务A会创建一个锁，里面包含自己的trx_id和等待状态，然后锁和这行数据关联上。
![lock1](image/undo-log/lock1.PNG)

现在这个时候，事务B也想更新这行数据，此时先检查一下，当前这行数据有没有其他事务加锁。发现事务A已经加锁。
事务B也加锁，生成一个加锁数据结构，里面有事务B的trx_id和等待状态，因为是在排队等待状态，所以等待状态为true。
![lock2](image/undo-log/lock2.PNG)

接下来事务A更新完成并提交事务，释放锁。在释放锁后，会去查找是否有其他事务也对该数据加锁，发现事务B对该数据也加锁了，
修改等待状态为false，然后唤醒事务B继续执行。
![lock3](image/undo-log/lock3.PNG)


### 共享锁和独占锁

独占锁，当一个事务加了独占锁后，其他事务要想再更新该行数据也需要加独占锁，但是只能生成独占锁等待。
当有事务在更新该行记录的时候，其他事务读取该行数数据默认是无需加锁的，直接走MVCC机制。

如果想查询的时候加锁，可以加共享锁，通过`lock in share mode`加锁。
```sql
select * from table lock in share mode
```

共享锁和独占锁的互斥关系：

|  锁类型   | 独占锁  |  共享锁  |
|  ----  | ----  | ----  |
| 独占锁 | 互斥 | 互斥 |
| 共享锁 | 互斥 | 不互斥 |

另外，查询也能加独占锁

`select * from table where id=1 for update`



### 磁盘数据页的存储结构
MySQL中的数据都是存储到磁盘中的，表中的数据是按照数据页的方式存储在磁盘。这个时候会有大量的数据页，
大量的数据页是按照顺序存储的，它们两两相邻的数据页之间会采用双向链表的格式相互引用。

![store1](image/undo-log/store1.PNG)

其实一个数据页在磁盘上就是一段数据，可能是二进制或特殊格式，然后数据中包含两个指针，一个指针指向自己上一个的数据页的物理地址，
一个指针指向自己下一个的数据页的物理地址。和下面的内容类似：
```
DataPage: xx=xx, xx=xx, linked_list_pre_pointer=15367, linked_list_next_pointer=34126
|| DataPage: xx=xx, xx=xx, linked_list_pre_pointer=23789, linked_list_next_pointer=46589
|| DataPage: xx=xx, xx=xx, linked_list_pre_pointer=33198, linked_list_next_pointer=55681
```

在每个数据页内部存储一行行的数据，也就是添加到表中的一行行数据，然后数据页里的每一行数据都会按照主键大小进行排序，
同时每一行都有指针指向下一行的位置，形成单向链表。
![store2](image/undo-log/store2.PNG)

数据页之间组成通过双向链表，数据页内部的数据组成通过单向链表，而且数据行是按照主键从小到大排序。

### 不通过索引查询数据
通过遍历数据页实现数据查找。


### 页分裂的过程
索引运作的一个核心基础是要求后一个数据页的主键值都大于前面一个数据页的主键值。
如果主键是自增长的那么可以保证这一点。但是如果主键是自定义设置的，
可能出现后一个数据页中的数据的主键小于前一个数据页中数据的主键。所以此时需要一个操作，
移动页中的数据，叫做页分裂，挪动两个数据页的数据达到后一个数据页的主键值大于前一个数据页的主键值。


### 主键索引是如何设计的
主键索引就是将每个数据页的页号和这个数据页中的最小的主键id放到一起，组成一个索引的目录。如下图：
![primary_key_index](image/index/primary_key_index.PNG)


### B+树的形成
通过主键索引目录结合二分查找可以快速通过主键查询，但是如果表中的数据很多，百万、千万、上亿都有可能。
这个时候会有大量的数据页，主键索引目录就要存储大量的数据页号和数据页对应的最小主键值。
为解决这个问题采用了将索引存放在数据页的方式，称为索引页。
![primary_key_index2](image/index/primary_key_index2.PNG)

但是同样会出现很多索引页，通过主键id无法确认要到哪个索引页进行查询。
于是再加一个层级，在更高的层级里，保存了每个索引页的最小主键值。如下图：
![primary_key_index3](image/index/primary_key_index3.PNG)

现在假设要查找id等于46的记录，通过上层的索引可以知道到索引20去查找。再对索引20进行二分查找可以很快
在数据页8中找到。
如果这个时候上层索引还是太多怎么办，只能再次分裂，再加一层索引页，如下图：
![primary_key_index4](image/index/primary_key_index4.PNG)

持续分裂，这个时候很像一个倒立的树，这就是B+树。


### 聚簇索引
如果一颗B+树索引数据结构里，叶子节点就是数据页本身，那么此时这颗B+树索引为聚簇索引。
增删改都是直接针对聚簇索引进行操作的。

### 对主键外的其他字段建立索引
其实和建立主键的聚簇索引类似。假设现在要为name字段建立一个索引，那么在添加数据的时候会重新弄一颗B+树，
这颗B+树的的叶子节点也是数据页，但是这个数据页仅仅只存放了name和主键两个字段。排序规则和聚簇索引一致。
![other_key_index1](image/index/other_key_index1.PNG)

name字段的索引B+树里，叶子节点的数据页中的name值都是按大小排序的，下一个数据页里的name字段值都大于上一个数据页
里的name字段值。name字段的索引B+树也会构建多层级的索引页。当前索引页存放的就是下一层的最小name字段值。
假设这个时候要根据name字段来搜索数据，那过程就是：通过name字段的B+树索引的根节点开始找，使用二分查找方法，一层一层
往下找，一直找到叶子节点。定位到name的值和对应的主键值。这个时候仅仅找到的name的值和主键的值。
如果查询记录还有其他的字段，例如查询语句是：`select * from table where name='lilei'`。
则需要进行**回表**再次查询，这个回表就是根据主键值，再到聚簇索引中从根节点直到找到叶子节点，定位到主键
对应的完整记录。
因为通过name索引B+树后找到主键后，还需要根据主键再到聚簇索引中进行二次查询。所以对于这种普通字段的索引
称为二级索引，一级索引就是聚簇索引。

![other_key_index2](image/index/other_key_index2.PNG)

联合索引的运行原理也和二级索引类似，只不过建立独立索引B+树，叶子节点里存放了name+age+id，然后默认按name排序，
如果name相同则按age排序。


### 插入数据的时候如何维护不同的B+树
当新建一张表的时候，开始的时候就是一个数据页，这个数据页属于聚簇索引的一部分，而且还是空的。
添加的数据的时候直接在这个数据添加即可，因为这个时候只有一个数据页也不需要什么索引页，只需要按照主键大小排序存放。
这个数据页也就是根数据页。
随着数据越来越多，一个数据页已经满了，需要再申请一个新的数据页。这个时候需要将根数据页的数据拷贝按照主键大小进行挪动。
达到第二个数据页的主键都大于第一个数据页的主键。
此时的根页就需要进行升级，升级为索引页，包含的是两个数据页中的最小主键id和数据页号。
![other_key_index3](image/index/other_key_index3.PNG)

随着数据添加的越来越多，索引的记录也将越来越多。索引这个时候也需要新创建一个，对索引页进行分裂。
一个索引页分裂成两个。索引是按照主键从小到大排序的。这个时候根页继续往上提一个层级，将分裂的两个数据页
中的主键最小的和对应的索引页号。当随着数据增加，数据页增多、索引页增加、根索引页的数据越来越多。
继续进行分裂，往上再提一个层级，以此类推。这就是增删改的时候聚簇索引维护的一个过程。二级索引也类似
![other_key_index4](image/index/other_key_index4.PNG)


### 联合索引查询原来及全值匹配规则
在设计系统时一般都是设计联合索引，很少使用单个字段作为索引。假设现在有一张学生表，
主键id是自增长的，主键会建立一个聚簇索引。还包含：学生班级、学生姓名、科目名称、成绩
平常比较多的查询是通过：学生班级、学生姓名和科目名称查询。
所以通过学生班级、学生姓名、科目名称建立联合索引。
假设现在有两个数据页
![combination_key_index1](image/index/combination_key_index1.PNG)

第一个数据页包含三条记录，每条记录包含联合索引的三个字段和主键值，数据内部是按照顺序排列的。
首先按照学生班级值来排序，如果相同则按照学生姓名排序，如果再相同则按照科目名称排序。并组成单链链表
而且数据页之间也是有顺序的，数据页2的值大于数据页1。 并组成双向链表
索引页中只有两条记录，分别是第一个数据页的最小记录并指向了对应的数据页，第二个数据页的最小值并指向对应的数据页。
索引页中的数据页也是有序并组成单链链表。多个数据页之间组成双向链表


#### 查询过程
现在假设要执行如下查询：
`select * from student_score where class_name='1班' and student_name='张小强' and subject_name='数学'`
此时会涉及到一个索引使用规则，首先查询语句都是等值查询，where语句中的几个字段的名称和顺序和联合索引一致。
上面的SQL语句可以完全使用联合索引来查询。
查询过程：
首先在索引页中查询，因为索引页中存放的是数据页中的最小值记录，此时直接在索引页中使用二分查找。定位到`1班`的数据页，
![combination_key_index2](image/index/combination_key_index2.PNG)
再在数据页1中通过二分查找，先按`1班`条件查询比较，发现记录都是`1班`。此时在按照姓名`张小强`进行二分查询，
此时会发现多条记录的姓名字段都是`张小强`，接着按照科目名称`数学`进行二分查找。找到记录后获取主键id，再通过
聚簇索引进行回表查询获取完整记录。

### 索引使用规则
- 等值匹配规则，上面已经介绍

- 最左则匹配规则，例如现在有联合索引KEY(class_name, student_name, subject_name)，其实只要根据最
左则部分字段来查询也是可以的。例如：`select * from student_score where class_name='班1' and student_name='张小强'`
但是如果写出`select * from student_score where subject_name='数学'` 这样是不行的，因为联合索引的B+树中，
是必须先按`class_name`查，再按`student_name`查，不能跳过前面两个字段。
如果写成`select * from student_score where class_name='班级1' and subject_name='数学'`，那么只有
class_name查询条件可以使用索引，剩下的subject_name无法使用索引的，同理。

- 最左前缀匹配规则，即使用查询关键字`like`的时候，比如 `select * from student_score where class_name like'1%'`，
查找所有1打头的班级，这样是可以通过索引查询的。但是如果写出`class_name like '%班'`，那就无法使用索引了。
因为你不知道左则前缀是什么，无法在索引页中查询。

- 范围查找规则，意思就是通过字段的范围查询，`select * from student_score where class_name>'1班' and class_name<'5班'`。
这个时候会使用到索引，因为索引的最下层的数据页是按照顺序组成的双向链表。先找到`1班`对应的数据页，再找到`5班`对应的数据页，
两个数据页中间的的数据页就是范围查询的数据。
但是如果写成这样，`select * from student_score where class_name>'1班' and class_name<'5班' and student_name>'张小强'`
只能`class_name`字段能使用到索引，其他字段无法使用索引查询。这里还有一条规则，如果where语句中使用了范围查询，
那只有对联合索引里最左则的列进行范围查询才能用到索引。

- 等值匹配+范围匹配的规则，如下sql语句`select * from student_score where class_name='1班级' and student_name>'张晓强' and student_name<'张中'`
那么此时可以使用`class_name`字段在索引中定位到一部分数据，接下来对这波数据按照`student_name`进行排序，索引`student_name>'张晓强'`会基于索引查找的，
但是接下来的`student_name<'张中'`不能使用索引。


### SQL排序的时候使用索引
因为组合索引的字段值都是按照由小到大依次排序的，所以在SQL语句中最好按照索引字段值的顺序进行`order by x,y,z`排序。
或全部是都DESC降序排列，就是`order by x desc,y desc, z desc`。
如果在`order by` 语句中既有升序又有降序则无法使用索引。
还有排序的字段不在索引或使用了复杂的函数，也不能使用索引。


### SQL分组的时候，如果使用索引
分组语句类似于：
`select count(*) from table group by xx`
因为索引是按照大小顺序进行排序，那么相同的数据的值肯定都在一起，所以可以直接提取一组一组的数据，
然后进行分组函数计算。对于group by后面的字段最好也是按照索引字段顺序。

### 回表查询对性能的影响
一个索引都对应一颗单独的索引B+树。如果不是聚簇索引的B+树的节点仅仅包含主键及索引字段的值。
如果通过`select *` 查询则还需要其他字段，这个时候会执行一个回表操作，通过主键到聚簇索引再次查询。
这个时候MySQL的执行引擎甚至有可能认为，类似于`select * from table1 order by xx1,xx2,xx3`。
表示需要先通过联合索引找到主键，接着每一条记录再通过聚簇索引查询结果。两个索引都需要扫描一遍，那还不如
不走索引，直接全表扫描，这样只扫描一个索引而已。

### 覆盖索引
覆盖索引，一种基于索引查询的方式。
意思是针对类似于`select xx1,xx2,xx3 from table order by xx1,xx2,xx3`这样的语句
仅仅需要联合索引的几个字段的值，那么其实就只要查询联合索引就可以，不需要回表去聚簇索引里查找其他字段。
所以这个时候，需要的字段值只需要扫描索引树就可以了，不需要回表去聚簇索引里面查找其他字段，这就是覆盖索引。


### 设计索引时，需要考虑的因素
- 第一条，针对SQL里的where条件，order by条件以及group by条件去设计索引。

- 第二条，一般建立索引，尽量使用那些基数比较大的字段，就是值比较多的字段，这样才能发挥出B+树二分查找的优势。

- 第三条，在查询语句的字段使用了函数不能使用索引

- 第四条，索引不要设计太多，建议两三个联合索引就应该覆盖掉这个表的查询(因为索引的值都是有顺序的，
如果太多的值可能会设计到太多的页分裂)
- 第五条，建议主键用自增的，不要使用UUID之类的。因为自增起码聚簇索引不会频繁的分裂。

### 前缀索引
针对字段的值过长，只使用字符的部分值作为索引。例如假设一个字段类型是`varchar(255)`，如果针对整个字段的进行索引
索引字段过长，可以仅仅针对前20个字符建立索引，也就是说将这个字段的值前20个字符放入到索引树中。
此时建立索引如下：`KEY my_index(name(20),age,course)`。此时在where条件里索引的时候，如果根据name字段来搜索，
那么此时会先到索引树里面根据name字段的前20个字符去搜索，定位到之后前20个字符的前缀匹配数据之后，再回到聚簇索引提取出完整的name
字段的值进行完整匹配。
但是如果order by name，因为name字段在索引树只包含前20个字符，所以这个排序是没法用上索引的。
group by也是同理。


### 执行计划包含哪些内容
根据索引直接可以快速查找数据的过程，在执行计划中称为`const`。例如：`select * from table where id=1`，
直接通过主键查询。或`select * from table where name='zhangsan'`，通过二级索引+聚簇索引回源查询到数据。
这里二级索引必须是unique key唯一索引。

如果是是一个普通的二级索引，在执行计划里称为`ref`。

这里还有例外，如果用name is NULL这种语法，即使name是主键或者唯一索引，还是只能走`ref`方式。
如果是针对一个二级索引同时比较了一个值还限定了is NULL，类似于`select * from table where name=x or name is NULL`。
那么此时在执行计划里叫做`ref_or_null`。

如果是范围查询那么这种方式就是`rang`，比如SQL是`select * from table where age >=10 and age <=40`

只需要通过二级索引就可以拿到查询数据，而不需要回源到聚簇索引的访问方式，叫做`index`访问方式。例如：
假设有一张表的索引是KEY(x1,x2,x3)，查询语句是：`select x1,x2,x3 from table where x2=xxx`
这里where查询的条件不能使用索引，因为不匹配最左侧的字段。但是查询的结果集的字段和索引的字段一样，
所以会直接遍历索引匹配所有x2=xxx的记录

如果使用多个索引查询取交集或并集，有时候一个SQL可能会用到多个索引，例如 `xx1=xx or xx2=xx` 的语句。

总结：

**const**：通过主键或唯一索引查询的时候。<br/>
**ref**：通过一个普通的二级索引，或查询的字段的条件包含`name is NULL` 这种语法且字段是主键或unique key索引。<br/>
**ref_or_null**：查询的字段的条件包含`name is NULL` 这种语法且字段是二级索引。<br/>
**rang**：通过范围查询索引字段时。<br/>
**index**：只需要通过二级索引就可以拿到查询数据，而不需要回源到聚簇索引的访问方式。<br/>
**all**：全表扫描。<br/>
**intersection** ：交集。<br/>
**union**：并集。<br/>


### 多表关联的SQL如何运行
多表关联的SQL：
`select * from t1,t2 where t1.x1='xxx' and t1.x2=t2.x2 and t2.x3='xxx'` 查询过程可能是：
首先根据条件`t1.x1='xxx'` 进行筛选，去t1表查询数据，测试可能是const、ref，也可能是index或all，主要看索引如何建立。
假设条件`t1.x1='xxx'`筛选出两条记录，然后依次使用这两条数据的x2字段的值和`t2.x3='xxx'`条件去t2表里查找t2表的x2字段和
t2表的x3字段为`xxx`的数据，然后将数据和t1表的数据关联起来。
这里将先从一个表里查询一波数据，这个表称为`驱动表`，再根据这波数据去查找另一个表进行关联，这个表称为`被驱动表` 。


### 执行计划和执行成本

执行成本解释：跑一个SQL，一般有两个成本，将数据读到内存中，因为MySQL的数据都是按照数据页进行存储的，一页页的读取。
读一页的成本大约为1.0。
还有另一个成本，对数据做一些运算，比如比较数据是不是满足条件，或进行排序和分组之类的，属于CPU成本，一般约定读取和检测一条数据
是否符合条件的成本为0.2。
当执行如下SQL时：`select * from t1 where x1='xxx' and x2='xxx'`。 此时有两个索引分别针对x1和x2。
SQL执行引擎，判断查询条件是否能使用索引，可以使用索引的IO成本和CPU成本各是多少。
SQL执行引擎对全表扫描，全表扫描的时候会针对索引中的所有叶子节点读取到内存中，这里有多少数据页就需要耗费多少IO成本，接着对内存中的每一条数据进行判断
是否符合搜索条件，有多少条数据就要耗费多少CUP成本。
通过如下命令：
`show table status like '表名'`
可以拿到表的统计信息，对表进行增删改的时候，MySQL会维护这个表的统计信息，包含：row和data_length两个信息，不过对于InnoDB引擎来说
这些都是估计值。
**row**：表示表的记录数。
**data_length**：表示聚簇索引的字节数大小，使用该值除于1024就是KB为单位的大小，然后再除于16K（默认数据页一页的大小），可以得到数据页的数量。


### 索引查询成本计算
通过索引查询数据，分为通过聚簇索引直接查询和通过二级索引查询。通过聚簇索引查询只需查询一次即可。使用二级索引查询还需要进行回表。
二级索引的查询的成本计算：
查询成本计算：查询条件涉及到几个范围，比如name的值在25~100，250~350两个区间，那么就是两个范围，否则name=xx就是一个区间范围。
一般一个范围就认为一个数据页。读取一个数据页的IO成本是1.0。然后还有条件判断的成本这里主要是CPU的成本，每条数据的大概成本是0.2。
当拿到满足条件的数据后，就得再次回表进行查询完整数据。假设回表的记录数是100条记录，每条数据都需要访问一个数据页，总共需要100*1.0=100.0的IO
成本。所以整个成本是，假设查询100条记录：1+0.2*100+100*1=121。


### 子查询的执行计划如何执行的
有如下语句：
`select * from t1 where x1=(select x1 from t2 where id=xxx)`
上面的语句执行分为两个步骤，第一个步骤先执行子查询`select x1 from t2 where id=xxx`，然后再通过子查询语句查询的结果作为条件执行
`select * from t1 where x1=子查询的结果`。
还有`in`关键字的的子查询，具体如下：
`select * from t1 where x1 in (select x2 from t2 where t2.x3=xxx)`
先执行子查询也就是`select x2 from t2 where t2.x3=xxx`，把查询的数据写入到一个临时表，也可以叫做物化表，意思就是中间结果及进行物化。
这个物化表可能基于memory存储引擎，如果数据量太大，则可能采用普通的B+树聚簇索引存放在磁盘。需要注意这个物化表建立了索引。接下来需要判断
t1表的数据的结果集的数量和物化表的数据集的数量，如果t1表的记录数是100万，而物化表是500条，则会对物化表进行全表扫描，判断每一条记录是否和
t2表中是否存在，如果存在则提取出来。否则反之。

### 通过explain得到SQL的执行计划
假设执行如下语句：
`explain select * from t1` 结果如下：

|  id   | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
|  ---- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1     | SIMPLE       | t1      | NULL         | ALL   | NULL          | NULL| NULL    | NULL| 3457 | 100.00   | NULL  |

**id**：查询id
**select_type**：查询的类型：SIMPLE（如果是）, PRIMARY, SUBQUERY, UNION
**table**：表名
**partitions**：
**type**：使用的索引类型，const、ref、eq_ref、ref_or_null
**possible_keys**：可能使用到的索引
**key**：实际使用到的索引
**key_len**：实际使用索引的长度
**ref**：表示和索引列匹配的是什么？是等值匹配一个常量值？还是等值匹配一个字段的值？
**rows**：表示查询会查出多少记录，这是个估计的值
**filtered**：表示按照查询方式查询数据再用上其他的不在索引范围内的字段的查询条件，会过滤出来百分之的数据。
**extra**：

下面是多表关联的执行计划，SQL语句如下：
`explain select * from t1 join t2`

|  id   | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
|  --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1     | SIMPLE       | t1      | NULL         | ALL   | NULL          | NULL| NULL    | NULL| 3457 | 100.00   | NULL  |
| 1     | SIMPLE       | t2      | NULL         | ALL   | NULL          | NULL| NULL    | NULL| 4568 | 100.00   | Using join buffer(Block Nested Loop)  |

这是个关联查询，关联了两张表。所以查询了两次，查询表t1的时候使用的是全表扫描，共有3457条记录。
接着对表t2进行全表扫描，因为这个join没有条件所以是笛卡尔积，t1表的每条记录都会去t2表全表进行扫描。在extra字段是Nested Loop，也就是嵌套循环的访问方式。
而id都是1，表明就一个select语句。

子查询的执行计划，语句如下：
`explain select * from t1 where x1 in (select x1 from t2) or x3='xxx'`

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | PRIMARY      | t1      | NULL         | ALL   | index_x3      | NULL| NULL    | NULL| 3457 | 100.00   | Using where  |
| 2   | SUBQUERY     | t2      | NULL         | INDEX | index_x1      | index_x1| 507 | NULL| 4568 | 100.00   | Using index  |

因为这里有两个SELECT语句，所有id不同。第一条的select_type是PRIMARY，并不是SIMPLE，说明第一个执行计划的查询类型是主查询的意思，
对主查询而言，因为有`x3='xxx'`查询条件，所以possible_keys中是index_x3，就是可能会使用x3字段的索引，但是key字段为NULL，type为ALL，
说明并没有使用索引index_x3，而是选择了全表扫描。
接着第二条执行计划，select_type是SUBQUERY，说明是子查询，子查询查询的是t2表，子查询本身是全表扫描，但是对于主查询来说，会使用`x1 in`这个
条件进行查询，这里的type是index说明会使用index_x1索引，直接扫描t2的index_x1索引，来和子查询进行比对。

union SQL的执行计划，SQL如下：

`explain slect * from t1 union select * from t2`

执行计划如下：

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | PRIMARY      | t1      | NULL         | ALL   | NULL          | NULL| NULL    | NULL| 3457 | 100.00   | NULL  |
| 2   | UNION        | t2      | NULL         | ALL   | NULL          | NULL| NULL    | NULL| 4568 | 100.00   | NULL  |
| NULL| UNION RESULT | <UNION 1,2>| NULL      | ALL   | NULL          | NULL| NULL    | NULL| NULL | 100.00   | Using temporary  |

执行计划第一和第二条很好理解，两个执行计划对于两个id，分别从t1和t2表全表扫描查询数据。
union关键字的作用是将结果集取交集并去重，table是<UNION 1,2>，这是一张临时表，而且在extra中是，using temporary，也就是使用临时表的意思。


再看一条复杂的SQL语句：
`EXPLAIN SELECT * FROM t1 WHERE x1 IN(SELECT x1 FROM t2 WHERE x1='xxx' UNION SELECT x1 FROM t1 WHERE x1='xxx')`
执行计划如下：

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | PRIMARY      | t1      | NULL         | ALL   | NULL          | NULL| NULL    | NULL| 3467 | 100.00   | NULL  |
| 2   | DEPENDENT SUBQUERY| t2 | NULL         | index | index_x1      | index_x1| 899 | const | 59 | 100.00   | Using where,Using index |
| 3   | DEPENDENT UNION | t1   | NULL         | index | index_x1      | index_x1| 899 | const | 45 | 100.00   | Using where,Using index |
| null| UNION RESULT    | <union 2,3>  | NULL | ALL   | NULL          | NULL | NULL   | NULL  | NULL | 100.00   | Using temporary |

第一个查询是查询表t1，因为是主查询，类型是PRIMARY。
第二个查询是查询表t2，类型是dependent subQuery。
第三个查询是查询表t3，因为是在union后面的查询，所以类型是dependent union。
第四个查询是对查询结果进行去重合并，类型是union result。


下面的SQL涉及到派生表：
`EXPLAIN SELECT * FROM (SELECT x1,COUNT(*) AS CNT FROM t1 GROUP BY x1) as _t1 where CNT>10`

执行计划如下：

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | PRIMARY      | <DERIVED2> | NULL       | ALL   | NULL          | NULL| NULL    | NULL| 3467 | 33.00    | Using where  |
| 2   | DERIVED      | t1      | NULL         | index | index_x1      | index_x1| 899 | const | 3568 | 100.00 | Using index |

先看第二个查询的类型是derived，意思是说，针对这个查询将物化一张内部临时表，然后外查询针对这张临时表进行查询的。在执行分组查询的时候，是会
使用index_x1这个索引的，type是index，说明只需要在索引的叶子节点进行扫描，相同的x1的值统计出来即可。
第一个查询的type是primary，table是derived2，表示这个查询是从物化表中查询的。



### 执行计划中的ref字段
ref字段表示和索引列匹配的是什么？是等值匹配一个常量值？还是等值匹配一个字段的值？ 具体例子如下：
`explain select * from t1 where x1='xxx'`

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | SIMPLE       | t1      | NULL         | ref   | index_x1      | index_x1| 589 | const | 456 | 100.00  | Using where  |

上面的语句的type是ref说明使用的是二级索引并需要进行二次回表获取信息。ref字段的值是const，意思表示通过一个常量与index_x1进行等值匹配。

下面是一个稍微复杂一点的SQL，
`explain select * from t1 inner join t2 on t1.id=t2.id`  执行计划如下：

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | SIMPLE       | t1      | NULL         | all   | primary       | null| null    | null| 3456 | 100.00   | null  |
| 1   | SIMPLE       | t2      | NULL         | eq_ref| primary       | primary | 10  | testdb.t1.id| 1 | 100.00   | null |

执行计划是先全表扫描表t1，接着按照查询出来的t1表的id与t2表的主键进行等值匹配，所以t2表的ref是eq_ref，意思是被驱动表基于主键进行等值匹配，
而且使用的索引是primary。
ref，因为是与t1表对每一条主键进行等值匹配，所以肯定不是const，是与testdb.t1.id进行关联。

### row和filtered字段说明

**rows**：表示按照索引查询出多少条记录数
**filtered**：表示按照查询方式查询数据再用上其他的不在索引范围内的字段的查询条件，会过滤出来百分之的数据。

如下SQL：
`explain select * from t1 where x1>'xxx' and x2='xxx'`

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | SIMPLE       | t1      | NULL         | range | index_x1      | index_x1| 456 | NULL| 1978 | 13.00    | Using index condition，Using where  |

如上的查询计划是：查询使用的索引类型是range，使用的索引是index_x1，然后基于索引index_x1的条件x>'xxx'过滤出来的是记录数是1978，接着会对
1978的记录集进行where的其他条件进行过滤这里就是x2='xxx'进行过滤，过滤的百分比大概是13%，所以查询出来的大概记录数是：1978*0.13=258条左右。


### extra字段表示什么

比如下面的SQL
`explain select x1 from t1 where t1='xxx'` ，执行计划可能如下：

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | SIMPLE       | t1      | NULL         | ref   | index_x1      | index_x1| 456 | const| 25  | 100.00    | Using index |

这里的extra字段的值using index说明，仅需要使用索引就可以查询出数据无需二次回表查询。

如果是如下SQL：
`explain select * from t1 where x1>'xxx' and x1 like'xxx%'` 此时的查询是：
优先使用索引index_x1进行过滤，过滤出来的结果在进行x1 like 'xxx%'条件查询。这种情况下的extra显示的是：Using index condition。


下面的SQL使用的就是`Using where`
`select * from t1 where x2='xx'` x字段是没有索引的。执行计划如下：

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | SIMPLE       | t1      | NULL         | all   | null          | null| null    | null| 4578 | 15.00    | Using where |

针对表t1进行全表扫描，没有使用任何索引，扫描出4578条记录，通过条件`where x2='xx'` 进行过滤后的数据是：4578*0.15大概686条记录。
如果where条件中一个条件可以使用索引查询，有一个列是普通的列的筛选，类似如下SQL：
`explain select * from t1 where t1='xxx' and t2='xxx'`

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | SIMPLE       | t1      | NULL         | ref   | index_x1      | index_x1| 458 | const| 250 | 18.00    | Using where |

还有一种是join buffer的内存技术，如果是两个表关联的条件并不是索引，此时会使用一种叫做join buffer的内存技术来提升关联的性能。例如如下语句：
`explain select * from t1 inner join  t2 on t1.x2=t2.x2` 执行计划如下：

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | SIMPLE       | t1      | NULL         | all   | null          | null| null    | null| 4578 | 100.00   | null  |
| 1   | SIMPLE       | t2      | NULL         | all   | null          | null| null    | null| 3472 | 1.00   | Using where, Using join buffer  |

因为是join操作，先查询表t1的所有x2字段的值，使用全表扫描，查询出4578条记录，然后每条记录的x2的值再到表t2表进行全表扫描，这是使用的是
where条件。


### extra的using filesort是什么意思

假设有如下SQL：
`explain select * from t1 order by t1 limit 10` 执行计划如下：

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | SIMPLE       | t1      | NULL         | index   | null         | index_x1| 458| null| 10   | 100.00   | null  |

这个SQL使用了索引进行排序的。如果排序的字段用的不是索引字段呢，如下SQL：

`explain select * from t1 order by t2 limit 10` 执行计划如下：

x2字段是没有索引的

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | SIMPLE       | t1      | NULL         | all   | null          | null| 458     | null| 4578 | 100.00   | Using filesort  |

通过x2字段进行排序是无法使用索引的，只能将数据写入一个临时的磁盘文件，基于排序算法在磁盘文件按照x2字段的值完成排序，然后再按照limit 10取出
头10条数据。



### extra的using temporary是什么意思
当使用group by、union、distinct之类的语法时，如果没法直接使用索引来进行聚合，那么会直接基于临时表来完成，将会有大量的磁盘操作。例如如下SQL：

`explain select x2,count(*) from t1 group by x2` 这里x2没有索引，执行计划如下：

|  id | select_type  |  table  | partitions   | type  | possible_keys | key | key_len | ref | rows | filtered | extra |
| --- | ------------ | ------  | ------------ | ----- | ------------- | --- | ------- | --- | ---- | -------- | ----- |
| 1   | SIMPLE       | t1      | NULL         | all   | null          | null| 458     | null| 4578 | 100.00   | Using temporary  |


**在SQL调优的时候，核心就是分析执行计划里哪些地方出现了全表扫描，或扫描数据过大，尽可能通过合理优化索引保证执行计划每个步骤都可以基于索引。**





















































































































































































































































































































































































































































































































































