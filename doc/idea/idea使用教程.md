- [设置剪贴板数量](#设置剪贴板数量)
- [Live Template](#live-template)











## 设置剪贴板数量

通过`Editor->General` 设置，如下图

![剪切板保存数量](../../pic/idea/剪切板保存数量.jpg)

通过快捷键`ctrl+shinf+v` 调出剪切板来方便选择复制。



## Live Template

实时模板

![live_template实时模版](../../pic/idea/live_template实时模版.jpg)





