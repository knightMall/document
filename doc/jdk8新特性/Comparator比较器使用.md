



## 比较器简介

`Comparator` 接口在jdk1.2已经引入。在jdk8中加入了一些默认方法和静态方法。

基本使用如下：

```java
// 通过指定字符串的长度按照降序排序        
List<String> list = Arrays.asList("nihao","hello","welcome");
Collections.sort(list,((item1,item2 )-> item2.length()-item1.length()));
```

也可以使用指定的类型的比较器

```java
// 项目和上面的一样
Collections.sort(list, Comparator.comparingInt(String::length).reversed());
System.out.println(list);
```





## 多个比较器组合

多个比较器组合可以使用`Comparator` 接口的默认方法`thenComparing` 组合多个比较器。

```java
System.out.println("------------多个比较器组合------------");
// 先按照长度排序如果长度相同，则再按照ASC码排序
Collections.sort(list,Comparator.comparingInt(String::length).thenComparing(String.CASE_INSENSITIVE_ORDER));
System.out.println(list);
```









