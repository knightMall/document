- [流的概念介绍](#流的概念介绍)
- [Stream对象的创建方式](#stream对象的创建方式)
- [流对象转换成集合对象](#流对象转换成集合对象)
  - [流转换成数组集合](#流转换成数组集合)
  - [流转换成List集合](#流转换成list集合)
  - [流转换成Set集合](#流转换成set集合)
  - [流操作字符串](#流操作字符串)
- [集合转换成流对象](#集合转换成流对象)
  - [数组转换成流对象](#数组转换成流对象)
  - [集合转换成流对象](#集合转换成流对象)
- [Stream的Map方法使用](#stream的map方法使用)
- [Stream的使用示例](#stream的使用示例)
- [Stream的使用注意点](#stream的使用注意点)
- [内部迭代和外部迭代](#内部迭代和外部迭代)
- [集合和Stream的区别](#集合和stream的区别)
- [Stream的分组操作类型](#stream的分组操作类型)
- [Stream的分区操作类型](#stream的分区操作类型)
- [Collectors方法的使用](#collectors方法的使用)
- [练习题](#练习题)
- [自定义Collector](#自定义collector)
  - [Collector接口中各个方法讲解](#collector接口中各个方法讲解)
    - [supplier方法](#supplier方法)
    - [accumulator方法](#accumulator方法)
    - [combiner方法](#combiner方法)
    - [finisher方法](#finisher方法)
    - [characteristics方法](#characteristics方法)
    - [验证](#验证)
- [流的底层分析](#流的底层分析)
  - [中间操作方法底层分析](#中间操作方法底层分析)







## 流的概念介绍

流由3部分构成：

- 一个源，源可以是数组、集合、`a generator function`、`I/O channel`
- 零个或多个中间操作，再次转换为另外一个流。例如：`filter`
- 终止操作，例如 `count`



中间操作

|  操 作   | 类 型 |  返回类型   |     操作参数     |    函数描述     |
| :------: | :---: | :---------: | :--------------: | :-------------: |
|  filter  | 中间  | `Stream<T>` |  `Predicate<T>`  |  `T->boolean`   |
|   map    | 中间  | `Stream<T>` | `Function<T, R>` |    `T -> R`     |
|  limit   | 中间  | `Stream<T>` |                  |                 |
|  sorted  | 中间  | `Stream<T>` | `Comparator<T>`  | `(T, T) -> int` |
| distinct | 中间  | `Stream<T>` |                  |                 |



流的特点：

- 流不存储值，通过管道的方式获取值
- 本质是函数式的，对流的操作会生成一个结果，但并不会修改底层的数据源，集合可以作为流的底层数据源
- 延迟查找，很多流的操作(过滤、映射、排序)都是延迟实现的



## Stream对象的创建方式

具体有如下方式：

```java
        // 通过 Stream的of方式创建
        Stream<String> stream1 = Stream.of("hello", "world", "hello world");

        // 通过Arrays类的静态方法stream创建
        String[] arrays = {"hello", "world", "hello world"};
        Stream<String> stream2 = Arrays.stream(arrays);

        List<String> stringList = Arrays.asList(arrays);

        // 通过List集合创建
        Stream<String> stream3 = stringList.stream();
```



## 流对象转换成集合对象

### 流转换成数组集合

```java
        Stream<String> stream1 = Stream.of("hello", "world", "hello world");
        //String[] strings = stream1.toArray((length -> new String[length]));
        String[] strings = stream1.toArray(String[]::new);
        Arrays.asList(strings).forEach(System.out::println);
```



### 流转换成List集合

```java
        System.out.println("----------------------------");
        Stream<String> stream1 = Stream.of("hello", "world", "hello world");
        List<String> list = stream1.collect(Collectors.toList());
        list.forEach(System.out::println);
        // 转换成LinkedList
        System.out.println("-------------------");
        Stream<String> stream2 = Stream.of("hello", "world", "hello world");
        LinkedList<Object> list5 = stream2.collect(LinkedList::new, (list1, list2) -> list1.add(list2), (list3, list4) -> list3.addAll(list4));
        list5.forEach(System.out::println);
        //另外一种创建List的方法
        System.out.println("----------------");
        Stream<String> stream3 = Stream.of("hello", "world", "hello world");
        ArrayList<String> stream3List = stream3.collect(Collectors.toCollection(ArrayList::new));
        stream3List.forEach(System.out::println);
```



### 流转换成Set集合

```java
        //转换成Set集合
        System.out.println("-----------------");
        Stream<String> stream4 = Stream.of("hello", "world", "hello world");
        HashSet<String> stream4Set = stream4.collect(Collectors.toCollection(HashSet::new));
        stream4Set.forEach(System.out::println);
```



### 流操作字符串

```java
        //字符串的拼接
        System.out.println("-----------------");
        Stream<String> stream5 = Stream.of("hello", "world", "hello world");
        System.out.println(stream5.collect(Collectors.joining("-")));
```







## 集合转换成流对象

### 数组转换成流对象

```java
String[] stringArray = {"hello ", "welcome", "world", "hello", "hello","world","hello", "hello","welcome"};
Stream<String> stream = Arrays.stream(stringArray);
```



### 集合转换成流对象

```java
List<String> strings = Arrays.asList("hello welcome", "world hello", "hello world hello", "hello welcome");
Stream<String> stream = strings.stream();
```









## Stream的Map方法使用

Map方法可以对集合进行映射

```java
// 集合的元素转换成大写输出
List<String> strings = Arrays.asList("hello", "world", "hello world", "test");
strings.stream().map(String::toUpperCase).forEach(System.out::println);

System.out.println("--------------------");
// 将集合进行打平输出
Stream<List<Integer>> streamList = Stream.of(Arrays.asList(1),Arrays.asList(2,3),Arrays.asList(4,5,6));
streamList.flatMap(items->items.stream()).map(item->item * item).forEach(System.out::println);

List<String> list1 = Arrays.asList("Hi", "Hello", "你好");
List<String> list2 = Arrays.asList("zhangsan", "lisi", "wangwu", "zhaoliu");

// 实现功能 "Hi zhangsan","Hello zhangsan","你好 zhangsan"
List<String> collect = list1.stream().flatMap(item1 -> list2.stream().map(item2 -> item1 + " " + item2)).collect(Collectors.toList());
collect.forEach(System.out::println);
// 输出
Hi zhangsan
Hi lisi
Hi wangwu
Hi zhaoliu
Hello zhangsan
Hello lisi
Hello wangwu
Hello zhaoliu
你好 zhangsan
你好 lisi
你好 wangwu
你好 zhaoliu
```



## Stream的使用示例

```java
// 获取流的中的大于2的数据，并且将流中的数据乘以2，并忽略前两个元素，再取出前两个元素求和并输出
Stream<Integer> stream = Stream.iterate(1, item -> item + 2).limit(6);
System.out.println(stream.limit(6).filter(item1 -> item1 > 2).mapToInt(item2 -> item2 * 2).skip(2).limit(2).sum());
```





## Stream的使用注意点

流使用后或关闭后不能再次使用

错误的使用示例：

```java
Stream<Integer> stream = Stream.iterate(1, item -> item + 2).limit(6);
System.out.println(stream.filter(item -> item > 2));
System.out.println(stream.distinct());
```

将出现如下异常：

![](../../pic/jdk8/Stream重复使用的异常.jpg)





## 内部迭代和外部迭代

Stream使用的是内部迭代。普通的for循环或增强for循环都是外部循环。





## 集合和Stream的区别

集合关注的是数据与数据的存储。

流关注的则是对数据的计算，流将一个任务分解为多个小任务然后再汇总结果。使用的是fork join





## Stream的分组操作类型

通过Stream实现类似于SQL的分组功能

```java
Student student1 = new Student("zhangsan", 20, 100);
Student student2 = new Student("lisi", 20, 90);
Student student3 = new Student("wangwu", 30, 90);
Student student4 = new Student("zhangsan", 40, 80);

List<Student> students = Arrays.asList(student1, student2, student3, student4);

//按照学生的姓名进行排序
/*Map<String, List<Student>> groupByName = students.stream().collect(Collectors.groupingBy(Student::getName));
        System.out.println(groupByName);*/

//按照学生的名称进行排序并统计记录数
/*        Map<String, Long> groupByNameNum = students.stream().collect(Collectors.groupingBy(s -> s.getName(), Collectors.counting()));
        System.out.println(groupByNameNum);*/

//按照学生的名称进行排序并统计平均分
Map<String, Double> groupByNameAvgScore = students.stream().collect(Collectors.groupingBy(Student::getName, Collectors.averagingDouble(Student::getScore)));
        System.out.println(groupByNameAvgScore);
```



## Stream的分区操作类型

分区是分组的一种特例，只能有两种结果

```java
// 分区只能分为两种集合,按照score大于等于90进行分片
//Map<Boolean, List<Student>> partitionByScore = students.stream().collect(Collectors.partitioningBy(s -> s.getScore() >= 90));
//System.out.println(partitionByScore);

// 分区只能分为两种集合,按照score大于等于90进行分片计数
Map<Boolean, Long> partitionByScoreCount = students.stream().collect(Collectors.partitioningBy(s -> s.getScore() >= 90, Collectors.counting()));
System.out.println(partitionByScoreCount);
```



## Collectors方法的使用

```java
        Student student1 = new Student("zhangsan", 20, 100);
        Student student2 = new Student("lisi", 20, 90);
        Student student3 = new Student("wangwu", 30, 90);
        Student student4 = new Student("zhangsan", 40, 80);
        Student student5 = new Student("zhaoliu", 30, 90);

        List<Student> students = Arrays.asList(student1, student2, student3, student4);

        // 查询成绩最低的学生
     students.stream().min(Comparator.comparingInt(Student::getScore)).ifPresent(System.out::println);
        System.out.println("----------------");
        // 查询最高成绩
        students.stream().max(Comparator.comparingInt(Student::getScore)).ifPresent(System.out::println);
        // 查询平均成绩
        System.out.println("----------------");
        System.out.println(students.stream().collect(Collectors.averagingInt(Student::getScore)));
        // 查询总成绩
        System.out.println("----------------");
        System.out.println(students.stream().mapToInt(Student::getScore).sum());
        //概况
        IntSummaryStatistics statistics = students.stream().collect(Collectors.summarizingInt(Student::getScore));
        System.out.println(statistics);
        System.out.println("----------------");
        //将学生的姓名拼接并通过逗号分隔
        System.out.println(students.stream().map(Student::getName).collect(Collectors.joining(",")));
        System.out.println("--------------------");
        //两次分组 先按照分数分组再按照年龄分组
        Map<Integer, Map<Integer, List<Student>>> collect = students.stream().collect(Collectors.groupingBy(Student::getScore, Collectors.groupingBy(Student::getAge)));
        System.out.println(collect);
```



## 练习题

1. 找出2011年发生的所有交易，并按交易额排序（从低到高）。
2.  交易员都在哪些不同的城市工作过？
3.  查找所有来自于剑桥的交易员，并按姓名排序。
4.  返回所有交易员的姓名字符串，按字母顺序排序。
5. 有没有交易员是在米兰工作的？
6.  打印生活在剑桥的交易员的所有交易额。
7.  所有交易中，最高的交易额是多少？
8.  找到交易额最小的交易。







- 以下是你要处理的领域，一个 Traders 和 Transactions 的列表：

```java

Trader raoul = new Trader("Raoul", "Cambridge");
Trader mario = new Trader("Mario","Milan");
Trader alan = new Trader("Alan","Cambridge");
Trader brian = new Trader("Brian","Cambridge");
List<Transaction> transactions = Arrays.asList(
new Transaction(brian, 2011, 300),
new Transaction(raoul, 2012, 1000),
new Transaction(raoul, 2011, 400),
new Transaction(mario, 2012, 710),
new Transaction(mario, 2012, 700),
new Transaction(alan, 2012, 950)
);
```

```java
public class Trader {
    private final String name;
    private final String city;

    public Trader(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "Trader{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
```

```java
public class Transaction {

    private final Trader trader;
    private final int year;
    private final int value;

    public Transaction(Trader trader, int year, int value) {
        this.trader = trader;
        this.year = year;
        this.value = value;
    }

    public Trader getTrader() {
        return trader;
    }

    public int getYear() {
        return year;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "trader=" + trader +
                ", year=" + year +
                ", value=" + value +
                '}';
    }
}
```



## 自定义Collector

通过实现接口`java.util.stream.Collector`

```java
public class MyCollector<T> implements Collector<T, Set<T>, Set<T>> {

    @Override
    public Supplier<Set<T>> supplier() {
        System.out.println("supplier invoker!");
        return HashSet::new;
    }

    @Override
    public BiConsumer<Set<T>, T> accumulator() {
        System.out.println("accumulator invoker!");
        return Set::add;
    }

    @Override
    public BinaryOperator<Set<T>> combiner() {
        System.out.println("combiner invoker!");
        return (t1, t2) -> {
            t1.addAll(t2);
            return t1;
        };
    }

    @Override
    public Function<Set<T>, Set<T>> finisher() {
        System.out.println("finisher invoker!");
        return Function.identity();
    }

    @Override
    public Set<Characteristics> characteristics() {
        System.out.println("characteristics invoker");
        HashSet<Characteristics> set = new HashSet<>();
        set.add(Characteristics.IDENTITY_FINISH);
//        return set;
        return Collections.unmodifiableSet(EnumSet.of(Characteristics.IDENTITY_FINISH, Characteristics.UNORDERED));
    }

    public static void main(String[] args) {
        List<String> list = Arrays.asList("hello", "world", "welcome","hello");
        Set<String> stringSet = list.stream().collect(new MyCollector<>());
        System.out.println(stringSet);
    }
}

```



### Collector接口中各个方法讲解



#### supplier方法

```java
    /**
     * A function that creates and returns a new mutable result container.
     * 
     * @return a function which returns a new, mutable result container
     */
    Supplier<A> supplier();
```

作用是：

创建一个新的中间结果容器。返回一个`Supplier` 函数接口。在上面的例子中我们创建了一个`HashSet`集合

![Supplier函数接口](../../pic/jdk8/Supplier函数接口.jpg)



#### accumulator方法

```java
    /**
     * A function that folds a value into a mutable result container.
     *
     * @return a function which folds a value into a mutable result container
     */
    BiConsumer<A, T> accumulator();
```

作用是：

将值添加到中间结果容器中，上面的类是将值添加到`HashSet`集合中。



#### combiner方法

```java
    /**
     * A function that accepts two partial results and merges them.  The
     * combiner function may fold state from one argument into the other and
     * return that, or may return a new result container.
     *
     * @return a function which combines two partial results into a combined
     * result
     */
    BinaryOperator<A> combiner();
```

作用是：合并两个中间结果。可能将一个结果合并到另一个结果，或者返回一个新的结果容器。上面的类就是将一个结果合并到另一个结果。



#### finisher方法

```java
    /**
     * Perform the final transformation from the intermediate accumulation type
     * {@code A} to the final result type {@code R}.
     *
     * <p>If the characteristic {@code IDENTITY_TRANSFORM} is
     * set, this function may be presumed to be an identity transform with an
     * unchecked cast from {@code A} to {@code R}.
     *
     * @return a function which transforms the intermediate result to the final
     * result
     */
    Function<A, R> finisher();
```

作用：是最终的操作，从中间积累的结果A转换成最终结果R。这里还有一点特别的，如果设置了`IDENTITY_TRANSFORM` 特性将会执行强制类型转换，从A转换到R。



#### characteristics方法

```java
    /**
     * Returns a {@code Set} of {@code Collector.Characteristics} indicating
     * the characteristics of this Collector.  This set should be immutable.
     *
     * @return an immutable set of collector characteristics
     */
    Set<Characteristics> characteristics();
```

作用：通过这个方法可以指定`Collector`的特性集合，并且这个集合是不可以变的。



#### 验证

在创建一个类型实现一个`Collector`接口，来验证上面的结论，类如下：

```java
public class MyCollector2<T> implements Collector<T, Set<T>, Map<T, T>> {
    @Override
    public Supplier<Set<T>> supplier() {
        System.out.println("supplier invoker");
        return () -> {
            System.out.println("Perform supplier");
            return new HashSet<T>();
        };
    }

    @Override
    public BiConsumer<Set<T>, T> accumulator() {
        System.out.println("accumulator invoker");
        return (item1, item2) -> {
            System.out.println("Perform accumulator");
            item1.add(item2);
        };
    }

    @Override
    public BinaryOperator<Set<T>> combiner() {
        System.out.println("accumulator invoker");

        return (item1, item2) -> {
            System.out.println("Perform combiner");
            item1.addAll(item2);
            return item1;
        };
    }

    @Override
    public Function<Set<T>, Map<T, T>> finisher() {
        System.out.println("finisher invoker");
        return (item) -> {
            System.out.println("Perform finisher");
            Map<T, T> result = new HashMap<>(2);
            item.forEach(str -> {
                result.put(str, str);
            });
            return result;
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        System.out.println("characteristics invoker");
        return Collections.unmodifiableSet(EnumSet.of(Characteristics.IDENTITY_FINISH, Characteristics.UNORDERED));
    }

    public static void main(String[] args) {
        List<String> list = Arrays.asList("hello", "world", "welcome","hello");
        HashSet<String> hashSet = new HashSet<>();
        hashSet.addAll(list);
        Map<String, String> collect = hashSet.stream().collect(new MyCollector2<>());

        System.out.println(collect);
    }
}
```



首先验证Collector的characteristics方法，通过上面方法的描述我们能知道，这里可以设置三个值分别是：

```java
 /**
     * Characteristics indicating properties of a {@code Collector}, which can
     * be used to optimize reduction implementations.
     */
    enum Characteristics {
        /**
         * Indicates that this collector is <em>concurrent</em>, meaning that
         * the result container can support the accumulator function being
         * called concurrently with the same result container from multiple
         * threads.
         *
         * <p>If a {@code CONCURRENT} collector is not also {@code UNORDERED},
         * then it should only be evaluated concurrently if applied to an
         * unordered data source.
         */
        CONCURRENT,

        /**
         * Indicates that the collection operation does not commit to preserving
         * the encounter order of input elements.  (This might be true if the
         * result container has no intrinsic order, such as a {@link Set}.)
         */
        UNORDERED,

        /**
         * Indicates that the finisher function is the identity function and
         * can be elided.  If set, it must be the case that an unchecked cast
         * from A to R will succeed.
         */
        IDENTITY_FINISH
    }
```



我们上面的例子中使用了其中两个

![collector的characteristic的值](../../pic/jdk8/collector的characteristic的值.jpg)

`UNORDERED` 表示无序的，`IDENTITY_FINISH`表示不会执行`finisher` 方法，会执行上面我们说明的不校验的类型转换。`CONCURRENT`表示在方法`accumulator` 中支持在通过一个结果容器中并发的添加元素，也就是创建的容器支持并发写，如果中间有读的操作，就需要支持并发读写。执行上面的代码，结果如下：

![Collector错误的使用强制类型转换结果](../../pic/jdk8/Collector错误的使用强制类型转换结果.jpg)

说明没有执行`finisher` `方法，导致强制类型转换失败。如果我们修改方法如下，其他方法不变

```java
    @Override
    public Set<Characteristics> characteristics() {
        System.out.println("characteristics invoker");
        return Collections.unmodifiableSet(EnumSet.of(Characteristics.UNORDERED));
    }
```

输入结果如下：

![Collector没有identity特点](../../pic/jdk8/Collector没有identity特点.jpg)

方法`finisher` 被调用，并且被执行。



## 流的底层分析

### 中间操作方法底层分析

Stream对象中通过`Sink`建立链表，每个Stream都保留前一个Stream的引用。filter返回一个StatelessOp,我们记为StatelessOp1, 而map返回另外一个StatelessOp，我们记为StatelessOp2.
在调用StatelessOp1.map时， StatelessOp2是这样生成的：

```java
return new StatelessOp<P_OUT, R>(StatelessOp1,......);
```

jdk8中的实现：

`java.util.stream.AbstractPipeline#wrapSink`

```java
    final <P_IN> Sink<P_IN> wrapSink(Sink<E_OUT> sink) {
        Objects.requireNonNull(sink);

        for ( @SuppressWarnings("rawtypes") AbstractPipeline p=AbstractPipeline.this; p.depth > 0; p=p.previousStage) {
            sink = p.opWrapSink(p.previousStage.combinedFlags, sink);
        }
        return (Sink<P_IN>) sink;
    }
```

这个`wrapSink`包含了所有的操作包含中间和终止操作的sink，上面代码中的`opWrapSink`方法在中间操作中有对应的实现，下面就是map中间操作的实现：

```java
    public final <R> Stream<R> map(Function<? super P_OUT, ? extends R> mapper) {
        Objects.requireNonNull(mapper);
        return new StatelessOp<P_OUT, R>(this, StreamShape.REFERENCE,
                                     StreamOpFlag.NOT_SORTED | StreamOpFlag.NOT_DISTINCT) {
            @Override
            Sink<P_OUT> opWrapSink(int flags, Sink<R> sink) {
                return new Sink.ChainedReference<P_OUT, R>(sink) {
                    @Override
                    public void accept(P_OUT u) {
                        downstream.accept(mapper.apply(u));
                    }
                };
            }
        };
    }
```

下面这个方法调用Spliterator的forEachRemaining方法对每个元素执行中间和终止操作

```java
    final <P_IN> void copyInto(Sink<P_IN> wrappedSink, Spliterator<P_IN> spliterator) {
        Objects.requireNonNull(wrappedSink);

        if (!StreamOpFlag.SHORT_CIRCUIT.isKnown(getStreamAndOpFlags())) {
            wrappedSink.begin(spliterator.getExactSizeIfKnown());
            spliterator.forEachRemaining(wrappedSink);
            wrappedSink.end();
        }
        else {
            copyIntoWithCancel(wrappedSink, spliterator);
        }
    }
```

流中的元素分别进行多个中间操作和终止操作，并不是所有的元素执行完其中的某个操作后再执行其他操作。









