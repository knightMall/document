- [Java8日期和时间类](#java8日期和时间类)
  - [常用类介绍](#常用类介绍)
    - [localdate的创建](#LocalDate的创建)
    - [对年月日进行操作](#对年月日进行操作)
    - [格式化日期](#格式化日期)
    - [日期的比较](#日期的比较)
  - [LocalTime类](#localtime类)
    - [LocalTime的创建](#localtime的创建)
    - [常用方法](#常用方法)
  - [LocalDateTime类](#localdatetime类)
  - [TemporalAdjuster类](#temporaladjuster类)



## Java8日期和时间类

Java8中的日期和时间类都是不可变的。它们是`LocalDate`、`LocalTime`、`Instant`、`Duration`和`Period`。



### 常用类介绍

#### LocalDate类

LocalDate类关注的是年月日，提供了年月日的操作方法。该类是不可变类，它只提供了简单的日期，并不包含当天的时间信息。另外，它也不附带任何时区相关的信息

##### LocalDate的创建

```java
LocalDate now = LocalDate.now();
System.out.println(LocalDate.of(2019,1,2));
```



##### 对年月日进行操作

```java
public class Java8DateTest {
    public static void main(String[] args) {
        LocalDate now = LocalDate.now();
        System.out.println(now);
        System.out.println(now.plusDays(-1));
        System.out.println(now.plusYears(-2));
        System.out.println(now.plusMonths(1));
        System.out.println(now.plusWeeks(1));
        System.out.println(now.plus(2, ChronoUnit.YEARS));
        // 减对应的天数,可以传负数
        System.out.println(now.minusDays(1));
    }
}
```



##### 格式化日期

```java
public class Java8DateTest {
    public static void main(String[] args) {
        LocalDate now = LocalDate.now();
        System.out.println(now.format(DateTimeFormatter.ofPattern("yyyy年MM月dd日")));
    }
}
```



##### 日期的比较

```java
LocalDate date1 = LocalDate.of(2019, 1, 12);
LocalDate date2 = LocalDate.of(2019, 1, 13);
System.out.println(date1.isAfter(date2));
System.out.println(date1.isBefore(date2));
System.out.println(date1.isEqual(date2));
```



#### LocalTime类

表示一个没有时区的时间类，它是不可变的。表现方式是 hour-minute-second

##### LocalTime的创建

```java
LocalTime localTime = LocalTime.now();
System.out.println(localTime);
LocalTime time = LocalTime.of(10, 10, 10);
System.out.println(time);
LocalTime parse = LocalTime.parse("10:15:30");
System.out.println(parse);
```

##### 常用方法

```java
//获取时分秒属性
LocalTime now1 = LocalTime.now();
System.out.println(now1.getHour());
System.out.println(now1.getMinute());
System.out.println(now1.getSecond());
System.out.println("----------------------");
//设置时分秒
System.out.println(now1.withHour(1));
System.out.println(now1.withMinute(1));
System.out.println(now1.withSecond(1));
//格式化日期
LocalTime localTime1 = LocalTime.parse("13|35|23",DateTimeFormatter.ofPattern("HH|mm|ss"));
System.out.println(localTime1);
```

#### LocalDateTime类

这是LocalDate和LocalTime的合体。它同时表示了日期和时间，但不带时区信息。

```java
LocalDateTime localDateTime = LocalDateTime.of(2018, Month.MARCH,
        18, 23, 34, 10);
System.out.println(localDateTime);
LocalDateTime dateTime = LocalDateTime.of(LocalDate.of(2017, 10, 1),
        LocalTime.of(10, 20, 30));
System.out.println(dateTime);
System.out.println(dateTime.toLocalDate());
System.out.println(dateTime.toLocalTime());
```



#### TemporalAdjuster类

该类可以对日期进行更加复杂的操作。

```java
LocalDate date1 = LocalDate.of(2018, 4, 19);
// 2018-03-03 修改相对于月的日。范围是1至28/31
System.out.println(date1.withDayOfMonth(30));
// 2018-02-14 修改相对于年的日。范围是1至365/366
System.out.println(date1.withDayOfYear(45));
//获取当前日期的下一个星期天或者当前日期就是星期天则返回当天
System.out.println(date1.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY)));
// 获取当前日期月的最后一天
System.out.println(date1.with(TemporalAdjusters.lastDayOfMonth()));
// 创建一个新的日期，它的值为同一个月中每一周的第几天。如下表示4月第一周的星期一
System.out.println(date1.with(TemporalAdjusters.dayOfWeekInMonth(1, DayOfWeek.MONDAY)));
// 2018年4月19号是星期四，这个输出是2018年4月26号。表示的是下一个星期四
System.out.println(date1.with(TemporalAdjusters.next(DayOfWeek.THURSDAY)));
```















