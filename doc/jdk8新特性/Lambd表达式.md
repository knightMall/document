- [Lambda表达式](#lambda表达式)
  - [语法和结构](#语法和结构)
- [函数式接口](#函数式接口)
  - [Function接口详解](#function接口详解)
    - [方法apply](#方法apply)
    - [方法compose](#方法compose)
    - [方法andThen](#方法andthen)
  - [Predicate接口详解](#predicate接口详解)
    - [方法test](#方法test)
    - [方法and](#方法and)
    - [方法or](#方法or)
    - [方法negate](#方法negate)
    - [方法isEqual](#方法isequal)
  - [Supplier接口详解](#supplier接口详解)
    - [方法get](#方法get)
- [方法引用](#方法引用)









## Lambda表达式

 Lambda 表达式可以理解为一种匿名函数：它没有名称，但有参数列表、函数主体、返回类型，可能还有一个可以抛出的异常的列表。

### 语法和结构



![Lambda表示式结构](../../pic/jdk8/Lambda表示式结构.jpg)





## 函数式接口

函数式接口就是仅仅声明了一个抽象方法的接口

 Java 8 自带一些常用的函数式接口，放在 java.util.function 包里，包括 `Predicate<T> 、 Function<T,R> 、 Supplier<T> 、 Consumer<T> 和 BinaryOperator<T>`。

![java8常用的函数式接口](../../pic/jdk8/java8常用的函数式接口.jpg)

![java8常用的函数式接口2](../../pic/jdk8/java8常用的函数式接口2.jpg)



自定义函数式接口

```java

@FunctionalInterface
public interface BufferedReaderProcessor {

    static String processFile(BufferedReaderProcessor p) throws
            IOException {
        try (BufferedReader br =
                     new BufferedReader(new FileReader("D://data.txt"))) {
            return p.process(br);
        }
    }

    String process(BufferedReader reader) throws IOException;
}

```

使用方式：

```java
//非java8使用        
String line = BufferedReaderProcessor.processFile(new BufferedReaderProcessor() {
            @Override
            public String process(BufferedReader reader) throws IOException {
                return reader.readLine();
            }
        });
        System.out.println("line:" + line);

//java8语法，可以读多行
        String line2 = BufferedReaderProcessor.processFile(reader -> reader.readLine() + reader.readLine());
        System.out.println("line2:" + line2);
```



### Function接口详解



作用：

`java.util.function.Function`接收一个参数返回一个结果。



#### 方法apply

接收一个类型的参数返回另外一个类型

```java
    /**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
     */
    R apply(T t);
```

使用示例：

```java
/**
 * @Description: Function接口详解
 * @author: knight
 * @Date: 2019-04-21 15:24
 */
public class FunctionTest {

    public static void main(String[] args) {
        List<String> strings = Arrays.asList("Hello", "world", "Hello World");

        strings.forEach(str -> System.out.println(str.toUpperCase()));

        strings.stream().map(String::toUpperCase).forEach(System.out::println);

        FunctionTest functionTest = new FunctionTest();
		// 将行为作为参数转递
        functionTest.operation(2, (Integer num) -> {return 2 + 3;});


    }

    public Integer operation(Integer num, Function<Integer, Integer> behavior) {
        return behavior.apply(num);
    }

}
```



#### 方法compose

```java
/**
     * Returns a composed function that first applies the {@code before}
     * function to its input, and then applies this function to the result.
     * If evaluation of either function throws an exception, it is relayed to
     * the caller of the composed function.
     *
     * @param <V> the type of input to the {@code before} function, and to the
     *           composed function
     * @param before the function to apply before this function is applied
     * @return a composed function that first applies the {@code before}
     * function and then applies this function
     * @throws NullPointerException if before is null
     *
     * @see #andThen(Function)
     */
    default <V> Function<V, R> compose(Function<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        return (V v) -> apply(before.apply(v));
    }
```

该方法可以组合执行apply方法。该方法是在执行指定的Function的apply方法前执行。使用示例：

```java
 public Integer composeTest(Integer num, Function<Integer, Integer> behavior) {
        return behavior.compose((Integer num2) -> {
            System.out.println("composeTest");
            return num2 + 3;
        }).apply(num);
    }
// 调用
        FunctionTest functionTest = new FunctionTest();

        Integer composeTest = functionTest.composeTest(3, num -> {
            System.out.println("compose");
            return num + 1;
        });
```

执行结果：

```tex
composeTest
compose
```



#### 方法andThen

```java
    /**
     * Returns a composed function that first applies this function to
     * its input, and then applies the {@code after} function to the result.
     * If evaluation of either function throws an exception, it is relayed to
     * the caller of the composed function.
     *
     * @param <V> the type of output of the {@code after} function, and of the
     *           composed function
     * @param after the function to apply after this function is applied
     * @return a composed function that first applies this function and then
     * applies the {@code after} function
     * @throws NullPointerException if after is null
     *
     * @see #compose(Function)
     */
    default <V> Function<T, V> andThen(Function<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (T t) -> after.apply(apply(t));
    }
```

该方法可以组合执行apply方法。该方法是在执行指定的Function的apply方法后执行。使用示例：

```java
    public Integer andThenTest(Integer num, Function<Integer, Integer> behavior) {
        return behavior.andThen((Integer num2) -> {
            System.out.println("andThenTest");
            return num2 + 3;
        }).apply(num);
    }
// 调用
 FunctionTest functionTest = new FunctionTest();
        Integer andThenTest = functionTest.andThenTest(3, num -> {
            System.out.println("addThen");
            return num + 1;
        });
```

执行结果

```tex
addThen
andThenTest
```





### Predicate接口详解

Predicate表示一个判断返回boolean，只有一个参数

```java
/**
 * Represents a predicate (boolean-valued function) of one argument.
 *
 * <p>This is a <a href="package-summary.html">functional interface</a>
 * whose functional method is {@link #test(Object)}.
 *
 * @param <T> the type of the input to the predicate
 *
 * @since 1.8
 */
@FunctionalInterface
public interface Predicate<T> {}
```



#### 方法test

评估给定的参数

```java
    /**
     * Evaluates this predicate on the given argument.
     *
     * @param t the input argument
     * @return {@code true} if the input argument matches the predicate,
     * otherwise {@code false}
     */
    boolean test(T t);
```

使用示例：

```java
    private static void conditionFilter(List<Integer> list, Predicate<Integer> predicate) {
        for (Integer integer : list) {
            if (predicate.test(integer)) {
                System.out.println(integer);
            }
        }
    }

List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
conditionFilter(list, item -> item % 2 == 0);
```



#### 方法and

两个predicate与运算

```java
    /**
     * Returns a composed predicate that represents a short-circuiting logical
     * AND of this predicate and another.  When evaluating the composed
     * predicate, if this predicate is {@code false}, then the {@code other}
     * predicate is not evaluated.
     *
     * <p>Any exceptions thrown during evaluation of either predicate are relayed
     * to the caller; if evaluation of this predicate throws an exception, the
     * {@code other} predicate will not be evaluated.
     *
     * @param other a predicate that will be logically-ANDed with this
     *              predicate
     * @return a composed predicate that represents the short-circuiting logical
     * AND of this predicate and the {@code other} predicate
     * @throws NullPointerException if other is null
     */
    default Predicate<T> and(Predicate<? super T> other) {
        Objects.requireNonNull(other);
        return (t) -> test(t) && other.test(t);
    }
```

使用示例：

```java
private static void addTest(List<Integer> list, Predicate<Integer> predicate1, Predicate<Integer> predicate2) {
        for (Integer integer : list) {
            if (predicate1.and(predicate2).test(integer)) {
                System.out.println(integer);
            }
        }
    }
List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
// 大于5且能被3整除
addTest(list,integer -> integer>5,integer -> integer % 3==0);
```



#### 方法or

两个predicate或运算

```java
/**
     * Returns a composed predicate that represents a short-circuiting logical
     * OR of this predicate and another.  When evaluating the composed
     * predicate, if this predicate is {@code true}, then the {@code other}
     * predicate is not evaluated.
     *
     * <p>Any exceptions thrown during evaluation of either predicate are relayed
     * to the caller; if evaluation of this predicate throws an exception, the
     * {@code other} predicate will not be evaluated.
     *
     * @param other a predicate that will be logically-ORed with this
     *              predicate
     * @return a composed predicate that represents the short-circuiting logical
     * OR of this predicate and the {@code other} predicate
     * @throws NullPointerException if other is null
     */
    default Predicate<T> or(Predicate<? super T> other) {
        Objects.requireNonNull(other);
        return (t) -> test(t) || other.test(t);
    }
```

使用示例：

```java
private static void orTest(List<Integer> list,Predicate<Integer> predicate1,Predicate<Integer> predicate2){
        for (Integer integer : list) {
            if (predicate1.or(predicate2).test(integer)) {
                System.out.println(integer);
            }
        }
    }

List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
// 大于6或小于5
orTest(list,integer -> integer >6,integer -> integer <5);

```



#### 方法negate

对Predicate结果取反

```java
    /**
     * Returns a predicate that represents the logical negation of this
     * predicate.
     *
     * @return a predicate that represents the logical negation of this
     * predicate
     */
    default Predicate<T> negate() {
        return (t) -> !test(t);
    }
```

使用示例

```java
private static void negateTest(List<Integer> list,Predicate<Integer> predicate){
        for (Integer integer : list) {
            if (predicate.negate().test(integer)) {
                System.out.println(integer);
            }
        }
    }
List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
negateTest(list,integer -> integer < 0 );
```



#### 方法isEqual

就是比较两个参数是否相等

```java
    /**
     * Returns a predicate that tests if two arguments are equal according
     * to {@link Objects#equals(Object, Object)}.
     *
     * @param <T> the type of arguments to the predicate
     * @param targetRef the object reference with which to compare for equality,
     *               which may be {@code null}
     * @return a predicate that tests if two arguments are equal according
     * to {@link Objects#equals(Object, Object)}
     */
    static <T> Predicate<T> isEqual(Object targetRef) {
        return (null == targetRef)
                ? Objects::isNull
                : object -> targetRef.equals(object);
    }
```

使用示例

```java
System.out.println(Predicate.isEqual("test").test("abc"));
```





### Supplier接口详解

不接受参数，返回一个类型。类似于工厂

#### 方法get

```java
    /**
     * Gets a result.
     *
     * @return a result
     */
    T get();
```

使用示例：

```java
  Supplier<String> supplier = () -> "hello";
  System.out.println(supplier.get());
```







## 方法引用

方法引用让你可以重复使用现有的方法定义。当你需要使用方法引用时，目标引用放在分隔符 :: 前，方法的名称放在后面。例如，`Apple::getWeight` 就是引用了 Apple 类中定义的方法 `getWeight` 。请记住，不需要括号，因为
你没有实际调用这个方法。下面是Lambda及其等效方法引用

![方法引用](../../pic/jdk8/方法引用.jpg)



### 构建方法引用 

- 指向静态方法的方法引用（例如Integer的parseInt方法，写作`Integer::parseInt`）
- 指向任意类型实例方法的方法引用（例如String的length方法，写作`String::length`）
- 指向现有对象的实例方法的方法引用（假设你有一个局部变量 expensiveTransaction
  用于存放 Transaction 类型的对象，它支持实例方法 getValue ，那么你就可以写` expensive-
  Transaction::getValue` ）

```java
        // 方法引用
        List<Integer> integers = forEach(Arrays.asList("1", "2", "3", "4", "5"), Integer::parseInt);
        System.out.println(integers);
        forEach(Arrays.asList("1", "2", "3", "4", "5"), Long::parseLong);

        final List<String> str = Arrays.asList("c", "d", "f", "d");
        str.sort(String::compareToIgnoreCase);
        System.out.println(str);
        //通过方法引用创建接口实现
        Function<String, Integer> consumer = Integer::parseInt;
        //通过方法引用创建接口实现
        BiPredicate<List<String>, String> contains = List::contains;


    private static <T, R> List<R> forEach(List<T> list, Function<T, R> c) {
        List<R> results = new ArrayList<>();
        for (T t : list) {
            R result = c.apply(t);
            results.add(result);
        }
        return results;
    }
```



## 匿名内部类和Lambda表达式

从表象上来看Lambda好像是匿名内部类的语法糖，其他他们之间没有任何关系。

```java
/**
 * 匿名内部类和lamdb表达式
 * @author knight
 */
public class LamdbaTest {
        Runnable r1 = ()->System.out.println("r1:"+this);
        Runnable r2 = new Runnable() {
            @Override
            public void run() {
                System.out.println("r2:"+this);
            }
        };
    public static void main(String[] args) {
        LamdbaTest lamdbTest = new LamdbaTest();
        new Thread(lamdbTest.r1).start();
        new Thread(lamdbTest.r2).start();
    }
}
```

输出结果是:

```java
r1:com.knight.jdk8.method.reference.LamdbTest@38ff6483
r2:com.knight.jdk8.method.reference.LamdbTest$1@3d6ddd95
```

lambda表示的输出结果就是类的本身，而匿名内部类的输出结果表示是一个内部类。











