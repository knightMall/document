- [Optional类简介](#optional类简介)
  - [Optional的基本使用](#optional的基本使用)
    - [Optional的创建](#optional的创建)
    - [Optional的使用](#optional的使用)
- [注意事项](#注意事项)





## Optional类简介

是一个容器对象，可能包含或不包含一个非空值。主要用来预防NPE的。



### Optional的基本使用

#### Optional的创建

因为Optional的构造方法定义成私有的。但是可以通过静态方法`of`、`ofNullable`、`empty` 创建。

- `of` 方法表示传入的值一定不为空，如果为空将会抛出NEP
- `ofNullable` 方法表示传入的值可能为空也可能不为空
- `empty` 方法创建一个值为空的Optionalal



#### Optional的使用



```java
 // 模拟从数据库从获取数据，可能为空也可能不会空
Student student = Student.getStudent();
Optional<Student> optionalStudent = Optional.ofNullable(student);
// 不为空输出名称
optionalStudent.ifPresent(s-> System.out.println(s.getName()));
// 为空输出新创建的student的名称否则输出原来的名称
System.out.println(optionalStudent.orElse(new Student("新创建的", 20)).getName());

//为空返回一个student
optionalStudent.orElseGet(()-> new Student());
```



```java
Employee employee1 = new Employee();
employee1.setName("zhangsan");

Employee employee2 = new Employee();
employee2.setName("lisi");

Company company = new Company();
company.setName("company1");
List<Employee> employees = Arrays.asList(employee1, employee2);
//company.setEmployeeList(employees);


// 如果company类的employeeList属性为空则返回一个空集合否则返回当前的集合
Optional<Company> optionalCompany = Optional.ofNullable(company);
System.out.println(optionalCompany.map(otherCompany -> otherCompany.getEmployeeList()).orElse(Collections.emptyList()));
```







## 注意事项

Optional不能作为方法参数和类的成员变量使用。



