## 阻塞 (Block) 和非阻塞

阻塞和非阻塞是进程在访问数据的时候，数据是否准备就绪的一种处理方式,当数据没有准备的时候。
阻塞：往往需要等待缓冲区中的数据准备好过后才处理其他的事情，否则一直等待在那里。
非阻塞:当我们的进程访问我们的数据缓冲区的时候，如果数据没有准备好则直接返回，不会等待。如果数据已经
准备好，也直接返回。



##　同步 (Synchronization) 和异步

同步和异步都是基于应用程序和操作系统处理 IO 事件所采用的方式。比如同步：是应用程序要直接参与 IO 读写
的操作。异步：所有的 IO 读写交给操作系统去处理，应用程序只需要等待通知。
同步方式在处理 IO 事件的时候，必须阻塞在某个方法上面等待我们的 IO 事件完成(阻塞 IO 事件或者通过轮询 IO
事件的方式),对于异步来说，所有的 IO 读写都交给了操作系统。这个时候，我们可以去做其他的事情，并不需要去完
成真正的 IO 操作，当操作完成 IO 后，会给我们的应用程序一个通知。
同步 : 阻塞到 IO 事件，阻塞到 read 或则 write。这个时候我们就完全不能做自己的事情。让读写方法加入到线
程里面，然后阻塞线程来实现，对线程的性能开销比较大。



代码示例

### BIO代码：

```java
public class BIOServer {
    public static void main(String[] args) {
        int port = 8080;
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while (true){
                //会阻塞
                Socket socket = serverSocket.accept();

                prcess(socket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void prcess(Socket socket) {
        //拿到客户端发送过来的数据
        InputStream inputStream = null;
        try {
            inputStream = socket.getInputStream();
            byte[] buffer = new byte[1024];
            int len = inputStream.read(buffer);
            if (len > 0) {
                String content = new String(buffer,0,len);
                System.out.println("接受到的内容："+content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (inputStream!=null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }
}
```

```java

public class BIOClient {
    public static void main(String[] args) {
        OutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            Socket socket = new Socket("localhost",8080);
            outputStream = socket.getOutputStream();

            outputStream.write(new String("发送信息").getBytes());

            inputStream = socket.getInputStream();

            byte[] buffer = new byte[1024];
            int len = inputStream.read(buffer);
            if (len > 0) {
                String content = new String(buffer,0,len);
                System.out.println("接受到的内容："+content);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (outputStream!=null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
```



### NIO代码

```java
public class NIOServer {

    private Selector selector;
    private ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

    private int port;


    public NIOServer(int port) {
        this.port = port;

        try {
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            //绑定端口和ip
            serverSocketChannel.bind(new InetSocketAddress(8080));
            serverSocketChannel.configureBlocking(false);

            selector = Selector.open();

            //关联selector
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void listen() {
        System.out.println("监听端口:" + port);
        try {
            while (true) {
                selector.select();
                Set<SelectionKey> keys = selector.selectedKeys();

                Iterator<SelectionKey> iterator = keys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey selectionKey = iterator.next();
                    iterator.remove();
                    process(selectionKey);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void process(SelectionKey key) {
        if (key.isAcceptable()) {
            ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
            try {
                SocketChannel socketChannel = serverSocketChannel.accept();
                socketChannel.configureBlocking(false);
                socketChannel.register(selector,SelectionKey.OP_READ);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(key.isReadable()){
            SocketChannel socketChannel = (SocketChannel) key.channel();
            int len = 0;
            try {
                len = socketChannel.read(byteBuffer);
                if (len>0){
                    byteBuffer.flip();
                    String content = new String(byteBuffer.array(), 0, len);
                    System.out.println("接收到的内容："+content);
                }
                socketChannel.register(selector,SelectionKey.OP_WRITE);
                key.attach("响应内容");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(key.isWritable()){
            try {
                SocketChannel socketChannel = (SocketChannel) key.channel();

                String attachment = key.attachment().toString();
                socketChannel.write(ByteBuffer.wrap(("输出：" + attachment).getBytes()));
                socketChannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new NIOServer(8080).listen();
    }
}
```



## NIO主要类

Buffer

Selector

Channel

