## 什么是TCP粘包拆包

如下图所示

![TCP粘包拆包](../../images/22/02/TCP粘包拆包.jpg)

假设客户端分别发送D2和D1两个数据包到服务端，由于服务端一次读取的数是不确定的。所以可能出现下面四种情况：

- 服务端分两次读取到了两个独立的数据包，分别是D1和D2，没有粘包和拆包。
- 服务端一次接受到了两个数据包，D1和D2粘合在一起，被称为TCP粘包。
- 服务端分两次读取到了两个数据包，第一次读取到了D1和D2包的一部分D2_1，第二次读取到了D2包剩余内容，这称为拆包。
- 服务端分两次读取到了两个数据包，第一次读取了D1包的一部分D1_1，第二次读取到了D1包剩余内容和D2包的整包。



## 发生拆包和粘包的原因

问题产生的主要原因如下：

- 应用程序writer写入的字节大小大于套接口发送缓冲区大小；
- 进行MSS大小的TCP分段；
- 以太网帧payload大于MTU进行IP分片；



## 粘包问题的解决策略

由于底层TCP无法理解上层的业务数据，所以在底层是无法保证数据包不被拆分或重组的

这个问题只能通过上层的应用协议设计来解决，根据业界的主流协议的解决方案，主要有

- 消息定长，例如每个报文的大小固定长度200字节，不够使用空格补位

- 在包尾增加回车换行符进行分割，例如FTP协议

- 将消息分为消息头和消息体，消息头中包含表示消息总长度的字段，通常设计思路为消息头的第一个

  字段使用int32来表示消息的总长度

- 更复杂的应用层协议













## 为什么都说Netty是高性能的RPC框架？

- IO模型
  - 接收和发送ByteBuffer使用堆外直接内存进行Socket读写。
  - 提供了组合Buffer对象，可以聚合多个ByteBuffer对象
  - transferTo()直接将文件缓冲区的数据发送到目标Channel    
  - Pooled与UnPooled（池化与非池化）
  - Unsafe和非Unsafe（底层读写与应用程序读写）
  - Heap和Direct（堆内存与堆外内存）
- 线程模型
  - EventLoop聚合了多路复用器Selector，可以同时并发处理成百上千个客户端Channel，由于读
    写操作都是非阻塞的，这样就可以充分提升IO线程的运行效率，避免由于频繁IO阻塞导致线程挂起。
  - Reactor单线程模型
  - Reactor多线程模型
  - 主从Reactor多线程模型
  - 无锁化的串行设计理念（Pipelinec）
  - Netty的高效并发编程主要体现如下：
    - volatile的大量、正确使用
    - CAS和原子类的广泛使用
    - 线程安全容器的使用
    - 通过读写锁提升并发性能
- 序列化
  - 序列化的码流大小（网络带宽的占用）
  - 序列化和反序列化的性能（CPU资源占用）
  - 是否支持跨语言
- 灵活的TCP参数配置

   



##　服务端的Socket在哪里开始初始化？

是在方法`io.netty.channel.socket.nio.NioServerSocketChannel#newSocket` 初始化的，
调用的是 `SelectorProvider.openServerSocketChannel()` 方法。





## 服务端的Socket在哪里开始accept链接

` selectionKey = javaChannel().register(eventLoop().selector, 0, this);`







## 源码分析

### 绑定端口

因为Netty底层还是基于NIO实现的，所以Netty的启动过程肯定和也少不了下面的步骤，下面是通过NIO写的一个server端的代码

```java
//对应 io.netty.channel.nio.NioEventLoop#openSelector
selector = Selector.open();
ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
//绑定端口和ip
//对应 io.netty.channel.DefaultChannelPipeline.HeadContext#bind
serverSocketChannel.bind(new InetSocketAddress(8080));
serverSocketChannel.configureBlocking(false);

//关联selector
//对应  selectionKey = javaChannel().register(eventLoop().selector, 0, this);
serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
```

这个是通过netty写的服务端的代码

```java
public void start() {
     EventLoopGroup bossGroup = new NioEventLoopGroup();
     EventLoopGroup workerGroup = new NioEventLoopGroup();

     ServerBootstrap bootstrap = new ServerBootstrap();
     bootstrap.group(bossGroup, workerGroup)
             .channel(NioServerSocketChannel.class)
             .childHandler(new ChannelInitializer<SocketChannel>() {

                 @Override
                 protected void initChannel(SocketChannel ch) throws Exception {

                     ChannelPipeline pipeline = ch.pipeline();
                     /**
                      * maxFrameLength：框架的最大长度。如果帧的长度大于此值，则将抛出 TooLongFrameException。
                      * lengthFieldOffset：长度字段的偏移量：即对应的长度字段在整个消息数据中得位置
                      * lengthFieldLength：长度字段的长度。如：长度字段是 int 型表示，那么这个值就是 4（long 型就是 8）
                      * lengthAdjustment：要添加到长度字段值的补偿值
                      * initialBytesToStrip：从解码帧中去除的第一个字节数
                      */
                     pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
                     //自定义协议编码器
                     pipeline.addLast(new LengthFieldPrepender(4));
                     //对象参数类型编码器
                     pipeline.addLast("encoder", new ObjectEncoder());
                     pipeline.addLast("decoder", new ObjectDecoder(Integer.MAX_VALUE,
                             ClassResolvers.cacheDisabled(null)));
                     pipeline.addLast(new RegistryHandler());

                 }
             }).option(ChannelOption.SO_BACKLOG, 128)
             .childOption(ChannelOption.SO_KEEPALIVE, true);

     try {
         ChannelFuture future = bootstrap.bind(port).sync();
         System.out.println("RPC Server start listen at "+port);
         future.channel().closeFuture().sync();
     } catch (InterruptedException e) {
         e.printStackTrace();
         bossGroup.shutdownGracefully();
         workerGroup.shutdownGracefully();
     }

 }
```







上面是整个使用Nett有的服务端的代码。`ServerBootstrap bootstrap = new ServerBootstrap();` 这一行没什么看的，就是调用一个无参构造方法。`ChannelFuture future = bootstrap.bind(port).sync();` 我们接着看这一句代码

```java
public ChannelFuture bind(int inetPort) {
    return bind(new InetSocketAddress(inetPort));
}
public ChannelFuture bind(SocketAddress localAddress) {
    validate();
    if (localAddress == null) {
        throw new NullPointerException("localAddress");
    }
    return doBind(localAddress);
}

private ChannelFuture doBind(final SocketAddress localAddress) {
     final ChannelFuture regFuture = initAndRegister();
     final Channel channel = regFuture.channel();
     if (regFuture.cause() != null) {
         return regFuture;
     }

     if (regFuture.isDone()) {
         // At this point we know that the registration was complete and successful.
         ChannelPromise promise = channel.newPromise();
         doBind0(regFuture, channel, localAddress, promise);
         return promise;
     } else {
         // Registration future is almost always fulfilled already, but just in case it's not.
         final PendingRegistrationPromise promise = new PendingRegistrationPromise(channel);
         regFuture.addListener(new ChannelFutureListener() {
             @Override
             public void operationComplete(ChannelFuture future) throws Exception {
                 Throwable cause = future.cause();
                 if (cause != null) {
                     // Registration on the EventLoop failed so fail the ChannelPromise directly to not cause an
                     // IllegalStateException once we try to access the EventLoop of the Channel.
                     promise.setFailure(cause);
                 } else {
                     // Registration was successful, so set the correct executor to use.
                     // See https://github.com/netty/netty/issues/2586
                     promise.registered();

                     doBind0(regFuture, channel, localAddress, promise);
                 }
             }
         });
         return promise;
     }
 }
```

调用的核心是 `doBind` 方法



