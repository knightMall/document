

## 解决循环引用问题

在 org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory#getEarlyBeanReference

```java
/**
	 * Obtain a reference for early access to the specified bean,
	 * typically for the purpose of resolving a circular reference.
	 * @param beanName the name of the bean (for error handling purposes)
	 * @param mbd the merged bean definition for the bean
	 * @param bean the raw bean instance
	 * @return the object to expose as bean reference
	 */
	protected Object getEarlyBeanReference(String beanName, RootBeanDefinition mbd, Object bean) {
		Object exposedObject = bean;
		if (!mbd.isSynthetic() && hasInstantiationAwareBeanPostProcessors()) {
			for (BeanPostProcessor bp : getBeanPostProcessors()) {
				if (bp instanceof SmartInstantiationAwareBeanPostProcessor) {
					SmartInstantiationAwareBeanPostProcessor ibp = (SmartInstantiationAwareBeanPostProcessor) bp;
					exposedObject = ibp.getEarlyBeanReference(exposedObject, beanName);
				}
			}
		}
		return exposedObject;
	}
```


## Spring解析xml

Resource resource = new ClassPathResource("application-bean.xml");

DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

BeanDefinitionReader definitionReader = new XmlBeanDefinitionReader(beanFactory);
int number = definitionReader.loadBeanDefinitions(resource);

Student student = beanFactory.getBean("student", Student.class);
Desk desk = beanFactory.getBean("desk", Desk.class);

通过上面的代码，我们可以清晰的知道，Spring创建IOC主要经历下面几个步骤：
1、将xml封装成Resource对象。
2、创建BeanFactory对象。
3、创建BeanDefinitionReader对象并和前一步创建的BeanFactory进行关联上，存放到beanDefinitionMap中
4、通过前一步创建的BeanDefinitionReader加载Resource对象，并返回Resource对象包含的beanDefinition数量
5、通过Bean名称获取bean，当然这里面也包含创建Bean的过程


## 解析BeanDefinition加载Resource步骤

第一步：封装Resource对象为EncodedResource对象，再变成InputSource对象
第二步，通过SAX的DocumentBuilder解析将xml文件封装成InputSource的对象，解析后封装到Document对象
第三步，创建BeanDefinitionDocumentReader来注册BeanDefinition，但真正parseBeanDefinition的是通过代理模式创建的BeanDefinitionParserDelegate解析的
第四步，通过Delegate解析后的将结果封装到BeanDefinitionHolder对象中
第五步，将BeanDefinition的对象存储到beanDefinitionMap中，以beanName为key,解析的BeanDefinition为value

我们重点看一下第四步的解析过程，其实要说简单也是很简单的，就是解析对应的bean的标签元素和元素对应的属性然后封装到BeanDefition对象中。其实我们看一下BeanDefition的一个默认实现AbstractBeanDefinition就很清楚了。

	public AbstractBeanDefinition parseBeanDefinitionElement(
			Element ele, String beanName, @Nullable BeanDefinition containingBean) {

		this.parseState.push(new BeanEntry(beanName));

		String className = null;
		if (ele.hasAttribute(CLASS_ATTRIBUTE)) {
			className = ele.getAttribute(CLASS_ATTRIBUTE).trim();
		}
		String parent = null;
		if (ele.hasAttribute(PARENT_ATTRIBUTE)) {
			parent = ele.getAttribute(PARENT_ATTRIBUTE);
		}

		try {
		   
			AbstractBeanDefinition bd = createBeanDefinition(className, parent);
            //解析定义的属性
			parseBeanDefinitionAttributes(ele, beanName, containingBean, bd);
			bd.setDescription(DomUtils.getChildElementValueByTagName(ele, DESCRIPTION_ELEMENT));
            //解析元数据
			parseMetaElements(ele, bd);
			//
			parseLookupOverrideSubElements(ele, bd.getMethodOverrides());
			parseReplacedMethodSubElements(ele, bd.getMethodOverrides());
            //解析构造方法元素
			parseConstructorArgElements(ele, bd);
			//解析属性元素
			parsePropertyElements(ele, bd);
			//
			parseQualifierElements(ele, bd);

			bd.setResource(this.readerContext.getResource());
			bd.setSource(extractSource(ele));

			return bd;
		}
		catch (ClassNotFoundException ex) {
			error("Bean class [" + className + "] not found", ele, ex);
		}
		catch (NoClassDefFoundError err) {
			error("Class that bean class [" + className + "] depends on not found", ele, err);
		}
		catch (Throwable ex) {
			error("Unexpected failure during bean definition parsing", ele, ex);
		}
		finally {
			this.parseState.pop();
		}

		return null;
	}

继续看createBeanDefinition方法，创建一个GenericBeanDefinition类，并通过className和classLoaader创建一个Class对象
	public static AbstractBeanDefinition createBeanDefinition(
			@Nullable String parentName, @Nullable String className, @Nullable ClassLoader classLoader) throws ClassNotFoundException {

		GenericBeanDefinition bd = new GenericBeanDefinition();
		bd.setParentName(parentName);
		if (className != null) {
			if (classLoader != null) {
				bd.setBeanClass(ClassUtils.forName(className, classLoader));
			}
			else {
				bd.setBeanClassName(className);
			}
		}
		return bd;
	}
	
	
/**
	 * Process the given bean element, parsing the bean definition
	 * and registering it with the registry.
	 */
	protected void processBeanDefinition(Element ele, BeanDefinitionParserDelegate delegate) {
		BeanDefinitionHolder bdHolder = delegate.parseBeanDefinitionElement(ele);
		if (bdHolder != null) {
			bdHolder = delegate.decorateBeanDefinitionIfRequired(ele, bdHolder);
			try {
				// Register the final decorated instance.
				BeanDefinitionReaderUtils.registerBeanDefinition(bdHolder, getReaderContext().getRegistry());
			}
			catch (BeanDefinitionStoreException ex) {
				getReaderContext().error("Failed to register bean definition with name '" +
						bdHolder.getBeanName() + "'", ele, ex);
			}
			// Send registration event.
			getReaderContext().fireComponentRegistered(new BeanComponentDefinition(bdHolder));
		}
	}

	
然后再封装成BeanDefinitionHolder对象返回。通过方法 BeanDefinitionReaderUtils.registerBeanDefinition(bdHolder, getReaderContext().getRegistry()); 添加到beanDefinitionMap中。调用getReaderContext().fireComponentRegistered(new BeanComponentDefinition(bdHolder));方法发送一个注册事件。
以上就是整个加载bean definition到BeanFactory的过程。



接着分析创建Bean的流程，通过下面这段代码来探索创建Bean的整个过程。
Student student = beanFactory.getBean("student", Student.class);
进入上面的方法，实际执行的是下面的代码：
protected <T> T doGetBean(final String name, @Nullable final Class<T> requiredType,
			@Nullable final Object[] args, boolean typeCheckOnly) throws BeansException {





















