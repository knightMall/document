- [AOP主要应用的场景](#AOP主要应用的场景)
- [AOP的基本概念](#AOP的基本概念)
  - [切面(Aspect)](#切面aspect)
  - [连接点](#连接点)
  - [通知(Advice)](#通知advice)
  - [切入点](#切入点)
  - [切面](#切面)
  - [织入 Weaving](#织入weaving)
  - [目标对象](#目标对象)
  - [AOP代理](#aop代理)
  - [前置通知](#前置通知)
  - [后置通知](#后置通知)
  - [返回后通知](#返回后通知)
  - [环绕通知](#环绕通知)
  - [异常通知](#异常通知)
- [Spring AOP源码分析](#springaop源码分析)
  - [寻找入口](#寻找入口)
  - [选择策略](#选择策略)
  - [调用方法](#调用方法)
  - [触发通知](#触发通知)







### AOP主要应用的场景

AOP是OOP的延续，是Aspect Oriented Programming的缩写，意思是面向切面编程。可以通过预编译方式和运行期动态代理实现在不修改源代码的情况下给程序动态统一添加功能的一种技术。AOP设计模式孜孜不倦追求的是调用者和被调用者之间的解耦，AOP 可以说也是这种目标的一种实现。我们现在做的一些非业务，如：日志、事务、安全等都会写在业务代码中(也即是说，这些非业务类横切于业务类)，但这些代码往往是重复，复制——粘贴式的代码会给程序的维护带来不便，AOP 就实现了把这些业务需求与系统需求分开来做。这种解决的方式也称代理机制。

### AOP的基本概念

#### 切面(Aspect)

具有相同规则的方法的集合体。“切面”在ApplicationContext 中`<aop:aspect>`来配置

#### 连接点

能够插入切面的一个点，例如调用方法时、抛出异常时、修改字段时。

#### 通知(Advice)

切面对于某个连接点产生的动作。其中一个切面可以包含多个通知

#### 切入点

匹配连接点的断言，在 AOP 中通知和一个切入点表达式关联。切面中的所有通知所关注的连接点，都由切入点表达式来决定。需要代理的具体方法

#### 切面

切面是切点和通知的结合。

#### 织入 Weaving

织入就是将切面应用到目标对象来创建新的代理对象的过程。在对象的生命周期可以在多个阶段进行织入：

- 编译期
- 类加载期
- 运行期，Spring使用的这种方式，基于子类或实现接口的方式创建一个代理对象



#### 目标对象

被一个或者多个切面所通知的对象。被代理的对象

#### AOP代理

在 Spring AOP 中有两种代理方式，JDK 动态代理和 CGLib 代理。默认情况下，TargetObject 实现了接口时，则采用 JDK 动态代理，
强制使用 CGLib 代理需要将 `<aop:config>`的 proxy-target-class 属性设为 true。

#### 前置通知

在某连接点（JoinPoint）之前执行的通知，但这个通知不能阻止连接点前的执行。ApplicationContext
中在`<aop:aspect>`里面使用`<aop:before>`元素进行声明

#### 后置通知

当某连接点退出的时候执行的通知（不论是正常返回还是异常退出）。ApplicationContext 中在`<aop:aspect>`里面使用`<aop:after>`元素进行声明。

#### 返回后通知

在某连接点正常完成后执行的通知，不包括抛出异常的情况。ApplicationContext 中在<aop:aspect>里面使用`<after-returning>`元素进行声明。

#### 环绕通知

包围一个连接点的通知，类似 Web 中 Servlet 规范中的 Filter 的 doFilter 方法。可以在方法的调用前后完成自定义的行为，也可以选择不执行。ApplicationContext 中在`<aop:aspect>`里面使用
`<aop:around>`元素进行声明。

#### 异常通知

在 方 法 抛 出 异 常 退 出 时 执 行 的 通 知 。 ApplicationContext 中 在 `<aop:aspect>` 里 面 使 用`<aop:after-throwing>`元素进行声明。注：可以将多个通知应用到一个目标对象上，即可以将多个切面织入到同一目标对象。



通常情况下，表达式中使用”execution“就可以满足大部分的要求。表达式格式如下

```properties
execution(modifiers-pattern? ret-type-pattern declaring-type-pattern? name-pattern(param-pattern)
throws-pattern?
```

modifiers-pattern：方法的操作权限

ret-type-pattern：返回值

declaring-type-pattern：方法所在的包

name-pattern：方法名

parm-pattern：参数名

throws-pattern：异常

其中 ，除 ret-type-pattern 和 name-pattern 之外，其他都是可选的。上例中，`execution(*
com.spring.service.*.*(..))`表示 com.spring.service 包下，返回值为任意类型；方法名任意；参数不作
限制的所有方法





### Spring AOP源码分析

具体流程：

![spring-aop-流程](../../images/210718/spring-aop-流程.png)





#### 寻找入口

Spring 的 AOP 是通过接入 `BeanPostProcessor` 后置处理器开始的，它是 Spring IOC 容器经常使用到的一个特性，这个 Bean 后置处理器是一个监听器，可以监听容器触发的 Bean 声明周期事件。后置处理器向容器注册以后，容器中管理的 Bean 就具备了接收 IOC 容器事件回调的能力。BeanPostProcessor 的使用非常简单，只需要提供一个实现接口 BeanPostProcessor 的实现类，然后在 Bean 的配置文件中设置即可。调用的入口是在：org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory#doCreateBean方法中调用了`populateBean`方法后在调用初始化bean的方法 initializeBean  都在  AbstractAutowireCapableBeanFactory 类中。在这个方法中会执行在xml中配置的 init-method 属性指定的方法。如下图

![](../../images/210719/bean的init-method属性.png)

在执行这个方法之前会执行 applyBeanPostProcessorsBeforeInitialization ，执行之后会执行 applyBeanPostProcessorsAfterInitialization 都在类 AbstractAutowireCapableBeanFactory 中。它们分别执行的其实就是`org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization` 和 `org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization` 然后其中实现AOP主要功能的也是`BeanPostProcessor`的一个子类`org.springframework.aop.framework.autoproxy.AbstractAutoProxyCreator`。这里就找到入口了。

![AbstractAutoProxyCreator类结构图](../../images/210720/AbstractAutoProxyCreator类结构图.png)





#### 选择策略

在org.springframework.aop.framework.autoproxy.AbstractAutoProxyCreator#postProcessAfterInitialization方法重写了`postProcessAfterInitialization` 方法

```java
	/**
	 * Create a proxy with the configured interceptors if the bean is
	 * identified as one to proxy by the subclass.
	 * @see #getAdvicesAndAdvisorsForBean
	 */
	@Override
	public Object postProcessAfterInitialization(@Nullable Object bean, String beanName) {
		if (bean != null) {
			Object cacheKey = getCacheKey(bean.getClass(), beanName);
			if (this.earlyProxyReferences.remove(cacheKey) != bean) {
                //在这个方法创建代理对象
				return wrapIfNecessary(bean, beanName, cacheKey);
			}
		}
		return bean;
	}


	protected Object wrapIfNecessary(Object bean, String beanName, Object cacheKey) {
		if (StringUtils.hasLength(beanName) && this.targetSourcedBeans.contains(beanName)) {
			return bean;
		}
        //判断是不是应该代理这个类
		if (Boolean.FALSE.equals(this.advisedBeans.get(cacheKey))) {
			return bean;
		}
        //判断bean是否是InfrastructureClass
        //InfrastructureClass表示是一些基础类，例如 Advice/PointCut/Advisor 等接口的实现类
        
		if (isInfrastructureClass(bean.getClass()) || shouldSkip(bean.getClass(), beanName)) {
			this.advisedBeans.put(cacheKey, Boolean.FALSE);
			return bean;
		}
		//获取Bean对应的advice
		// Create proxy if we have advice.
		Object[] specificInterceptors = getAdvicesAndAdvisorsForBean(bean.getClass(), beanName, null);
		if (specificInterceptors != DO_NOT_PROXY) {
			this.advisedBeans.put(cacheKey, Boolean.TRUE);
            //创建代理对象
			Object proxy = createProxy(
					bean.getClass(), beanName, specificInterceptors, new SingletonTargetSource(bean));
			this.proxyTypes.put(cacheKey, proxy.getClass());
			return proxy;
		}

		this.advisedBeans.put(cacheKey, Boolean.FALSE);
		return bean;
	}
	/**
	 * Create an AOP proxy for the given bean.
	 * @param beanClass the class of the bean
	 * @param beanName the name of the bean
	 * @param specificInterceptors the set of interceptors that is
	 * specific to this bean (may be empty, but not null)
	 * @param targetSource the TargetSource for the proxy,
	 * already pre-configured to access the bean
	 * @return the AOP proxy for the bean
	 * @see #buildAdvisors
	 */
protected Object createProxy(Class<?> beanClass, @Nullable String beanName,
			@Nullable Object[] specificInterceptors, TargetSource targetSource) {

		if (this.beanFactory instanceof ConfigurableListableBeanFactory) {
			AutoProxyUtils.exposeTargetClass((ConfigurableListableBeanFactory) this.beanFactory, beanName, beanClass);
		}

		ProxyFactory proxyFactory = new ProxyFactory();
		proxyFactory.copyFrom(this);

		if (!proxyFactory.isProxyTargetClass()) {
			if (shouldProxyTargetClass(beanClass, beanName)) {
				proxyFactory.setProxyTargetClass(true);
			}
			else {
				evaluateProxyInterfaces(beanClass, proxyFactory);
			}
		}

		Advisor[] advisors = buildAdvisors(beanName, specificInterceptors);
		proxyFactory.addAdvisors(advisors);
		proxyFactory.setTargetSource(targetSource);
		customizeProxyFactory(proxyFactory);

		proxyFactory.setFrozen(this.freezeProxy);
		if (advisorsPreFiltered()) {
			proxyFactory.setPreFiltered(true);
		}

		return proxyFactory.getProxy(getProxyClassLoader());
	}
```

进入`proxyFactory`的`getProxy`方法，最终调用的是`org.springframework.aop.framework.DefaultAopProxyFactory#createAopProxy`方法

```java
public class DefaultAopProxyFactory implements AopProxyFactory, Serializable {

	@Override
	public AopProxy createAopProxy(AdvisedSupport config) throws AopConfigException {
		if (config.isOptimize() || config.isProxyTargetClass() || hasNoUserSuppliedProxyInterfaces(config)) {
			Class<?> targetClass = config.getTargetClass();
			if (targetClass == null) {
				throw new AopConfigException("TargetSource cannot determine target class: " +
						"Either an interface or a target is required for proxy creation.");
			}
			if (targetClass.isInterface() || Proxy.isProxyClass(targetClass)) {
				return new JdkDynamicAopProxy(config);
			}
			return new ObjenesisCglibAopProxy(config);
		}
		else {
			return new JdkDynamicAopProxy(config);
		}
	}
```

从这里可以看到，默认是使用JDK动态代理创建代理对象的.是实现了接口使用JDK动态代理



#### 调用方法

通过上面的生成AOP代理的有两种策略，JDK动态代理和Cglib。这里选择JDK动态代理创建代理对象为例。上面已经说明了，如下图

![spring-AOP代理策略创建](../../images/210724/spring-AOP代理策略创建.png)

接着继续看`getProxy`方法。首选看一下Spring AOP的主要组件的类图结构

![ProxyFactory结构图](../../images/210724/ProxyFactory结构图.png)

![JdkDynamicAopProxy结构图](../../images/210724/JdkDynamicAopProxy结构图.png)

创建代理对象的方法就在`JdkDynamicAopProxy`类中

```java
    /**
    * 获取代理类要实现的接口 , 除了 Advised 对象中配置的 , 还会加上 SpringProxy, Advised(opaque=false)
    * 检查上面得到的接口中有没有定义 equals 或者 hashcode 的接口
    * 调用 Proxy.newProxyInstance 创建代理对象
    */
	@Override
	public Object getProxy(@Nullable ClassLoader classLoader) {
		if (logger.isTraceEnabled()) {
			logger.trace("Creating JDK dynamic proxy: " + this.advised.getTargetSource());
		}
        //这个方法会添加SpringProx和Advised接口
		Class<?>[] proxiedInterfaces = AopProxyUtils.completeProxiedInterfaces(this.advised, true);
		findDefinedEqualsAndHashCodeMethods(proxiedInterfaces);
        //基于这个方法真正创建出代理对象
		return Proxy.newProxyInstance(classLoader, proxiedInterfaces, this);
	}
```

通过注释我们应该已经看得非常明白代理对象的生成过程，此处不再赘述。下面的问题是，代理对象生成了，那切面是如何织入的？
我们知道 InvocationHandler 是 JDK 动态代理的核心，生成的代理对象的方法调用都会委托到InvocationHandler.invoke()方法。而从 JdkDynamicAopProxy 的源码我们可以看到这个类其实也实现了 InvocationHandler，下面我们分析 Spring AOP 是如何织入切面的，直接上源码看 invoke()方法：

```java
	/**
	 * Implementation of {@code InvocationHandler.invoke}.
	 * <p>Callers will see exactly the exception thrown by the target,
	 * unless a hook method throws an exception.
	 */
	@Override
	@Nullable
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object oldProxy = null;
		boolean setProxyContext = false;

		TargetSource targetSource = this.advised.targetSource;
		Object target = null;

		try {
            //
			if (!this.equalsDefined && AopUtils.isEqualsMethod(method)) {
				// The target does not implement the equals(Object) method itself.
				return equals(args[0]);
			}
			else if (!this.hashCodeDefined && AopUtils.isHashCodeMethod(method)) {
				// The target does not implement the hashCode() method itself.
				return hashCode();
			}
			else if (method.getDeclaringClass() == DecoratingProxy.class) {
				// There is only getDecoratedClass() declared -> dispatch to proxy config.
				return AopProxyUtils.ultimateTargetClass(this.advised);
			}
            //Advised 接口或者其父接口中定义的方法,直接反射调用,不应用通知
			else if (!this.advised.opaque && method.getDeclaringClass().isInterface() &&
					method.getDeclaringClass().isAssignableFrom(Advised.class)) {
				// Service invocations on ProxyConfig with the proxy config...
				return AopUtils.invokeJoinpointUsingReflection(this.advised, method, args);
			}

			Object retVal;

			if (this.advised.exposeProxy) {
				// Make invocation available if necessary.
				oldProxy = AopContext.setCurrentProxy(proxy);
				setProxyContext = true;
			}
			//获取目标对象
			// Get as late as possible to minimize the time we "own" the target,
			// in case it comes from a pool.
			target = targetSource.getTarget();
			Class<?> targetClass = (target != null ? target.getClass() : null);

			// Get the interception chain for this method.
			List<Object> chain = this.advised.getInterceptorsAndDynamicInterceptionAdvice(method, targetClass);

             //如果没有可以应用到此方法的通知(Interceptor)，此直接反射调用 Method.invoke(target, args)
			// Check whether we have any advice. If we don't, we can fallback on direct
			// reflective invocation of the target, and avoid creating a MethodInvocation.
			if (chain.isEmpty()) {
				// We can skip creating a MethodInvocation: just invoke the target directly
				// Note that the final invoker must be an InvokerInterceptor so we know it does
				// nothing but a reflective operation on the target, and no hot swapping or fancy proxying.
				Object[] argsToUse = AopProxyUtils.adaptArgumentsIfNecessary(method, args);
				retVal = AopUtils.invokeJoinpointUsingReflection(target, method, argsToUse);
			}
			else {
                 //创建 MethodInvocation
				// We need to create a method invocation...
				MethodInvocation invocation =
						new ReflectiveMethodInvocation(proxy, target, method, args, targetClass, chain);
				// Proceed to the joinpoint through the interceptor chain.
				retVal = invocation.proceed();
			}

			// Massage return value if necessary.
			Class<?> returnType = method.getReturnType();
			if (retVal != null && retVal == target &&
					returnType != Object.class && returnType.isInstance(proxy) &&
					!RawTargetAccess.class.isAssignableFrom(method.getDeclaringClass())) {
				// Special case: it returned "this" and the return type of the method
				// is type-compatible. Note that we can't help if the target sets
				// a reference to itself in another returned object.
				retVal = proxy;
			}
			else if (retVal == null && returnType != Void.TYPE && returnType.isPrimitive()) {
				throw new AopInvocationException(
						"Null return value from advice does not match primitive return type for: " + method);
			}
			return retVal;
		}
		finally {
			if (target != null && !targetSource.isStatic()) {
				// Must have come from TargetSource.
				targetSource.releaseTarget(target);
			}
			if (setProxyContext) {
				// Restore old proxy.
				AopContext.setCurrentProxy(oldProxy);
			}
		}
	}

```

主要实现思路可以简述为：首先获取应用到此方法上的通知链（Interceptor Chain）。如果有通知，则应用通知，并执行 JoinPoint；如果没有通知，则直接反射执行 JoinPoint。而这里的关键是通知链是如何获取的以及它又是如何执行的呢？现在来逐一分析。首先，从上面的代码可以看到，通知链是通过Advised.getInterceptorsAndDynamicInterceptionAdvice()这个方法来获取的，我们来看下这个方法的实现逻辑：

```java
	public List<Object> getInterceptorsAndDynamicInterceptionAdvice(Method method, @Nullable Class<?> targetClass) {
		MethodCacheKey cacheKey = new MethodCacheKey(method);
		List<Object> cached = this.methodCache.get(cacheKey);
		if (cached == null) {
			cached = this.advisorChainFactory.getInterceptorsAndDynamicInterceptionAdvice(
					this, method, targetClass);
			this.methodCache.put(cacheKey, cached);
		}
		return cached;
	}
```

通过上面的源码我们可以看到，实际获取通知的实现逻辑其实是由 AdvisorChainFactory 的
getInterceptorsAndDynamicInterceptionAdvice()方法来完成的，且获取到的结果会被缓存，
缓存的key是以全类目加方法名拼接而成。下面来分析 DefaultAdvisorChainFactory的getInterceptorsAndDynamicInterceptionAdvice()方法的实现

```java
	@Override
	public List<Object> getInterceptorsAndDynamicInterceptionAdvice(
			Advised config, Method method, @Nullable Class<?> targetClass) {

		// This is somewhat tricky... We have to process introductions first,
		// but we need to preserve order in the ultimate list.
		AdvisorAdapterRegistry registry = GlobalAdvisorAdapterRegistry.getInstance();
		Advisor[] advisors = config.getAdvisors();
		List<Object> interceptorList = new ArrayList<>(advisors.length);
		Class<?> actualClass = (targetClass != null ? targetClass : method.getDeclaringClass());
		Boolean hasIntroductions = null;

		for (Advisor advisor : advisors) {
			if (advisor instanceof PointcutAdvisor) {
				// Add it conditionally.
				PointcutAdvisor pointcutAdvisor = (PointcutAdvisor) advisor;
				if (config.isPreFiltered() || pointcutAdvisor.getPointcut().getClassFilter().matches(actualClass)) {
					MethodMatcher mm = pointcutAdvisor.getPointcut().getMethodMatcher();
					boolean match;
					if (mm instanceof IntroductionAwareMethodMatcher) {
						if (hasIntroductions == null) {
							hasIntroductions = hasMatchingIntroductions(advisors, actualClass);
						}
						match = ((IntroductionAwareMethodMatcher) mm).matches(method, actualClass, hasIntroductions);
					}
					else {
						match = mm.matches(method, actualClass);
					}
					if (match) {
						MethodInterceptor[] interceptors = registry.getInterceptors(advisor);
						if (mm.isRuntime()) {
							// Creating a new object instance in the getInterceptors() method
							// isn't a problem as we normally cache created chains.
							for (MethodInterceptor interceptor : interceptors) {
								interceptorList.add(new InterceptorAndDynamicMethodMatcher(interceptor, mm));
							}
						}
						else {
							interceptorList.addAll(Arrays.asList(interceptors));
						}
					}
				}
			}
			else if (advisor instanceof IntroductionAdvisor) {
				IntroductionAdvisor ia = (IntroductionAdvisor) advisor;
				if (config.isPreFiltered() || ia.getClassFilter().matches(actualClass)) {
					Interceptor[] interceptors = registry.getInterceptors(advisor);
					interceptorList.addAll(Arrays.asList(interceptors));
				}
			}
			else {
				Interceptor[] interceptors = registry.getInterceptors(advisor);
				interceptorList.addAll(Arrays.asList(interceptors));
			}
		}

		return interceptorList;
	}

```

这个方法执行完成后，Advised 中配置能够应用到连接点（JoinPoint）或者目标类（Target Object）
的 Advisor 全部被转化成了 MethodInterceptor，接下来我们再看下得到的拦截器链是怎么起作用的。在JdkDynamicAopProxy的invoke方法中有如下一段代码：

```java
	if (chain.isEmpty()) {
		// We can skip creating a MethodInvocation: just invoke the target directly
		// Note that the final invoker must be an InvokerInterceptor so we know it does
		// nothing but a reflective operation on the target, and no hot swapping or fancy proxying.
		Object[] argsToUse = AopProxyUtils.adaptArgumentsIfNecessary(method, args);
		retVal = AopUtils.invokeJoinpointUsingReflection(target, method, argsToUse);
	}
	else {
		// We need to create a method invocation...
		MethodInvocation invocation =
				new ReflectiveMethodInvocation(proxy, target, method, args, targetClass, chain);
		// Proceed to the joinpoint through the interceptor chain.
		retVal = invocation.proceed();
	}

```

从这段代码可以看出，如果得到的拦截器链为空，则直接反射调用目标方法，否则创建
MethodInvocation，调用其 proceed()方法，触发拦截器链的执行，来看下具体代码:

```java
	@Override
	@Nullable
	public Object proceed() throws Throwable {
         //如果 Interceptor 执行完了，则执行 joinPoint
		// We start with an index of -1 and increment early.
		if (this.currentInterceptorIndex == this.interceptorsAndDynamicMethodMatchers.size() - 1) {
			return invokeJoinpoint();
		}

		Object interceptorOrInterceptionAdvice =
				this.interceptorsAndDynamicMethodMatchers.get(++this.currentInterceptorIndex);
		if (interceptorOrInterceptionAdvice instanceof InterceptorAndDynamicMethodMatcher) {
            //如果要动态匹配 joinPoint
			// Evaluate dynamic method matcher here: static part will already have
			// been evaluated and found to match.
			InterceptorAndDynamicMethodMatcher dm =
					(InterceptorAndDynamicMethodMatcher) interceptorOrInterceptionAdvice;
			Class<?> targetClass = (this.targetClass != null ? this.targetClass : this.method.getDeclaringClass());
			if (dm.methodMatcher.matches(this.method, targetClass, this.arguments)) {
				return dm.interceptor.invoke(this);
			}
			else {
                //动态匹配失败时,略过当前 Intercetpor,调用下一个 Interceptor
				// Dynamic matching failed.
				// Skip this interceptor and invoke the next in the chain.
				return proceed();
			}
		}
		else {
            //执行当前 Intercetpor
			// It's an interceptor, so we just invoke it: The pointcut will have
			// been evaluated statically before this object was constructed.
			return ((MethodInterceptor) interceptorOrInterceptionAdvice).invoke(this);
		}
	}

```

至此，通知链就完美地形成了。我们再往下来看 invokeJoinpointUsingReflection()方法，其实就是反
射调用:

```java
	/**
	 * Invoke the given target via reflection, as part of an AOP method invocation.
	 * @param target the target object
	 * @param method the method to invoke
	 * @param args the arguments for the method
	 * @return the invocation result, if any
	 * @throws Throwable if thrown by the target method
	 * @throws org.springframework.aop.AopInvocationException in case of a reflection error
	 */
	@Nullable
	public static Object invokeJoinpointUsingReflection(@Nullable Object target, Method method, Object[] args)
			throws Throwable {

		// Use reflection to invoke the method.
		try {
			ReflectionUtils.makeAccessible(method);
			return method.invoke(target, args);
		}
		catch (InvocationTargetException ex) {
			// Invoked method threw a checked exception.
			// We must rethrow it. The client won't see the interceptor.
			throw ex.getTargetException();
		}
		catch (IllegalArgumentException ex) {
			throw new AopInvocationException("AOP configuration seems to be invalid: tried calling method [" +
					method + "] on target [" + target + "]", ex);
		}
		catch (IllegalAccessException ex) {
			throw new AopInvocationException("Could not access method [" + method + "]", ex);
		}
	}
```



#### 触发通知

在为 AopProxy 代理对象配置拦截器的实现中，有一个取得拦截器的配置过程，这个过
程是由 DefaultAdvisorChainFactory 实现的，这个工厂类负责生成拦截器链，在它的
getInterceptorsAndDynamicInterceptionAdvice 方法中，有一个适配器和注册过程，
通过配置 Spring 预先设计好的拦截器，Spring 加入了它对 AOP 实现的处理。

```java
/**
* 从提供的配置实例 config 中获取 advisor 列表 , 遍历处理这些 advisor. 如果是 IntroductionAdvisor,
* 则判断此 Advisor 能否应用到目标类 targetClass 上 . 如果是 PointcutAdvisor, 则判断
* 此 Advisor 能否应用到目标方法 Method 上 . 将满足条件的 Advisor 通过 AdvisorAdaptor 转化成 Interceptor 列表返回 .
*/
@Override
public List<Object> getInterceptorsAndDynamicInterceptionAdvice(
Advised config, Method Method, @Nullable Class<?> targetClass) {
 List<Object> interceptorList = new ArrayList<>(config.getAdvisors().length);
Class<?> actualClass = (targetClass != null ? targetClass : Method.getDeclaringClass());
//查看是否包含 IntroductionAdvisor
boolean hasIntroductions = hasMatchingIntroductions(config, actualClass);
//这里实际上注册一系列 AdvisorAdapter,用于将 Advisor 转化成 MethodInterceptor
AdvisorAdapterRegistry registry = GlobalAdvisorAdapterRegistry.getInstance();
...
return interceptorList;
}
```

GlobalAdvisorAdapterRegistry 负责拦截器的适配和注册过程。

```java
public abstract class GlobalAdvisorAdapterRegistry {
/**
* Keep track of a single instance so we can return it to classes that request it.
*/
private static AdvisorAdapterRegistry instance = new DefaultAdvisorAdapterRegistry();
/**
* Return the singleton {@link DefaultAdvisorAdapterRegistry} instance.
*/
public static AdvisorAdapterRegistry getInstance() {
return instance;
}
/**
* Reset the singleton {@link DefaultAdvisorAdapterRegistry}, removing any
* {@link AdvisorAdapterRegistry#registerAdvisorAdapter(AdvisorAdapter) registered}
* adapters.
*/
static void reset() {
instance = new DefaultAdvisorAdapterRegistry();
}
}
```

而 GlobalAdvisorAdapterRegistry 起到了适配器和单例模式的作用，提供了一个
DefaultAdvisorAdapterRegistry，它用来完成各种通知的适配和注册过程

```java
public class DefaultAdvisorAdapterRegistry implements AdvisorAdapterRegistry, Serializable
{
    private final List<AdvisorAdapter> adapters = new ArrayList<>(3);
/**
* Create a new DefaultAdvisorAdapterRegistry, registering well-known adapters.
*/
public DefaultAdvisorAdapterRegistry() {
registerAdvisorAdapter(new MethodBeforeAdviceAdapter());
registerAdvisorAdapter(new AfterReturningAdviceAdapter());
registerAdvisorAdapter(new ThrowsAdviceAdapter());
}
@Override
public Advisor wrap(Object adviceObject) throws UnknownAdviceTypeException {
if (adviceObject instanceof Advisor) {
return (Advisor) adviceObject;
}
if (!(adviceObject instanceof Advice)) {
throw new UnknownAdviceTypeException(adviceObject);
}
Advice advice = (Advice) adviceObject;
if (advice instanceof MethodInterceptor) {
// So well-known it doesn't even need an adapter.
return new DefaultPointcutAdvisor(advice);
}
for (AdvisorAdapter adapter : this.adapters) {
// Check that it is supported.
if (adapter.supportsAdvice(advice)) {
return new DefaultPointcutAdvisor(advice);
}
}
throw new UnknownAdviceTypeException(advice);
}
@Override
public MethodInterceptor[] getInterceptors(Advisor advisor) throws
    UnknownAdviceTypeException {
    List<MethodInterceptor> interceptors = new ArrayList<>(3);
    Advice advice = advisor.getAdvice();
if (advice instanceof MethodInterceptor) {
	interceptors.add((MethodInterceptor) advice);
}
for (AdvisorAdapter adapter : this.adapters) {
       if (adapter.supportsAdvice(advice)) {
    interceptors.add(adapter.getInterceptor(advisor));
    }
    }
    if (interceptors.isEmpty()) {
    throw new UnknownAdviceTypeException(advisor.getAdvice());
    }
    return interceptors.toArray(new MethodInterceptor[interceptors.size()]);
    }
    @Override
    public void registerAdvisorAdapter(AdvisorAdapter adapter) {
    this.adapters.add(adapter);
    }
}
```

DefaultAdvisorAdapterRegistry 设置了一系列的是配置，正是这些适配器的实现，为
Spring AOP 提供了编织能力。下面以 MethodBeforeAdviceAdapter 为例，看具体的
实现：

```java
class MethodBeforeAdviceAdapter implements AdvisorAdapter, Serializable {
@Override
public boolean supportsAdvice(Advice advice) {
return (advice instanceof MethodBeforeAdvice);
}
@Override
public MethodInterceptor getInterceptor(Advisor advisor) {
MethodBeforeAdvice advice = (MethodBeforeAdvice) advisor.getAdvice();
return new MethodBeforeAdviceInterceptor(advice);
}
}
```

Spring AOP 为了实现 advice 的织入，设计了特定的拦截器对这些功能进行了封装。我
们接着看 MethodBeforeAdviceInterceptor 如何完成封装的？

```java
public class MethodBeforeAdviceInterceptor implements MethodInterceptor, Serializable {
private MethodBeforeAdvice advice;
/**
* Create a new MethodBeforeAdviceInterceptor for the given advice.
* @param advice the MethodBeforeAdvice to wrap
*/
public MethodBeforeAdviceInterceptor(MethodBeforeAdvice advice) {
Assert.notNull(advice, "Advice must not be null");
this.advice = advice;
}
@Override
public Object invoke(MethodInvocation mi) throws Throwable {
this.advice.before(mi.getMethod(), mi.getArguments(), mi.getThis() );
return mi.proceed();
}
}    
```

可以看到，invoke 方法中，首先触发了 advice 的 before 回调，然后才是 proceed。
AfterReturningAdviceInterceptor 的源码：

```java
public class AfterReturningAdviceInterceptor implements MethodInterceptor, AfterAdvice,
Serializable {
private final AfterReturningAdvice advice;
/**
* Create a new AfterReturningAdviceInterceptor for the given advice.
* @param advice the AfterReturningAdvice to wrap
*/
public AfterReturningAdviceInterceptor(AfterReturningAdvice advice) {
Assert.notNull(advice, "Advice must not be null");
this.advice = advice;
}
@Override
public Object invoke(MethodInvocation mi) throws Throwable {
Object retVal = mi.proceed();
this.advice.afterReturning(retVal, mi.getMethod(), mi.getArguments(), mi.getThis());
return retVal;
}
}
```

















