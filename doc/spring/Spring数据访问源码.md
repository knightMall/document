

## 什么是事务

事务(Transaction)是访问并可能更新数据库中各种数据项的一个程序执行单元(unit)。
特点：事务是恢复和并发控制的基本单位。事务应该具有4个属性，这四个属性通常称为ACID特性。

- 原子性（Atomicity），一个事务是一个不可分割的工作单位，事务中包括的诸操作要
  么都做，要么都不做。
- 一致性 （Consistency），事务必须是使数据库从一个一致性状态变到另一个一致性状态。
  一致性与原子性是密切相关的。
- 隔离性（Isolation），一个事务的执行不能被其他事务干扰。即一个事务内部的操作及
  使用的数据对并发的其他事务是隔离的，并发执行的各个事务之间不能互相干扰。
- 持久性（Durability），持久性也称永久性（Permanence），指一个事务一旦提交，它
  对数据库中数据的改变就应该是永久性的。接下来的其他操作或故障不应该对其有任何
  影响。

## Spring 事务的传播属性

所谓 spring 事务的传播属性，就是定义在存在多个事务同时存在的时候，spring 应该如
何处理这些事务的行为。这些属性在 `TransactionDefinition `中定义，具体常量的解释见
下表：

| 常量名称                  | 常量解释                                                     |
| ------------------------- | ------------------------------------------------------------ |
| PROPAGATION_REQUIRED      | 支持当前事务，如果当前没有事务，就新建一个事务。这是最常见的选择，也是 Spring默认的事务的传播。 |
| PROPAGATION_REQUIRES_NEW  | 新建事务，如果当前存在事务，把当前事务挂起。新建的事务将和被挂起的事务没有任<br/>何关系，是两个独立的事务，外层事务失败回滚之后，不能回滚内层事务执行的结果，<br/>内层事务失败抛出异常，外层事务捕获，也可以不处理回滚操作。 |
| PROPAGATION_SUPPORTS      | 支持当前事务，如果当前没有事务，就以非事务方式执行。         |
| PROPAGATION_MANDATORY     | 支持当前事务，如果当前没有事务，就抛出异常。                 |
| PROPAGATION_NOT_SUPPORTED | 以非事务方式执行操作，如果当前存在事<br/>务，就把当前事务挂起。 |
| PROPAGATION_NEVER         | 以非事务方式执行，如果当前存在事务，则<br/>抛出异常。        |
| PROPAGATION_NESTED        | 如果一个活动的事务存在，则运行在一个嵌套的事务中。如果没有活动事务，则按<br/>REQUIRED 属性执行。它使用了一个单独的<br/>事务，这个事务拥有多个可以回滚的保存<br/>点。内部事务的回滚不会对外部事务造成影<br/>响。它只对<br/>DataSourceTransactionManager 事务管<br/>理器起效。 |



## 数据库隔离级别

| 隔离级别         | 隔离级别的值 | 导致的问题                                                   |
| ---------------- | ------------ | ------------------------------------------------------------ |
| Read-Uncommitted | 0            | 导致脏读                                                     |
| Read-Committed   | 1            | 避免脏读，允许不可重复读和幻读                               |
| Repeatable-Read  | 2            | 避免脏读，不可重复读，允许幻读                               |
| Serializable     | 3            | 串行化读，事务只能一个一个执行，避免了<br/>脏读、不可重复读、幻读。执行效率慢，使<br/>用时慎重 |

脏读：一事务对数据进行了增删改，但未提交，另一事务可以读取到未提交的数据。如
果第一个事务这时候回滚了，那么第二个事务就读到了脏数据。

不可重复读：一个事务中发生了两次读操作，第一次读操作和第二次操作之间，另外一
个事务对数据进行了修改，这时候两次读取的数据是不一致的。

幻读：第一个事务对一定范围的数据进行批量修改，第二个事务在这个范围增加一条数
据，这时候第一个事务就会丢失对新增数据的修改。

总结：隔离级别越高，越能保证数据的完整性和一致性，但是对并发性能的影响也越大。
大多数的数据库默认隔离级别为 Read Commited，比如 SqlServer、Oracle
少数数据库默认隔离级别为：Repeatable Read 比如： MySQL InnoDB

## Spring的事务隔离级别

| 常量                       | 解释                                                         |
| -------------------------- | ------------------------------------------------------------ |
| ISOLATION_DEFAULT          | 这是个 PlatfromTransactionManager 默<br/>认的隔离级别，使用数据库默认的事务隔离<br/>级别。另外四个与 JDBC 的隔离级别相对<br/>应 |
| ISOLATION_READ_UNCOMMITTED | 这是事务最低的隔离级别，它允许另外一个<br/>事务可以看到这个事务未提交的数据。这种<br/>隔离级别会产生脏读，不可重复读和幻像<br/>读。 |
| ISOLATION_READ_COMMITTED   | 保证一个事务修改的数据提交后才能被另<br/>外一个事务读取。另外一个事务不能读取该<br/>事务未提交的数据 |
| ISOLATION_REPEATABLE_READ  | 这种事务隔离级别可以防止脏读，不可重复<br/>读。但是可能出现幻像读。 |
| ISOLATION_SERIALIZABLE     | 这是花费最高代价但是最可靠的事务隔离<br/>级别。事务被处理为顺序执行。 |





