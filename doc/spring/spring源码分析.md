## Spring加载XML文件流程

具体如下图：

![Spring加载xml流程](../../images/spring/Spring加载xml流程.png)

1.将xml文件封装成一个resource资源。

2.定义好Bean工厂

3.定义好XMLBeanDefitionReader对象，将工厂类作为参数，使两者联系起来

4.再包装成EncoderResource对象，该对象包含了编码

5.判断缓存中是否已经加载该资源

6.获取resource对应的流对象，通过SAX解析获取到Document对象

7.创建BeanDefintionDocumentReader对象，该对象定义了解析XML的模板模式

8.创建委托者，通过委托者解析import、bean、alias、beans标签

9.将XML文件解析为BeanDefition对象，封装到BeanDefitionHolder对象中

10.注册BeanDefition对象到Bean工厂中的beanDefitionMap中，以bean name为key，BeanDefition为值

11.创建完后触发解析XML完成的event，响应的监听器方法执行。（使用观察者模式）



  

## Spring创建Bean的过程

具体如下图：

![从Spring容器中获取Bean](../../images/spring/从Spring容器中获取Bean.png)

1.Spring管理的Bean实际上存放在缓存CurrentHashMap中，（singletonObjects对象中）

2.Map中key是bean的名称，value是一个Object对象

3.首先，判断该Bean是否已经缓存在该Map对象中

4.根据bean的scope来创建Bean，但是具体的创建过程是一样的

5.标记bean正在创建

6.获取BeanDefition对象，判断是否还有其他依赖。如果有则先创建依赖

7.创建RootBeanDefition对象，如果有父类则父类的BeanDefition对象也会封装进来，该对象贯穿整个Bean的创建流程

8.通过反射创建Bean，如果构造方法私有同样可以创建，设置setAccessible(true)

9.封装成BeanWrapper对象返回

10.组装创建Bean的属性

11.如果是scope是singleton，将创建好的Bean放到singletonObjects对象中







## Spring AOP功能

### APO核心功能

- advice

  通知，增强的代码。也就是公共代码

- pointcut

  确定那些方法或类的需要增强

- Advisor

  对通知和切点的组合



### Spring AOP的具体实现

ProxyFactoryBean是Spring AOP的底层实现与源头。ProxyFactoryBean的构成：

- target

  目标对象，需要对其进行切面增强

- proxyInterface

  被代理对象实现的接口

- interceptorNames

  通知器列表（advisor)，通知器中包含了通知与切点

ProxyFactoryBean的主要作用：

针对target对象创建一个代理对象，将目标对象的方法调用转移到代理对象的方法调用，并且可以在代理对象的方法的调用前后执行与之匹配的通知器中定义好的通知。该对象也是被BeanFactory管理的，只是在创建该Bean是和普通的Bean流程不太一样，在该对象中有一个`geBean` 方法，当创建该对象时，实际上该对象会调用`getBean` 方法，结合上面的目标对象（target）、被代理对象的接口（proxyInterface)、通知器列表（interceptorNames），基于JDK动态代理或CgLib或ObjensisCglibAopProxy 创建一个实现了被代理对象接口参数的对象返回。如果目标对象是singleton的则会将创建出来的代理对象放到bean的缓存Map中

![Spring AOP中BeanFactory](../../images/spring/Spring AOP中BeanFactory.png)



代理方法调用流程：

![Spring AOP代理对象创建流程和被代理类方法调用](../../images/spring/Spring AOP代理对象创建流程.png)

当调用被代理对象的方法时：

1.调用JdkDynamicAopProxy的invoke方法

2.获取拦截器链

3.通过代理对象，目标对象，方法，方法参数和拦截器链共同构造ReflectiveMethodInvocation对象

4.ReflectiveMethodInvocation对象的proceed方法

5.判断拦截器链是否已经调用完成，如果没有的话，通过递归的方式调用链

6.如果拦截器链已经调用完成，则会使用AOPUtils调用目标对象的方法

7.返回结果





### Spring创建代理对象的方法

- JDK动态代理
- Cglib
- ObjenesisCglibAopProxy (Spring4.0 开始使用，基于Cglib实现，不需要调用类的构造方法)





## 普通动态代理的编写

首先要有一个目标接口

```java
public interface Subject{
    void request()
}
```

具体的实现

```java
public class RealSubject implements Subject{
    @Override
    public void request(){
        System.out.println("invoke request");
    }
}
```

创建一个动态代理的对象

```java
public class DynamicSubject implements InvocationHandler {

    private RealSubject realSubject;

    public DynamicSubject(RealSubject realSubject) {
        this.realSubject = realSubject;
    }

    /**
     *
     * @param proxy 生成的代理对象
     * @param method 调用目标对象的方法
     * @param args 调用目标对象的方法参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("proxy:"+proxy.getClass());
        System.out.println("before invoke");
        Object returnObject = method.invoke(realSubject, args);
        System.out.println("after invoke");
        return returnObject;
    }
}
```



Client端

```java
public class DynamicProxyClient {
    //-Dsun.misc.ProxyGenerator.saveGeneratedFiles=true 配置JVM参数保存动态代理对象的字节码
    public static void main(String[] args) {
        RealSubject realSubject = new RealSubject();

        InvocationHandler invocationHandler = new DynamicSubject(realSubject);
        Class<?> clazz = invocationHandler.getClass();
        // 创建代理对象
        Subject proxyInstance = (Subject)Proxy.newProxyInstance(clazz.getClassLoader(), realSubject.getClass().getInterfaces(),invocationHandler);
        
        proxyInstance.request();
        System.out.println(proxyInstance.getClass());
    }
}
```





















