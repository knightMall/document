## WebSocketConfig
```java
/**
 * @author knight
 */
@Configuration
public class WebSocketConfig {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
```

## WebSocket服务
```java
/**
 * @author knight
 */
@ServerEndpoint("/websocket/{token}")
@Component
public class WebSocketController {

    private static final Logger log = LoggerFactory.getLogger(WebSocketController.class);

    public static Map<String, Session> SESSION_MAP = new ConcurrentHashMap<>();

    /**
     * 连接建立成功调用的方法
     *
     * @param session
     * @param token
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("token") String token) {
        log.info("{}建立连接", token);
        SESSION_MAP.put(token, session);
        log.info("建立连接后session数量={}", SESSION_MAP.size());
        send(token, "Hello, connection opened!");
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        log.info("session.id={}", session.getId());
        if (StrUtil.isNotBlank(message)) {
            JSONArray objects = JSONUtil.parseArray(message);
            if (objects != null && objects.size() > 0) {
                JSONObject jsonObject = (JSONObject) objects.get(0);
                String toUserId = jsonObject.get("toUserId").toString();
                JSONObject contentObject = (JSONObject) objects.get(0);
                String contentText = contentObject.get("contentText").toString();

                for (Map.Entry<String, Session> entry : SESSION_MAP.entrySet()) {
                    if (toUserId.equals(entry.getKey())) {
                        send(toUserId, contentText);
                        break;
                    }
                }
            }
        }

        log.info("接收到消息：{}", message);

    }

    @OnClose
    public void onClose(Session session) {
        log.info("有连接关闭");
        Map<String, Session> map = new HashMap<>();
        SESSION_MAP.forEach((k, v) -> {
            if (!session.getId().equals(v.getId())) {
                map.put(k, v);
            }
        });
        SESSION_MAP = map;
        log.info("断开连接后session数量={}", SESSION_MAP.size());
    }

    void send(String token, String message) {
        Session session = SESSION_MAP.get(token);
        if (session != null) {
            try {
                session.getBasicRemote().sendText(message);
            } catch (IOException e) {
                log.error("发送信息错误{}", e);
                e.printStackTrace();
            }
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }
}
```

## 页面
```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>websocket通讯</title>
</head>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script>
    var socket;
    function openSocket() {
        if(typeof(WebSocket) == "undefined") {
            console.log("您的浏览器不支持WebSocket");
        }else{
            console.log("您的浏览器支持WebSocket");
            //实现化WebSocket对象，指定要连接的服务器地址与端口  建立连接
            //等同于socket = new WebSocket("ws://localhost:8080/xxxx/im/25");
            //var socketUrl="${request.contextPath}/im/"+$("#userId").val();
            var socketUrl="http://localhost:8080/websocket/"+$("#userId").val();
            socketUrl=socketUrl.replace("https","ws").replace("http","ws");
            console.log(socketUrl)
            socket = new WebSocket(socketUrl);
            //打开事件
            socket.onopen = function() {
                console.log("websocket已打开");
                //socket.send("这是来自客户端的消息" + location.href + new Date());
            };
            //获得消息事件
            socket.onmessage = function(msg) {
                console.log(msg.data);
                //发现消息进入    开始处理前端触发逻辑
            };
            //关闭事件
            socket.onclose = function() {
                console.log("websocket已关闭");
            };
            //发生了错误事件
            socket.onerror = function() {
                console.log("websocket发生了错误");
            }
        }
    }
    function sendMessage() {
        if(typeof(WebSocket) == "undefined") {
            console.log("您的浏览器不支持WebSocket");
        }else {
            console.log("您的浏览器支持WebSocket");
            console.log('[{"toUserId":"'+$("#toUserId").val()+'","contentText":"'+$("#contentText").val()+'"}]');
            socket.send('[{"toUserId":"'+$("#toUserId").val()+'","contentText":"'+$("#contentText").val()+'"}]');
        }
    }
</script>
<body>
    <p>【userId】：<div><input id="userId" name="userId" type="text" value="1"></div>
    <p>【toUserId】：<div><input id="toUserId" name="toUserId" type="text" value="2"></div>
    <p>【toUserId】：<div><input id="contentText" name="contentText" type="text" value="这是信息"></div>
    <p>【操作】：<div><a onclick="openSocket()">开启socket</a></div>
    <p>【操作】：<div><a onclick="sendMessage()">发送消息</a></div>
</body>
</html>
```
## 操作webSocket
```java
    @PostMapping("send")
    @ResponseBody
    public String sendMessage(@RequestBody String id) {
        Map<String, Session> sessionMap = WebSocketController.SESSION_MAP;

        for (Map.Entry<String, Session> entry : sessionMap.entrySet()) {
            if (id.equals(entry.getKey())) {
                Session session = entry.getValue();
                try {
                    session.getBasicRemote().sendText("这是发给" + id + "的消息,内容是：" + RandomUtil.randomString(4));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        return "success";
    }
```