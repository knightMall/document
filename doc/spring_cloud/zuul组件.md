



## Zuul简介

Zuul提供了服务网关的功能，可以实现负载均衡、反向代理、动态路由、请求转发等功能。Zuul大部分功能都是通过过滤器实现的。



## Zuul过滤器

### Pre Filters

在请求被路由之前调用



### Route Filters

在路由请求时被调用



### Post Filters

在route和error过滤器之后被调用



### Error Filters 

处理请求时发生错误时被调用







