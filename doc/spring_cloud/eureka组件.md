- [Eureka功能](#rureka功能)
- [Eureka基本架构](#eureka基本架构)
- [Eureka Server维护了 系统中服务的元信息，元信息的组成](#eureka-server维护了-系统中服务的元信息元信息的组成)
- [元信息是怎么存储的](#元信息是怎么存储的)
- [心跳检测配置](#心跳检测配置)
- [注册表抓取间隔](#注册表抓取间隔)
- [自我保护模式](#自我保护模式)









## Eureka功能

- Service Registry(服务注册)
- Service Discovery(服务发现)

## Eureka基本架构

eureka由三个角色组成

- Eureka  Server，提供服务注册于发现

- Service Provider，服务提供方

  将自身服务注册到Eureka Server上，从而让Eureka Server持有服务的元信息，让其他的服务消费方能够找到当前服务。

- Service Consumer，服务消费方，从Eureka Server上获取注册服务列表，从而能够消费服务

- Service Provider/Consumer 于相对于Server做 ，都叫做Eureka Client 

![01_Eureka基本架构](../../pic/spring-cloud/01_Eureka基本架构.png)

## Eureka Server维护了 系统中服务的元信息，元信息的组成

框架自带的元数据，包括id、主机名称、IP地址等。



## 元信息是怎么存储的

注册的服务列表保存在一个嵌套的hash map中：

- 第一层hash map的key是app name，也就是应用名字
- 第二层hash map的key是instance name，也就是实例名字



## 心跳检测配置

客户端的实例会向服务器发送周期性的心跳，默认是30秒发送一次。通过`eureka.instance.lease-renewal-interval-in-seconds` 修改。



## 注册表抓取间隔

客户端默认每隔30秒去Eureka Server抓取注册表，并将服务器的注册表保存到本地缓存中。`eureka.client.registry-fetch-interval-seconds`



## 自我保护模式

客户端发送心跳到服务端，如果心跳的失败率超过一定比例，服务端将这些实例保护起来，并不会马上将其从注册表中删除。









