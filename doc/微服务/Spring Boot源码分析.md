## Spring Boot是什么

Spring Boot是一个服务于框架的框架。Spring Boot的核心是约定优于配置原则。
基于这个原则实现可以快速搭建一个Spring的项目。无需繁重的XML或Java configuration配置。
底层还是基于Spring的IOC、AOP和SpringMVC等Spring的原生技术。



## 约定优于配置的体现

- maven的目录结构
  - 默认用 resources 文件夹存放配置文件
  - 默认打包方式为 jar
- spring-boot-starter-web 中默认包含 spring mvc 相关
  依赖以及内置的 tomcat 容器，使得构建一个 web 应用
  更加简单
- 默认提供 application.properties/yml 文件
- 默认通过 spring.profiles.active 属性来决定运行环境时读取的配置文件
- EnableAutoConfiguration 默认对于依赖的 starter 进行自动装载



## @SpringBootApplication注解

SpringBootApplication注解的构成如下：

```java
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan(excludeFilters = { @Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
		@Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class) })
public @interface SpringBootApplication 
```

从上面看出 SpringBootApplication 注解是由 `@ComponentScan` 、`@EnableAutoConfiguration` 和 `@Configuration`



### @Configuration注解

Configuration 这个注解大家应该有用过，它是 JavaConfig形式的基于 Spring IOC 容器的
配置类使用的一种注解。类似于原来的XML配置文件

```java
@Configuration
public class Demo1Configuration {
    @Bean
    public Student student(){
        return new Student();
    }

}

public class Demo1App {
    public static void main(String[] args) {
       ApplicationContext applicationContext =  new AnnotationConfigApplicationContext(Demo1Configuration.class);

        Student student = applicationContext.getBean(Student.class);

        student.say();
    }
}
```


### @ComponentScan注解
相当于 xml 配置文件中的`<context:component-scan>`。自动装配到 spring 的 Ioc 容器中。
标识需要装配的类的形式主要是：`@Component、@Repository、@Service、@Controller` 这类的注解标识的
类。

```java
@Configuration
@ComponentScan(basePackages = "com.knight.spring.demo2")
public class Demo2Configuration {

}
@Component
public class Student {

    public void say(){
        System.out.println("hello2");
    }
}
public class Demo2App {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Demo2Configuration.class);
        Student student = applicationContext.getBean(Student.class);
        student.say();
    }
}
```



### @EnableAutoConfiguration注解

了解了 ImportSelector 和 ImportBeanDefinitionRegistrar后，对于 EnableAutoConfiguration 的理解就容易一些了
它会通过 import 导入第三方提供的 bean 的配置类：
```java
AutoConfigurationImportSelector
@Import(AutoConfigurationImportSele
ctor.class)
```

从名字来看，可以猜到它是基于 ImportSelector 来实现
基于动态 bean 的加载功能。之前我们讲过 Springboot@Enable*注解的工作原理 ImportSelector 接口
selectImports 返回的数组（类的全类名）都会被纳入到spring 容器中。
那么可以猜想到这里的实现原理也一定是一样的，定位到AutoConfigurationImportSelector 这个类中的
selectImports 方法本质上来说，其实 EnableAutoConfiguration 会帮助
springboot 应用把所有符合@Configuration 配置都加载到当前 SpringBoot 创建的 IoC 容器，而这里面借助了
Spring 框架提供的一个工具类 SpringFactoriesLoader 的支持。以及用到了 Spring 提供的条件注解
@Conditional，选择性的针对需要加载的 bean 进行条件过滤SpringFactoriesLoader
为了给大家补一下基础，我在这里简单分析一下SpringFactoriesLoader 这个工具类的使用。它其实和
java 中的 SPI 机制的原理是一样的，不过它比 SPI 更好的点在于不会一次性加载所有的类，而是根据 key 进行加载。
首 先 ， SpringFactoriesLoader 的 作 用 是 从classpath/META-INF/spring.factories 文件中，根据 key 来加载对应的类到 spring IoC 容器中。





## Spring Boot是如何实现导入对应的包就会自动加载相应的类的

首先，是在注解 `@SpringBootApplication` 中，该注解是由 `@Configuration` 、`@ComponentScan`和 `@EnableAutoConfiguration` 注解组成。其中`@Configuration`  注解类似于xml配置文件，`@ComponentScan` 注解是配置扫描包的。最重要的就是 `@EnableAutoConfiguration`  注解了。该注解包含了 `@AutoConfigurationPackage` 和 `@Import`注解配置了 `ImportSelector` 接口的实现，这个接口的方法返回的数组就会纳入到Spring容器中，这里借助了SpringFactoriesLoader ， SpringFactoriesLoader 的 作 用 是 从classpath/META-INF/spring.factories 文件中，根据 key 来
加载对应的类到 spring IoC 容器中。。这里的触发条件就是在容器refresh方法时实现了`BeanFacotryProcessor`接口，这个接口可以在运行时修改BeanDefinition对象。



## Spring Boot中的SPI机制的原理是什么？

SpringFactoriesLoader 这个工具类的使用。它其实和java 中的 SPI 机制的原理是一样的，不过它比 SPI 更好的
点在于不会一次性加载所有的类，而是根据 key 进行加载。首 先 ， SpringFactoriesLoader 的 作 用 是 从
classpath/META-INF/spring.factories 文件中，根据 key 来加载对应的类到 spring IoC 容器中

