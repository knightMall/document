

## Dubbo是什么

Doubbo是微服务开发框架，提供了RPC通信和服务治理两大核心功能。

服务治理主要是针对大规模服务化以后，服务之间的路由、负载均衡、容错机制、
服务降级这些问题的解决方案，而 Dubbo 实现的不仅仅是远程服务通信，
并且还解决了服务路由、负载、降级、容错等功能



## 入门

引入依赖：

```xml
<dependency>
    <groupId>org.apache.dubbo</groupId>
    <artifactId>dubbo</artifactId>
    <version>2.7.2</version>
</dependency>
```

在resource目录创建application.xml文件。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
       xsi:schemaLocation="http://www.springframework.org/schema/beans        http://www.springframework.org/schema/beans/spring-beans-4.3.xsd        http://dubbo.apache.org/schema/dubbo        http://dubbo.apache.org/schema/dubbo/dubbo.xsd">

</beans>
```



### 创建service API项目：

![image-20211213213317257](../../images/21/1213/service-api.png)



```java
public interface PayService {

    void pay(String info);

}
```

这个相关需要服务端和客户端进行依赖

### 创建Server项目：

![image-20211213213446669](../../images/21/1213/server.png)

```java
public class PayServiceImpl implements PayService {

    @Override
    public void pay(String info) {
        System.out.println("payservice pay info: "+info);
    }
}
```

配置application.xml文件，下面配置的是通过无注册中心暴露服务的方式:

```java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
       xsi:schemaLocation="http://www.springframework.org/schema/beans        http://www.springframework.org/schema/beans/spring-beans-4.3.xsd        http://dubbo.apache.org/schema/dubbo        http://dubbo.apache.org/schema/dubbo/dubbo.xsd">

    <!-- 提供方应用信息，用于计算依赖关系 -->
    <dubbo:application name="pay-service"/>
    <!--不配置注册中心-->
    <dubbo:registry address="N/A"/>

    <bean id="payService" class="com.knight.pay.impl.PayServiceImpl"/>
    <dubbo:service interface="com.knight.pay.PayService" ref="payService"/>

    <dubbo:protocol port="20880" name="dubbo"/><!--指定通过dubbo协议发布到20880端口-->

</beans>
```



### client项目

![image-20211213213727264](../../images/21/1213/client.png)

```java
public class OrderServiceImpl {

    private PayService payService;


    public PayService getPayService() {
        return payService;
    }

    public void setPayService(PayService payService) {
        this.payService = payService;
    }

    public Integer createOrder(String orderInfo) {
        payService.pay("支付100");

        return 1;
    }
```

在resource文件中创建application.xml

```java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
       xsi:schemaLocation="http://www.springframework.org/schema/beans        http://www.springframework.org/schema/beans/spring-beans-4.3.xsd        http://dubbo.apache.org/schema/dubbo        http://dubbo.apache.org/schema/dubbo/dubbo.xsd">

    <dubbo:application name="order-service"/>
    <dubbo:reference interface="com.knight.pay.PayService" id="payService" url="dubbo://127.0.0.1:20880/com.knight.pay.PayService"/>

    <bean id="orderService" class="com.knight.order.OrderServiceImpl">
        <property name="payService" ref="payService"/>
    </bean>
</beans>
```



### 集成ZK

#### 服务端：

在application.xml文件中添加：

```xml
 <dubbo:registry id="rg1" address="zookeeper://127.0.0.1:2181"/><!--配置注册中心-->
```

整个如下：

```java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
       xsi:schemaLocation="http://www.springframework.org/schema/beans        http://www.springframework.org/schema/beans/spring-beans-4.3.xsd        http://dubbo.apache.org/schema/dubbo        http://dubbo.apache.org/schema/dubbo/dubbo.xsd">

    <!-- 提供方应用信息，用于计算依赖关系 -->
    <dubbo:application name="pay-service"/>
    <!--不配置注册中心-->
    <dubbo:registry address="N/A"/>

    <dubbo:registry id="rg1" address="zookeeper://127.0.0.1:2181"/><!--配置注册中心-->

    <bean id="payService" class="com.knight.pay.impl.PayServiceImpl"/>

    <dubbo:service interface="com.knight.pay.PayService" ref="payService" registry="rg1"/>

    <dubbo:protocol port="20880" name="dubbo"/><!--指定通过dubbo协议发布到20880端口-->
</beans>
```

#### 客户端

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
       xsi:schemaLocation="http://www.springframework.org/schema/beans        http://www.springframework.org/schema/beans/spring-beans-4.3.xsd        http://dubbo.apache.org/schema/dubbo        http://dubbo.apache.org/schema/dubbo/dubbo.xsd">

    <dubbo:application name="order-service"/>

    <dubbo:registry id="zk" address="zookeeper://127.0.0.1:2181"/>

    <dubbo:reference interface="com.knight.pay.PayService" id="payService" registry="zk"/>
    <bean id="orderService" class="com.knight.order.OrderServiceImpl">
        <property name="payService" ref="payService"/>
    </bean>

</beans>
```



### 配置多个注册中心

服务端：

```java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
       xsi:schemaLocation="http://www.springframework.org/schema/beans        http://www.springframework.org/schema/beans/spring-beans-4.3.xsd        http://dubbo.apache.org/schema/dubbo        http://dubbo.apache.org/schema/dubbo/dubbo.xsd">

    <!-- 提供方应用信息，用于计算依赖关系 -->
    <dubbo:application name="pay-service"/>
    <!--不配置注册中心-->
    <dubbo:registry address="N/A"/>

    <dubbo:registry id="rg1" address="zookeeper://127.0.0.1:2181"/><!--配置注册中心-->
    <dubbo:registry id="rg2" address="zookeeper://127.0.0.1:2182"/><!--配置注册中心-->

    <!--    <dubbo:registry id="rg2" address="zookeeper://192.168.8.125:2181"/>&lt;!&ndash;配置注册中心&ndash;&gt;-->

    <bean id="payService" class="com.knight.pay.impl.PayServiceImpl"/>
    <bean id="prodSkuService" class="com.knight.prod.impl.ProdSkuServiceImpl"/>

    <dubbo:service interface="com.knight.pay.PayService" ref="payService" registry="rg1"/>
    <dubbo:service interface="com.knight.prod.ProdSkuService" ref="prodSkuService" registry="rg2"/>


    <dubbo:protocol port="20880" name="dubbo"/><!--指定通过dubbo协议发布到20880端口-->

</beans>
```







客户端：

```java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
       xsi:schemaLocation="http://www.springframework.org/schema/beans        http://www.springframework.org/schema/beans/spring-beans-4.3.xsd        http://dubbo.apache.org/schema/dubbo        http://dubbo.apache.org/schema/dubbo/dubbo.xsd">


    <dubbo:application name="order-service"/>

    <dubbo:registry id="zk" address="zookeeper://127.0.0.1:2181"/>
    <dubbo:registry id="zk2" address="zookeeper://127.0.0.1:2182"/>

    <dubbo:reference interface="com.knight.pay.PayService" id="payService" registry="zk"/>
    <dubbo:reference interface="com.knight.prod.ProdSkuService" id="prodSkuService" registry="zk2"/>

    <bean id="orderService" class="com.knight.order.OrderServiceImpl">
        <property name="payService" ref="payService"/>
        <property name="prodSkuService" ref="prodSkuService"/>
    </bean>
</beans>
```



### 配置webService服务

添加webService服务

```xml
 <dependency>
     <groupId>org.apache.cxf</groupId>
     <artifactId>cxf-rt-frontend-simple</artifactId>
     <version>3.3.2</version>
 </dependency>
 <dependency>
     <groupId>org.apache.cxf</groupId>
     <artifactId>cxf-rt-transports-http</artifactId>
     <version>3.3.2</version>
 </dependency>
 <dependency>
     <groupId>org.eclipse.jetty</groupId>
     <artifactId>jetty-server</artifactId>
     <version>9.4.19.v20190610</version>
 </dependency>
 <dependency>
     <groupId>org.eclipse.jetty</groupId>
     <artifactId>jetty-servlet</artifactId>
     <version>9.4.19.v20190610</version>
 </dependency>
```

注册成webService：

```xml
  <dubbo:protocol name="webservice" port="8082" server="jetty"/> <!--指定通过webservice协议发布到20880端口-->

  <bean id="payService" class="com.knight.pay.impl.PayServiceImpl"/>
  <bean id="prodSkuService" class="com.knight.prod.impl.ProdSkuServiceImpl"/>

  <dubbo:service interface="com.knight.pay.PayService" ref="payService" registry="rg1" protocol="webservice"/>
```

其他地方无需修改。启动服务，然后访问：`http://localhost:8082/com.knight.pay.PayService?wsdl`



## Spring Boot与Dubbo整合

整合的目的：

- 可以加入到Spring的体系
- 标准化

### 整合服务端

基于Spring.io创建一个Spring Boot项目无需依赖其他组件，引入dubbo starter。全部的包依赖如下：

```xml
<dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <artifactId>dubbo-service-api</artifactId>
            <groupId>com.knight</groupId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo-spring-boot-starter</artifactId>
            <version>2.7.1</version>
        </dependency>
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo</artifactId>
            <version>2.7.2</version>
        </dependency>
        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-framework</artifactId>
            <version>4.0.0</version>
        </dependency>
        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-recipes</artifactId>
            <version>4.0.0</version>
        </dependency>
    </dependencies>
```

在application.properties中配置Dubbo的配置：

```properties
#配置需要扫描服务的包
dubbo.scan.base-packages=com.knight.dubbo.service.impl
dubbo.application.name=spring-boot-dubbo
dubbo.protocol.name=dubbo
dubbo.protocol.port=20880
dubbo.registry.address=zookeeper://127.0.0.1:2181
```

创建服务：

![image-20211216163156527](../../images/21/1216/服务示例.png)

启动项目



### 整合客户端

基于spring.io创建一个Spring Boot项目，这里依赖we starter。pom文件如下：

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <dependency>
        <artifactId>dubbo-service-api</artifactId>
        <groupId>com.knight</groupId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
    <dependency>
        <groupId>org.apache.dubbo</groupId>
        <artifactId>dubbo-spring-boot-starter</artifactId>
        <version>2.7.1</version>
    </dependency>
    <dependency>
        <groupId>org.apache.dubbo</groupId>
        <artifactId>dubbo</artifactId>
        <version>2.7.2</version>
    </dependency>
 <dependency>
         <groupId>org.apache.curator</groupId>
         <artifactId>curator-framework</artifactId>
         <version>4.0.0</version>
     </dependency>
     <dependency>
         <groupId>org.apache.curator</groupId>
         <artifactId>curator-recipes</artifactId>
         <version>4.0.0</version>
     </dependency>

     <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-test</artifactId>
         <scope>test</scope>
     </dependency>
 </dependencies>
```

在application.properties文件中配置dubbo相关配置：

```properties
#因为dubbo本地启动8080端口已被占用
server.port=8081

dubbo.application.name=spring-boot-dubbo-client
dubbo.protocol.name=dubbo
dubbo.registry.address=zookeeper://127.0.0.1:2181
```

Controller的编写：

![image-20211216164613306](../../images/21/1216/客户端Controller.png)





## 负载均衡

### 使用方法

@Service(loadbalance = "random")或@Reference(loadbalance = "random")



### 负载均衡算法

- Random-权重随机算法
- LeastActive-最小活跃数
- RoundRobin-轮询/权重轮询
- ConsistentHash 一致性hash

### 扩展

继承`org.apache.dubbo.rpc.cluster.LoadBalance`



## 集群容错

### 使用方法

`@Service(cluster = "failfast")或@Reference(cluster = "failfast")`



### 容错策略

- failover

  默认，失败自动切换，当出现失败，重试其它服务器。通常用于读操作，但重试会带来更长延迟。可通过 retries="2" 来设置重试次数(不含第一次)

- failfast

  快速失败，只发起一次调用，失败立即报错。通常用于非幂等性的写操作，比如新增记录。

- failsafe

  失败安全，出现异常时，直接忽略。通常用于写入审计日志等操作。

- failback

  失败自动恢复，后台记录失败请求，定时重发。通常用于消息通知操作

- forking

  并行调用多个服务器，只要一个成功即返回。通常用于实时性要求较高的读操作，但需要浪费更多服务资源

- broadcast

  广播调用所有提供者，逐个调用，任意一台报错则报错。通常用于通知所有提供者更新缓存或日志等本地资源信息。



### 扩展

扩展接口`org.apache.dubbo.rpc.cluster.Cluster`







## Dubbo的SPI

### 使用

以扩展协议为例子

1.创建一个类实现 `org.apache.dubbo.rpc.Protocol` 接口

2.在resource目录下创建`META-INFO/dubbo/org.apache.dubbo.rpc.Protocol` 的纯文本文件

3.内容是`key=类的全路径`。例如：`myprotocol=com.knight.dubbo.spi.MyProtocol`

测试

```java
public static void main(String[] args) {
    Protocol myprotocol = ExtensionLoader.getExtensionLoader(Protocol.class).getExtension("myprotocol");
    System.out.println(myprotocol.getDefaultPort());
}
```



### 实现原理

基于上面的代码`ExtensionLoader.getExtensionLoader(Protocol.class).getExtension("myprotocol");` 

查看具体的实现

- 加载扩展类

  解析对应目录的对应的扩展文件，上面的例子就是`org.apache.dubbo.rpc.Protocol`文件。

  文件里面的内容保存到`extensionClasses`中，具体定义是`Map<String, Class<?>>`。

- 实例化扩展类

  通过要传入的要实例化的名称这里就是`myprotocol`，从上面定义的Map获取Class对象，调用clazz的newInstance方法进行实例化。

  并保存到缓存中。

  



## Dubbo服务发布源码分析



### Dubbo如何扩展Spring的元素标签的？
通过扩展`org.springframework.beans.factory.xml.NamespaceHandlerSupport`接口

### 服务配置信息如何解析？
我们以ServiceBean作为例子，该类实现了`org.springframework.beans.factory.InitializingBean`
接口，该接口的有方法`afterPropertiesSet`，该方法在实例话的bean初始化完且属性也设置完成并且
相关Aware接口都执行完成后调用。Dubbo在该方法中进行配置的解析

### 暴露服务如何触发？
Dubbbo实现了ApplicationListener<ContextRefreshedEvent>接口，在泛型中定义的就是Listen的事件
这就是Spring容器的上下文刷新事件。该接口有`onApplicationEvent`方法来接受触发的事件，Dubbo
在该方法中触发服务的暴露。







### 解析配置文件

Dubbo通过实现`NamespaceHandler`接口来处理自定义的标签元素。接口`org.springframework.beans.factory.xml.NamespaceHandler`

的申明中有如下三个方法。

```java
public interface NamespaceHandler {
	void init();
	@Nullable
	BeanDefinition parse(Element element, ParserContext parserContext);
	@Nullable
	BeanDefinitionHolder decorate(Node source, BeanDefinitionHolder definition, ParserContext parserContext);
}
```

在Dubbo的扩展handler的init方法中就注册了很多不同的`BeanDefinitionParser`，在后面的解析阶段将会标识到不同的标签使用不同的

`BeanDefinitionParser`。


```java
public class DubboNamespaceHandler extends NamespaceHandlerSupport {

    static {
        Version.checkDuplicate(DubboNamespaceHandler.class);
    }

    @Override
    public void init() {
        registerBeanDefinitionParser("application", new DubboBeanDefinitionParser(ApplicationConfig.class, true));
        registerBeanDefinitionParser("module", new DubboBeanDefinitionParser(ModuleConfig.class, true));
        registerBeanDefinitionParser("registry", new DubboBeanDefinitionParser(RegistryConfig.class, true));
        registerBeanDefinitionParser("config-center", new DubboBeanDefinitionParser(ConfigCenterBean.class, true));
        registerBeanDefinitionParser("metadata-report", new DubboBeanDefinitionParser(MetadataReportConfig.class, true));
        registerBeanDefinitionParser("monitor", new DubboBeanDefinitionParser(MonitorConfig.class, true));
        registerBeanDefinitionParser("metrics", new DubboBeanDefinitionParser(MetricsConfig.class, true));
        registerBeanDefinitionParser("provider", new DubboBeanDefinitionParser(ProviderConfig.class, true));
        registerBeanDefinitionParser("consumer", new DubboBeanDefinitionParser(ConsumerConfig.class, true));
        registerBeanDefinitionParser("protocol", new DubboBeanDefinitionParser(ProtocolConfig.class, true));
        registerBeanDefinitionParser("service", new DubboBeanDefinitionParser(ServiceBean.class, true));
        registerBeanDefinitionParser("reference", new DubboBeanDefinitionParser(ReferenceBean.class, false));
        registerBeanDefinitionParser("annotation", new AnnotationBeanDefinitionParser());
    }

}
```

解析Spring自定义标签的入口

![image-20211224161047225](../../images/21/1224/dubbo扩展元素.png)



使用方式：

在META-INF目录下创建下面两个目录

![image-20211224161338382](../../images/21/1224/dubbo扩展元素使用.png)

`spring.handlers` 文件的内容

```properties
http\://dubbo.apache.org/schema/dubbo=org.apache.dubbo.config.spring.schema.DubboNamespaceHandler
http\://code.alibabatech.com/schema/dubbo=org.apache.dubbo.config.spring.schema.DubboNamespaceHandler
```



文件`spring.handlers` 是在类`org.springframework.beans.factory.xml.DefaultNamespaceHandlerResolver` 被解析的

![image-20211224161829469](../../images/21/1224/namespaceHandlerResolver.png)

解析`spring.handlers`文件并封装到 `handlerMappings`中 ，以`namespace`为key，具体的`NamespaceHandler`为value。

![image-20211224162048881](../../images/21/1224/将handler封装到Map中.png)

然后在类`org.springframework.beans.factory.xml.BeanDefinitionParserDelegate` 的 `parseCustomElement`方法中处理，获取到对应的

Handler调用其parse方法返回`BeanDefinition`对象。

![image-20211224162507254](../../images/21/1224/handler的处理.png)

Dubbo的handler继承自`NamespaceHandlerSupport`类，parse调用的是`NamespaceHandlerSupport`的方法

```java
public BeanDefinition parse(Element element, ParserContext parserContext) {
	BeanDefinitionParser parser = findParserForElement(element, parserContext);
	return (parser != null ? parser.parse(element, parserContext) : null);
}
```

```java
private BeanDefinitionParser findParserForElement(Element element, ParserContext parserContext) {
	String localName = parserContext.getDelegate().getLocalName(element);
	BeanDefinitionParser parser = this.parsers.get(localName);
	if (parser == null) {
		parserContext.getReaderContext().fatal(
				"Cannot locate BeanDefinitionParser for element [" + localName + "]", element);
	}
	return parser;
}
```

通过上面的`init`方法已经将`BeanDefinitionParser` 添加到了`parsers`中。上面的方法就是获取到对应的`parsers`然后调用对应的

`parse`方法进行解析。封装成`BeanDefinition`返回







### 发布服务

我们这里以`service` 为例子来看Dubbo的发布服务过程。通过上面的分析，我们知道`service`标签将解析

成`org.apache.dubbo.config.spring.ServiceBean`类

```java
public class ServiceBean<T> extends ServiceConfig<T> implements InitializingBean, DisposableBean,
        ApplicationContextAware, ApplicationListener<ContextRefreshedEvent>, BeanNameAware,
        ApplicationEventPublisherAware {
```

该类实现了 `InitializingBean`, `DisposableBean`,  `ApplicationContextAware`, `ApplicationListener<ContextRefreshedEvent>`,

`BeanNameAware`, `ApplicationEventPublisherAware`接口。













