- [Mapping概念](#Mapping概念)
- [自定义Mapping](#自定义mapping)
  - [Mapping演示](#mapping演示)
    - [创建索引](#创建索引)
    - [获取索引Mapping](#获取索引mapping)
    - [查询](#查询)
    - [设置dynamic为strict](#设置dynamic为strict)
  - [属性设置](#属性设置)
    - [copy_to属性](#copy_to属性)
    - [index属性](#index属性)
    - [index_options属性](#index_options属性)
    - [null_value](#null_value属性)
    - [文档说明](#文档说明)
  - [数据类型](#数据类型)
    - [多字段特性](#多字段特性multi-fields)
    - [文档说明](#文档说明)
  - [Dynamic-Mapping](#dynamic-mapping)
    - [dynamic日期和数字识别](#dynamic日期和数字识别)
  - [Dynamic-Template](#dynamic-template)
    - [匹配规则](#匹配规则)
    - [使用示例](#使用示例)
  - [自定义Mapping的建议](#自定义mapping的建议)
  - [索引模板](#索引模板)







## Mapping概念

- 类似于数据库的表结构定义，主要作用如下：
  - 定义Index下的字段名(Field Name)
  - 定义字段的类型，比如数值型、字符串型、布尔型等
  - 定义倒排索引相关的配置，比如是否索引、记录position等

## 自定义Mapping

自定义Maping的API和请求参数：

![自定义Mapping](../../pic/es/自定义Mapping.jpg)

- Mapping中的字段类型一旦设定后，禁止直接修改，原因如下：

  - Lucene实现的倒排索引生成后不允许修改
  - 如果必须要修改，需重新建立新的索引，然后做reindex操作

- 允许新增字段

  - 通过`dynamic`参数来控制字段的新增

    - `true` (默认)允许自动新增字段
    - `false` 不允许自动新增字段，但是文档可以正常写入，但无法对字段进行查询等操作
    - `strict` mapping不能修改，必须严格按照mapping设置，否在报错
    - 可以在mappings和properties中的Object类型设置

    ![自定义Mapping-dynamic](../../pic/es/自定义Mapping-dynamic.jpg)



### Mapping演示

#### 创建索引

```json
PUT my_index
{
  "mappings": {
    "doc":{
      "dynamic":"false",
      "properties":{
        "title":{
          "type":"text"
        },
        "name":{
          "type":"keyword"
        },
        "age":{
          "type":"integer"
        }
      }
    }
  }
}
```

响应结果：

```json
{
  "acknowledged": true,
  "shards_acknowledged": true,
  "index": "my_index"
}
```



#### 获取索引Mapping

```json
GET my_index/_mapping
```

响应结果，和创建的时候一样：

```json
{
  "my_index": {
    "mappings": {
      "doc": {
        "dynamic": "false",
        "properties": {
          "age": {
            "type": "integer"
          },
          "name": {
            "type": "keyword"
          },
          "title": {
            "type": "text"
          }
        }
      }
    }
  }
}
```



#### 添加数据到索引

```json
PUT my_index/doc/1
{
  "title":"hello,world",
  "desc":"nothing here"
}
```

响应结果：

```json
{
  "_index": "my_index",
  "_type": "doc",
  "_id": "1",
  "_version": 1,
  "result": "created",
  "_shards": {
    "total": 2,
    "successful": 2,
    "failed": 0
  },
  "_seq_no": 0,
  "_primary_term": 1
}
```

#### 查询

```json
GET my_index/doc/_search
{
  "query": {
    "match": {
      "title": "hello"
    }
  }
}
```

响应结果，desc字段存在于结果中，但是再次获取索引的mapping是不变的：

```json
{
  "took": 1193,
  "timed_out": false,
  "_shards": {
    "total": 5,
    "successful": 5,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": 1,
    "max_score": 0.2876821,
    "hits": [
      {
        "_index": "my_index",
        "_type": "doc",
        "_id": "1",
        "_score": 0.2876821,
        "_source": {
          "title": "hello,world",
          "desc": "nothing here"
        }
      }
    ]
  }
}
```

通过desc字段查询

```json
GET my_index/doc/_search
{
  "query": {
    "match": {
      "desc": "here"
    }
  }
}
```

响应结果如下：

```json
{
  "took": 8,
  "timed_out": false,
  "_shards": {
    "total": 5,
    "successful": 5,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": 0,
    "max_score": null,
    "hits": []
  }
}
```

`dynamic`属性设置为false是无法查询的



#### 设置dynamic为strict

mapping结构如下：

```json
{
  "my_index": {
    "mappings": {
      "doc": {
        "dynamic": "strict",
        "properties": {
          "age": {
            "type": "integer"
          },
          "name": {
            "type": "keyword"
          },
          "title": {
            "type": "text"
          }
        }
      }
    }
  }
}
```

如果我们再添加上面包含`desc`字段的记录将会报错：

```json
{
  "error": {
    "root_cause": [
      {
        "type": "strict_dynamic_mapping_exception",
        "reason": "mapping set to strict, dynamic introduction of [desc] within [doc] is not allowed"
      }
    ],
    "type": "strict_dynamic_mapping_exception",
    "reason": "mapping set to strict, dynamic introduction of [desc] within [doc] is not allowed"
  },
  "status": 400
}
```





### 属性设置

#### copy_to属性

- 将该字段的值复制到目标字段，实现类似`_all`的作用
- 不会出现在`_source`中，只用来搜索

- 创建方法：

```json
PUT my_index
{
  "mappings": {
    "doc":{
      "properties":{
        "first_name":{
          "type":"text",
          "copy_to":"full_name"
        },
        "last_name":{
          "type":"text",
          "copy_to":"full_name"
        },
        "full_name":{
          "type":"text"
        }
      }
    }
  }
}
```

通过`full_name`字段查询

```json
GET my_index/doc/_search
{
  "query": {
    "match": {
      "full_name": {
        "query": "John Smith",
        "operator": "and"
      }
    }
  }
}
```

查询结果，不包含`full_name`字段：

```json
{
  "took": 552,
  "timed_out": false,
  "_shards": {
    "total": 5,
    "successful": 5,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": 1,
    "max_score": 0.5753642,
    "hits": [
      {
        "_index": "my_index",
        "_type": "doc",
        "_id": "1",
        "_score": 0.5753642,
        "_source": {
          "first_name": "John",
          "last_name": "Smith"
        }
      }
    ]
  }
}
```



#### index属性

- 控制当前字段是否索引，默认是true，即索引记录，false不记录，即不可搜索

- 设置index属性

  ```json
  PUT my_index
  {
    "mappings": {
      "doc":{
        "properties":{
          "cookie":{
            "type":"text",
            "index":false
          }
        }
      }
    }
  }
  ```

  添加文档

  ```json
  PUT my_index/doc/1
  {
    "cookie":"name=alfred"
  }
  ```

  查询文档

  ```json
  GET my_index/doc/_search
  {
    "query": {
      "match": {
        "cookie": "name"
      }
    }
  }
  ```

  ![自定义Mapping-index属性](../../pic/es/自定义Mapping-index属性.jpg)

#### index_options属性

- `index_options`用于控制倒排索引记录的内容，有如下4种配置
  - `docs` 只记录doc id
  - `freqs` 记录doc id和term frequencies
  - `positions` 记录doc id、term frequencies和term position
  - `offsets` 记录 doc id、term frequencies、term position和character offsets
- `text` 类型默认配置为 `positions`，其他默认为docs
- 记录越多的内容，占用空间越大

`index_options`设置如下：

```json
PUT my_index
{
  "mappings": {
    "doc":{
      "properties":{
        "cookie":{
          "type":"text",
          "index_options":"offsets"
        }
      }
    }
  }
}
```



####  null_value属性

- 当字段遇到null值时的处理策略，默认是null，即空值，此时es会忽略该值。可以通过设置该字段的默认值。使用如下：

  ```json
  PUT my_index
  {
    "mappings": {
      "_doc": {
        "properties": {
          "status_code": {
            "type":       "keyword",
            "null_value": "NULL" 
          }
        }
      }
    }
  }
  ```





#### 文档说明

[Mapping属性](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-params.html)



## 数据类型

- 核心数据类型

  - 字符串型 `text`、`keyword`
  - 数值型 `long`、`integer`、`short`、`byte`、`double`、`float`、`half_float`、`scaled_float`
  - 日期类型 `date`
  - 布尔类型 `boolean`
  - 二进制类型 `binary`
  - 范围类型 `integer_range`、`float_range`、`long_range`、`double_range`、`date_range`

- 复杂数据类型

  - 数组类型 `array`
  - 对象类型 `object`
  - 嵌套 `nested object`

- 地理位置数据类型

  - `geo_point`
  - `geo_shape`

- 专用类型

  - 记录ip地址 ip
  - 实现自动补全 completion
  - 记录分词数 token_count
  - 记录字符串hash值murmur3
  - percolator
  - join


### 多字段特性multi-fields

允许对同一个字段采用不同的配置，比如分词，常见的例子如对人名实现拼音搜索，只需要在人名中新增一个子字段即可

### 文档说明

[Mapping数据类型](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-types.html)



## Dynamic-Mapping

- es可以自动识别文档字段类型，从而降低用户使用成本。

```json
PUT /test_index/doc/1
{
  "username":"alfred",
  "age":1
}
```

获取索引的`Mapping`

```json
GET test_index/_mapping
```

响应结果：

```json
{
  "test_index": {
    "mappings": {
      "doc": {
        "properties": {
          "age": {
            "type": "long"
          },
          "username": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          }
        }
      }
    }
  }
}
```



- es是依靠JSON文档的字段类型来实现自动识别字段类型，支持的类型如下：

  | JSON类型 |                            es类型                            |
  | :------: | :----------------------------------------------------------: |
  |   null   |                             忽略                             |
  | boolean  |                           boolean                            |
  | 浮点类型 |                            float                             |
  |   整形   |                             long                             |
  |  object  |                            object                            |
  |  array   |                  由第一个非null值的类型判断                  |
  |  string  | 匹配为日期则设为date类型(默认开启)。匹配为数字的话设置为float或long类型(默认关闭)。设为text类型，并附带keyword的子字段 |

  示例如下，创建一个文档

  ```json
  PUT /test_index/doc/1
  {
    "username":"alfred",
    "age":1,
    "birth":"1988-11-03",
    "married":false,
    "year":"20",
    "tags":["boy","fashion"],
    "money":1000.11
  }
  ```

  获取es自动创建索引的mapping

  ```json
  {
    "test_index": {
      "mappings": {
        "doc": {
          "properties": {
            "age": {
              "type": "long"
            },
            "birth": {
              "type": "date"
            },
            "married": {
              "type": "boolean"
            },
            "money": {
              "type": "float"
            },
            "tags": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            },
            "username": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            },
            "year": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            }
          }
        }
      }
    }
  }
  ```



### dynamic日期和数字识别

- 日期的自动识别可以自行配置日期格式，以满足各种需求
  - 默认是`strict_date_optional_time`、还支持`yyyy/MM/dd HH:mm:ss Z`、`yyyy/MM/dd Z`
  - 通过`dynamic_date_formats`参数可以自定义日期类型
  - 通过`date_detection`参数关闭日期自动识别的机制

```json
PUT my_index
{
  "mappings": {
    "my_type":{
      "dynamic_date_formats":["MM/dd/yyyy"]
    }
  }
}

PUT my_index/my_type/1
{
  "create_date":"09/25/2015"
}
```

获取Mapping：

```json
GET my_index/_mapping

{
  "my_index": {
    "mappings": {
      "my_type": {
        "dynamic_date_formats": [
          "MM/dd/yyyy"
        ],
        "properties": {
          "create_date": {
            "type": "date",
            "format": "MM/dd/yyyy"
          }
        }
      }
    }
  }
}
```

设置不自动识别：

```json
PUT my_index
{
  "mappings": {
    "my_type":{
      "dynamic_date_formats":["MM/dd/yyyy"],
      "date_detection":false
    }
  }
}
```

```json
PUT my_index/my_type/1
{
  "create_date":"09/25/2015"
}

GET my_index/_mapping
```

获取的Mapping结构：

```json
{
  "my_index": {
    "mappings": {
      "my_type": {
        "dynamic_date_formats": [
          "MM/dd/yyyy"
        ],
        "date_detection": false,
        "properties": {
          "create_date": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          }
        }
      }
    }
  }
}
```



- 字符串是数字时，默认不会自动识别为整形，因为字符串中出现数字时完全合理的。

  - `numeric_detection`参数可以开启字符串中数字的自动识别，如下所示：

  ```json
  DELETE my_index
  
  PUT my_index
  {
    "mappings": {
      "my_type":{
        "numeric_detection":true
      }
    }
  }
  
  PUT my_index/my_type/1
  {
    "my_float":"1.0",
    "my_integer":"1"
  }
  
  GET my_index/_mapping
  ```

  Mapping结构：

  ```json
  {
    "my_index": {
      "mappings": {
        "my_type": {
          "numeric_detection": true,
          "properties": {
            "my_float": {
              "type": "float"
            },
            "my_integer": {
              "type": "long"
            }
          }
        }
      }
    }
  }
  ```



## Dynamic-Template

- 允许根据es自动识别的数据类型、字段名等来动态设定字段类型，可以实现如下效果：

  - 所有字符串类型都设定为keyword类型，即默认不分词
  - 所有以message开头的字段都设定为text类型，即分词
  - 所有以long_开头的字段都设定为long类型
  - 所有自动匹配为double类型的都设定为float类型，以节省空间

- API如下图：

  ![动态模板API](../../pic/es/动态模板API.jpg)



### 匹配规则

- `match_mapping_type` 匹配es自动识别的字段类型，如boolean、long、string等
- `match`、`unmatch`匹配字段名
- `path_match`、`path_unmatch`匹配路径



### 使用示例

- 字符串默认使用keyword类型

  - es默认会为字符串设置为text类型，并增加一个keyword的子字段

  ```json
  # 删除索引
  DELETE test_index
  # 创建索引，如果匹配到是string类型，则设置类型为keyword
  PUT test_index
  {
    "mappings":{
      "doc":{
        "dynamic_templates":[
            {
              "strings_as_keywords":{
                "match_mapping_type":"string",
                "mapping":{
                  "type":"keyword"
                }
              }
            }
          ]
      }
    }
  }
  # 添加文档
  PUT test_index/doc/1
  {
    "name":"alfred"
  }
  # 获取mapping结构
  GET test_index/_mapping
  ```

  mapping结构：

  ```json
  {
    "test_index": {
      "mappings": {
        "doc": {
          "dynamic_templates": [
            {
              "strings_as_keywords": {
                "match_mapping_type": "string",
                "mapping": {
                  "type": "keyword"
                }
              }
            }
          ],
          "properties": {
            "name": {
              "type": "keyword"
            }
          }
        }
      }
    }
  }
  ```

- 以message开头的字段都设置为text类型

  ```json
  # 注意设置dynamic template的顺序，系统是从上到下匹配，匹配到一个就结束
  PUT test_index
  {
    "mappings":{
      "doc":{
        "dynamic_templates":[
          {
          "message_as_text":{
            "match_mapping_type":"string",
            "match":"message*",
            "mapping":{
              "type":"text"
            }
          }
        },
            {
              "strings_as_keywords":{
                "match_mapping_type":"string",
                "mapping":{
                  "type":"keyword"
                }
              }
            }
          ]
      }
    }
  }
  
  PUT test_index/doc/1
  {
    "name":"alfred",
    "message":"handsome boy"
  }
  
  GET test_index/_mapping
  ```

  获取的Mapping的结构：

  ```json
  {
    "test_index": {
      "mappings": {
        "doc": {
          "dynamic_templates": [
            {
              "message_as_text": {
                "match": "message*",
                "match_mapping_type": "string",
                "mapping": {
                  "type": "text"
                }
              }
            },
            {
              "strings_as_keywords": {
                "match_mapping_type": "string",
                "mapping": {
                  "type": "keyword"
                }
              }
            }
          ],
          "properties": {
            "message": {
              "type": "text"
            },
            "name": {
              "type": "keyword"
            }
          }
        }
      }
    }
  }
  ```




## 自定义Mapping的建议

- 自定义Mapping的操作步骤如下：
  - 如果已经存在文档，则写入一条文档到es的临时索引中，获取es自动生成的mapping
  - 修改步骤1得到的mapping，自定义相关配置
  - 使用上一步修改后的mapping创建实际所需索引





## 索引模板

- 索引模板，英文名为Index Template，主要用于在新建索引时自动应用预先设定的配置，简化索引创建的操作步骤：
  - 可以设置索引的配置和mapping
  - 可以有多个模板，根据order值设置，order大的覆盖小的配置

- 索引模板的API，endpoint是`_template`，具体如下：

  ![索引模板](../../pic/es/索引模板.jpg)

- 获取与删除的API如下：

  ```json
  # 获取所有的模板
  GET _template
  # 获取指定的模板
  GET _template/security-index-template
  
  ```

- 实践：

  ```json
  # 创建索引模板
  PUT _template/test_template
  {
    "index_patterns":["te*","bar*"],
    "order":0,
    "settings":{
      "number_of_shards":1
    },
    "mappings":{
      "doc":{
        "_source":{
          "enabled":false
        },
        "properties":{
          "name":{
            "type":"keyword"
          }
        }
      }
    }
  }
  
  # 创建第二个模板
  PUT _template/test_template2
  {
    "index_patterns":["test*"],
    "order":1,
    "settings":{
      "number_of_shards":1
    },
    "mappings":{
      "doc":{
        "_source":{
          "enabled":true
        }
      }
    }
  }
  ```

  创建索引：

  ```json
  # 该索引不匹配任何模版
  PUT foo_index
  GET foo_index
  # 结果如下：
  {
    "foo_index": {
      "aliases": {},
      "mappings": {},
      "settings": {
        "index": {
          "creation_date": "1542425033811",
          "number_of_shards": "5",
          "number_of_replicas": "1",
          "uuid": "fPTiz1zvR_eed-0Nj5y5Qw",
          "version": {
            "created": "6040299"
          },
          "provided_name": "foo_index"
        }
      }
    }
  }
  ```

  ```json
  # 匹配索引模板test_template
  PUT bar_index
  GET bar_index
  # 结果如下：
  {
    "bar_index": {
      "aliases": {},
      "mappings": {
        "doc": {
          "_source": {
            "enabled": false
          },
          "properties": {
            "name": {
              "type": "keyword"
            }
          }
        }
      },
      "settings": {
        "index": {
          "creation_date": "1542425058598",
          "number_of_shards": "1",
          "number_of_replicas": "1",
          "uuid": "Mzl0ecg3T9q4kGktJp-ULA",
          "version": {
            "created": "6040299"
          },
          "provided_name": "bar_index"
        }
      }
    }
  }
  ```

  ```json
  # 匹配两个索引模板，order的大的将小的覆盖
  PUT test_index
  # "enabled": false没有了
  GET test_index
  {
    "test_index": {
      "aliases": {},
      "mappings": {
        "doc": {
          "properties": {
            "name": {
              "type": "keyword"
            }
          }
        }
      },
      "settings": {
        "index": {
          "creation_date": "1542425093864",
          "number_of_shards": "1",
          "number_of_replicas": "1",
          "uuid": "PSz3BjjKRIKamHSnW0QhVQ",
          "version": {
            "created": "6040299"
          },
          "provided_name": "test_index"
        }
      }
    }
  }
  ```

- 删除索引模板

  ```json
  DELETE _template/test_template
  ```





