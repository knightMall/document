

* [正排索引](#正排索引)
* [倒排索引](#倒排索引)
  * [倒排索引-查询流程](#倒排索引-查询流程)
  * [倒排索引组成](#倒排索引组成)
  * [ES中倒排索引的存储](#es中倒排索引的存储)

* [分词](#分词)
  * [分词器](#分词器)
  * [分词器调用顺序](#分词器调用顺序)
  * [分词API使用](#分词api使用)
    * [使用分词器](#使用分词器)
    * [直接指定索引中的字段](#直接指定索引中的字段)
    * [自定义分词器](#自定义分词器)
    * [预定义的分词器](#预定义的分词器)
      * [Standard Analyzer](#standard-analyzer)
      * [Simple Analyzer](#simple-analyzer)
      * [Whitespace Analyzer](#whitespace-analyzer)
      * [Stop Analyzer](#stop-analyzer)
      * [Keyword Analyzer](#keyword-analyzer)
      * [Pattern Analyzer](#pattern-analyzer)
      * [Language Analyzer](#language-analyzer)
    * [中文分词](#中文分词)
    * [自定义分词](#自定义分词)
      * [自定义Character Filters](#自定义character-filters)
      * [自定义Tokenizer](#自定义tokenizer)
      * [自定义Token Filters](#自定义token-filters)
    * [分词的使用说明](#分词的使用说明)
    * [查看文档](#查看文档)




## 正排索引

- 文档id到文档内容、单词的关联关系

  | 文档id |            文档内容             |
  | :----: | :-----------------------------: |
  |   1    | elasticsearch是最流行的搜索引擎 |
  |   2    |      php是世界上最好的语言      |
  |   3    |      搜索引擎是如何诞生的       |


## 倒排索引

- 单词到文档id的关联关系

|     单词      | 文档id列表 |
| :-----------: | :--------: |
| elasticsearch |     1      |
|     流行      |     1      |
|   搜索引擎    |    1,3     |
|      php      |     2      |
|     世界      |     2      |
|     最好      |     2      |
|     语言      |     2      |
|     如何      |     3      |
|     诞生      |     3      |

上面的表格就是正排索引转换成倒排索引的

![正派索引和倒排索引的关系](../../pic/es/正派索引和倒排索引的关系.jpg)

### 倒排索引-查询流程

- 查询包含“搜索引擎”的文档
  - 通过倒排索引获得“搜索引擎”对应的文档有1和3
  - 通过正排索引查询1和3的完整内容
  - 返回结果

### 倒排索引组成

倒排索引是搜索引擎的核心，主要包含两部分：

- 单词词典，一般使用B+ Tree实现

  - 记录所有文档的单词，一般比较大
  - 记录单词到倒排列表的关联信息

- 倒排列表，记录了单词对应的文档集合，由倒排索引项组成。主要包含如下信息：

  - 文档Id，用于获取原始信息

  - 单词频率(TF,Term Frequency)，记录该单词在该文档中出现次数，用于后续相关性算分

  - 位置(Position)，记录单词在文档中的分词位置(多个)，用于做词语搜索

  - 偏移(Offset)，记录单词在文档的开始和结束位置，用于高亮显示

    具体如下图：

    ![倒排列表](../../pic/es/倒排列表.jpg)

- 单词字典与倒排列表整合在一起的结构：

  ![单词字典与倒排列表](../../pic/es/单词字典与倒排列表.jpg)

- 整个搜索流程，先通过B+树找到关键字，然后再找到倒排列表然后进行相关性算分再通过正排索引获取文档

### ES中倒排索引的存储

- es存储的是一个json格式，包含多个字段，每个字段会有自己的倒排索引，例如

  ```json
  {
      "name":"alfred",
      "job":"programmer"
  }
  ```

  这个文件，name有一个倒排索引、job有一个倒排索引

## 分词

分词是指文本转出一系列单词的过程，也可以叫做文本分析，在es中称为Analysis，如下图

![分词示例](../../pic/es/分词示例.jpg)



### 分词器

分词器是es中专门处理分词的组件，英文为Analyzer，组成如下：

- Character Filters
  - 针对原始文本进行处理，比如去除html特殊标记符
- Tokenizer
  - 将原始文本按照一定规则切分为单词
- Token Filters
  - 针对tokenizer处理的单词进行再加工，比如转小写、删除或新增等处理

### 分词器调用顺序

![分词器-调用顺序](../../pic/es/分词器-调用顺序.jpg)

### 分词API使用

es提供了一个测试分词的api接口，方便验证分词效果，地址是`_analyze`

- 可以直接指定analyzer进行测试
- 可以直接指定索引中的字段进行测试
- 可以自定义分词器进行测试

#### 使用分词器

```json
POST _analyze
{
  "analyzer": "standard",
  "text": "hello world"
}
```

响应结果

```json
{
  "tokens": [
    {
      "token": "hello",
      "start_offset": 0,
      "end_offset": 5,
      "type": "<ALPHANUM>",
      "position": 0
    },
    {
      "token": "world",
      "start_offset": 6,
      "end_offset": 11,
      "type": "<ALPHANUM>",
      "position": 1
    }
  ]
}
```

字段说明，请求字段说明：

- analyzer：分词器
- text：测试文本

响应结果说明：

- token：分词结果
- start_offset：起始偏移
- end_offset：结束偏移
- position：分词位置



#### 直接指定索引中的字段

```json
POST test_index/_analyze
{
  "field": "username",
  "text": "hello world"
}
```

请求结果字段解释：

- field：测试字段
- text：测试文本

响应结果：

```json
{
  "tokens": [
    {
      "token": "hello",
      "start_offset": 0,
      "end_offset": 5,
      "type": "<ALPHANUM>",
      "position": 0
    },
    {
      "token": "world",
      "start_offset": 6,
      "end_offset": 11,
      "type": "<ALPHANUM>",
      "position": 1
    }
  ]
}
```

#### 自定义分词器

```json
POST _analyze
{
  "tokenizer": "standard",
  "filter": ["lowercase"], 
  "text": "Hello World!"
}
```

请求字段解释：

- tokenizer：分词器
- filter：Token Filters，转小写
- text：测试文本

响应结果

```json
{
  "tokens": [
    {
      "token": "hello",
      "start_offset": 0,
      "end_offset": 5,
      "type": "<ALPHANUM>",
      "position": 0
    },
    {
      "token": "world",
      "start_offset": 6,
      "end_offset": 11,
      "type": "<ALPHANUM>",
      "position": 1
    }
  ]
}
```



#### 预定义的分词器

es自带如下的分词器

- Standard
- Simple
- Whitespace
- Stop
- Keyword
- Pattern
- Language

##### Standard Analyzer

- 默认分词器

- 组成如下图，特性是

  - 按词切分，支持多语言
  - 小写处理

  ![Standard分词器](../../pic/es/Standard分词器.jpg)

  - 使用如下：

    ```json
    POST _analyze
    {
      "analyzer": "standard",
      "text": "The 2 QUICK Brown-Foxes jumped over the lazy dog's bone."
    }
    ```

##### Simple Analyzer

- 特性是，组成如下图：
  - 按照非字母切分
  - 小写处理

![Simple Analyzer分词器.jpg](../../pic/es/Simple-Analyzer分词器.jpg)

- 使用如下

  ```json
  POST _analyze
  {
    "analyzer": "simple",
    "text": "The 2 QUICK Brown-Foxes jumped over the lazy dog's bone."
  }
  ```


##### Whitespace Analyzer

- 组成如图，特性是
  - 按照空格切分

![whitespace Analyzer空格切分](../../pic/es/whitespace-Analyzer空格切分.jpg)

- 使用如下

  ```json
  POST _analyze
  {
    "analyzer": "whitespace",
    "text": "The 2 QUICK Brown-Foxes jumped over the lazy dog's bone."
  }
  ```

##### Stop Analyzer

- Stop Word指语气助词等修饰性的词语，比如the、an、的、这等等

- 其组成如图，特性为

  - 相比Simple Analyzer多了Stop Word处理

  ![stop-analyzer分词器.jpg](../../pic/es/stop-analyzer分词器.jpg)

  - 使用如下

    ```json
    POST _analyze
    {
      "analyzer": "stop",
      "text": "The 2 QUICK Brown-Foxes jumped over the lazy dog's bone."
    }
    ```


##### Keyword Analyzer

- 组成如图，特性为：
  - 不分词，直接将输入作为一个单词输出

![Keyword Analyzer分词器](../../pic/es/Keyword-Analyzer分词器.jpg)

- 使用如下

  ```json
  POST _analyze
  {
    "analyzer": "keyword",
    "text": "The 2 QUICK Brown-Foxes jumped over the lazy dog's bone."
  }
  ```


##### Pattern Analyzer

- 其组成如图，特性是

  - 通过正则表达式自定义分割符
  - 默认是\W+，即非字词的符号作为分隔符

  ![Pattern Analyzer](../../pic/es/Pattern-Analyzer.jpg)

- 使用

  ```json
  POST _analyze
  {
    "analyzer": "pattern",
    "text": "The 2 QUICK Brown-Foxes jumped over the lazy dog's bone."
  }
  ```



##### Language Analyzer

- 提供了30+常见语言分词器



#### 中文分词

常见的中文分词系统

- IK

  - 实现中英文单词的切分，支持ik_smart、ik_maxword等模式

  - 可自定义词库，支持热更新分词词典

  - [IK分词Git项目](https://github.com/medcl/elasticsearch-analysis-ik )

- jieba

  - python中最流行的分词系统，支持分词和词性标注

  - 支持繁体分词、自定义词典、并行分词

  - [jieba分词Git项目](https://github.com/fxsjy/jieba)


#### 自定义分词

当自带的分词无法满足需求时，可以自定义分词。通过自定义`Character Filters`、`Tokenizer`和`Token Filter`实现

##### 自定义Character Filters

- 在Tokenizer之前对原始文本进行处理，比如增加、删除或替换字符等

- es自带的Character Filters

  - html_strip 去除html标签和转换html实体
  - Mapping 进行字符替换操作
  - Pattern Replace进行正则匹配替换

- 字符串进行Character Filters后使用tokenizer解析会影响postion和offset信息

- 使用

  ```json
  POST _analyze
  {
    "tokenizer": "keyword",  //keyword类型的tokenizer可以直接输出结果
    "char_filter": ["html_strip"], //指明要使用的char_filter
    "text": "<p>I&apos;m so <b>happy</b>!</p>"
  }
  ```

  响应结果：

  ```json
  {
    "tokens": [
      {
        "token": """
  
  I'm so happy!
  
  """,
        "start_offset": 0,
        "end_offset": 32,
        "type": "word",
        "position": 0
      }
    ]
  }
  ```

  maping Filter的使用

  ```json
  POST _analyze
  {
    "tokenizer": "keyword",
    "char_filter": [{
      "type":"mapping",
      "mappings":[
         ":) => _happy_",
          ":( => _sad_"
        ]
    }],
    "text": "I'm delighted about it :("
  }
  ```

  结果如下：

  ```json
  {
    "tokens": [
      {
        "token": "I'm delighted about it _sad_",
        "start_offset": 0,
        "end_offset": 25,
        "type": "word",
        "position": 0
      }
    ]
  }
  ```



##### 自定义Tokenizer

- 将原始文本按照一定规则切分为单词(term or token)
- 自带的如下：
  - standard按照单词进行分割
  - letter按照非字符类进行分割
  - whitespace 按照空格进行分割
  - UAZ URL Email按照standard分割，但不会分割邮箱和url
  - NGram和Edge NGram连词分割
  - path-hierarchy按照文件路径进行切割
- 使用

```json
POST _analyze
{
  "tokenizer": "path_hierarchy",
  "text": "/one/two/three"
}
```

响应结果：

```json
{
  "tokens": [
    {
      "token": "/one",
      "start_offset": 0,
      "end_offset": 4,
      "type": "word",
      "position": 0
    },
    {
      "token": "/one/two",
      "start_offset": 0,
      "end_offset": 8,
      "type": "word",
      "position": 0
    },
    {
      "token": "/one/two/three",
      "start_offset": 0,
      "end_offset": 14,
      "type": "word",
      "position": 0
    }
  ]
}
```



##### 自定义Token Filters

- 对于tokenizer输出的单词(term)进行增加、删除、修改等操作

- 自带的如下：

  - lowercase将所有term转换为小写
  - stop 删除stop words
  - NGram和Edge NGram 连词分割
  - Synonym 添加近义词term

- 使用如下

  ```json
  POST _analyze
  {
    "tokenizer": "standard",
    "filter": [
        "stop",
        "lowercase",
        {
        "type":"ngram",
        "min_gram":2,
        "max_gram":4
        }
      ],
      "text": "a Hello World"
  }
  ```

  响应结果：

  ```json
  {
    "tokens": [
      {
        "token": "he",
        "start_offset": 2,
        "end_offset": 7,
        "type": "<ALPHANUM>",
        "position": 1
      },
      {
        "token": "hel",
        "start_offset": 2,
        "end_offset": 7,
        "type": "<ALPHANUM>",
        "position": 1
      },
      {
        "token": "hell",
        "start_offset": 2,
        "end_offset": 7,
        "type": "<ALPHANUM>",
        "position": 1
      },
      {
        "token": "el",
        "start_offset": 2,
        "end_offset": 7,
        "type": "<ALPHANUM>",
        "position": 1
      },
      {
        "token": "ell",
        "start_offset": 2,
        "end_offset": 7,
        "type": "<ALPHANUM>",
        "position": 1
      },
      {
        "token": "ello",
        "start_offset": 2,
        "end_offset": 7,
        "type": "<ALPHANUM>",
        "position": 1
      },
      {
        "token": "ll",
        "start_offset": 2,
        "end_offset": 7,
        "type": "<ALPHANUM>",
        "position": 1
      },
      {
        "token": "llo",
        "start_offset": 2,
        "end_offset": 7,
        "type": "<ALPHANUM>",
        "position": 1
      },
      {
        "token": "lo",
        "start_offset": 2,
        "end_offset": 7,
        "type": "<ALPHANUM>",
        "position": 1
      },
      {
        "token": "wo",
        "start_offset": 8,
        "end_offset": 13,
        "type": "<ALPHANUM>",
        "position": 2
      },
      {
        "token": "wor",
        "start_offset": 8,
        "end_offset": 13,
        "type": "<ALPHANUM>",
        "position": 2
      },
      {
        "token": "worl",
        "start_offset": 8,
        "end_offset": 13,
        "type": "<ALPHANUM>",
        "position": 2
      },
      {
        "token": "or",
        "start_offset": 8,
        "end_offset": 13,
        "type": "<ALPHANUM>",
        "position": 2
      },
      {
        "token": "orl",
        "start_offset": 8,
        "end_offset": 13,
        "type": "<ALPHANUM>",
        "position": 2
      },
      {
        "token": "orld",
        "start_offset": 8,
        "end_offset": 13,
        "type": "<ALPHANUM>",
        "position": 2
      },
      {
        "token": "rl",
        "start_offset": 8,
        "end_offset": 13,
        "type": "<ALPHANUM>",
        "position": 2
      },
      {
        "token": "rld",
        "start_offset": 8,
        "end_offset": 13,
        "type": "<ALPHANUM>",
        "position": 2
      },
      {
        "token": "ld",
        "start_offset": 8,
        "end_offset": 13,
        "type": "<ALPHANUM>",
        "position": 2
      }
    ]
  }
  ```




#### 分词的使用说明

- 分词会在如下两个时机使用
  - 创建和更新文档时(Index Time)，会对相应的文档进行分词处理
  - 查询时(Search Time)，会对查询语句进行分词
    - 查询的时候通过`analyzer`指定分词器
    - 通过index mapping设置`search_analyzer`
- 分词的使用建议
  - 明确字段是否需要分词，不需要分词的字段就将`type`设置为`keyword`，可以节省空间和提高写性能
  - 善用`_analyze API`，查看文档的具体分词结果

#### 查看文档

[官方文档](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis.html )

