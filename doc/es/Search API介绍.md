- [ES查询API](#es查询api)
  - [查询方式](#查询方式)
    - [URI Search](#uri-search)
    - [查询字符串语法(Query String Syntax)](#查询字符串语法query-string-syntax)
    - [范查询示例](#范查询示例)
    - [布尔操作符](#布尔操作符)
    - [范围查询，支持数值和日期](#范围查询支持数值和日期)
    - [通配符查询](#通配符查询)
    - [正则表达式](#正则表达式)
    - [模糊查询 fuzzy query](#模糊匹配-fuzzy-query)
    - [近似度查询](#近似度查询)
  - [Request Body Search](#request-body-search)
    - [Query DSL介绍](#query-dsl介绍)
    - [字段类查询](#字段类查询)
      - [Match Query](#match-query)
    - [相关性算分](#相关性算分)
      - [相关性算分的相关概念](#相关性算分的相关概念)
      - [ES算分模型](#es算分模型)
    - [Match Phrase Query](#match-phrase-query)
    - [Query String Query](#query-string-query)
    - [Simple Query String Query](#simple-query-string-query)
    - [Term Query](#term-query)
    - [Terms Query](#terms-query)
    - [Range Query](#range-query)
    - [复合查询](#复合查询)
      - [Constan Score Query](#constan-score-query)
      - [Bool Query](#bool-query)
        - [Bool Query之Filter](#bool-query之filter)
        - [Bool Query之Must](#bool-query之must)
        - [Bool Query之Must not](#bool-query之must-not)
        - [Bool Query之Should](#bool-query之should)
      - [Query Context和Filter Context的区别](#query-context和filter-context的区别)
    - [Count API](#count-api)
    - [Source Filtering](#source-filtering)











## ES查询API

- 实现对ES中存储的数据进行查询分析，endpoint为`_search`，可以同时指定多个索引查询，如下所示：

  ```json
  GET /_search
  GET /索引名/_search
  GET /索引名1，索引名2/_search
  GET /index_*/_search
  ```

### 查询方式

查询方式主要有两种形式：

- URI Search

  - 操作简单，方便通过命令行测试
  - 仅包含部分查询语法
  - `GET /my_index/_search?q=user:alfred`

- Request Body Search

  - ES提供的完备查询语法Query DSL(Domain Specific Language)

    ```json
    GET /my_index/_search
    {
        "query":{
            "term":{"user":"alfred"}
        }
    }
    ```



### URI Search

通过url query参数来实现搜索，常用参数如下：

- q指定查询的语句，语法为Query String Syntax

- df(default field) 当q中不指定字段时默认查询的字段，如果不指定，es会查询所有字段

- sort 排序

- timeout 指定超时时间，默认不超时

- from，size用于分页

  ![url-search](../../pic/es/2018-11-17_url-search.jpg)



#### 查询字符串语法(Query String Syntax)

- term与phrase
  - `alfred way`等于`alfred OR way`
  - `"alfred way"` 词语查询，要求先后查询。这个就是`phrase`

- 泛查询

  - alfred等效于在所有字段去匹配该 term

- 指定字段

  - `name:alfred`

- Group 分组设定，使用括号指定匹配的规则
  - `(quick OR brown) AND fox` ，通过括号指定优先级
  - `status:(active OR pending) title:(full text search)` 

- 示例使用：

  - 创建基础索引和数据

    ```json
    # 创建索引
    PUT test_search_index
    {
      "settings": {
        "index":{
          "number_of_shards":1
        }
      }
    }
    
    # 批量添加文档
    POST test_search_index/doc/_bulk
    {
      "index": {
        "_id": "1"
      }
    }
    {
      "username": "alfred way",
      "job": "java engineer",
      "age": 18,
      "birth": "1990-01-02",
      "isMarride": false
    }
    {
      "index": {
        "_id": "2"
      }
    }
    {
      "username": "alfred",
      "job": "java senior enginner and java specialist",
      "age": 28,
      "birth": "1980-05-07",
      "isMarried": true
    }
    {
      "index": {
        "_id": "3"
      }
    }
    {
      "username": "lee",
      "job": "java and ruby engineer",
      "age": 22,
      "birth": "1985-08-07",
      "isMarried": false
    }
    {
      "index": {
        "_id": "4"
      }
    }
    {
      "username": "alfred junior way",
      "job": "ruby engineer",
      "age": 23,
      "birth": "1989-08-07",
      "isMarried": false
    }
    {
      "index": {
        "_id": "5"
      }
    }
    {
      "username": "lee way",
      "job": "java engineer and java specialist",
      "age": 30,
      "birth": "1978-04-07",
      "isMarried": true
    }
    ```



##### 范查询示例

```json
# 查询所有字段包含alfred term字段的值
GET test_search_index/_search?q=alfred

# 指定profile属性为true，可以查看查询过程
GET test_search_index/_search?q=alfred
{
  "profile": "true"
}
# 响应结果如下
 "profile": {
    "shards": [
      {
        "id": "[-mLslJlsS8y-IHMNPkPxzw][test_search_index][0]",
        "searches": [
          {
            "query": [
              {
                "type": "DisjunctionMaxQuery",
                "description": """(username.keyword:alfred | MatchNoDocsQuery("failed [birth] query, caused by parse_exception:[failed to parse date field [alfred] with format [strict_date_optional_time||epoch_millis]]") | job:alfred | MatchNoDocsQuery("failed [age] query, caused by number_format_exception:[For input string: "alfred"]") | username:alfred | job.keyword:alfred)""",
                "time_in_nanos": 890338,
                "breakdown": {
                  "score": 12618,
                  "build_scorer_count": 2,
                  "match_count": 0,
                  "create_weight": 594826,
                  "next_doc": 13172,
                  "match": 0,
                  "create_weight_count": 1,
                  "next_doc_count": 4,
                  "score_count": 3,
                  "build_scorer": 269712,
                  "advance": 0,
                  "advance_count": 0
                },
                "children": [
                  {
                    "type": "TermQuery",
                    "description": "username.keyword:alfred",
                    "time_in_nanos": 438233,
                    "breakdown": {
                      "score": 804,
                      "build_scorer_count": 2,
                      "match_count": 0,
                      "create_weight": 359284,
                      "next_doc": 1230,
                      "match": 0,
                      "create_weight_count": 1,
                      "next_doc_count": 2,
                      "score_count": 1,
                      "build_scorer": 76909,
                      "advance": 0,
                      "advance_count": 0
                    }
                  },
                  {
                    "type": "MatchNoDocsQuery",
                    "description": """MatchNoDocsQuery("failed [birth] query, caused by parse_exception:[failed to parse date field [alfred] with format [strict_date_optional_time||epoch_millis]]")""",
                    "time_in_nanos": 2545,
                    "breakdown": {
                      "score": 0,
                      "build_scorer_count": 1,
                      "match_count": 0,
                      "create_weight": 1728,
                      "next_doc": 0,
                      "match": 0,
                      "create_weight_count": 1,
                      "next_doc_count": 0,
                      "score_count": 0,
                      "build_scorer": 815,
                      "advance": 0,
                      "advance_count": 0
                    }
                  },
                  {
                    "type": "TermQuery",
                    "description": "job:alfred",
                    "time_in_nanos": 55409,
                    "breakdown": {
                      "score": 0,
                      "build_scorer_count": 1,
                      "match_count": 0,
                      "create_weight": 54173,
                      "next_doc": 0,
                      "match": 0,
                      "create_weight_count": 1,
                      "next_doc_count": 0,
                      "score_count": 0,
                      "build_scorer": 1234,
                      "advance": 0,
                      "advance_count": 0
                    }
                  },
                  {
                    "type": "MatchNoDocsQuery",
                    "description": """MatchNoDocsQuery("failed [age] query, caused by number_format_exception:[For input string: "alfred"]")""",
                    "time_in_nanos": 1242,
                    "breakdown": {
                      "score": 0,
                      "build_scorer_count": 1,
                      "match_count": 0,
                      "create_weight": 879,
                      "next_doc": 0,
                      "match": 0,
                      "create_weight_count": 1,
                      "next_doc_count": 0,
                      "score_count": 0,
                      "build_scorer": 361,
                      "advance": 0,
                      "advance_count": 0
                    }
                  },
                  {
                    "type": "TermQuery",
                    "description": "username:alfred",
                    "time_in_nanos": 128494,
                    "breakdown": {
                      "score": 6826,
                      "build_scorer_count": 2,
                      "match_count": 0,
                      "create_weight": 39694,
                      "next_doc": 2777,
                      "match": 0,
                      "create_weight_count": 1,
                      "next_doc_count": 4,
                      "score_count": 3,
                      "build_scorer": 79187,
                      "advance": 0,
                      "advance_count": 0
                    }
                  },
                  {
                    "type": "TermQuery",
                    "description": "job.keyword:alfred",
                    "time_in_nanos": 35461,
                    "breakdown": {
                      "score": 0,
                      "build_scorer_count": 1,
                      "match_count": 0,
                      "create_weight": 34360,
                      "next_doc": 0,
                      "match": 0,
                      "create_weight_count": 1,
                      "next_doc_count": 0,
                      "score_count": 0,
                      "build_scorer": 1099,
                      "advance": 0,
                      "advance_count": 0
                    }
                  }
                ]
              }
            ],
            "rewrite_time": 29187,
            "collector": [
              {
                "name": "CancellableCollector",
                "reason": "search_cancelled",
                "time_in_nanos": 71227,
                "children": [
                  {
                    "name": "SimpleTopScoreDocCollector",
                    "reason": "search_top_hits",
                    "time_in_nanos": 39775
                  }
                ]
              }
            ]
          }
        ],
        "aggregations": []
      }
    ]
  }
}
```

通过查看详细的查询过程可以看出整个查询经历了那些过程，详细字段介绍可以查看文档

[profile字段介绍](https://www.elastic.co/guide/en/elasticsearch/reference/current/_profiling_queries.html)

```json
# 查询username字段包含alfred的所有字段
GET test_search_index/_search?q=username:alfred

# 查询username字段包含alfred或所有字段中包含way的记录 这是是BooleanQuery
GET test_search_index/_search?q=username:alfred way

# 查询username字段等于alfred way字段的值，这是使用了PhraseQuery。可以通过设置profile为true
GET test_search_index/_search?q=username:"alfred way"

# 查询username包含alfred或username包含way的记录，BooleanQuery加TermQuery
GET test_search_index/_search?q=username:(alfred way)
```



##### 布尔操作符

- AND(&&)、OR(||)、NOT(!)

  - `name:(tom NOT lee)` 表示查询name字段中不包含lee但是可以包含tom的文档
  - 注意需要大写，不能小写

- `+` 对应must，`-` 对应must_not

  - `name:(tom +lee -alfred)` 表示查询name字段必须包含lee，必须不包含alfred，可以包含tom的文档
  - `name:((lee && !alfred) || (tom && lee && !alfred))` 表示的查询意义同上
  - `+` 在url中会被解析为空格，需要使用encode后的结果，%2B

- 使用示例：

  ```json
  # 查询username包含alfred 且 其他字段包含 way的文档，BooleanQuery然后再是范查询
  GET test_search_index/_search?q=username:alfred AND way
  
  # 查询username包含alfred且包含way的文档
  GET test_search_index/_search?q=username:(alfred AND way)
  
  # 查询username中包含alfred但是不包含way的文档
  GET test_search_index/_search?q=username:(alfred NOT way)
  # 查询username中必须包含way但可以包含alfred的文档
  GET test_search_index/_search?q=username:(alfred %2Bway)
  
  ```





##### 范围查询，支持数值和日期

- 区间写法，闭区间用[]，开区间用{}

  - `age:[1 TO 10] `表示 `1<=age<=10`
  - `age:[1 TO 10} `表示 `1<=age<10`
  - `age:[1 TO} `表示 `age>=1`
  - `age:[* TO 10]` 表示 `age<=10`

- 算数符号写法

  - `age:>=1`
  - `age:(>=1 && <=10)` 或者 `age:(+>=1 +<=10)`

- 使用示例

  ```json
  # 查询username字段包含 alfred 或 age字段大于20的文档
  GET test_search_index/_search?q=username:alfred age:>20
  # 查询username字段包含 alfred 且 age大于20的文档
  GET test_search_index/_search?q=username:alfred AND age:>20
  # 查询birth字段在 1980 和 1990之间范围的数据
  GET test_search_index/_search?q=birth:(>1980 AND <1990)
  ```



##### 通配符查询 

- ? 代表1个字符，*代表0或多个字符

  - `name:t?m`
  - `name:tom*`
  - `name:t*m`

- 通配符匹配执行效率低，且占用较多内存，不建议使用

- 如无特殊需求，不要将`?/*`字符放在最前面

- 使用示例

  ```:bookmark_tabs:
  # 查询username以l开头的所有文档
  GET test_search_index/_search?q=username:l*
  ```



##### 正则表达式

通过正则表达式匹配，比较耗费内存



##### 模糊匹配 fuzzy query

- `name:roam~1`
- 匹配与roam差1个character的词，比如foam，roams等
- 主要用于纠错

```:robot:
GET test_search_index/_search?q=username:alfed~1
```



##### 近似度查询

- `"fox quick"~5`
- 以term为单位进行差异比较，比如`"quick fox" "quick brown fox"`都会被匹配
- 主要用于纠错

```json
# 查询java enginner之间可以允许添加2个单词
GET test_search_index/_search?q=job:"java engineer"~2
```





### Request Body Search

将查询语句通过http request body 发送到es，主要包含如下参数：

- query符合 Query DSL语法的查询语句

- from、size

- timeout

- sort

  ![2018-11-18_QDSL](../../pic/es/2018-11-18_QDSL.jpg)

#### Query DSL介绍

基于JSON定义的查询语言，主要包含如下两种类型：

- 字段类查询
  - 如`term`、`match`、`range`等，只针对某个字段进行查询
- 复合查询
  - 如`bool`查询等，包含一个或多个字段类查询或者复合查询语句



#### 字段类查询

字段类查询主要包括以下两类：

- 全文匹配
  - 针对`text`类型的字段进行全文检索，会对查询语句先进行分词处理，如`match`、`match_phrase`等query类型
- 单词匹配
  - 不会对查询语句做分词处理，直接去匹配字段的倒排索引，如`term`、`terms`、`range`等query类型



##### Match Query

对字段作全文检索，最基本和常用的查询类型，API示例如下：

![2018-11-18_match_query](../../pic/es/2018-11-18_match_query.jpg)

```json
# 查询usernam包含alfre或username包含way的文档，同样也可以通过设置profile为true查看查询详情
GET test_search_index/_search
{
  "query": {
    "match": {
      "username": "alfred way"
    }
  }
}
```

- Match Query查询流程

  ![2018-11-18_match_query查询流程](../../pic/es/2018-11-18_match_query查询流程.jpg)

- 上图中提到分词后各个单词之间的关系，可以通过关键字`operator`指定，可选值为`or`和`and`。语法格式有变化：

  ![2018-11-18_match_query2](../../pic/es/2018-11-18_match_query2.jpg)

- 使用示例

  ```json
  # 查询username字段包含alfred并且还包含way的文档
  GET test_search_index/_search
  {
    "query": {
      "match": {
        "username": {
          "query": "alfred way",
          "operator": "and"
        }
      }
    },
    "profile": "true"
  }
  ```

- 通过参数控制需要匹配的单词数

  ![match_query_匹配单词数](../../pic/es/match_query_匹配单词数.jpg)

- 使用示例

  ```json
  # 查询job字段中至少包含'java ruby engineer' 三个单词中的两个
  GET test_search_index/_search
  {
    "query": {
      "match": {
        "job": {
          "query": "java ruby engineer",
          "minimum_should_match": 2
        }
      }
    }
  }
  ```



#### 相关性算分

相关性算分是指文档与查询语句间的相关度，英文为`relevance`

- 通过倒排索引可以获取与查询语句相匹配的文档列表



##### 相关性算分的相关概念

- **Term Frequency(TF)** 词频，即单词在该文档出现的次数。词频越高，相关度越高
- **Document Frequency(DF)** 文档频率，即单词出现的文档数
- **Inverse Document Frequency(IDF)** 逆向文档频率，与文档频率相反；简单理解为1/DF。即单词出现的文档数越少，相关度越高
- **Field-length Norm** 文档越短，相关性越高



##### ES算分模型

- **TF/IDF 模型**

- **BM25模型** 5.x之后的默认模型

- 通过使用`explain`参数来查看具体的计算方法，但是需要注意：

  - es的算分是按照shard进行的，即shard的分数计算是相互独立的，所以在使用`explain`的时候注意分片数
  - 可以通过设置索引的分片数为1来避免这个问题

- 使用示例

  ```json
  GET test_search_index/_search
  {
    "query": {
      "match": {
        "username": {
          "query": "alfred way"
        }
      }
    },
    "explain": "true"
  }
  ```



#### Match Phrase Query

- 对字段作检索，有顺序要求，API示例如下：

  ![2018-11-18_match_phrase_query](../../pic/es/2018-11-18_match_phrase_query.jpg)

- 通过`slop`参数可以控制单词间的间隔

- 示例如下

  ```json
  # 查询job包含'java engineer'，必须要按照该顺序。使用phraseQuery
  GET test_search_index/_search
  {
    "query": {
      "match_phrase": {
        "job": "java engineer"
      }
    }
  }
  
  # 通过使用slop实现单词间的间隔，使用近似度查询 
  GET test_search_index/_search
  {
    "query": {
      "match_phrase": {
        "job": {
          "query": "java engineer",
          "slop": 1
        }
      }
    },
    "profile": "true"
  }
  
  ```




#### Query String  Query

类似于URI Search中的q参数查询

![2018-11-18_query_string_query](../../pic/es/2018-11-18_query_string_query.jpg)

- 使用示例

  ```json
  #查询username字段包含alfred且包含way的文档
  GET test_search_index/_search
  {
    "query": {
      "query_string": {
        "default_field": "username",
        "query": "alfred AND way"
      }
    }
  }
  #查询username字段包含alfred或job字段包含alfred或者username同时包含java和ruby或job同时包含java和ruby的
  GET test_search_index/_search
  {
    "query": {
      "query_string": {
        "fields": ["username","job"],
        "query": "alfred OR (java AND ruby)"
      }
    }
  }
  ```



#### Simple Query String Query

- 类似`Query String`，但是会忽略错误的查询语法，并且仅支持部分查询语法

- 其常用的逻辑符号如下，不能使用`AND`、`OR`、`NOT` 等关键字：

  - `+`替代 `AND`

  - `|`替代 `OR`

  - `-`替代 `NOT`

  - 示例

    ![2018-11-18_simple_query_string_query](../../pic/es/2018-11-18_simple_query_string_query.jpg)

  - 使用示例

    ```json
    # 查询username字段同时包含alfred和way的
    GET test_search_index/_search
    {
      "query": {
        "simple_query_string": {
          "query": "alfred +way",
          "fields": ["username"]
        }
      },
      "profile": "true"
    }
    ```





#### Term Query

- 将查询语句作为整个单词进行查询，即不对查询语句做分词处理，如下所示：

  ![2018-11-18_term_query](../../pic/es/2018-11-18_term_query.jpg)

- 使用示例

  ```json
  # username中包含lee的文档
  GET test_search_index/_search
  {
    "query": {
      "term": {
        "username": {
          "value": "lee"
        }
      }
    }
  }
  ```




#### Terms Query

一次传入多个单词进行查询，如下所示：

![2018-11-18_terms_query](../../pic/es/2018-11-18_terms_query.jpg)

- 使用示例如下

  ```json
  # 查询username字段中包含lee或包含alfred的文档
  GET test_search_index/_search
  {
    "query": {
      "terms": {
        "username": [
          "lee",
          "alfred"
        ]
      }
    },
    "profile": "true"
  }
  ```





#### Range Query

- 范围查询主要针对数值和日期类型，如下：

![2018-11-18_range_query](../../pic/es/2018-11-18_range_query.jpg)

- 针对日期做查询如下：

  ![2018-11-18_rang_query_日期查询](../../pic/es/2018-11-18_rang_query_日期查询.jpg)

  - Date Math的使用，格式如下

    ![2018-11-18_date_math](../../pic/es/2018-11-18_date_math.jpg)

  - 单位主要有如下几种：

    - y - years
    - M - months
    - w - weeks
    - d - days
    - h - hours
    - m - minutes
    - s - seconds

  - 示例使用，假设now为 2018-01-02 12:00:00，那么如下的计算结果实际为：

    |      计算公式       |      实际结果       |
    | :-----------------: | :-----------------: |
    |       now+1h        | 2018-01-02 13:00:00 |
    |       now-1h        | 2018-01-02 11:00:00 |
    |      now-1h/d       | 2018-01-02 00:00:00 |
    | 2016-01-01\|\|+1M/d | 2016-02-01 00:00:00 |

  - 使用示例

    ```json
    # 查询age字段在10到20的文档
    GET test_search_index/_search
    {
      "query": {
        "range": {
          "age": {
            "gte": 10,
            "lte": 20
          }
        }
      }
    }
    # birth大于1980-01-01的文档
    GET test_search_index/_search
    {
      "query": {
        "range": {
          "birth": {
            "gte": "1980-01-01"
          }
        }
      }
    }
    ```



#### 复合查询

复合查询是指包含字段类查询或复合查询的类型，主要包括以下几类：

- **constant_score query**
- **bool query**
- **dis_max query**
- **function_score query**
- **boosting query**



##### Constant Score Query

该查询将其内部的查询结果文档得分都设定为1或者boost的值；多用于结合bool查询实现自定义得分。具体如下：

![2018-11-18_constant_score_query](../../pic/es/2018-11-18_constant_score_query.jpg)

- 使用示例如下

  ```json
  # 分数都返回1
  GET test_search_index/_search
  {
    "query": {
      "constant_score": {
        "filter": {
          "match":{
            "username":"alfred"
          }
        }
      }
    },
    "profile": "true"
  }
  
  # 指定固定的分数
  GET test_search_index/_search
  {
    "query": {
      "constant_score": {
        "filter": {
          "match":{
            "username":"alfred"
          }
        },
        "boost": 1.2
      }
    },
    "profile": "true"
  }
  
  ```


##### Bool Query

布尔查询由一个或多个布尔子句组成，主要包含如下4个：

| 组成子句关键字 |                    实现效果                    |
| :------------: | :--------------------------------------------: |
|     filter     |     只过滤符合条件的文档，不计算相关性得分     |
|      must      |   文档必须符合must中的条件，会影响相关性得分   |
|    must_not    |       文档必须不符合must_not中的所有条件       |
|     should     | 文档中可以符合should中的条件，会影响相关性得分 |



- Bool查询的API如下图

  ![2018-11-18_bool_query_API](../../pic/es/2018-11-18_bool_query_API.jpg)

###### Bool Query之Filter

- Filter 查询只过滤符合条件的文档，不会进行相关性算分

  - es针对filter会有智能缓存，因此其执行效率很高

  - 做简单匹配查询且不考虑算分时，推荐使用filter替代query等

  - 使用示例：

    ```json
    # 通过filter进行过滤，查询username字段中包含lee的文档
    GET test_search_index/_search
    {
      "query": {
        "bool": {
          "filter": {
            "term": {
              "username": {
                "value":"lee"
              }
            }
          }
        }
      }
    }
    ```



###### Bool Query之Must

使用如下：

```json
# 查询username包含alfred且job包含ruby的文档
GET test_search_index/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "username": {
              "query":"alfred"
            }
          }
        },{
          "match": {
            "job": {
              "query":"ruby"
            }
          }
        }
      ]
    }
  }
}
```



###### Bool Query之Must not

使用示例如下：

```json
# 查询job中包含java但是不包含ruby的文档
GET test_search_index/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "job": {
              "query": "java"
            }
          }
        }
      ],
      "must_not": [
        {
          "match": {
            "job": {
              "query": "ruby"
            }
          }
        }
      ]
    }
  }
}
```





###### Bool Query之Should

Should使用分两种情况：

- bool查询只包含`should`，不包含`must`查询

  - 只包含`should`时，文档必须满足至少一个条件。`minimum_should_match`可以控制满足条件的个数或者百分比

  - 实例使用：

    ```json
    # 查询job字段包含java，或者username字段包含lee的文档
    GET test_search_index/_search
    {
      "query": {
        "bool": {
          "should": [
            {"term": {
              "job": {
                "value": "java"
              }
            }},
            {
              "term": {
                "username": {
                  "value": "lee"
                }
              }
            }
          ]
        }
      }
    }
    
    # 查询job字段包含java，或job字段包含ruby，或job字段包含specialist。但是必须同时满足两项
    GET test_search_index/_search
    {
      "query": {
        "bool": {
          "should": [
            {"term": {
              "job": "java"
            }},
            {
              "term": {
                "job": "ruby"
              }
            },
            {
              "term":{
                "job": "specialist"
              }
            }
          ],
          "minimum_should_match": 2
        }
      }
    }
    ```

- bool查询中同时包含`should`和`must`查询

  - 同时包含`should`和`must`时，文档不必满足`should`中的条件，但是如果满足条件，会增加相关性得分

  - 使用示例：

    ```json
    # 查询username字段必须包含alfred，但字段job可以包含ruby的文档，如果包含则相关性算分将增加
    GET test_search_index/_search
    {
      "query": {
        "bool": {
          "must": [
            {
              "term": {
                "username": {
                  "value": "alfred"
                }
              }
            }
          ],
          "should": [
            {
              "term": {
                "job": {
                  "value": "ruby"
                }
              }
            }
          ]
        }
      }
    }
    ```



##### Query Context和Filter Context的区别

当一个查询语句位于Query或者Filter时，es执行的结果会不同，对比如下：

| 上下文类型 |                          执行类型                          |                      使用方式                      |
| :--------: | :--------------------------------------------------------: | :------------------------------------------------: |
|   Query    | 查询与查询语句最匹配的文档，对所有文档进行相关性算分并排序 |            query；bool中的must和should             |
|   Filter   |                 查询与查询语句相匹配的文档                 | bool中的filter与must not；constant score中的filter |



#### Count API

获取符合条件的文档数，endpoint为`_count`。如下图：

![2018-11-19_count_api](../../pic/es/2018-11-19_count_api.jpg)

```json
GET test_search_index/_count
{
  "query": {
    "match": {
      "username": "lee"
    }
  }
}
```



#### Source Filtering

过滤返回结果中`_source`中的字段，主要包含如下几种方式

![2018-11-19_source_filter](../../pic/es/2018-11-19_source_filter.jpg)

- 使用示例

  ```json
  # source中只返回username
  GET test_search_index/_search?_source=username
  # 禁用source
  GET test_search_index/_search
  {
    "_source": false
  }
  # 通过数组指定返回username和age字段
  GET test_search_index/_search
  {
    "_source": ["username","age"]
  }
  
  # 使用通配符的方式
  GET test_search_index/_search
  {
    "_source": {
      "includes": "*i*",
      "excludes": "birth"
    }
  }
  ```














































