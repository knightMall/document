- [Logstash介绍](#logstash介绍)
- [架构简介](#架构简介)
  - [概念介绍](#概念介绍)
    - [Pipeline](#pipeline)
    - [Logstash Event](#logstash-event)
  - [配置文件](#配置文件)
  - [详细架构](#详细架构)
  - [Queue的分类](#queue的分类)
    - [PQ(Persistent Queue)的基本配置](#pqpersistent-queue的基本配置)
  - [线程简介](#线程简介)
    - [相关配置](#相关配置)
  - [配置简介](#配置简介)
    - [Logstash配置文件](#logstash配置文件)
    - [yamal语法](#yamal语法)
    - [logstash.yml配置项](#logstashyml配置项)
    - [logstash命令行配置项](#logstash命令行配置项)
    - [pipeline配置简介](#pipeline配置简介)
      - [pipeline配置语法](#pipeline配置语法)
    - [Logstash插件详解](#logstash插件详解)
      - [Input插件](#input插件)
        - [stdin](#stdin)
        - [file](#file)
        - [kafka](#kafka)
      - [Codec Plugin](#codec-plugin)
      - [Filter Plugin](#filter-plugin)
        - [Filter Plugin-date](#filter-plugin-date)
        - [Filter Plugin-grok](#filter-plugin-grok)
        - [ Filter Plugin-dissect](#filter-plugin-dissect)
        - [Filter Plugin-mutate](#filter-plugin-mutate)
        - [Filter Plugin-json](#filter-plugin-json)
        - [Filter Plugin-geoip](#filter-plugin-geoip)
        - [Filter Plugin-ruby](#filter-plugin-ruby)
      - [Output Plugin](#output-plugin)
        - [Output Plugin-stdout](#output-plugin-stdout)
        - [Output Plugin-file](#output-plugin-file)
        - [Output Plugin-elasticsearch](#output-plugin-elasticsearch)
      - [文档](#文档)















## Logstash介绍

- 数据收集处理引擎

- ETL工具

  ![2018-12-18_logstash位置](../../pic/es/2018-12-18_logstash位置.jpg)



## 架构简介

主要包含如下三个部分

![2018-12-18_logstash架构简介](../../pic/es/2018-12-18_logstash架构简介.jpg)

![2018-12-18_logstash架构简介详细](../../pic/es/2018-12-18_logstash架构简介详细.jpg)



### 概念介绍

#### Pipeline

- input、filter、output的3 阶段处理流程
- 队列管理
- 插件生命周期管理

#### Logstash Event

- 内部流转的数据表现形式

- 原始数据在input被转换成Event，在output event被转换为目标格式数据

- 在配置文件中可以对Event中的属性进行增删改查

  ![2018-12-18_logstash_event](../../pic/es/2018-12-18_logstash_event.jpg)

  ![2018-12-18_logstash_event整体图解](../../pic/es/2018-12-18_logstash_event整体图解.jpg)

### 配置文件

![2018-12-18_logstash配置文件](../../pic/es/2018-12-18_logstash配置文件.jpg)

- 执行命令

  ```shell
  bin/logstash -f codec.conf
  ```

  ```shell
  echo "foo
  bar"|bin/logstash -f test.conf
  ```

  结果如下：

  ![2018-12-18_logstash示例执行结果](../../pic/es/2018-12-18_logstash示例执行结果.jpg)



### 详细架构

![2018-12-19_logstash6.x架构图](../../pic/es/2018-12-19_logstash6.x架构图.jpg)

**Queue**负责将流入的数据分发到不同的Pipeline中，Batcher负责从Queue中获取数据



### Queue的分类

- In Memory
  - 无法处理进程Crash、机器宕机等情况，会导致数据丢失
- Persistent Queue In Disk
  - 可处理进程Crash等情况，保证数据不丢失
  - 保证数据至少消费一次
  - 充当缓存区，可以替代Kafka等消息队列的作用



#### PQ(Persistent Queue)的基本配置

- queue.type:persisted
  - 默认是memory
- queue.max_bytes:4gb
  - 队列存储最大数据量





### 线程简介

#### 相关配置

- pipeline.workers | w
  - pipeline 线程数，即filter_output的处理线程数，默认是cpu数
- pipeline.batch.size | -b
  - Batcher 一次批量获取的待处理文档数，默认125，可以根据输出进行调整，越大会占用越多的heap空间，可以通过jvm.options调整
- pipeline.batch.delay | -u
  - Batcher 等待的时长，单位为ms



## 配置简介

### Logstash配置文件

- logstash设置相关的配置文件(在conf文件夹中，setting files)
  - logstash.yml logstash相关的配置，比如node.name、path.data、pipeline.workers、queue.type等，这其中的配置可以被命令参数中的相关参数覆盖
  - jvm.options 修改jvm的相关参数，比如修改heap size等
- pipeline配置文件
  - 定义数据处理流程的文件，以`.conf`结尾

### yamal语法

支持两种形式：

- 层级结果

  ![2018-12-20_yaml层级结构](../../pic/es/2018-12-20_yaml层级结构.jpg)

- 偏平结构

  ![2018-12-20_偏平结构](../../pic/es/2018-12-20_偏平结构.jpg)



### logstash.yml配置项

- node.name
  - 节点名，便于识别
- path.data
  - 持久化存储数据的文件夹，默认是logstash home目录下的data
- path.config
  - 设定pipeline配置文件的目录
- path.logs
  - 设定pipeline日志文件的目录
- pipeline.workers
  - 设定pipeline的线程数(filter + output)，优化的常用项
- pipeline.batch.size/delay
  - 设定批量处理数据的数目和延迟
- queue.type
  - 设定队列类型，默认是memory
- queue.max_bytes
  - 队列总容量，默认是1g



### logstash命令行配置项

- `--node.name`
- `-f --path.config` pipeline路径，可以是文件或者文件夹
- `--path.settings` logstash配置文件夹路径，其中要包含logstash.yml
- `-e --config.string` 指明pipeline内容，多用于测试使用
- `-w --pipeline.workers` 
- `-b --pipeline.batch.size`
- `--path.data`
- `--debug`
- `-t --config.test_and_exit`
- `-r` 配置热加载的方式启动logstash



### pipeline配置简介

- 用于配置input、filter和output插件，框架如下所示

  ![2018-12-22_pipeline配置框架](../../pic/es/2018-12-22_pipeline配置框架.jpg)





#### pipeline配置语法

- 主要有如下的数值类型

  - 布尔类型Boolean
    - isFailed=>true
  - 数值类型Number
    - port=>33
  - 字符串类型String
    - name=>"Hello world"
  - 数组Array/List
    - users=>[{id=>1,name=>bobo},{id=>2,name=>jane}]
    - path=>["/var/log/message","/var/log/*.log"]
  - 哈希类型Hash
    - match=>{"field1"=>"value1" "field2"=>"value2"}
  - 注释
    - 井号 `#this is a comment`

- 在配置中可以引用Logstash Event的属性(字段)，主要有如下两种方式

  - 直接引用字段

    ![2018-12-22_引用event的属性](../../pic/es/2018-12-22_引用event的属性.jpg)

  - 在字符串中以`sprintf`方式引用

    ![2018-12-22_sprintf引用event的属性](../../pic/es/2018-12-22_sprintf引用event的属性.jpg)

  - 支持条件判断语法，从而扩展了配置的多样性，格式如下

    ![2018-12-22_pipeline配置条件判断](../../pic/es/2018-12-22_pipeline配置条件判断.jpg)

    - 表达式主要包含如下的操作符：
      - 比较：==、!=、<、>、<=、>=
      - 正则是否匹配：=~、!~
      - 包含(字符串或数组)：in、not in
      - 布尔操作：and、or、nand、xor、!
      - 分组操作符：()

  - 语法示例

    ```
    # 如果属性action等于字符串login
    if[action]=="login"{} 
    # 如果event loglevel等于字符串ERROR且event deployment等于字符串production
    if[loglevel]=="ERROR" and [deployment]=="production"{}
    # 如果event foo在event footbar中
    if[foo] in [foobar]{}
    # 如果event foo在指定的数值中
    if[foo] in ["hello","world","foo"]{}
    ```






## Logstash插件详解

### Input插件

- input插件指定数据输入源，一个pipeline可以有多个input插件，下面介绍如下几个input插件：
  - stdin
  - file
  - kafka

#### stdin

- 最简单的输入，从标准输入读取数据，通用配置为：

  - codec类型为codec
  - type类型为string，自定义该事件的类型，可用于后续判断
  - tags类型为数组，自定义该事件的tag，可用于后续判断
  - add_field类型为hash，为该事件添加字段

- 示例如下：

  ```
  input{
      stdin{
         codec => "plain"
         type => "std"
         tags => ["test"]
         add_field => {"key" => "value"}
       }
  }
  output{
        stdout{
          codec => "rubydebug"
        }
    } 
  ```

  结果：

  ![2018-12-23_input的stdin插件](../../pic/es/2018-12-23_input的stdin插件.jpg)



#### file

- 从文件读取数据，如常见的日志文件。文件读取通常要解决几个问题

  - 文件内容如何只被读取一次？即重启logstash时，从上次读取的位置继续；
    - 通过`sincedb` 方式
  - 如何及时读取文件的新内容
    - 定时检查文件是否有更新
  - 如何发现新文件并进行读取
    - 定时检查新文件
  - 如果文件发生了归档操作，是否影响当前的内容读取
    - 不影响，被归档的文件内容可以继续被读取

- 属性设置

  - path 类型为数组，指明读取的文件路径，基于glob匹配语法
    - path => ["/var/log/**/*.log","/var/log/message"]
  - exclue 类型为数组排除不想监听的文件规则，基于glob匹配语法
    - exclude => "*.gz"
  - sincedb_path 类型为字符串，记录sincedb文件路径
  - start_postion 类型为字符串，beginning or end(默认)，是否从头读取文件
  - stat_interval 类型为数值，单位秒，定时检查文件是否有更新，默认1秒
  - discover_interval 类型为数值，单位秒，定时检查是否有新文件待读取，默认15秒
  - ignore_older 类型为数值，单位秒，扫描文件列表时，如果该文件上次更改时间超过设定的时长，则不做处理，但依然会监控是否有新内容，默认关闭
  - close_older 类型为数值，单位秒，如果监听的文件在超过该设定时间内没有新内容，会被关闭文件句柄，释放资源，但依然会监控是否有新内容，默认3600秒，即一个小时

- glob匹配语法

  - `*` 匹配任意字符，但不以`.` 开头的隐藏文件，匹配这类文件是要使用`.*`来匹配
  - `**` 递归匹配子目录
  - `?` 匹配单一字符
  - `[]` 匹配多个字符，比如`[a-z]、[^a-z]`
  - `{}` 匹配多个单词，比如{foo,bar,hello}
  - `\` 转义符号
  - glob匹配语法示例
    - `/var/log/*.log`
      - 匹配 /var/log 目录下以.log结尾的文件
    - `/var/log/**/*.log`
      - 匹配/var/log所有子目录下以.log结尾的文件
    - `/var/log/{app1,app2,app3}/*.log`
      - 匹配/var/log目录下app1、app2、app3目录中以.log结尾的文件

- 示例

  ```
  input{
     file{
      path=>"/home/first/opt/logstash-6.5.1/imooc/data/*.log"
      sincedb_path=>"/dev/null"
      start_position=>"beginning"
    }
  }
  output{
      stdout{codec=>"rubydebug"}
  }
  ```

​      文件地址：

​     [下载](../../pic/es/static.access.log)

#### kafka

- kafka是最流行的消息队列，也是Elastic Stack 架构中常用的，使用相对简单

  ```
  input{
      kafka{
          zk_connect => "kafka:2181"
          group_id => "logstash"
          topic_id => "apache_logs"
          consumer_threads => 16
      }
  }
  ```





### Codec Plugin

- Codec Plugin 作用于input和output plugin，负责将数据在原始与Logstash Event之间转换，常见的codec有：

  - plain 读取原始内容

  - dots 将内容简化为点进行输出

  - rubydebug 将Logstash Events 按照ruby格式输出，方便调试

  - line 处理带有换行符的内容

  - json 处理json格式的内容

  - multiline 处理多行数据的内容

    - 常用来堆栈日志信息的处理

    - 主要设置参数如下：

      - pattern 设置行匹配的正则表达式，可以使用grok

      - what previous|next，如果匹配成功，那么匹配行是归属上一个事件还是下一个事件

      - negate true or false 是否对pattern的结果取反

        ```
        input{
           stdin{
             codec=>multiline{
               pattern=>"^\s"
               what=>"previous"
             }
           }
        }
        output{
           stdout{
               codec=>"rubydebug"
            }
        }
        ```

- 使用示例

  ```shell
  bin/logstash -e "input{stdin{codec=>line}}output{stdout{codec=>dots}}"
  bin/logstash -e "input{stdin{codec=>line}}output{stdout{codec=rubydebug}}"
  bin/logstash -e "input{stdin{codec=>json}}output{stdout{codec=>rubydebug}}"
  ```




### Filter Plugin

- Filter是logstash功能强大的主要原因，它可以对Logstash Event进行丰富的处理，比如解析数据、删除字段、类型转换等等，常见的有如下几个：
  - date 日期解析
  - grok 正则匹配解析
  - dissect 分割符解析
  - mutate 对字段作处理，比如重命名、删除、替换等
  - json 按照json解析字段内容到指定字段中
  - geoip 增加地理位置数据
  - ruby 利用ruby代码来动态修改Logstash Event



#### Filter Plugin-date

- date Filter的参数：
  - match
    - 类型为数组，用于指定日期匹配的格式，可以一次指定多种日期格式
    - `match=>["logdate":"MMM dd yyyy HH:mm:ss","yyyy-MM-dd HH:mm:ss"]`
  - target
    - 类型为字符串，用于指定赋值的字段名，默认是`@timestamp`
  - timezone
    - 类型为字符串，用于指定时区

- 使用示例，conf文件：

  ```json
  input{
     stdin{
        codec=>json
     }
  }
  filter{
      date{
         match=>["logdate","yyyy-MM-dd HH:mm:ss"]
     }
  }
  output{
      stdout{
         codec=>rubydebug
     }
  }
  ```



#### Filter Plugin-grok

- grok语法

  - `%{SYNTAX:SEMANTIC}`
  - SYNTAX为grok pattern的名称，SEMANTIC为赋值字段名称
    - 使用示例：`%{NUMBER:duration}` 可以配置数值类型，但是grok匹配出的内容都是字符串类型，可以通过在最后指定为int或者float来强制转换类型。`%{NUMBER:duration:float}`
  - github上有一些常用的Pattern。[常用Pattern](https://github.com/logstash-plugins/logstash-patterns-core/tree/master/patterns)

- 使用示例

  通过热加载的方式启动logstash，加`-r`

  ```shell
  bin/logstash -f imooc/filter-grok.conf -r
  ```

  conf文件

  ```
  # Starting http input listener
  input{
     http{port=>7474}
  }
  # 在GitHub上引用的Pattern
  filter{
      grok{
        match=>{
                "message"=>"%{IPORHOST:clientip} %{USER:ident} %{USER:auth} \[%{HTTPDATE:timestamp}\] \"%{WORD:verb} %{DATA:reques
  t} HTTP/%{NUMBER:httpversion}\" %{NUMBER:response:int} (?:-|%{NUMBER:bytes:int}) %{QS:referrer} %{QS:agent}"
                }
      }
      #移除headers属性
      mutate{
        remove_field=>"headers"
     }
  }
  
  output{
      stdout{codec=>rubydebug}
  }
  ```

  通过postman请求该地址：

  ![2018-12-23_请求grok方式](../../pic/es/2018-12-23_请求grok方式.jpg)

  解析结果：

  ![2018-12-23_grok解析nginx日志](../../pic/es/2018-12-23_grok解析nginx日志.jpg)

- 基于正则自定义匹配规则

  - `(?<service_name>[0-9a-z](5,11))` 这个正则表示的意思是匹配0到9或a到z的长度为5到11的字符串，替换名称是service_name

  - conf文件如下

    ```
    input{
       http{port=>7474}
    }
    
    filter{
        grok{
          match=>{"message"=>"(?<service_name>[0-9a-z]{5,11})"}
       }
        mutate{
            remove_field=>"headers"
        }
    }
    
    output{
        stdout{codec=>rubydebug}
    }
    
    ```

    还是使用上面的方式请求，解析结果是：

    ![2018-12-23_grok自定义匹配规则](../../pic/es/2018-12-23_grok自定义匹配规则.jpg)

  - 自定义grok pattern

    - pattern_definitions参数，以键值对的方式定义pattern名称和内容

    - pattern_dir 参数，以文件的形式被读取

    - 使用示例

      ![2018-12-26_定义grok-pattern](../../pic/es/2018-12-26_定义grok-pattern.jpg)

      ```
      input{
         http{port=>7474}
      }
      
      filter{
          grok{
            match=>{"message"=>"%{SERVICE:service}"}
            pattern_definitions => {"SERVICE" => "[a-z0-9]{5,11}"}
         }
          mutate{
              remove_field=>"headers"
          }
      }
      
      output{
          stdout{codec=>rubydebug}
      }
      
      ```

      解析结果：

      ![2018-12-26_定义grok-pattern结果](../../pic/es/2018-12-26_定义grok-pattern结果.jpg)

  - match 配置多种样式

    ```
    match=>{"message"=>"%{SERVICE:service} %{NUMBER:message}"}
    ```

    ![2018-12-26_定义grok-match配置多种样式](../../pic/es/2018-12-26_定义grok-match配置多种样式.jpg)

  - overwrite，配置多种样式时，出现匹配的值追加的方式。就如上图所示，message字段是一个数组。这个属性可以解决上述问题，改为覆盖的方式

    ```
    input{
       http{port=>7474}
    }
    
    filter{
        grok{
          match=>{"message"=>"%{SERVICE:service} %{NUMBER:message}"}
          overwrite=>["message"]
          pattern_definitions => {"SERVICE" => "[a-z0-9]{5,11}"}
       }
        mutate{
            remove_field=>"headers"
        }
    }
    
    output{
        stdout{codec=>rubydebug}
    }
    
    ```

    执行结果：

    ![2018-12-26_定义grok-overwrite结果](../../pic/es/2018-12-26_定义grok-overwrite结果.jpg)

  - tag_on_failure

    - 和对应的正则不匹配的标记。默认是`_grokparsefailure`，可以基于此做判断

      ![2018-12-26_定义grok-tag_on_failure](../../pic/es/2018-12-26_定义grok-tag_on_failure.jpg)

  - grok调试建议

    - 正则表达式

      - `https://www.debuggex.com/`
      - `https://regexr.com/`

    - grok调试

      - `http://grok.elasticsearch.cn/`

      - `http://grokdebug.herokuapp.com/`

      - 使用如下：

        ![2018-12-26_grok-debug网站](../../pic/es/2018-12-26_grok-debug网站.jpg)



#### Filter Plugin-dissect

- 基于分割符原理解析数据，解决grok解析时消耗过多CPU的问题

- dissect的应用有一定的局限性

  - 主要适用于每行格式相似且分割符明确简单的场景

- dissect语法比较简单，由一系列字段和分隔符组成

  - `%{}` 字段

  - `%{}` 之间是分隔符

  - 语法示例

    ![2018-12-27_filter插件dissect](../../pic/es/2018-12-27_filter插件dissect.jpg)

  - 图解语法示例

    ![2018-12-27_filter插件dissect语法图解](../../pic/es/2018-12-27_filter插件dissect语法图解.jpg)

    ![2018-12-27_filter插件dissect语法图解1](../../pic/es/2018-12-27_filter插件dissect语法图解1.jpg)

    ![2018-12-27_filter插件dissect语法图解2](../../pic/es/2018-12-27_filter插件dissect语法图解2.jpg)

- 使用示例：

  ```
  input{
    http{port=>7474}
  }
  
  filter{
    dissect{
        mapping=>{"message"=>"%{ts} %{+ts} %{+ts} %{src} %{prog}[%{pid}]: %{msg}"}
    }
  }
  
  output{
    stdout{codec=>rubydebug}
  }
  ```

  还是通过Postman进行请求，请求数据如下：

  ```
  Apr 26 12:20:02 localhost systemd[1]: Starting system activity accounting tool
  ```

  ![2018-12-27_filter插件dissect使用示例](../../pic/es/2018-12-27_filter插件dissect使用示例.jpg)

  演示排序，conf文件

  ```
  input{
    http{port=>7474}
  }
  
  filter{
    dissect{
         mapping=>{"message"=>"%{+order/2} %{+order/3} %{+order/1} %{+order/4}"}
    }
     mutate{
          remove_field=>"headers"
      }
  
  }
  
  output{
    stdout{codec=>rubydebug}
  }
  
  ```

  还是通过Postman请求，请求数据是：`two three one go`。响应结果：

  ![2018-12-27_filter插件dissect排序功能](../../pic/es/2018-12-27_filter插件dissect排序功能.jpg)

  切分请求参数

  ![2018-12-27_filter插件dissect切分请求参数](../../pic/es/2018-12-27_filter插件dissect切分请求参数.jpg)

  conf文件：

  ```
  input{
    http{port=>7474}
  }
  
  filter{
    dissect{
        mapping=>{"message"=>"%{?key1}=%{&key1}&%{?key2}=%{&key2}"}
    }
     mutate{
          remove_field=>"headers"
      }
  }
  output{
    stdout{codec=>rubydebug}
  }
  
  ```

  请求方式：

  ![2018-12-27_filter插件dissect切分请求参数请求](../../pic/es/2018-12-27_filter插件dissect切分请求参数请求.jpg)

  解析结果

  ![](../../pic/es/2018-12-27_filter插件dissect切分请求参数结果.jpg)



#### Filter Plugin-mutate

- 使用最频繁的操作，可以对字段进行各种操作，比如重命名、删除、替换、更新等，主要操作如下：
  - convert 类型转换
  - gsub字符串替换
  - split\join\merge 字符串切割、数组合并为字符串、数组合并为数组
  - rename 字段重命名
  - update/replace 字段内容更新或替换
  - remove_field 删除字段

- convert

  - 实现字段类型的转换，类型为hash，仅支持转换为integer、float、string和boolean

  - 具体使用如下：

    ```
    input{
       http{port=>7474}
    }
    
    filter{
       mutate{
         convert=>{"message"=>"integer"}
       }
       mutate{
           remove_field=>"headers"
      }
    }
    
    output{
       stdout{codec=>rubydebug}
    }
    ```

    结果：

    ![2018-12-27_filter插件mutate-convert](../../pic/es/2018-12-27_filter插件mutate-convert.jpg)

- gsub

  - 对字段内容进行替换，类型为数组，每3项为一个替换配置

  - 使用示例

    ```
    input{
       http{port=>7474}
    }
    
    filter{
       mutate{
         # 将/替换为_
         gsub=>["message","/","_"]
       }
       mutate{
           remove_field=>"headers"
      }
    }
    
    output{
       stdout{codec=>rubydebug}
    }
    
    ```

    ![](../../pic/es/2018-12-27_filter插件mutate-gusb请求参数.jpg)

    ![2018-12-27_filter插件mutate-gusb](../../pic/es/2018-12-27_filter插件mutate-gusb.jpg)

- split

  - 将字符串切割为数组

  - 使用示例

    ```
    input{
       http{port=>7474}
    }
    
    filter{
       mutate{
        #convert=>{"message"=>"integer"}
        #gsub=>["message","/","_"]
         split=>{"message"=>","}
       }
       mutate{
           remove_field=>"headers"
      }
    }
    
    output{
       stdout{codec=>rubydebug}
    }
    ```

    请求参数

    ![2018-12-27_filter插件mutate-split请求参数](../../pic/es/2018-12-27_filter插件mutate-split请求参数.jpg)

    结果如下：

    ![2018-12-27_filter插件mutate-split结果](../../pic/es/2018-12-27_filter插件mutate-split结果.jpg)



- join

  - 将数组拼接为字符串

  - 使用示例

    ```
    input{
       http{port=>7474}
    }
    
    filter{
       mutate{
          split=>{"message"=>"_"}
          join=>{"message" => ","}
       }
       mutate{
           remove_field=>"headers"
      }
    }
    output{
       stdout{codec=>rubydebug}
    }
    ```

    请求参数

    ![2018-12-27_filter插件mutate-join请求](../../pic/es/2018-12-27_filter插件mutate-join请求.jpg)

    结果

    ![2018-12-27_filter插件mutate-join结果](../../pic/es/2018-12-27_filter插件mutate-join结果.jpg)

- merge

  - 将两个数组合并为1个数组，字符串会被转为1个元素的数组进行操作

  - 使用示例

    ```
    input{
       http{port=>7474}
    }
    
    filter{
       dissect{
          mapping=>{"message"=>"%{a} - %{b} - %{c}"}
       }
       mutate{
           # 将a的值合并到b上
           merge=>{"b"=>"a"}
       }
       mutate{
           remove_field=>"headers"
      }
    }
    
    output{
       stdout{codec=>rubydebug}
    }
    ```

    请求参数

    ![2018-12-27_filter插件mutate-merge请求](../../pic/es/2018-12-27_filter插件mutate-merge请求.jpg)

    结果

    ![2018-12-27_filter插件mutate-merge结果](../../pic/es/2018-12-27_filter插件mutate-merge结果.jpg)





- rename

  - 将字段重命名

  - 使用示例

    ```
    input{
       http{port=>7474}
    }
    
    filter{
       dissect{
          mapping=>{"message"=>"%{a} - %{b} - %{c}"}
       }
       mutate{
           merge=>{"b"=>"a"}
       }
       mutate{
          # 将d替换为b
           rename=>{"b"=>"d"}
       }
       mutate{
           remove_field=>"headers"
      }
    }
    
    output{
       stdout{codec=>rubydebug}
    }
    ```

    请求参数

    ![2018-12-27_filter插件mutate-rename请求](../../pic/es/2018-12-27_filter插件mutate-rename请求.jpg)

    结果

    ![2018-12-27_filter插件mutate-rename结果](../../pic/es/2018-12-27_filter插件mutate-rename结果.jpg)

- update/replace

  - 更新字段内容，区别在于update只在字段存在时生效，而replace在字段不存在时会执行新增字段的操作

  - 使用示例：

    conf文件

    ```
    input{
       http{port=>7474}
    }
    
    filter{
       dissect{
          mapping=>{"message"=>"%{a} - %{b} - %{c}"}
       }
       mutate{
           update=>{"a"=>"source from c:%{c}"}
       }
       mutate{
           remove_field=>"headers"
      }
    }
    
    output{
       stdout{codec=>rubydebug}
    }
    ```

    请求数据

    ![2018-12-27_filter插件mutate-update请求](../../pic/es/2018-12-27_filter插件mutate-update请求.jpg)

    结果

    ![2018-12-27_filter插件mutate-update结果](../../pic/es/2018-12-27_filter插件mutate-update结果.jpg)

    replace的conf

    ```
    input{
       http{port=>7474}
    }
    
    filter{
       dissect{
          mapping=>{"message"=>"%{a} - %{b} - %{c}"}
       }
       mutate{
           replace=>{"e"=>"source from c:%{c}"}
       }
       mutate{
           remove_field=>"headers"
      }
    }
    
    output{
       stdout{codec=>rubydebug}
    }
    ```

    请求数据和上面一样

    结果：

    ![2018-12-27_filter插件mutate-replace结果](../../pic/es/2018-12-27_filter插件mutate-replace结果.jpg)





#### Filter Plugin-json

- 将字段内容为json的格式的数据进行解析

    ![](../../pic/es/2018-12-27_filter插件json语法.jpg)

- 使用示例

  conf文件

  ```
  input{
     http{port=>7474}
  }
  
  filter{
     json{
         source=>"message"
         target=>"msg_json"
     }
     mutate{
         remove_field=>"headers"
    }
  }
  
  output{
     stdout{codec=>rubydebug}
  }
  ```

  请求参数

  ![2018-12-27_filter插件json请求参数](../../pic/es/2018-12-27_filter插件json请求参数.jpg)

  结果

  ![2018-12-27_filter插件json结果](../../pic/es/2018-12-27_filter插件json结果.jpg)



#### Filter Plugin-geoip

- 常用的插件，根据ip地址提供对应的地域信息，比如经纬度、城市名等，方便进行地理数据分析

  ![2018-12-27_filter插件geoip](../../pic/es/2018-12-27_filter插件geoip.jpg)

- 使用示例

  conf文件

  ```json
  input{
     http{port=>7474}
  }
  
  filter{
     geoip{
         source=>"message"
     }
     mutate{
         remove_field=>"headers"
    }
  }
  
  output{
     stdout{codec=>rubydebug}
  }
  ```

  请求

  ![2018-12-27_filter插件geoip请求](../../pic/es/2018-12-27_filter插件geoip请求.jpg)

  结果如下

  ![2018-12-27_filter插件geoip结果](../../pic/es/2018-12-27_filter插件geoip结果.jpg)





#### Filter Plugin-ruby

- 最灵活的插件，可以以ruby语言来随心所欲的修改Logstash Event对象

  ![2018-12-27_filter插件ruby语法](../../pic/es/2018-12-27_filter插件ruby语法.jpg)

- 使用示例

  conf文件

  ```
  input{
     http{port=>7474}
  }
  
  filter{
     ruby{
       code=>'size=event.get("message").size;event.set("message_size",size)'
     }
     mutate{
         remove_field=>"headers"
    }
  }
  
  output{
     stdout{codec=>rubydebug}
  }
  ```

  请求

  ![2018-12-27_filter插件ruby请求](../../pic/es/2018-12-27_filter插件ruby请求.jpg)

  结果

  ![](../../pic/es/2018-12-27_filter插件ruby结果.jpg)





### Output Plugin

- 负责将Logstash Event输出，常见的插件如下：
  - stdout
  - file
  - elasticsearch



#### Output Plugin-stdout

- 输出到标准输出，多用于调试

  ![2018-12-27_output-plugin之stdout](../../pic/es/2018-12-27_output-plugin之stdout.jpg)



#### Output Plugin-file

- 输出到文件，实现将分散在多地的文件统一到一处的需求，比如讲所有web机器的web日志收集到1个文件中，从而方便查阅信息

  ![2018-12-27_output-plugin之file](../../pic/es/2018-12-27_output-plugin之file.jpg)

  path指定存储位置

- 使用示例

  conf文件

  ```
  input{
     http{port=>7474}
  }
  
  filter{
     mutate{
         remove_field=>"headers"
    }
  }
  
  output{
     file{
        path=>"/home/first/opt/logstash-6.5.1/imooc/data/web.log"
        codec=>line{format=>"%{message}"}
     }
  }
  ```

  请求

  ![](../../pic/es/2018-12-27_output-plugin之file请求.jpg)

  结果

  ![2018-12-27_output-plugin之file结果](../../pic/es/2018-12-27_output-plugin之file结果.jpg)



#### Output Plugin-elasticsearch

- 输出到elasticsearch，是最常用的常用，基于HTTP协议实现

  ![2018-12-27_output-plugin之elasticsearch语法](../../pic/es/2018-12-27_output-plugin之elasticsearch语法.jpg)



### 文档

- [input插件](https://www.elastic.co/guide/en/logstash/current/input-plugins.html)
- [output插件](https://www.elastic.co/guide/en/logstash/current/output-plugins.html)
- [filter插件](https://www.elastic.co/guide/en/logstash/current/filter-plugins.html)
- [codec插件](https://www.elastic.co/guide/en/logstash/current/codec-plugins.html)



## 实战建议

- 调试阶段建议大家使用如下配置
  - http作为input，方便输入测试数据，并且可以结合reload特性(stdin无法reload)

  - stdout作为output，codec使用rubydebug，即时查看解析结果

  - 测试错误输入情况下的输出，以便对错误情况进行处理

    ```
    input{
      http{
         port=>7474
      }
    }
    filter{}
    output{
       stdout{
         codec=>rubydebug
       }
    }
    ```

- `@metadata` 特殊字段，其内容不会输出在output中

  - 适合用来存储做条件判断、临时存储的字段

  - 相比`remove_field`有一定的性能提升，使用示例如下：

    ```
    # 添加字段[@metadata][debug]，如果为true则按照rubydebug输出，否则输出符号.
    # {metadata=>true} 则表示在输出结果输出metadata的值
    input{
     http{
       port=>7474
     }
    }
     filter{
          mutate{
                add_field=> {"[@metadata][debug]"=>true}
                add_field=> {"show"=>"This data will be in the output"}
               }
        mutate{
          remove_field=>"headers"
       }
      }
    output{
      if[@metadata][debug]{
         stdout{codec=>rubydebug
                {metadata=>true}
               }
      }else{
        stdout{codec=>dots}
      }
    }
    ```

    结果如下：

    ![metadata使用结果](../../pic/es/metadata使用结果.jpg)






















































































