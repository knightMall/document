- [Search的运行机制](#search的运行机制)
  - [Search的运行机制-Query阶段](#search的运行机制-query阶段)
  - [Search的运行机制-Fetch阶段](#search的运行机制-fetch阶段)
  - [Search的运行机制-相关性算分问题](#search的运行机制-相关性算分问题)
    - [解决相关性算分问题](#解决相关性算分问题)
- [ES中的排序](#es中的排序)
  - [按字符串排序](#按字符串排序)
  - [ES排序过程](#es排序过程)
    - [Fielddata](#fielddata)
    - [docvalue_fields查询属性](#docvalue_fields查询属性)
- [分页与遍历](#分页与遍历)
  - [From Size](#from-size)
    - [深度分页问题](#深度分页问题)
  - [Scroll分页](#scroll分页)
  - [Search After](#search-after)
    - [Search After如何避免深度分页](#search-after如何避免深度分页)
  - [分页类型的应用场景](#分页类型的应用场景)
- [ES中Search文档说明](#es中search文档说明)





## Search的运行机制

- Search执行的时候实际分两个步骤运行
  - Query 阶段
  - Fetch 阶段
  - 在ES中上述两个步骤称为`Query-Then-Fetch`



###  Search的运行机制-Query阶段

- 假设node3接收到用户查询请求，在node3接收到用户的search请求后，会先进行Query阶段(此时node3是Coordinating Node角色)

- node3在6个主副分片中随机选择3个分片，发送search_request

- 被选中的3个分片会分别执行查询并排序，返回from+size个文档Id和排序值。例如：from为10，size为10 。则每个分片将返回20个文档。node3将拿到20*(3个分片)，60个文档。为什么需要从每个分片获取20个文档，因为需要查询的20个文档并不知道分布在哪一个分片上，只能从每个分片都拿20个然后从这60个并排序，从排好序的60个文档获取从10到20的文档

- node3整合3个分片返回的from+size个文档Id，根据排序值排序后选取from到from+size的文档Id

  ![2018-11-22_Search运行机制-query阶段](../../pic/es/2018-11-22_Search运行机制-query阶段.jpg)



### Search的运行机制-Fetch阶段

- node3根据Query阶段获取的文档Id列表去对应的Shard上获取文档详情数据

  - node3向相关的分片发送`multi_get`请求

  - 3个分片返回文档详细数据

  - node3拼接返回的结果并返回给客户

    ![2018-11-22_Search运行机制-fetch阶段](../../pic/es/2018-11-22_Search运行机制-fetch阶段.jpg)

### Search的运行机制-相关性算分问题

- 相关性算分在shard与shard间是相互独立的，也就意味着同一个Term的IDF等值在不同shard上是不同的。文档的相关性算分和它所处的shard有关

- 上面的特性导致，在文档数量不多时，会导致相关性算分严重不准确的情况发生

- 具体如下的例子：

  ```json
  DELETE test_search_relevance
  
  # 创建索引默认是5个分片
  POST test_search_relevance/doc
  {
    "name":"hello"
  }
  
  POST test_search_relevance/doc
  {
    "name":"hello,word"
  }
  
  
  POST test_search_relevance/doc
  {
    "name":"hello,word!a beautiful word"
  }
  
  
  GET test_search_relevance/_search
  {
    "explain": false, 
    "query": {
      "match": {
        "name": "hello"
      }
    }
  }
  ```

  查询结果是：

  ```json
  {
    "took": 27,
    "timed_out": false,
    "_shards": {
      "total": 5,
      "successful": 5,
      "skipped": 0,
      "failed": 0
    },
    "hits": {
      "total": 3,
      "max_score": 0.2876821,
      "hits": [
        {
          "_index": "test_search_relevance",
          "_type": "doc",
          "_id": "rnlRO2cBbGxOoKYKMcfS",
          "_score": 0.2876821,
          "_source": {
            "name": "hello,word"
          }
        },
        {
          "_index": "test_search_relevance",
          "_type": "doc",
          "_id": "sHlRO2cBbGxOoKYKRceA",
          "_score": 0.2876821,
          "_source": {
            "name": "hello,word!a beautiful word"
          }
        },
        {
          "_index": "test_search_relevance",
          "_type": "doc",
          "_id": "r3lRO2cBbGxOoKYKO8cn",
          "_score": 0.2876821,
          "_source": {
            "name": "hello"
          }
        }
      ]
    }
  }
  ```

  每个分数相同，可以将`"explain": true`，显示算分过程。如果将shard数设置为1时，在添加上面的数据，如下：

  ```json
  PUT test_search_relevance
  {
    "settings": {
     "index":{
       "number_of_shards":1
     }
    }
  }
  POST test_search_relevance/doc
  {
    "name":"hello"
  }
  
  
  POST test_search_relevance/doc
  {
    "name":"hello,word"
  }
  
  
  POST test_search_relevance/doc
  {
    "name":"hello,word!a beautiful word"
  }
  
  GET test_search_relevance/_search
  {
    "explain": false, 
    "query": {
      "match": {
        "name": "hello"
      }
    }
  }
  ```

  查询结果：

  ```json
  {
    "took": 2,
    "timed_out": false,
    "_shards": {
      "total": 1,
      "successful": 1,
      "skipped": 0,
      "failed": 0
    },
    "hits": {
      "total": 3,
      "max_score": 0.17940095,
      "hits": [
        {
          "_index": "test_search_relevance",
          "_type": "doc",
          "_id": "unlUO2cBbGxOoKYK-siz",
          "_score": 0.17940095,
          "_source": {
            "name": "hello"
          }
        },
        {
          "_index": "test_search_relevance",
          "_type": "doc",
          "_id": "xXlVO2cBbGxOoKYKAcg3",
          "_score": 0.14874382,
          "_source": {
            "name": "hello,word"
          }
        },
        {
          "_index": "test_search_relevance",
          "_type": "doc",
          "_id": "xnlVO2cBbGxOoKYKB8gH",
          "_score": 0.09833273,
          "_source": {
            "name": "hello,word!a beautiful word"
          }
        }
      ]
    }
  }
  ```

  按照我们期望的结果显示



#### 解决相关性算分问题

- 解决思路有两个：

  - 方案一是设置分片数为1，从根本上排除问题，在文档数量不多的时候可以考虑该方案，比如百万到千万级别的文档数量

  - 方案二是使用DFS Query-then-Fetch查询方式

    ```json
    GET test_search_relevance/_search?search_type=dfs_query_then_fetch
    {
      "explain": false, 
      "query": {
        "match": {
          "name": "hello"
        }
      }
    }
    ```

    响应结果如下：

    ```json
    {
      "took": 172,
      "timed_out": false,
      "_shards": {
        "total": 5,
        "successful": 5,
        "skipped": 0,
        "failed": 0
      },
      "hits": {
        "total": 3,
        "max_score": 0.17940095,
        "hits": [
          {
            "_index": "test_search_relevance",
            "_type": "doc",
            "_id": "unlYO2cBbGxOoKYKeskS",
            "_score": 0.17940095,
            "_source": {
              "name": "hello"
            }
          },
          {
            "_index": "test_search_relevance",
            "_type": "doc",
            "_id": "xXlYO2cBbGxOoKYKgMmo",
            "_score": 0.14874382,
            "_source": {
              "name": "hello,word"
            }
          },
          {
            "_index": "test_search_relevance",
            "_type": "doc",
            "_id": "xnlYO2cBbGxOoKYKhskg",
            "_score": 0.09833273,
            "_source": {
              "name": "hello,word!a beautiful word"
            }
          }
        ]
      }
    }
    ```



## ES中的排序

- ES默认会采用相关性算分排序，用户可以通过设定`sort`参数来自行设定排序规则。使用的查询数据源还是介绍SearchAPI创建的文档

  ```json
  GET test_search_index/_search
  {
    "sort": [
      {
        "birth": {
          "order": "desc"
        }
      }
    ]
  }
  
  # 响应结果 _score字段为null，并新加sort字段
  {
    "took": 14,
    "timed_out": false,
    "_shards": {
      "total": 1,
      "successful": 1,
      "skipped": 0,
      "failed": 0
    },
    "hits": {
      "total": 5,
      "max_score": null,
      "hits": [
        {
          "_index": "test_search_index",
          "_type": "doc",
          "_id": "1",
          "_score": null,
          "_source": {
            "username": "alfred way",
            "job": "java engineer",
            "age": 18,
            "birth": "1990-01-02",
            "isMarried": false
          },
          "sort": [
            631238400000
          ]
        },
        {
          "_index": "test_search_index",
          "_type": "doc",
          "_id": "4",
          "_score": null,
          "_source": {
            "username": "alfred junior way",
            "job": "ruby engineer",
            "age": 23,
            "birth": "1989-08-07",
            "isMarried": false
          },
          "sort": [
            618451200000
          ]
        },
        {
          "_index": "test_search_index",
          "_type": "doc",
          "_id": "3",
          "_score": null,
          "_source": {
            "username": "lee",
            "job": "java and ruby engineer",
            "age": 22,
            "birth": "1985-08-07",
            "isMarried": false
          },
          "sort": [
            492220800000
          ]
        },
        {
          "_index": "test_search_index",
          "_type": "doc",
          "_id": "2",
          "_score": null,
          "_source": {
            "username": "alfred",
            "job": "java senior engineer and java specialist",
            "age": 28,
            "birth": "1980-05-07",
            "isMarried": true
          },
          "sort": [
            326505600000
          ]
        },
        {
          "_index": "test_search_index",
          "_type": "doc",
          "_id": "5",
          "_score": null,
          "_source": {
            "username": "lee way",
            "job": "java engineer and java specialist",
            "age": 30,
            "birth": "1978-04-07",
            "isMarried": true
          },
          "sort": [
            260755200000
          ]
        }
      ]
    }
  }
  ```

  指定多个字段：

  ```json
  GET test_search_index/_search
  {
    "sort": [
      {
        "birth": {
          "order": "desc"
        }
      },
      {
        "_score":"desc"
      },
      {
        "_doc":"desc"
      }
    ]
  }
  ```



### 按字符串排序

- 按字符串排序比较特殊，因为ES有text和keyword两种类型。text类型排序如下：

  ```json
  GET test_search_index/_search
  {
    "sort": [
      {
        "username": {
          "order": "desc"
        }
      }
    ]
  }
  
  # 响应结果，text类型不能直接用来排序、必须设置fielddata=true或使用keyword字段排序
  "root_cause": [
        {
          "type": "illegal_argument_exception",
          "reason": "Fielddata is disabled on text fields by default. Set fielddata=true on [username] in order to load fielddata in memory by uninverting the inverted index. Note that this can however use significant memory. Alternatively use a keyword field instead."
        }
      ]
  
  
  # 按照keyword进行排序
  GET test_search_index/_search
  {
    "sort": [
      {
        "username.keyword": {
          "order": "desc"
        }
      }
    ]
  }
  # 可以返回预期的结果
  {
    "took": 31,
    "timed_out": false,
    "_shards": {
      "total": 1,
      "successful": 1,
      "skipped": 0,
      "failed": 0
    },
    "hits": {
      "total": 5,
      "max_score": null,
      "hits": [
        {
          "_index": "test_search_index",
          "_type": "doc",
          "_id": "5",
          "_score": null,
          "_source": {
            "username": "lee way",
            "job": "java engineer and java specialist",
            "age": 30,
            "birth": "1978-04-07",
            "isMarried": true
          },
          "sort": [
            "lee way"
          ]
        },
  ```



### ES排序过程

- 排序的过程实质是对字段原始内容排序的过程，这个过程中倒排索引无法发挥作用，需要用到正排索引。

- es对此提供了2种实现方式

  - fielddata默认禁用

  - doc values默认启用，除了text类型

  - 对比fielddata和doc values

    ![2018-11-22_fielddata_DocValues](../../pic/es/2018-11-22_fielddata_DocValues.jpg)

#### Fielddata

- Fielddata默认是关闭的，可以通过如下api开启

  - 此时字符串是按照分词后的term排序，往往结果很难符合预期
  - 一般是在对分词做聚合分析的时候开启
  - 只有`text`类型支持fielddata

  ![2018-11-23_fielddata](../../pic/es/2018-11-23_fielddata.jpg)

```json
# 开启fielddata属性为true
PUT test_search_index/_mapping/doc
{
  "properties": {
    "username":{
      "type": "text",
      "fielddata": true
    }
  }
}

# 再执行通过username排序
GET test_search_index/_search
{
  "sort": [
    {
      "username": {
        "order": "desc"
      }
    }
  ]
}

# 部分响应结果
{
  "took": 95,
  "timed_out": false,
  "_shards": {
    "total": 1,
    "successful": 1,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": 5,
    "max_score": null,
    "hits": [
      {
        "_index": "test_search_index",
        "_type": "doc",
        "_id": "1",
        "_score": null,
        "_source": {
          "username": "alfred way",
          "job": "java engineer",
          "age": 18,
          "birth": "1990-01-02",
          "isMarried": false
        },
        "sort": [
          "way"
        ]
      },
      {
        "_index": "test_search_index",
        "_type": "doc",
        "_id": "4",
        "_score": null,
        "_source": {
          "username": "alfred junior way",
          "job": "ruby engineer",
          "age": 23,
          "birth": "1989-08-07",
          "isMarried": false
        },
        "sort": [
          "way"
        ]
      },
      {
        "_index": "test_search_index",
        "_type": "doc",
        "_id": "5",
        "_score": null,
        "_source": {
          "username": "lee way",
          "job": "java engineer and java specialist",
          "age": 30,
          "birth": "1978-04-07",
          "isMarried": true
        },
        "sort": [
          "way"
        ]
      }


# 关闭fielddata
PUT test_search_index/_mapping/doc
{
  "properties": {
    "username":{
      "type": "text",
      "fielddata": false
    }
  }
}
```



#### Doc Values

- Doc Values默认是启用的，可以在创建索引的时候关闭

  - 如果后面要再开启doc values，需要做reindex操作

  ![2018-11-23_doc-values](../../pic/es/2018-11-23_doc-values.jpg)



#### docvalue_fields查询属性

- 可以通过该字段获取fielddata或者doc values中存储的内容

  ```json
  # 开启fielddata
  PUT test_search_index/_mapping/doc
  {
    "properties": {
      "username":{
        "type": "text",
        "fielddata": true
      }
    }
  }
  
  # 获取doc values或fielddata
  GET test_search_index/_search
  {
    "docvalue_fields": [
      "username", 
      "username.keyword",
      "age"
      ]
  }
  ```

- doc value设置后不能修改

  ```json
  # 设置doc_values为false
  PUT test_doc_values
  {
    "mappings": {
      "doc":{
        "properties":{
          "username":{
            "type":"keyword",
            "doc_values":false
          },
          "hobby":{
            "type":"keyword"
          }
        }
      }
    }
  }
  
  # 使用username字段排序
  GET test_doc_values/_search
  {
    "sort": [
      {
        "username": {
          "order": "desc"
        }
      }
    ]
  }
  # 出现错误信息，部分结果
  "root_cause": [
        {
          "type": "illegal_argument_exception",
          "reason": "Can't load fielddata on [username] because fielddata is unsupported on fields of type [keyword]. Use doc values instead."
        }
      ]
  ```



## 分页与遍历

- ES提供了三种方式来解决分页与遍历的问题
  - form/size
  - scroll
  - search_after



### From Size

- 最常见的分页方案

  - from 指明开始位置
  - size 指明获取总数

  ![2018-11-23_fromsize](../../pic/es/2018-11-23_fromsize.jpg)





#### 深度分页问题

- 深度分页是一个经典的问题：在数据分片存储的情况下如何获取前1000个文档
  - 获取从990~1000的文档时，会在每个分片都先获取1000个文档，然后再由Coordinating Node聚合所有分片的结果后再排序选取前1000个文档
  - 页数越深，处理文档越多，占用内存越多，耗时越长。尽量避免深度分页，ES通过`index.max_result_window`限制最多到10000条记录。

![2018-11-23_深度分页问题](../../pic/es/2018-11-23_深度分页问题.jpg)

使用如下查询：

```json
GET test_doc_values/_search
{
  "from": 10000,
  "size": 2
}
# 部分响应
root_cause": [
      {
        "type": "query_phase_execution_exception",
        "reason": "Result window is too large, from + size must be less than or equal to: [10000] but was [10002]. See the scroll api for a more efficient way to request large data sets. This limit can be set by changing the [index.max_result_window] index level setting."
      }
    ]
```





### Scroll分页

- 遍历文档集的API，以快照的方式来避免深度分页的问题

  - 不能用来做实时搜索，因为数据不是实时的
  - 尽量不要使用复杂的sort条件，使用_doc最高效
  - 使用有点复杂

- 具体使用

  - 第一步发起一个scroll search，如下所示

    - ES在收到该请求后会根据查询条件创建文档Id合集的快照

      ```json
      # 发起scroll search
      GET test_search_index/_search?scroll=5m
      {
        "size": 1
      }
      # 部分响应结果，_scroll_id后续调用scroll api时的参数
      {
        "_scroll_id": "DXF1ZXJ5QW5kRmV0Y2gBAAAAAAAAADYWLW1Mc2xKbHNTOHktSUhNTlBrUHh6dw==",
        "took": 22,
        "timed_out": false,
        "_shards": {
          "total": 1,
          "successful": 1,
          "skipped": 0,
          "failed": 0
        }
      ```

  - 第二步调用scroll search的API,获取文档集合，如下所示：

    - 不断迭代调用直到返回hits.hits数组为空时停止

      ```json
      # 迭代调用，scroll指明有效时间，scroll_id上一步返回的id
      POST _search/scroll
      {
        "scroll":"5m",
        "scroll_id":"DXF1ZXJ5QW5kRmV0Y2gBAAAAAAAAADYWLW1Mc2xKbHNTOHktSUhNTlBrUHh6dw=="
      }
      # 部分响应结果，_scroll_id下次调用使用
      "_scroll_id": "DXF1ZXJ5QW5kRmV0Y2gBAAAAAAAAADYWLW1Mc2xKbHNTOHktSUhNTlBrUHh6dw==",
        "took": 153,
        "timed_out": false,
        "terminated_early": true,
        "_shards": {
          "total": 1,
          "successful": 1,
          "skipped": 0,
          "failed": 0
        }
      ```

    #### 删除Scroll

    - 过多的Scroll调用会占用大量内存，可以通过clear api删除过多的Scroll快照；

      ```json
      # 删除所有的Scroll
      DELETE /_search/scroll/_all
      # 指定scroll_id删除
      DELETE /_search/scroll?scroll_id=DXF1ZXJ5QW5kRmV0Y2gBAAAAAAAAl7MWSmVQdV9wVGhTWUsyV3EtV0paelZtZw==
      ```



### Search After

- 避免深度分页的性能问题，提供实时的下一页获取文档功能，使用简单。缺点是：

  - 不能是from参数，即不能指定页数
  - 只能下一页，不能上一页

- 使用流程

  - 第一步为正常的搜索，但是要指定sort值，并保证值唯一

  - 第二步使用上一步最后一个文档的sort值进行查询

    ![2018-11-24_search_after](../../pic/es/2018-11-24_search_after.jpg)

  ```json
  # 指定排序和查询
  GET test_search_index/_search
  {
    "size": 1,
    "sort":{
      "age":"desc",
      "_id":"desc"
    }
  }
  
  #响应结果
  {
    "took": 262,
    "timed_out": false,
    "_shards": {
      "total": 1,
      "successful": 1,
      "skipped": 0,
      "failed": 0
    },
    "hits": {
      "total": 5,
      "max_score": null,
      "hits": [
        {
          "_index": "test_search_index",
          "_type": "doc",
          "_id": "5",
          "_score": null,
          "_source": {
            "username": "lee way",
            "job": "java engineer and java specialist",
            "age": 30,
            "birth": "1978-04-07",
            "isMarried": true
          },
          "sort": [
            30,
            "5"
          ]
        }
      ]
    }
  }
  
  # 下一页查询请求
  GET test_search_index/_search
  {
    "size": 1,
    "search_after":[ 30,"5"],
    "sort":{
      "age":"desc",
      "_id":"desc"
    }
  }
  ```



#### Search After如何避免深度分页

- 通过唯一排序值定位将每次要处理的文档数都控制在size内

- 每个一个节点只需要返回排序值后面指定size的文档数；图例是的话表示每个节点只需返回排序值后面的10个文档数，coordinating Node只需要处理50个文档，在获取前10个文档

  ![2018-11-24_search_after如何避免深度分页](../../pic/es/2018-11-24_search_after如何避免深度分页.jpg)



### 分页类型的应用场景

|     类型     |                    场景                    |
| :----------: | :----------------------------------------: |
|  From Size   | 需要实时获取顶部的部分文档，且需要自由翻页 |
|    Scroll    |     需要全部文档，如导出所有数据的功能     |
| Search_After |        需要全部文档，不需要自由翻页        |



## ES中Search文档说明

[Search文档说明](https://www.elastic.co/guide/en/elasticsearch/reference/6.4/search.html)

[QueryDSL](https://www.elastic.co/guide/en/elasticsearch/reference/6.4/query-dsl.html)






















