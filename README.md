<h1>目录</h1>


- ElasticSearch相关资料
  - [索引相关API](doc/es/索引相关API.md)
  - [倒排索引与分词](doc/es/倒排索引与分词.md)
  - [Mapping设置](doc/es/Mapping设置.md)
  - [Search API介绍](doc/es/Search%20API介绍.md)
  - [分布式特性](doc/es/分布式特性.md)
  - [深入了解Search的运行机制](doc/es/深入了解Search的运行机制.md)
  - [聚合分析](doc/es/聚合分析.md)
  - [数据建模](doc/es/数据建模.md)
  - [集群调优建议](doc/es/集群调优建议.md)
  - [Logstash入门与运行机制](doc/es/Logstash入门与运行机制.md)
- JVM
  - [GC算法](doc/jvm/GC算法.md)
  - [GC种类](doc/jvm/GC种类.md)
- 面试题
  - [MQ面试题](doc/questions/MQ面试题.md)
  - [综合面试题](doc/questions/面试题.md)
  - [多线程面试题](doc/questions/多线程面试题.md)
  - [spring面试题](doc/questions/spring面试题.md)
  - [es面试题](doc/questions/es面试题.md)
  - [缓存面试题](doc/questions/缓存面试题.md)
  - [分布式面试题](doc/questions/分布式面试题.md)
  - [分库分表面试题](doc/questions/分库分表面试题.md)
  - [数据库面试](doc/questions/数据库面试.md)
  - [Spring Cloud面试题](doc/questions/SpringCloud面试题.md)
  - [Java基础面试题](doc/questions/Java基础面试题.md)
  - [JVM面试题](doc/questions/JVM面试题.md)
- 工具
  - [idea](doc/idea/idea使用教程.md)
- Spring
  - [eureka组件](doc/spring_cloud/eureka组件.md)
  - [zuul组件](doc/spring_cloud/zuul组件.md)
  - [spring源码分析](doc/spring/spring源码分析.md)
  - [spring-aop源码](doc/spring/spring-aop源码.md)
  - [spring-mvc源码](doc/spring/spring-mvc源码.md)
- JDK8新特性
  - [Lambd表达式](doc/jdk8新特性/Lambd表达式.md)
  - [Optional使用](doc/jdk8新特性/Optional使用.md)
  - [方法引用](doc/jdk8新特性/方法引用.md)
  - [Stream流](doc/jdk8新特性/Stream流.md)
  - [日期与时间API](doc/jdk8新特性/日期与时间API.md)
- 多线程
  - [多线程下的单例模式](doc/multiple_thread/多线程下的单例模式.md)
  - [线程休息室](doc/multiple_thread/线程休息室.md)
  - [volatile关键字](doc/multiple_thread/volatile关键字.md)
  - [类加载器](doc/multiple_thread/类加载器.md)
  - [多线程常用类](doc/multiple_thread/多线程常用类.md)



<hr/>


先找到 org.springframework.web.servlet.DispatcherServlet 的init方法，定义在其父类
org.springframework.web.servlet.HttpServletBean 中

```java
public final void init() throws ServletException {

	// Set bean properties from init parameters.
	PropertyValues pvs = new ServletConfigPropertyValues(getServletConfig(), this.requiredProperties);
	if (!pvs.isEmpty()) {
		try {
			BeanWrapper bw = PropertyAccessorFactory.forBeanPropertyAccess(this);
			ResourceLoader resourceLoader = new ServletContextResourceLoader(getServletContext());
			bw.registerCustomEditor(Resource.class, new ResourceEditor(resourceLoader, getEnvironment()));
			initBeanWrapper(bw);
			bw.setPropertyValues(pvs, true);
		}
		catch (BeansException ex) {
			if (logger.isErrorEnabled()) {
				logger.error("Failed to set bean properties on servlet '" + getServletName() + "'", ex);
			}
			throw ex;
		}
	}

	// Let subclasses do whatever initialization they like.
	initServletBean();
}
```

主要看怎么拿到我们配置的Spring的配置文件和怎么初始化Spring的容器？
上面代码除了 initServletBean(); 这一行，主要功能就是设置contextConfigLocation属性的值为我们在web.xml中配置的值。主要是通过反射的方式进行设置的。
我们再看一下方法 initServletBean

protected final void initServletBean() throws ServletException {
	getServletContext().log("Initializing Spring " + getClass().getSimpleName() + " '" + getServletName() + "'");
	if (logger.isInfoEnabled()) {
		logger.info("Initializing Servlet '" + getServletName() + "'");
	}
	long startTime = System.currentTimeMillis();

	try {
		this.webApplicationContext = initWebApplicationContext();
		initFrameworkServlet();
	}
	catch (ServletException | RuntimeException ex) {
		logger.error("Context initialization failed", ex);
		throw ex;
	}
	//...
}

主要功能就是初始化Spring容器，通过方法 initWebApplicationContext();，在方法initWebApplicationContext中有下面一句代码
wac = createWebApplicationContext(rootContext);最终调用的是：

protected WebApplicationContext createWebApplicationContext(@Nullable ApplicationContext parent) {
	Class<?> contextClass = getContextClass();
	if (!ConfigurableWebApplicationContext.class.isAssignableFrom(contextClass)) {
		throw new ApplicationContextException(
				"Fatal initialization error in servlet with name '" + getServletName() +
				"': custom WebApplicationContext class [" + contextClass.getName() +
				"] is not of type ConfigurableWebApplicationContext");
	}
	ConfigurableWebApplicationContext wac =
			(ConfigurableWebApplicationContext) BeanUtils.instantiateClass(contextClass);

	wac.setEnvironment(getEnvironment());
	wac.setParent(parent);
	String configLocation = getContextConfigLocation();
	if (configLocation != null) {
		wac.setConfigLocation(configLocation);
	}
	configureAndRefreshWebApplicationContext(wac);

	return wac;
}

 
上面代码的contextClass就是 XmlWebApplicationContext。configLocation就是我们在web.xml中配置的。
接着我们进入方法 configureAndRefreshWebApplicationContext。通过方法的名称我们也能猜出个大概
这个方法主要是配置和刷新Spring的容器。



```java
protected void configureAndRefreshWebApplicationContext(ConfigurableWebApplicationContext wac) {
	if (ObjectUtils.identityToString(wac).equals(wac.getId())) {
	
		if (this.contextId != null) {
			wac.setId(this.contextId);
		}
		else {
			// Generate default id...
			wac.setId(ConfigurableWebApplicationContext.APPLICATION_CONTEXT_ID_PREFIX +
					ObjectUtils.getDisplayString(getServletContext().getContextPath()) + '/' + getServletName());
		}
	}

	wac.setServletContext(getServletContext());
	wac.setServletConfig(getServletConfig());
	wac.setNamespace(getNamespace());
	wac.addApplicationListener(new SourceFilteringListener(wac, new ContextRefreshListener()));

	ConfigurableEnvironment env = wac.getEnvironment();
	if (env instanceof ConfigurableWebEnvironment) {
		((ConfigurableWebEnvironment) env).initPropertySources(getServletContext(), getServletConfig());
	}

	postProcessWebApplicationContext(wac);
	applyInitializers(wac);
	wac.refresh();
}
```

前面部分代码主要功能就是配置WebApplicationContext。重点是 wac.refresh(); 方法，这个方法是老熟人了
在介绍ApplicationContext容器的时候已经介绍了该方法的整个流程。这里主要介绍一下重写了在普通的ApplicationContext中方法。
其中一个就有 onRefresh 方法，在介绍普通的ApplicationContex的时候有提过一句在WebApplicationContext中会被重写，默认是空方法。
```java
/**
  * Initialize the theme capability.
*/
protected void onRefresh() {
	this.themeSource = UiApplicationContextUtils.initThemeSource(this);
}
```
比较简单就是将WebApplicationContext的themeSource属性设置为ResourceBundleThemeSource的实例。

还有一个地方和普通的ApplicationContext不一样。我们都知道Servlet要初始化九大组件，这九大组件肯定也是在初始化容器的时候进行初始化的，
那是在什么时候初始化的呢？
我们先找到初始化九大组件的方法：

```java
private class ContextRefreshListener implements ApplicationListener<ContextRefreshedEvent> {

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		FrameworkServlet.this.onApplicationEvent(event);
	}
}
public void onApplicationEvent(ContextRefreshedEvent event) {
	this.refreshEventReceived = true;
	synchronized (this.onRefreshMonitor) {
		onRefresh(event.getApplicationContext());
	}
}
/**
 * This implementation calls {@link #initStrategies}.
 */
@Override
protected void onRefresh(ApplicationContext context) {
	initStrategies(context);
}

/**
 * Initialize the strategy objects that this servlet uses.
 * <p>May be overridden in subclasses in order to initialize further strategy objects.
 */
protected void initStrategies(ApplicationContext context) {
	initMultipartResolver(context);
	initLocaleResolver(context);
	initThemeResolver(context);
	initHandlerMappings(context);
	initHandlerAdapters(context);
	initHandlerExceptionResolvers(context);
	initRequestToViewNameTranslator(context);
	initViewResolvers(context);
	initFlashMapManager(context);
}
```
调用链是通过一个Listener并且监听的是ContextRefreshedEvent事件。我们通过查找applicationContext的refresh()方法找到了方法 finishRefresh()。

```java
protected void finishRefresh() {
	// Clear context-level resource caches (such as ASM metadata from scanning).
	clearResourceCaches();

	// Initialize lifecycle processor for this context.
	initLifecycleProcessor();

	// Propagate refresh to lifecycle processor first.
	getLifecycleProcessor().onRefresh();

	// Publish the final event.
	publishEvent(new ContextRefreshedEvent(this));

	// Participate in LiveBeansView MBean, if active.
	LiveBeansView.registerApplicationContext(this);
}
```

在上面方法中有一个publishEvent里面真是创建的是ContextRefreshedEvent类对应的就是applicationContext的refresh事件。

在方法onApplicationEvent中，执行了onRefresh方法该方法中执行初始化九大组件的功能，如下：
```java
protected void initStrategies(ApplicationContext context) {
	initMultipartResolver(context);
	initLocaleResolver(context);
	initThemeResolver(context);
	initHandlerMappings(context);
	initHandlerAdapters(context);
	initHandlerExceptionResolvers(context);
	initRequestToViewNameTranslator(context);
	initViewResolvers(context);
	initFlashMapManager(context);
}
```

MultipartResolver用来作为文件上传解析器
LocaleResolver用来作为本地化解析
ThemeResolver用来作为不同的主题视图解析

## 初始化HandlerMappings
initHandlerMappings的方法如下：

```java
private void initHandlerMappings(ApplicationContext context) {
	this.handlerMappings = null;

	//查找所有的handlerMapping
	if (this.detectAllHandlerMappings) {
		// 在ApplicationContext查找所有的HandlerMapping
		Map<String, HandlerMapping> matchingBeans =
				BeanFactoryUtils.beansOfTypeIncludingAncestors(context, HandlerMapping.class, true, false);
		if (!matchingBeans.isEmpty()) {
			this.handlerMappings = new ArrayList<>(matchingBeans.values());
			// We keep HandlerMappings in sorted order.
			AnnotationAwareOrderComparator.sort(this.handlerMappings);
		}
	}
	else {
		try {
		    //从Spring容器中通过beanName获取HandlerMapping
			HandlerMapping hm = context.getBean(HANDLER_MAPPING_BEAN_NAME, HandlerMapping.class);
			this.handlerMappings = Collections.singletonList(hm);
		}
		catch (NoSuchBeanDefinitionException ex) {
			// Ignore, we'll add a default HandlerMapping later.
		}
	}

	//如果通过上面的方式都没有获取到HandlerMapping则加载默认的HandlerMapping
	if (this.handlerMappings == null) {
		this.handlerMappings = getDefaultStrategies(context, HandlerMapping.class);
		if (logger.isTraceEnabled()) {
			logger.trace("No HandlerMappings declared for servlet '" + getServletName() +
					"': using default strategies from DispatcherServlet.properties");
		}
	}
}
```
系统默认执行的就是获取默认的HandlerMapping，进入到方法getDefaultStrategies方法。
在方法中使用到了defaultStrategies，这个变量是在静态代码块中初始化的：
```java
static {
	try {
		ClassPathResource resource = new ClassPathResource(DEFAULT_STRATEGIES_PATH, DispatcherServlet.class);
		defaultStrategies = PropertiesLoaderUtils.loadProperties(resource);
	}
	catch (IOException ex) {
		throw new IllegalStateException("Could not load '" + DEFAULT_STRATEGIES_PATH + "': " + ex.getMessage());
	}
}
```
上面的静态代码块是从claspath下加载文件 `DispatcherServlet.properties`，文件内容如下：
```properties
org.springframework.web.servlet.LocaleResolver=org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver

org.springframework.web.servlet.ThemeResolver=org.springframework.web.servlet.theme.FixedThemeResolver

org.springframework.web.servlet.HandlerMapping=org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping,\
	org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping,\
	org.springframework.web.servlet.function.support.RouterFunctionMapping

org.springframework.web.servlet.HandlerAdapter=org.springframework.web.servlet.mvc.HttpRequestHandlerAdapter,\
	org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter,\
	org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter,\
	org.springframework.web.servlet.function.support.HandlerFunctionAdapter


org.springframework.web.servlet.HandlerExceptionResolver=org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver,\
	org.springframework.web.servlet.mvc.annotation.ResponseStatusExceptionResolver,\
	org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver

org.springframework.web.servlet.RequestToViewNameTranslator=org.springframework.web.servlet.view.DefaultRequestToViewNameTranslator

org.springframework.web.servlet.ViewResolver=org.springframework.web.servlet.view.InternalResourceViewResolver

org.springframework.web.servlet.FlashMapManager=org.springframework.web.servlet.support.SessionFlashMapManager
```

回到方法getDefaultStrategies，这个方法比较简单，就是通过`org.springframework.web.servlet.HandlerMapping`从解析后的Properties获取了
key对应的value值。这里返回的就是
```
org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping,\
	org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping,\
	org.springframework.web.servlet.function.support.RouterFunctionMapping
```
通过逗号进行切分然后封装成List返回。HandlerMapping初始化完成了。其他组件的初始化也和这个类似。


回到方法getDefaultStrategies，这个方法比较简单，就是通过`org.springframework.web.servlet.HandlerMapping`从解析后的Properties获取了
key对应的value值。这里返回的就是
```
org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping,\
	org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping,\
	org.springframework.web.servlet.function.support.RouterFunctionMapping
```
通过逗号进行切分然后封装成List返回。HandlerMapping初始化完成了。其他组件的初始化也和这个类似。


我们接下来看一个请求是如何处理？根据Servlet规范，所有的请求都会进入service方法，
在DisptacherServlet中并没有service方法，找其父类FrameworkServlet，父类service方法如下：
```java
protected void service(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

	HttpMethod httpMethod = HttpMethod.resolve(request.getMethod());
	if (httpMethod == HttpMethod.PATCH || httpMethod == null) {
		processRequest(request, response);
	}
	else {
		super.service(request, response);
	}
}
```
我们的请求一般不是PATCH方式，所以执行的是super.service方法也就是HttpServlet.service。在super.service方法中
会对不同的请求方式执行不同的方法。例如：GET请求会执行doGet()请求，POST请求会执行doPost方法，
但是他们最终执行都是FrameworkServlet的processRequest方法（方法过长就不贴出来了，逻辑也比较简单），
实际的处理方法是在DispatcherServlet的doService方法中。
在doService方法中，主要是做了往request的域中设置了一些attribute
例如：`request.setAttribute(WEB_APPLICATION_CONTEXT_ATTRIBUTE, getWebApplicationContext());`
说明DispatcherServlet实现了ApplicationContextAware接口，最主要的是调用了DispatcherServlet.doDispatch方法
在该方法中处理了Spring MVC的controller的调用的整个流程。
总结一下：
一个请求过来到调用对应的controller之前调用方法过程如下：
用户请求过来===>FrameworkServlet.service ====> HttpServlet.service ==> FrameworkServlet.doGet 
																					           ↓↓
DispatcherServlet.doDispatch<===DispatcherServlet.doService<==FrameworkServlet.processRequest<==


## 获取Handler
在方法DispatcherServlet的doDispatch方法如下：
```java
protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpServletRequest processedRequest = request;
		HandlerExecutionChain mappedHandler = null;
		boolean multipartRequestParsed = false;
		try {
			ModelAndView mv = null;
			Exception dispatchException = null;
			try {
				// Determine handler for the current request.
				mappedHandler = getHandler(processedRequest);
				if (mappedHandler == null) {
					noHandlerFound(processedRequest, response);
					return;
				}
				// Determine handler adapter for the current request.
				HandlerAdapter ha = getHandlerAdapter(mappedHandler.getHandler());
				// Process last-modified header, if supported by the handler.
				String method = request.getMethod();
				boolean isGet = "GET".equals(method);
				if (isGet || "HEAD".equals(method)) {
					long lastModified = ha.getLastModified(request, mappedHandler.getHandler());
					if (new ServletWebRequest(request, response).checkNotModified(lastModified) && isGet) {
						return;
					}
				}

				if (!mappedHandler.applyPreHandle(processedRequest, response)) {
					return;
				}

				// Actually invoke the handler.
				mv = ha.handle(processedRequest, response, mappedHandler.getHandler());
				if (asyncManager.isConcurrentHandlingStarted()) {
					return;
				}
				applyDefaultViewName(processedRequest, mv);
				mappedHandler.applyPostHandle(processedRequest, response, mv);
			}
			processDispatchResult(processedRequest, response, mappedHandler, mv, dispatchException);
		}
	}
```

上面的方法主要分为
```java
//获取handler
mappedHandler = getHandler(processedRequest);
//获取handlerAdapter
HandlerAdapter ha = getHandlerAdapter(mappedHandler.getHandler());
//调用adapter
mv = ha.handle(processedRequest, response, mappedHandler.getHandler());
//重定向到model view
processDispatchResult(processedRequest, response, mappedHandler, mv, dispatchException);
```
getHandler方法如下：
```java
protected HandlerExecutionChain getHandler(HttpServletRequest request) throws Exception {
	if (this.handlerMappings != null) {
		for (HandlerMapping mapping : this.handlerMappings) {
			HandlerExecutionChain handler = mapping.getHandler(request);
			if (handler != null) {
				return handler;
			}
		}
	}
	return null;
}
```
这里的handlerMappings变量就是在方法initHandlerMappings中赋值的，通过前面的代码我们知道
这里默认总共有三个HandlerMapping被初始化了。这里进入方法getHandler

```java
public final HandlerExecutionChain getHandler(HttpServletRequest request) throws Exception {
		Object handler = getHandlerInternal(request);
		if (handler == null) {
			handler = getDefaultHandler();
		}
		if (handler == null) {
			return null;
		}
		// Bean name or resolved handler?
		if (handler instanceof String) {
			String handlerName = (String) handler;
			handler = obtainApplicationContext().getBean(handlerName);
		}
		HandlerExecutionChain executionChain = getHandlerExecutionChain(handler, request);

		if (hasCorsConfigurationSource(handler) || CorsUtils.isPreFlightRequest(request)) {
			CorsConfiguration config = (this.corsConfigurationSource != null ? this.corsConfigurationSource.getCorsConfiguration(request) : null);
			CorsConfiguration handlerConfig = getCorsConfiguration(handler, request);
			config = (config != null ? config.combine(handlerConfig) : handlerConfig);
			executionChain = getCorsHandlerExecutionChain(request, executionChain, config);
		}
		return executionChain;
	}
```
在上面的方法中 getHandlerInternal 方法是关键获取Handler的方法。该方法分别有四个实现
这里调用的是RequestMappingHandlerMapping的父类RequestMappingInfoHandlerMapping的实现
```java
protected HandlerMethod getHandlerInternal(HttpServletRequest request) throws Exception {
	request.removeAttribute(PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE);
	try {
		return super.getHandlerInternal(request);
	}
	finally {
		ProducesRequestCondition.clearMediaTypesAttribute(request);
	}
}
```
这里只是移除了一个attribute，这里不用太关注。最终还是调用的是父类的getHandlerInternal方法，具体如下：

```java
protected HandlerMethod getHandlerInternal(HttpServletRequest request) throws Exception {
    //通过request获取到请求的path
	String lookupPath = getUrlPathHelper().getLookupPathForRequest(request);
	//将请求设置到request的attribute中
	request.setAttribute(LOOKUP_PATH, lookupPath);
	//拿读锁
	this.mappingRegistry.acquireReadLock();
	try {
		//通过请求的path和request获取到对应的HandlerMethod
		HandlerMethod handlerMethod = lookupHandlerMethod(lookupPath, request);
		return (handlerMethod != null ? handlerMethod.createWithResolvedBean() : null);
	}
	finally {
		this.mappingRegistry.releaseReadLock();
	}
}
```
这里就有一个问题了？mappingRegistry这个属性是在什么时候初始化的？通过debug查看这个属性的结构
也能看出它里面包含的RequestMappingInfo已经包含了请求path和HandlerMethod的对应关系。（这里截图查看属性的值）
我们回到RequestMappingHandlerMapping的类的继承关系（截图类继承关系）。
它的父类实现了 InitializingBean 接口，这个接口只有一个方法 afterPropertiesSet ，
该方法是在bean属性设置完成后调用的。找到 RequestMappingHandlerMapping的afterPropertiesSet方法实现

```java
public void afterPropertiesSet() {
	this.config = new RequestMappingInfo.BuilderConfiguration();
	this.config.setUrlPathHelper(getUrlPathHelper());
	this.config.setPathMatcher(getPathMatcher());
	this.config.setSuffixPatternMatch(useSuffixPatternMatch());
	this.config.setTrailingSlashMatch(useTrailingSlashMatch());
	this.config.setRegisteredSuffixPatternMatch(useRegisteredSuffixPatternMatch());
	this.config.setContentNegotiationManager(getContentNegotiationManager());

	super.afterPropertiesSet();
}
```
父类的afterPropertiesSet方法

```java
public void afterPropertiesSet() {
	initHandlerMethods();
}

protected void initHandlerMethods() {
    //获取所有的候选bean的名称，通过查找类型为Object类型的所有bean
	for (String beanName : getCandidateBeanNames()) {
	    //过滤了bean以SCOPED_TARGET_NAME_PREFIX开头的，以这个开头的表示是代理对象
		if (!beanName.startsWith(SCOPED_TARGET_NAME_PREFIX)) {
		    //处理候选的bean
			processCandidateBean(beanName);
		}
	}
	handlerMethodsInitialized(getHandlerMethods());
}

protected void processCandidateBean(String beanName) {
	Class<?> beanType = null;
	try {
	    //通过bean name获取到对应的bean类型
		beanType = obtainApplicationContext().getType(beanName);
	}
	catch (Throwable ex) {
		//An unresolvable bean type, probably from a lazy bean - let's ignore it.
		if (logger.isTraceEnabled()) {
			logger.trace("Could not resolve type for bean '" + beanName + "'", ex);
		}
	}
	if (beanType != null && isHandler(beanType)) {
	    //查找handler method
		detectHandlerMethods(beanName);
	}
}
//判断是否是handler
protected boolean isHandler(Class<?> beanType) {
    //判断对应的bean 类型是否有 Controller 和 RequestMapping 注解
	return (AnnotatedElementUtils.hasAnnotation(beanType, Controller.class) ||
			AnnotatedElementUtils.hasAnnotation(beanType, RequestMapping.class));
}

protected void detectHandlerMethods(Object handler) {
   //获取handler的class类型
	Class<?> handlerType = (handler instanceof String ?
			obtainApplicationContext().getType((String) handler) : handler.getClass());

	if (handlerType != null) {
		Class<?> userType = ClassUtils.getUserClass(handlerType);
		//这个方法返回的就是这样的一个格式[Method,RequestMappingInfo] （截图），处理逻辑主要是：
		//1.通过获取controller和controller中的方法的@RequestMapping注解的值，还有Controller配置的prefix的值
		//2.组装上面获取的值封装成RequestMappingInfo返回
		Map<Method, T> methods = MethodIntrospector.selectMethods(userType,
				(MethodIntrospector.MetadataLookup<T>) method -> {
					try {
						return getMappingForMethod(method, userType);
					}
					catch (Throwable ex) {
						throw new IllegalStateException("Invalid mapping on handler class [" +
								userType.getName() + "]: " + method, ex);
					}
				});
		
		//
		methods.forEach((method, mapping) -> {
			Method invocableMethod = AopUtils.selectInvocableMethod(method, userType);
			//注册HandlerMethod
			registerHandlerMethod(handler, invocableMethod, mapping);
		});
	}
}
//注册HandlerMethod
protected void registerHandlerMethod(Object handler, Method method, T mapping) {
	this.mappingRegistry.register(mapping, handler, method);
}
//正真的注册HandlerMethod
public void register(T mapping, Object handler, Method method) {
	// Assert that the handler method is not a suspending one.
	if (KotlinDetector.isKotlinType(method.getDeclaringClass()) && KotlinDelegate.isSuspend(method)) {
		throw new IllegalStateException("Unsupported suspending handler method detected: " + method);
	}
	//获取写锁
	this.readWriteLock.writeLock().lock();
	try {
	    //通过handler和method创建HandlerMethod
		HandlerMethod handlerMethod = createHandlerMethod(handler, method);
		validateMethodMapping(handlerMethod, mapping);
		this.mappingLookup.put(mapping, handlerMethod);

		List<String> directUrls = getDirectUrls(mapping);
		for (String url : directUrls) {
			this.urlLookup.add(url, mapping);
		}

		String name = null;
		if (getNamingStrategy() != null) {
			name = getNamingStrategy().getName(handlerMethod, mapping);
			addMappingName(name, handlerMethod);
		}

		CorsConfiguration corsConfig = initCorsConfiguration(handler, method, mapping);
		if (corsConfig != null) {
			this.corsLookup.put(handlerMethod, corsConfig);
		}
        //将mapping添加到registry的map中
		this.registry.put(mapping, new MappingRegistration<>(mapping, handlerMethod, directUrls, name));
	}
	finally {
		this.readWriteLock.writeLock().unlock();
	}
}

```



----------------0105

现在回到刚开始分析到的getHandlerInternal方法，上次分析到`HandlerMethod handlerMethod = lookupHandlerMethod(lookupPath, request);`这
一行
```java
protected HandlerMethod getHandlerInternal(HttpServletRequest request) throws Exception {
	String lookupPath = getUrlPathHelper().getLookupPathForRequest(request);
	request.setAttribute(LOOKUP_PATH, lookupPath);
	this.mappingRegistry.acquireReadLock();
	try {
		HandlerMethod handlerMethod = lookupHandlerMethod(lookupPath, request);
		//已经找到了HandlerMethod，调用方法createWithResolvedBean通过beanName创建对应的handler
		return (handlerMethod != null ? handlerMethod.createWithResolvedBean() : null);
	}
	finally {
		this.mappingRegistry.releaseReadLock();
	}
}

public HandlerMethod createWithResolvedBean() {
	Object handler = this.bean;
	//如果是String类型则通过BeanFactory获取到对象的Bean
	if (this.bean instanceof String) {
		Assert.state(this.beanFactory != null, "Cannot resolve bean name without BeanFactory");
		String beanName = (String) this.bean;
		handler = this.beanFactory.getBean(beanName);
	}
	//封装成HandlerMethod返回
	return new HandlerMethod(this, handler);
}
```

然后会回到getHandler方法，已经获取到了handler
所以现在执行到了 `HandlerExecutionChain executionChain = getHandlerExecutionChain(handler, request);` 这行
将handler封装成HandlerExecutionChain返回，因为也没有配置相关Cors的配置，所以这里直接返回了HandlerExecutionChain对象
```
public final HandlerExecutionChain getHandler(HttpServletRequest request) throws Exception {
	Object handler = getHandlerInternal(request);
	if (handler == null) {
		handler = getDefaultHandler();
	}
	if (handler == null) {
		return null;
	}
	// Bean name or resolved handler?
	if (handler instanceof String) {
		String handlerName = (String) handler;
		handler = obtainApplicationContext().getBean(handlerName);
	}

	HandlerExecutionChain executionChain = getHandlerExecutionChain(handler, request);
	//这里没有配置Cors的信息
	if (hasCorsConfigurationSource(handler) || CorsUtils.isPreFlightRequest(request)) {
		CorsConfiguration config = (this.corsConfigurationSource != null ? this.corsConfigurationSource.getCorsConfiguration(request) : null);
		CorsConfiguration handlerConfig = getCorsConfiguration(handler, request);
		config = (config != null ? config.combine(handlerConfig) : handlerConfig);
		executionChain = getCorsHandlerExecutionChain(request, executionChain, config);
	}
	//返回HandlerExecutionChain的对象
	return executionChain;
}

//--------
protected HandlerExecutionChain getHandlerExecutionChain(Object handler, HttpServletRequest request) { 
    //将handler封装成HandlerExecutionChain返回
	HandlerExecutionChain chain = (handler instanceof HandlerExecutionChain ?
			(HandlerExecutionChain) handler : new HandlerExecutionChain(handler));

	String lookupPath = this.urlPathHelper.getLookupPathForRequest(request, LOOKUP_PATH);
	//这里没有配置相关的adaptedInterceptor，所以这里为空
	for (HandlerInterceptor interceptor : this.adaptedInterceptors) {
		if (interceptor instanceof MappedInterceptor) {
			MappedInterceptor mappedInterceptor = (MappedInterceptor) interceptor;
			if (mappedInterceptor.matches(lookupPath, this.pathMatcher)) {
				chain.addInterceptor(mappedInterceptor.getInterceptor());
			}
		}
		else {
			chain.addInterceptor(interceptor);
		}
	}
	return chain;
}

```

现在可以返回到org.springframework.web.servlet.DispatcherServlet#doDispatch方法中了。
```java
//上面已经分析完getHandler方法
mappedHandler = getHandler(processedRequest);
if (mappedHandler == null) {
	noHandlerFound(processedRequest, response);
	return;
}

// Determine handler adapter for the current request.
HandlerAdapter ha = getHandlerAdapter(mappedHandler.getHandler());
```
上面已经分析完getHandler方法，接下来分析getHandlerAdapter方法，mappedHandler.getHandler()返回的就是HandlerMethod对象
里面封装了请求url对应的Controller和method。getHandlerAdapter方法如下：
```java
protected HandlerAdapter getHandlerAdapter(Object handler) throws ServletException {
	if (this.handlerAdapters != null) {
		for (HandlerAdapter adapter : this.handlerAdapters) {
			if (adapter.supports(handler)) {
				return adapter;
			}
		}
	}
}
```
`handlerAdapters`属性的值怎么来的已经分析过了，通过加载`DispatcherServlet.properties`文件配置的HandlerAdapter的

```java
org.springframework.web.servlet.HandlerAdapter=org.springframework.web.servlet.mvc.HttpRequestHandlerAdapter,\
	org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter,\
	org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter,\
	org.springframework.web.servlet.function.support.HandlerFunctionAdapter
```
这里返回的就是`RequestMappingHandlerAdapter` adapter，它的supports方法如下
```java
public final boolean supports(Object handler) {
	return (handler instanceof HandlerMethod && supportsInternal((HandlerMethod) handler));
}
```

我们再回到DispatcherServlet的doDispatch方法中

```java
// 已经获取到RequestMappingHandlerAdapter
HandlerAdapter ha = getHandlerAdapter(mappedHandler.getHandler());

//...

//因为没有interceptor这里返回true
if (!mappedHandler.applyPreHandle(processedRequest, response)) {
	return;
}

//这里是整个spring MVC的核心，这个方法回去调用Controller并返回ModelAndView对象
mv = ha.handle(processedRequest, response, mappedHandler.getHandler());
```

## HandlerAdapter的handle方法

```java
protected ModelAndView handleInternal(HttpServletRequest request,
		HttpServletResponse response, HandlerMethod handlerMethod) throws Exception {

	ModelAndView mav;
	//校验request，无需关注
	checkRequest(request);

	// 默认在会话级别也不同步，返回false
	if (this.synchronizeOnSession) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			Object mutex = WebUtils.getSessionMutex(session);
			synchronized (mutex) {
				mav = invokeHandlerMethod(request, response, handlerMethod);
			}
		}
		else {
			// No HttpSession available -> no mutex necessary
			mav = invokeHandlerMethod(request, response, handlerMethod);
		}
	}
	else {
		// 调用的是invokeHandlerMethod方法
		mav = invokeHandlerMethod(request, response, handlerMethod);
	}

	if (!response.containsHeader(HEADER_CACHE_CONTROL)) {
		if (getSessionAttributesHandler(handlerMethod).hasSessionAttributes()) {
			applyCacheSeconds(response, this.cacheSecondsForSessionAttributeHandlers);
		}
		else {
			prepareResponse(response);
		}
	}

	return mav;
}

protected ModelAndView invokeHandlerMethod(HttpServletRequest request,
		HttpServletResponse response, HandlerMethod handlerMethod) throws Exception {
	//将request和response封装成ServletWebRequest对象
	ServletWebRequest webRequest = new ServletWebRequest(request, response);
	try {
	    //通过handlerMethod构造WebDataBinderFactory
		WebDataBinderFactory binderFactory = getDataBinderFactory(handlerMethod);
		//创建ModelFactory
		ModelFactory modelFactory = getModelFactory(handlerMethod, binderFactory);
		//将handlerMethod封装成ServletInvocableHandlerMethod
		ServletInvocableHandlerMethod invocableMethod = createInvocableHandlerMethod(handlerMethod);
		//该adapter也实现了InitializingBean接口，argumentResolvers的初始化在方法afterPropertiesSet
		//argumentResolver参数解析器负责解析url中的参数，在后面调用Controller将会使用到
		if (this.argumentResolvers != null) {
			invocableMethod.setHandlerMethodArgumentResolvers(this.argumentResolvers);
		}
		//该adapter也实现了InitializingBean接口，returnValueHandlers的初始化在方法afterPropertiesSet
		if (this.returnValueHandlers != null) {
			invocableMethod.setHandlerMethodReturnValueHandlers(this.returnValueHandlers);
		}
		invocableMethod.setDataBinderFactory(binderFactory);
		//设置参数名称发现器，默认提供了两种
		//StandardReflectionParameterNameDiscoverer
		//LocalVariableTableParameterNameDiscoverer
		invocableMethod.setParameterNameDiscoverer(this.parameterNameDiscoverer);

		//创建ModelAndView容器
		ModelAndViewContainer mavContainer = new ModelAndViewContainer();
		mavContainer.addAllAttributes(RequestContextUtils.getInputFlashMap(request));
		modelFactory.initModel(webRequest, mavContainer, invocableMethod);
		mavContainer.setIgnoreDefaultModelOnRedirect(this.ignoreDefaultModelOnRedirect);

		AsyncWebRequest asyncWebRequest = WebAsyncUtils.createAsyncWebRequest(request, response);
		asyncWebRequest.setTimeout(this.asyncRequestTimeout);

		WebAsyncManager asyncManager = WebAsyncUtils.getAsyncManager(request);
		asyncManager.setTaskExecutor(this.taskExecutor);
		asyncManager.setAsyncWebRequest(asyncWebRequest);
		asyncManager.registerCallableInterceptors(this.callableInterceptors);
		asyncManager.registerDeferredResultInterceptors(this.deferredResultInterceptors);

		if (asyncManager.hasConcurrentResult()) {
			Object result = asyncManager.getConcurrentResult();
			mavContainer = (ModelAndViewContainer) asyncManager.getConcurrentResultContext()[0];
			asyncManager.clearConcurrentResult();
			LogFormatUtils.traceDebug(logger, traceOn -> {
				String formatted = LogFormatUtils.formatValue(result, !traceOn);
				return "Resume with async result [" + formatted + "]";
			});
			invocableMethod = invocableMethod.wrapConcurrentResult(result);
		}
        //整个就是调用Controller的方法
		invocableMethod.invokeAndHandle(webRequest, mavContainer);
		if (asyncManager.isConcurrentHandlingStarted()) {
			return null;
		}

		return getModelAndView(mavContainer, modelFactory, webRequest);
	}
	finally {
		webRequest.requestCompleted();
	}
}


public void invokeAndHandle(ServletWebRequest webRequest, ModelAndViewContainer mavContainer,
		Object... providedArgs) throws Exception {

    //这是真实通过反射调用controller的方法
	Object returnValue = invokeForRequest(webRequest, mavContainer, providedArgs);
	setResponseStatus(webRequest);

	if (returnValue == null) {
		if (isRequestNotModified(webRequest) || getResponseStatus() != null || mavContainer.isRequestHandled()) {
			disableContentCachingIfNecessary(webRequest);
			mavContainer.setRequestHandled(true);
			return;
		}
	}
	else if (StringUtils.hasText(getResponseStatusReason())) {
		mavContainer.setRequestHandled(true);
		return;
	}

	mavContainer.setRequestHandled(false);
	Assert.state(this.returnValueHandlers != null, "No return value handlers");
	try {
	    //通过返回值Handler来处理返回值，具体方法在下面
		this.returnValueHandlers.handleReturnValue(
				returnValue, getReturnValueType(returnValue), mavContainer, webRequest);
	}
	catch (Exception ex) {
		if (logger.isTraceEnabled()) {
			logger.trace(formatErrorForReturnValue(returnValue), ex);
		}
		throw ex;
	}
}

public void handleReturnValue(@Nullable Object returnValue, MethodParameter returnType,
		ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception {
	
	//通过返回的value和返回value的类型查找合适的HandlerMethodReturnValueHandler
	//这里返回ViewNameMethodReturnValueHandler
	HandlerMethodReturnValueHandler handler = selectHandler(returnValue, returnType);
	if (handler == null) {
		throw new IllegalArgumentException("Unknown return value type: " + returnType.getParameterType().getName());
	}
	//具体方法在下面
	handler.handleReturnValue(returnValue, returnType, mavContainer, webRequest);
}

private HandlerMethodReturnValueHandler selectHandler(@Nullable Object value, MethodParameter returnType) {
	boolean isAsyncValue = isAsyncReturnValue(value, returnType);
	for (HandlerMethodReturnValueHandler handler : this.returnValueHandlers) {
		if (isAsyncValue && !(handler instanceof AsyncHandlerMethodReturnValueHandler)) {
			continue;
		}
		if (handler.supportsReturnType(returnType)) {
			return handler;
		}
	}
	return null;
}


public void handleReturnValue(@Nullable Object returnValue, MethodParameter returnType,
		ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception {
	//这里返回的就是String类型
	if (returnValue instanceof CharSequence) {
		String viewName = returnValue.toString();
		//将viewName设置到mavContainer
		mavContainer.setViewName(viewName);
		if (isRedirectViewName(viewName)) {
			mavContainer.setRedirectModelScenario(true);
		}
	}
	else if (returnValue != null) {
		// should not happen
		throw new UnsupportedOperationException("Unexpected return type: " +
				returnType.getParameterType().getName() + " in method: " + returnType.getMethod());
	}
}
```


再回到方法invokeHandlerMethod， 现在执行到代码 `getModelAndView(mavContainer, modelFactory, webRequest)`

```java
private ModelAndView getModelAndView(ModelAndViewContainer mavContainer,
		ModelFactory modelFactory, NativeWebRequest webRequest) throws Exception {

	modelFactory.updateModel(webRequest, mavContainer);
	if (mavContainer.isRequestHandled()) {
		return null;
	}
	ModelMap model = mavContainer.getModel();
	//通过mavContainer的viewName，model和mavContainer的status，构建一个ModelAndView对象
	ModelAndView mav = new ModelAndView(mavContainer.getViewName(), model, mavContainer.getStatus());
	if (!mavContainer.isViewReference()) {
		mav.setView((View) mavContainer.getView());
	}
	if (model instanceof RedirectAttributes) {
		Map<String, ?> flashAttributes = ((RedirectAttributes) model).getFlashAttributes();
		HttpServletRequest request = webRequest.getNativeRequest(HttpServletRequest.class);
		if (request != null) {
			RequestContextUtils.getOutputFlashMap(request).putAll(flashAttributes);
		}
	}
	return mav;
}
```

至此，HandlerAdapter的handle方法已经分析完了。接下来回到DispatcherServlet的doDispatch方法中
剩下下面三行代码
```java
//应用默认的view name，只有在mv为空的时候才会应用
applyDefaultViewName(processedRequest, mv);
//调用HandlerInterceptor的postHandle方法。所以这里也能看出HandlerInterceptor的在controller执行前和执行后
//分别调用preHandle和postHandle方法
mappedHandler.applyPostHandle(processedRequest, response, mv);
//处理分发result，具体在下面
processDispatchResult(processedRequest, response, mappedHandler, mv, dispatchException);

private void processDispatchResult(HttpServletRequest request, HttpServletResponse response,
		@Nullable HandlerExecutionChain mappedHandler, @Nullable ModelAndView mv,
		@Nullable Exception exception) throws Exception {
	boolean errorView = false;
	//异常是否为空
	if (exception != null) {
	    //不为空则判断是否是ModelAndViewDefiningException
		if (exception instanceof ModelAndViewDefiningException) {
			logger.debug("ModelAndViewDefiningException encountered", exception);
		    //强转成ModelAndViewDefiningException对象
			mv = ((ModelAndViewDefiningException) exception).getModelAndView();
		}
		else {
		     //获取到handler
			Object handler = (mappedHandler != null ? mappedHandler.getHandler() : null);
			//处理exception的handler异常，具体见下面
			mv = processHandlerException(request, response, handler, exception);
			errorView = (mv != null);
		}
	}
	// Did the handler return a view to render?
	if (mv != null && !mv.wasCleared()) {
	    //渲染mv，具体分析见下面
		render(mv, request, response);
		if (errorView) {
			WebUtils.clearErrorRequestAttributes(request);
		}
	}
	else {
		if (logger.isTraceEnabled()) {
			logger.trace("No view rendering, null ModelAndView returned.");
		}
	}
	if (WebAsyncUtils.getAsyncManager(request).isConcurrentHandlingStarted()) {
		// Concurrent handling started during a forward
		return;
	}
	if (mappedHandler != null) {
		// Exception (if any) is already handled..
		mappedHandler.triggerAfterCompletion(request, response, null);
	}
}


//-----
protected ModelAndView processHandlerException(HttpServletRequest request, HttpServletResponse response,
		@Nullable Object handler, Exception ex) throws Exception {

	// Success and error responses may use different content types
	request.removeAttribute(HandlerMapping.PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE);

	// Check registered HandlerExceptionResolvers...
	ModelAndView exMv = null;
	//通过HandlerExceptionResolver处理异常
	if (this.handlerExceptionResolvers != null) {
		for (HandlerExceptionResolver resolver : this.handlerExceptionResolvers) {
			exMv = resolver.resolveException(request, response, handler, ex);
			if (exMv != null) {
				break;
			}
		}
	}
	if (exMv != null) {
		if (exMv.isEmpty()) {
			request.setAttribute(EXCEPTION_ATTRIBUTE, ex);
			return null;
		}
		// We might still need view name translation for a plain error model...
		if (!exMv.hasView()) {
			String defaultViewName = getDefaultViewName(request);
			if (defaultViewName != null) {
				exMv.setViewName(defaultViewName);
			}
		}
		WebUtils.exposeErrorRequestAttributes(request, ex, getServletName());
		return exMv;
	}

	throw ex;
}

//---
//
public void render(@Nullable Map<String, ?> model, HttpServletRequest request,
		HttpServletResponse response) throws Exception {
	
	Map<String, Object> mergedModel = createMergedOutputModel(model, request, response);
	prepareResponse(request, response);
	//将Model对象设置的值赋值到request的attribute中
	renderMergedOutputModel(mergedModel, getRequestToExpose(request), response);
}


protected void renderMergedOutputModel(
		Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {

	// 作为request的attribute暴露mode对象，通过这个可以看出jsp是通过将model对象设置到request的attribute属性来渲染页面的
	exposeModelAsRequestAttributes(model, request);

	// Expose helpers as request attributes, if any.
	exposeHelpers(request);

	// Determine the path for the request dispatcher.
	String dispatcherPath = prepareForRendering(request, response);

	// Obtain a RequestDispatcher for the target resource (typically a JSP).
	RequestDispatcher rd = getRequestDispatcher(request, dispatcherPath);
	if (rd == null) {
		throw new ServletException("Could not get RequestDispatcher for [" + getUrl() +
				"]: Check that the corresponding file exists within your web application archive!");
	}

	// If already included or response already committed, perform include, else forward.
	if (useInclude(request, response)) {
		response.setContentType(getContentType());
		rd.include(request, response);
	}
	else {
	    //
		rd.forward(request, response);
	}
}

```


Spring MVC如何解析请求参数的？


















































